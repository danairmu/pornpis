$(document).ready(function () {

    var names = [];
    var prices = [];
    var master_plan = '1';
    var inputs = $('#mapObject area');
    var m_main = $('#m_main');
    var hidden_user = $('#hidden_user').val();
    console.log("hidden_user: " + hidden_user);
    //resize
    $('map').imageMapResize();

    //init
    $('#mapContainer_p1').show();
    loadPlan1(names);

    $("#mapObject").draggable({
        cursor: 'move'

    });

    zoomMousewheel();


    $('#cusRadios').click(function () {
        $('map').imageMapResize();
        $('#mapContainer_p2').hide();
        $('#mapContainer_p1').show();
        loadPlan1(names);
        $("#mapObject").draggable();
        m_main = $('#m_main');
        zoomMousewheel();
        inputs = $('#mapObject area');
    });

    $('#saleplan').click(function () {
        $('map').imageMapResize();
        $('#mapContainer_p1').hide();
        $('#mapContainer_p2').show();
        loadPlan2(names);
        $("#mapObject2").draggable();
        m_main = $('#m_main2');
        zoomMousewheelImage2();
        inputs = $('#mapObject2 area');
    });

    //then select dropdown
    $("#inputState").change(function () {
        names = [];
        master_plan = $("#inputState option:selected").val();
        if (master_plan === '1') {
            master_plan = '1';
            inputs = $('#mapObject area');
            m_main = $('#m_main');
            $('map').imageMapResize();
            $('#mapContainer_p2').hide();
            $('#mapContainer_p3').hide();
            $('#mapContainer_p4').hide();
            $('#mapContainer_p1').show();
            loadPlan1(names);
            $("#mapObject").draggable();
            zoomMousewheel();

            $('#cusRadios').click(function () {
                inputs = $('#mapObject area');
                m_main = $('#m_main');
                $('map').imageMapResize();
                $('#mapContainer_p2').hide();
                $('#mapContainer_p3').hide();
                $('#mapContainer_p4').hide();
                $('#mapContainer_p1').show();
                loadPlan1(names);
                $("#mapObject").draggable();
                zoomMousewheel();
            });

            $('#saleplan').click(function () {
                inputs = $('#mapObject2 area');
                m_main = $('#m_main2');
                $('map').imageMapResize();
                $('#mapContainer_p1').hide();
                $('#mapContainer_p3').hide();
                $('#mapContainer_p4').hide();
                $('#mapContainer_p2').show();
                loadPlan2(names);
                $("#mapObject2").draggable();
                zoomMousewheelImage2();
            });

        } else if (master_plan === '2') {
            master_plan = '2';

            names = [];
            inputs = $('#mapObject5 area');
            m_main = $('#m_main5');
            $('map').imageMapResize();

            $('#mapContainer_p1').hide();
            $('#mapContainer_p2').hide();
            $('#mapContainer_p4').hide();
            $('#mapContainer_p3').hide();
            $('#mapContainer_p6').hide();
            $('#mapContainer_p5').show();

            loadPlan5(names);
            $("#mapObject5").draggable();
            zoomMousewheelImage5();

            $('#cusRadios').click(function () {

                names = [];
                inputs = $('#mapObject5 area');
                m_main = $('#m_main5');
                $('map').imageMapResize();

                $('#mapContainer_p1').hide();
                $('#mapContainer_p2').hide();
                $('#mapContainer_p4').hide();
                $('#mapContainer_p3').hide();
                $('#mapContainer_p6').hide();
                $('#mapContainer_p5').show();

                loadPlan5(names);
                $("#mapObject5").draggable();
                zoomMousewheelImage5();
            });

            $('#saleplan').click(function () {

                names = [];
                inputs = $('#mapObject6 area');
                m_main = $('#m_main6');
                $('map').imageMapResize();

                $('#mapContainer_p1').hide();
                $('#mapContainer_p2').hide();
                $('#mapContainer_p4').hide();
                $('#mapContainer_p3').hide();
                $('#mapContainer_p5').hide();
                $('#mapContainer_p6').show();

                loadPlan6(names);
                $("#mapObject6").draggable();
                zoomMousewheelImage6();
            });

        } else {
            master_plan = '3';
            names = [];
            inputs = $('#mapObject3 area');
            m_main = $('#m_main3');
            $('map').imageMapResize();
            $('#mapContainer_p1').hide();
            $('#mapContainer_p2').hide();
            $('#mapContainer_p4').hide();
            $('#mapContainer_p3').show();
            $('#mapContainer_p6').hide();
            $('#mapContainer_p5').hide();
            loadPlan3(names);
            $("#mapObject3").draggable();
            zoomMousewheelImage3();

            $('#cusRadios').click(function () {
                inputs = $('#mapObject3 area');
                m_main = $('#m_main3');
                $('map').imageMapResize();
                $('#mapContainer_p4').hide();
                $('#mapContainer_p1').hide();
                $('#mapContainer_p2').hide();
                $('#mapContainer_p3').show();
                $('#mapContainer_p6').hide();
                $('#mapContainer_p5').hide();
                loadPlan3(names);
                $("#mapObject3").draggable();
                zoomMousewheelImage3();
            });

            $('#saleplan').click(function () {
                inputs = $('#mapObject4 area');
                m_main = $('#m_main4');
                $('map').imageMapResize();
                $('#mapContainer_p3').hide();
                $('#mapContainer_p1').hide();
                $('#mapContainer_p2').hide();
                $('#mapContainer_p4').show();
                $('#mapContainer_p6').hide();
                $('#mapContainer_p5').hide();
                loadPlan4(names);
                $("#mapObject4").draggable();
                zoomMousewheelImage4();
            });
        }
    });



    //onclick map

    $('area').on('click', function () {

        var title = $(this).attr('title');
        var alt = $(this).attr('alt');

        //check status จองแล้ว หรือ ขายแล้ว
        var status = checkStatus(title, master_plan);
        if (status === 'S') {
            console.log("รายการได้ถูกขายแล้ว");
            names.push(title);
            $('#input_hidden_plan').val(JSON.stringify(names));
            $('#input_hidden_plan_type').val('1');
            $('#input_hidden_plan_master').val(master_plan);
            $("#form").submit();
        } else if (status === 'R') {
            console.log("รายการได้ถูกจองแล้ว");
            names.push(title);
            $('#input_hidden_plan').val(JSON.stringify(names));
            $('#input_hidden_plan_type').val('1');
            $('#input_hidden_plan_master').val(master_plan);
            $("#form").submit();

        } else {

            var data = $(this).data('maphilight') || {};

            if (!data.alwaysOn) {


                //add list
                names.push(title);



                var flag = '';
                if (names.length === 1) {
                    console.log("names.length === 1");
                    //hilight
                    data.alwaysOn = !data.alwaysOn,
                            data.fillColor = '0f14ce';
                    $(this).data('maphilight', data).trigger('alwaysOn.maphilight');
                } else {
                    console.log("names.length !=== 1: " + title);
                    var array = [];
                    var plans = getAearPlan();
                    console.log("plans: " + plans);
                    array = plans.split(',');
                    for (var i = 0; i < array.length; i++) {
                        if (title == array[i]) {
                            console.log("title: " + title + " : " + array[i]);
                            flag = 'Y';
                            break;
                        } else {
                            flag = 'N';
                        }
                    }
                    console.log("status: " + status);
                    if (flag === 'Y') {
                        //hilight
                        data.alwaysOn = !data.alwaysOn,
                                data.fillColor = '0f14ce';
                        $(this).data('maphilight', data).trigger('alwaysOn.maphilight');

                    } else {
                        console.log("unmaphilight: " + title);
                        names.splice($.inArray(title, names), 1);


                        //set onmobile
                        m_main.maphilight({
                            fillColor: '0f14ce',
                            fillOpacity: 0.7,
                            stroke: false,
                            alwaysOn: false,
                            width: 10,
                        });


                        for (var i = 0; i < names.length; i++) {
                            console.log("names[i] : " + names[i]);
                            inputs.each(function () {
                                if (this.title === names[i]) {
                                    var mdata = $(this).data('maphilight') || {};
                                    mdata.alwaysOn = true,
                                            mdata.fillColor = '0f14ce';
                                    $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
                                }
                            });
                        }

                    }
                }

            } else {

                data.alwaysOn = !data.alwaysOn,
                        data.fillColor = '0f14ce';
                $(this).data('maphilight', data).trigger('alwaysOn.maphilight');
                $(this).data('maphilight', data).bind('.maphilight');
                //var isMobile = false;
                //if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Opera Mobile|Kindle|Windows Phone|PSP|AvantGo|Atomic Web Browser|Blazer|Chrome Mobile|Dolphin|Dolfin|Doris|GO Browser|Jasmine|MicroB|Mobile Firefox|Mobile Safari|Mobile Silk|Motorola Internet Browser|NetFront|NineSky|Nokia Web Browser|Obigo|Openwave Mobile Browser|Palm Pre web browser|Polaris|PS Vita browser|Puffin|QQbrowser|SEMC Browser|Skyfire|Tear|TeaShark|UC Browser|uZard Web|wOSBrowser|Yandex.Browser mobile/i.test(navigator.userAgent))
                //    isMobile = true;


                console.log("unclick: " + title);
                names.splice($.inArray(title, names), 1);

                m_main.maphilight({
                    fillColor: '0f14ce',
                    fillOpacity: 0.7,
                    stroke: false,
                    alwaysOn: false,
                    width: 10,
                });

                for (var i = 0; i < names.length; i++) {
                    console.log("names[i] : " + names[i]);
                    inputs.each(function () {
                        if (this.title === names[i]) {
                            var mdata = $(this).data('maphilight') || {};
                            mdata.alwaysOn = true,
                                    mdata.fillColor = '0f14ce';
                            $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
                        }
                    });
                }


                var plans = '';

                if (names.length > 1) {
                    console.log("else names ====> 1 ");
                    for (var i = 0; i < names.length; i++) {
                        console.log("unheck names: " + names[i]);
                        plans += getAearPlanCheck(names[i]);
                    }
                    console.log("end plans: " + plans);
                    var arr = [];
                    arr = plans.split(",");
                    console.log("arr: " + arr);
                    for (var i = 0; i < names.length; i++) {
                        if (jQuery.inArray(names[i], arr) !== -1) {
                            console.log("is in array : " + names[i]);
                        } else {

                            console.log("is NOT in array : " + names[i]);
                            //var inputs = $('#mapObject area');
                            console.log("inputs: " + inputs);
                            inputs.each(function (index, value) {
                                if ($(this).attr("title") === names[i]) {
                                    names.splice($.inArray(names[i], names), 1);


                                    var mdata = $(this).data('maphilight') || {};
                                    mdata.alwaysOn = false,
                                            mdata.fillColor = '0f14ce';
                                    $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');

                                    //set onmobile
                                    m_main.maphilight({
                                        fillColor: '0f14ce',
                                        fillOpacity: 0.7,
                                        stroke: false,
                                        alwaysOn: false,
                                        width: 10,
                                    });


                                }
                            });
                        }
                    }
                }
            }


            console.log("names: " + names.length);
            names.sort();
            $('#input_hidden_plan').val(JSON.stringify(names));
            $('#input_hidden_plan_master').val(master_plan);
            if (names.length > 0) {
                var hidden_user = $('#hidden_user').val();
                if (hidden_user === 'general') {
                    $('#bt_selecthome').hide();
                } else {
                    $('#bt_selecthome').show();
                }
            } else {
                $('#bt_selecthome').hide();
            }

        }


    });
});


function test(id, type_arr) {
    var result = '';
    for (var i = 0; i < type_arr.length; i++) {
        if (id === type_arr[i]) {
            console.log("id: " + id + " type_arr[i]: " + type_arr[i])
            return type_arr[i];
        }
    }
    return 'x';
}


function checkStatus(title, masterplan) {
    console.log("masterplan: " + masterplan);
    var output = '';
    $.ajax({
        url: 'projectplan/getplan',
        data: {
            plan_name: title,
            master_plan: masterplan
        },
        type: 'POST',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = data[0].p_status;
    });
    return output;
}

function getAearPlan() {

    console.log("start getAearPlan: " + $('#input_hidden_plan_master').val());
    var plans = '';
    $.ajax({
        url: 'projectplan/getAearPlan',
        type: 'POST',
        data: {
            plans: $('#input_hidden_plan').val(),
            master_planid: $('#input_hidden_plan_master').val()
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        plans = data;
    });

    //console.log(plans);
    return plans;
}

function getAearPlanCheck(planid) {

    console.log("start getAearPlanCheck plan_master: " + $('#input_hidden_plan_master').val());
    var plans = '';
    $.ajax({
        url: 'projectplan/getAearPlanCheck',
        type: 'POST',
        data: {
            planid: planid,
            master_planid: $('#input_hidden_plan_master').val()
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        plans = data;
    });

    //console.log(plans);
    return plans;
}

function zoomMousewheel() {
    $('#mapObject').mousewheel(function (event) {
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            zoomIn($('#mapObject'));
        } else {
            zoomOut($('#mapObject'));
        }
    });
}

function zoomMousewheelImage2() {
    $('#mapObject2').mousewheel(function (event) {
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            zoomIn($('#mapObject2'));
        } else {
            zoomOut($('#mapObject2'));
        }
    });
}

function zoomMousewheelImage3() {
    $('#mapObject3').mousewheel(function (event) {
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            zoomIn($('#mapObject3'));
        } else {
            zoomOut($('#mapObject3'));
        }
    });
}

function zoomMousewheelImage4() {
    $('#mapObject4').mousewheel(function (event) {
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            zoomIn($('#mapObject4'));
        } else {
            zoomOut($('#mapObject4'));
        }
    });
}

function zoomMousewheelImage5() {
    $('#mapObject5').mousewheel(function (event) {
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            zoomIn($('#mapObject5'));
        } else {
            zoomOut($('#mapObject5'));
        }
    });
}

function zoomMousewheelImage6() {
    $('#mapObject6').mousewheel(function (event) {
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            zoomIn($('#mapObject6'));
        } else {
            zoomOut($('#mapObject6'));
        }
    });
}

function zoomIn(id) {
    var scale = parseFloat($('#zoomScale').val());
    if (scale < 6.0) {
        scale += 0.1;
    }
    zoom(id, scale);
}

function zoomOut(id) {
    var scale = parseFloat($('#zoomScale').val());

    if (scale > 0.9) {
        scale -= 0.1;
    }
    zoom(id, scale);
}

function zoom(el, scale) {
    $(el).css('zoom', scale);
    $('#zoomScale').val(scale);
}





function showHomeSelect() {
    //$('#myModalHome').modal('show');
}

function hideHomeSelect() {
    //$('#myModalHome').modal('hide');
}

function loadPlan1(names) {
    console.log("loadPlan1: " + names);
    $('#m_main').maphilight({
        fillColor: '0f14ce',
        fillOpacity: 0.7,
        stroke: false,
        alwaysOn: false,
        width: 10,
    });
    var planobject = $('#mapObject area');
    checkPlan(planobject, "1");

    for (var i = 0; i < names.length; i++) {
        console.log("names[i] : " + names[i]);
        planobject.each(function () {
            if (this.title === names[i]) {
                var mdata = $(this).data('maphilight') || {};
                mdata.alwaysOn = true,
                        mdata.fillColor = '0f14ce';
                $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
            }
        });
    }

}


function loadPlan2(names) {

    console.log("loadplan2 : " + names);

    $('#m_main2').maphilight({
        fillColor: '0f14ce',
        fillOpacity: 0.7,
        stroke: false,
        alwaysOn: false,
    });
    var planobject = $('#mapObject2 area');
    checkPlan(planobject, "1");


    //load click
    for (var i = 0; i < names.length; i++) {
        console.log("names[i] : " + names[i]);
        planobject.each(function () {
            if (this.title === names[i]) {
                var mdata = $(this).data('maphilight') || {};
                mdata.alwaysOn = true,
                        mdata.fillColor = '0f14ce';
                $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
            }
        });
    }

}

function checkPlan(planobject, planid) {
    $.ajax({
        type: 'GET',
        url: 'projectplan/getstatusplan/' + planid,
        async: true,
        success: function (data) {

            //console.log(data);
            for (var i = 0; i < data.length; i++) {
                if (data[i].p_status === 'R') {
                    planobject.each(function () {
                        if (this.title === data[i].p_name) {
                            //console.log("title: " + this.title + " name: " + data[i].p_name);
                            var mdata = $(this).data('maphilight') || {};
                            mdata.alwaysOn = true,
                                    mdata.fillColor = 'f5970a';
                            $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
                        }
                    });

                } else if (data[i].p_status === 'S') {
                    planobject.each(function () {
                        if (this.title === data[i].p_name) {
                            //console.log("title: " + this.title + " name: " + data[i].p_name);
                            var mdata = $(this).data('maphilight') || {};
                            mdata.alwaysOn = true,
                                    mdata.fillColor = 'd50000';
                            $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
                        }
                    });
                }
            }
        }
    });
}


function loadPlan3(names) {
    $('#m_main3').maphilight({
        fillColor: '0f14ce',
        fillOpacity: 0.7,
        stroke: false,
        alwaysOn: false,
    });

    var planobject = $('#mapObject3 area');
    checkPlan(planobject, "3");

    //load click
    for (var i = 0; i < names.length; i++) {
        console.log("names[i] : " + names[i]);
        planobject.each(function () {
            if (this.title === names[i]) {
                var mdata = $(this).data('maphilight') || {};
                mdata.alwaysOn = true,
                        mdata.fillColor = '0f14ce';
                $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
            }
        });
    }

}

function loadPlan4(names) {
    $('#m_main4').maphilight({
        fillColor: '0f14ce',
        fillOpacity: 0.7,
        stroke: false,
        alwaysOn: false,
    });

    var planobject = $('#mapObject4 area');
    checkPlan(planobject, "3");


    //load click
    for (var i = 0; i < names.length; i++) {
        console.log("names[i] : " + names[i]);
        planobject.each(function () {
            if (this.title === names[i]) {
                var mdata = $(this).data('maphilight') || {};
                mdata.alwaysOn = true,
                        mdata.fillColor = '0f14ce';
                $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
            }
        });


    }
}

function loadPlan5(names) {
    $('#m_main5').maphilight({
        fillColor: '0f14ce',
        fillOpacity: 0.7,
        stroke: false,
        alwaysOn: false,
    });

    var planobject = $('#mapObject5 area');
    checkPlan(planobject, "2");


    //load click
    for (var i = 0; i < names.length; i++) {
        console.log("names[i] : " + names[i]);
        planobject.each(function () {
            if (this.title === names[i]) {
                var mdata = $(this).data('maphilight') || {};
                mdata.alwaysOn = true,
                        mdata.fillColor = '0f14ce';
                $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
            }
        });


    }
}

function loadPlan6(names) {
    $('#m_main6').maphilight({
        fillColor: '0f14ce',
        fillOpacity: 0.7,
        stroke: false,
        alwaysOn: false,
    });

    var planobject = $('#mapObject6 area');
    checkPlan(planobject, "2");


    //load click
    for (var i = 0; i < names.length; i++) {
        console.log("names[i] : " + names[i]);
        planobject.each(function () {
            if (this.title === names[i]) {
                var mdata = $(this).data('maphilight') || {};
                mdata.alwaysOn = true,
                        mdata.fillColor = '0f14ce';
                $(this).data('maphilight', mdata).trigger('alwaysOn.maphilight');
            }
        });


    }
}

