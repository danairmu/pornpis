$(document).ready(function () {

    
    
    var total_plan = 0;
    var planid = $("#plan_id").val();
    console.log("planid:  " + planid);
    var h_name = $("#hidden_home_name1").val();
    var h_price_order = $("#hidden_home_price1").val();

    //init check box
    var data = $('#input_hidden_plan_save').val();
    console.log("data: " + data);
    var array = [];
    array = data.split(',');
    var p_name = "";
    var input = "";
    $("#total_plang").html(array.length);
    
    var plan_master = $('#input_hidden_plan_master').val();
    console.log("plan_master: " + plan_master);
    console.log("array.length: " + array.length);
    
    var d = "";
    if (array.length == 1) {
        d = "disabled";
        $('#price_plan_total_select').html($('#input_hidden_price').val());
        $('#hidden_price_plan_total_select').val($('#input_hidden_price').val());
        
        $("#price_plan_land_emty").html("0");
        $("#hidden_price_plan_land_emty").val();
    } else {
        $('#price_plan_total_select').html($('#input_hidden_price').val());
        $("#price_plan_land_emty").html("0");
    }

    var hidden_user = $('#hidden_user').val();
    console.log("hidden_user: " + hidden_user);
    var disible = '';
    if (hidden_user === 'general') {
        disible = "disabled";
    }else{
        disible = "disabled";
    }
    console.log("disible: " + disible);
    
    for (var i = 0; i < array.length; i++) {
        total_plan++;
        console.log("array[i]: " + array[i]);
        p_name = array[i];
        input += '<label  class="form-check-label" for="exampleCheck' + p_name.trim() + '" style="color: #ffffff;">แแปลงที่ ' + p_name.trim() + '</label>&nbsp;&nbsp;&nbsp;&nbsp;\n\
                  <input   name="checks[]" type="checkbox" class="form-check-input" id="exampleCheck' + p_name.trim() + '" value="' + p_name.trim() + '"  onclick="ischeckbox(this, \'' + p_name.trim() + '\', \'' + plan_master + '\');" ' + d + '>\n\
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $("#check_land").html(input);
    }
    //End Check box



    //Start DateOrder
    var orderArr = [];
    console.log("orderArr p_name: " + p_name.trim() + " plan_master: " + plan_master);
    orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
    console.log("orderArr.plan_id: " + orderArr.plan_id);


    if (orderArr.length === 0) {


        var today = new Date();
        var day = today.getDate() + "";
        var month = (today.getMonth() + 1) + "";
        var year = today.getFullYear() + "";
        var hour = today.getHours() + "";
        var minutes = today.getMinutes() + "";
        var seconds = today.getSeconds() + "";

        day = checkZero(day);
        month = checkZero(month);
        year = checkZero(year);
        hour = checkZero(hour);
        minutes = checkZero(minutes);
        seconds = checkZero(seconds);
        $("#hidden_date_order").val(year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds);

        var cdate = GetTodayDate();
        var cnew_date = cdate.replace(/\//g, '');
        var cday = cnew_date.substr(0, 2);
        var cmonth = cnew_date.substr(2, 2);
        var cyear = cnew_date.substr(4, 4);
        console.log("cday: " + cday + " cmonth: " + cmonth);
        var csrt_month = getMonthFull(cmonth);
        var crs_month = cday + " " + csrt_month + " พ.ศ. " + (parseInt(cyear) + 543);
        $("#date_order").html(crs_month);
        console.log("crs_month: " + crs_month);

    } else {

        //set checkboxx
        var orders = [];
        var order_no = orderArr.order_no;
        console.log("order_no****: " + order_no);
        orders = get_order_by_orderno(order_no);
        console.log("orders.length*****: " + orders.length);
        var land_emty_price = 0;
        for (var i = 0; i < orders.length; i++) {
            console.log("orders p_empty_land: " + orders[i].p_empty_land);
            console.log("orders ps_order.plan_name: " + orders[i].plan_name);
            if (orders[i].p_empty_land === 'Y') {
                $("input[type=checkbox][value=" + orders[i].plan_name + "]").prop("checked", true);
                land_emty_price += 3000;
            }
        }
        $("#price_plan_land_emty").html(formatMoney(land_emty_price));
        var input_price = $("#input_hidden_price").val().replace(/,/g, "");
        var price = parseFloat(input_price);
        console.log("price: " + price);
        var total = price + parseFloat(land_emty_price);
        $("#price_plan_total_select").html(formatMoney(total));
        $("#input_hidden_price").val(formatMoney(total));

        $("#hidden_date_order").val(orderArr.order_date_hidden);
        console.log("else order_date: " + orderArr.order_date);
        var cnew_date = orderArr.order_date.replace(/\//g, '');
        console.log("cnew_date: " + cnew_date);
        var cday = cnew_date.substr(0, 2);
        var cmonth = cnew_date.substr(2, 2);
        var cyear = cnew_date.substr(4, 4);
        var csrt_month = getMonthFull(cmonth);
        var crs_month = cday + " " + csrt_month + " พ.ศ. " + (parseInt(cyear) + 543);
        console.log("cnew_crs_mocrs_monthnthdate: " + crs_month);
        $("#date_order").html(crs_month);

    }

    //End DateOrder

    //check radio home
    var data = [];
    data = get_data_plan_arr(p_name.trim(), plan_master);
    console.log("data palan home data: " + p_name.trim() + " : " + plan_master + " data: " + data);
    console.log("data palan home id: " + data[0].p_home_id);

    if ($("#standard").is(':checked')) {
        var card_type = $("#standard").val();
        var homeType = '1';
        $('#div_standard').show();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        var h_price = getHomePrice('1');

        var arr = [];
        arr = getHomeWH('1');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        h_name = $("#hidden_home_name1").val();
        h_price_order = $("#hidden_home_price1").val();

        console.log("hidden_home_type1: " + $("#hidden_home_type1").val() + " homeType: " + homeType);
        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);
        showImage(card_type);

    } else if ($("#modem").is(':checked')) {
        var card_type = $("#modem").val();
        var homeType = '2';
        $('#div_standard').hide();
        $('#div_modem').show();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        var h_price = getHomePrice('2');

        var arr = [];
        arr = getHomeWH('2');

        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage(card_type);
        h_name = $("#hidden_home_name2").val();
        h_price_order = $("#hidden_home_price2").val();

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    } else if ($("#vintage").is(':checked')) {
        var card_type = $("#vintage").val();
        console.log("card_type:  " + card_type);
        var homeType = '3';
        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').show();
        $('#div_x').hide();
        var h_price = getHomePrice('3');

        var arr = [];
        arr = getHomeWH('3');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage(card_type);
        h_name = $("#hidden_home_name3").val();
        h_price_order = $("#hidden_home_price3").val();

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);


    } else if ($("#div_x").is(':checked')) {
        console.log("div_x");
        var card_type = $("#div_x").val();
        var homeType = '4';
        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').show();
        var h_price = getHomePrice('4');

        var arr = [];
        arr = getHomeWH('4');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage(card_type);
        h_name = $("#hidden_home_name4").val();
        h_price_order = $("#hidden_home_price4").val();

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    } else if ($("#h_name5").is(':checked')) {
        console.log("h_name5");
        var card_type = $("#h_name5").val();
        var homeType = '5';
        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        $('#h_name5').show();

        var h_price = getHomePrice('5');

        var arr = [];
        arr = getHomeWH('5');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage(card_type);
        h_name = $("#hidden_home_name5").val();
        h_price_order = $("#hidden_home_price5").val();

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    } else if ($("#h_name6").is(':checked')) {
        console.log("h_name6");
        var card_type = $("#h_name6").val();
        var homeType = '6';
        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        $('#h_name5').hide();
        $('#h_name6').show();

        var h_price = getHomePrice('6');

        var arr = [];
        arr = getHomeWH('6');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage(card_type);
        h_name = $("#hidden_home_name6").val();
        h_price_order = $("#hidden_home_price6").val();

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    } else if ($("#h_name7").is(':checked')) {
        console.log("h_name7");
        var card_type = $("#h_name7").val();
        var homeType = '7';
        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        $('#h_name5').hide();
        $('#h_name6').hide();
        $('#h_name7').show();

        var h_price = getHomePrice('7');

        var arr = [];
        arr = getHomeWH('7');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage(card_type);
        h_name = $("#hidden_home_name7").val();
        h_price_order = $("#hidden_home_price7").val();

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    } else if ($("#h_name8").is(':checked')) {
        console.log("h_name8");
        var card_type = $("#h_name8").val();
        var homeType = '8';
        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        $('#h_name5').hide();
        $('#h_name6').hide();
        $('#h_name7').hide();
        $('#h_name8').show();

        var h_price = getHomePrice('8');

        var arr = [];
        arr = getHomeWH('8');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage(card_type);
        h_name = $("#hidden_home_name8").val();
        h_price_order = $("#hidden_home_price8").val();

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    } else {
        console.log("Emty Home Select");
    }

    console.log("homeType: " + homeType);

    //click redio
    $('#standard').click(function () {
        $('#div_standard').show();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        homeType = '1';
        var h_price = getHomePrice('1');

        var arr = [];
        arr = getHomeWH('1');

        console.log("arr[0].h_width: " + arr[0].h_width);
        console.log("arr[0].h_height: " + arr[0].h_height);

        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage("1");
        h_name = $("#hidden_home_name1").val();
        h_price_order = $("#hidden_home_price1").val();
        console.log("h_name: " + h_name);
        console.log("hidden_home_type1: " + $("#hidden_home_type1").val() + " homeType: " + homeType);
        console.log("standard h_price_order: " + h_price_order);
        
        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0 || homeType !== $("#hidden_home_type1").val()) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);
    });

    $('#modem').click(function () {
        $('#div_standard').hide();
        $('#div_modem').show();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        homeType = '2';
        var h_price = getHomePrice('2');

        var arr = [];
        arr = getHomeWH('2');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage("2");
        h_name = $("#hidden_home_name2").val();
        h_price_order = $("#hidden_home_price2").val();
        console.log("h_name: " + h_name);
        console.log("hidden_home_type2: " + $("#hidden_home_type2").val() + " homeType: " + homeType);

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0 || homeType !== $("#hidden_home_type2").val()) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    });

    $('#vintage').click(function () {
        console.log("vintage");
        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_x4').hide();
        $('#div_vintage').show();
        homeType = '3';
        var h_price = getHomePrice('3');
        console.log(" vintage h_price: " + h_price);
        var arr = [];
        arr = getHomeWH('3');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage("3");
        h_name = $("#hidden_home_name3").val();
        h_price_order = $("#hidden_home_price3").val();
        console.log("h_name: " + h_name);
        console.log("hidden_home_type3: " + $("#hidden_home_type3").val() + " homeType: " + homeType);

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);
        if (orderArr.length === 0 || homeType !== $("#hidden_home_type3").val()) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    });

    $('#div_x').click(function () {

        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').show();
        homeType = '4';
        var h_price = getHomePrice('4');

        var arr = [];
        arr = getHomeWH('4');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage("4");
        h_name = $("#hidden_home_name4").val();
        h_price_order = $("#hidden_home_price4").val();
        console.log("h_name: " + h_name);
        console.log("hidden_home_type4: " + $("#hidden_home_type4").val() + " homeType: " + homeType);

        if (homeType !== $("#hidden_home_type4").val()) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    });

    $('#h_name5').click(function () {

        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        $('#h_name5').show();

        homeType = '5';
        var h_price = getHomePrice('5');
        console.log("h_name5 h_price: " + h_price);

        var arr = [];
        arr = getHomeWH('5');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage("5");
        h_name = $("#hidden_home_name5").val();
        h_price_order = $("#hidden_home_price5").val();
        console.log("h_name: " + h_name);
        console.log("hidden_home_type5: " + $("#hidden_home_type5").val() + " homeType: " + homeType);

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);

        if (orderArr.length === 0 || homeType !== $("#hidden_home_type5").val()) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    });

    $('#h_name6').click(function () {

        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        $('#h_name5').hide();
        $('#h_name6').show();

        homeType = '6';
        var h_price = getHomePrice('6');
        console.log("h_name6 h_price: " + h_price);

        var arr = [];
        arr = getHomeWH('6');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage("6");
        h_name = $("#hidden_home_name6").val();
        h_price_order = $("#hidden_home_price6").val();
        console.log("h_name: " + h_name);
        console.log("hidden_home_type6: " + $("#hidden_home_type6").val() + " homeType: " + homeType);

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);

        if (orderArr.length === 0 || homeType !== $("#hidden_home_type6").val()) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    });

    $('#h_name7').click(function () {

        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        $('#h_name5').hide();
        $('#h_name6').hide();
        $('#h_name7').show();

        homeType = '7';
        var h_price = getHomePrice('7');
        console.log("h_name7 h_price: " + h_price);

        var arr = [];
        arr = getHomeWH('7');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage("7");
        h_name = $("#hidden_home_name7").val();
        h_price_order = $("#hidden_home_price7").val();
        console.log("h_name: " + h_name);
        console.log("hidden_home_type7: " + $("#hidden_home_type7").val() + " homeType: " + homeType);

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);

        if (orderArr.length === 0 || homeType !== $("#hidden_home_type7").val()) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    });

    $('#h_name8').click(function () {

        $('#div_standard').hide();
        $('#div_modem').hide();
        $('#div_vintage').hide();
        $('#div_x4').hide();
        $('#h_name5').hide();
        $('#h_name6').hide();
        $('#h_name7').hide();
        $('#h_name8').show();

        homeType = '8';
        var h_price = getHomePrice('8');
        console.log("h_name8 h_price: " + h_price);

        var arr = [];
        arr = getHomeWH('8');
        $('#h_width').html(arr[0].h_width);
        $('#h_height').html(arr[0].h_height);
        $('#h_area').html(arr[0].h_area + " (ตารางเมตร)");

        showImage("7");
        h_name = $("#hidden_home_name8").val();
        h_price_order = $("#hidden_home_price8").val();
        console.log("h_name: " + h_name);
        console.log("hidden_home_type8: " + $("#hidden_home_type8").val() + " homeType: " + homeType);

        orderArr = get_order_no_by_plan_id(p_name.trim(), plan_master);
        console.log("orderArr.plan_id: " + orderArr.plan_id);

        if (orderArr.length === 0 || homeType !== $("#hidden_home_type8").val()) {
            $('#h_price').html(addCommas(h_price) + " (บาท)");
            $('#home_price').val(h_price);
            $('#home_price2').val(h_price_order);
        } else {
            $('#h_price').html(addCommas(h_price_order) + " (บาท)");
            $('#home_price').val(h_price_order);
            $('#home_price2').val(h_price_order);
        }

        $('#home_name2').val(h_name);
        $('#home_type2').val(homeType);

    });
    
    $('#confirm_bookhome').click(function () {

        if (homeType === null || homeType === '') {
            $("#idmessage-error").html("กรุณาเลือกแบบบ้าน");
            $("#mi-modal-message-error").modal('show');
        } else {
            var plan_master = $('#input_hidden_plan_master').val();
            var data = $('#input_hidden_plan_save').val();
            console.log("data: " + data);
            var listdata = [];
            var array = [];
            var seq = getSeq();
            array = data.split(',');
            for (var i = 0; i < array.length; i++) {
                listdata.push(array[i].trim());
                $('#planlist').val(JSON.stringify(listdata));

            }
            var seqArr = [];
            seqArr = listdata.sort();
            console.log("seqArr: " + seqArr);
            console.log("seqArr: " + seqArr[0]);

            console.log("planlist: " + $('#planlist').val());
            console.log("master_plan: " + $('#input_hidden_plan_master').val());

            var ids = [];
            $('input[name="checks[]"]:checked').each(function () {
                ids.push(this.value);
                $('#checkdata').val(JSON.stringify(ids));
            });
            console.log("ids: " + ids.length);
            console.log("checkdata: " + $('#checkdata').val());

            $.ajax({
                'type': 'POST',
                'data': {
                    order_no: 'P' + plan_master + "-" + pad(seq, 3) + "/" + getCurrYear(),
                    status: 'R',
                    plan_name: planid,
                    order_name: $('#planlist').val(),
                    comment: $('#comment').val(),
                    home_type: homeType,
                    home_price: $('#home_price').val(),
                    home_name: h_name,
                    master_plan: $('#input_hidden_plan_master').val(),
                    hname: h_name,
                    ri: $('#ri').val(),
                    ngan: $('#ngan').val(),
                    wa: $('#wa').val(),
                    order_date: $("#hidden_date_order").val(),
                    check_data: $("#checkdata").val(),
                    price_total_all: $("#hidden_price_plan_total_select").val(),
                },
                async: true,
                'url': 'set_plandata_status',
                'success': function (data) {
                    console.log(data);
                    if (data === 1) {

                        $("#idmessage").html("บันทึกข้อมูลการจองเรียบร้อยแล้ว");
                        $("#mi-modal-message").modal('show');

                    } else {
                        $("#idmessage").html("เกิดข้อผิดพลาดในการบันทึกข้อมูล");
                        $("#mi-modal-message").modal('show');
                    }

                }
            });
        }
    });


    //edit
    $('#edit_bookhome').click(function () {

        $("#lablepopup").html("ต้องการแก้ไขการจองหมายเลขแปลง  " + $('#input_hidden_plan_save').val());
        $('#modal-btn-y-edit').show();
        $('#modal-btn-n-edit').show();
        $('#modal-btn-y-cancel').hide();
        $('#modal-btn-n-cancel').hide();
        $("#mi-modal").modal('show');

    });

    $("#modal-btn-y-edit").click(function () {

        var data = $('#input_hidden_plan_save').val();
        console.log("data: " + data);
        var listdata = [];
        var array = [];

        array = data.split(',');
        for (var i = 0; i < array.length; i++) {
            listdata.push(array[i]);
            $('#planlist').val(JSON.stringify(listdata));
        }
        console.log("planlist: " + $('#planlist').val());
        console.log("homeType: " + homeType);

        var ids = [];
        $('input[name="checks[]"]:checked').each(function () {
            ids.push(this.value);
            $('#checkdata').val(JSON.stringify(ids));
        });
        console.log("ids: " + ids.length);

        $.ajax({
            'type': 'POST',
            'data': {
                comment: $('#comment').val(),
                home_type: homeType,
                home_price: $('#home_price').val(),
                home_name: h_name,
                planid: $('#planlist').val(),
                master_plan: $('#input_hidden_plan_master').val(),
                h_name: h_name,
                check_data: $("#checkdata").val(),
            },
            'url': 'set_update_order',
            'success': function (data) {
                if (data === 1) {
                    $("#mi-modal").modal('hide');
                    $("#idmessage").html("แก้ไขข้อมูลการจองเรียบร้อย");
                    $("#mi-modal-message").modal('show');
                }
            }
        });
    });


    $("#modal-btn-n-edit").on("click", function () {
        $("#mi-modal").modal('hide');
    });


    $('#cancel_bookhome').click(function () {

        $("#lablepopup").html("ต้องการยกเลิกการจองหมายเลขแปลง " + $('#input_hidden_plan_save').val());
        $('#modal-btn-y-edit').hide();
        $('#modal-btn-n-edit').hide();
        $('#modal-btn-y-cancel').show();
        $('#modal-btn-n-cancel').show();
        $("#mi-modal").modal('show');
    });

    $("#modal-btn-y-cancel").on("click", function () {

        var data = $('#input_hidden_plan_save').val();
        console.log("data: " + data);
        var listdata = [];
        var array = [];

        array = data.split(',');
        for (var i = 0; i < array.length; i++) {
            listdata.push(array[i]);
            $('#planlist').val(JSON.stringify(listdata));
        }

        console.log("planlist: " + $('#planlist').val());

        $.ajax({
            'type': 'POST',
            'data': {
                status: 'N',
                comment: $('#comment').val(),
                planid: $('#planlist').val(),
                master_plan: $('#input_hidden_plan_master').val()
            },
            'url': 'set_cancel_plan',
            'success': function (data) {
                if (data === 1) {
                    $("#mi-modal").modal('hide');
                    $("#idmessage").html("ยกเลิกการจองเรียบร้อยแล้ว");
                    $("#mi-modal-message").modal('show');

                } else {
                    $("#mi-modal").modal('hide');
                    $("#idmessage").html("เกิดข้อผิดพลาดในการยกเลิกแปลง");
                    $("#mi-modal-message").modal('show');
                }
            }
        });

    });

    //ทำสัญญาซื้อขาย

    $("#promit_promise").on("click", function () {
        console.log("promit_promise");
        $("#formselect").submit();

    });

    $("#modal-btn-n-cancel").on("click", function () {
        $("#mi-modal").modal('hide');
    });

    $("#modal-btn-n-message").on("click", function (s) {
        window.location.href = "../projectplan?menu=projectplan";
    });

    $("#modal-btn-n-message-error").on("click", function (s) {
        $("#mi-modal-message-error").modal('hide');
    });

    var hidden_user = $('#hidden_user').val();
    console.log("hidden_user: " + hidden_user);
    if (hidden_user === 'general') {
       
       $("#confirm_bookhome").attr("disabled", true);
       $("#cancel_bookhome").attr("disabled", true);
       $("#edit_bookhome").attr("disabled", true);
       $("#promit_bookhome").attr("disabled", true);
       $("#promit_promise").attr("disabled", true);
       
       $("#standard").attr("disabled", true);
       $("#modem").attr("disabled", true);
       $("#vintage").attr("disabled", true);
       $("#div_x").attr("disabled", true);
       $("#h_name5").attr("disabled", true);
       $("#h_name6").attr("disabled", true);
       $("#h_name7").attr("disabled", true);
       $("#h_name8").attr("disabled", true);
       
      
       
    }
    
});


function ischeckbox(cb, plan, master_plan) {
    var land = 3000;
    var total_land = 0;
    var total = 0;
    var price = 0;
    var arr = [];
    var isChecked = $(cb).prop('checked');
    console.log("isChecked: " + isChecked);
    console.log("plan: " + plan + " master_plan: " + master_plan);
    var input_price = $("#input_hidden_price").val().replace(/,/g, "");
    var input_price_emty = $("#hidden_price_plan_land_emty").val();

    if (isChecked) {
        price = parseFloat(input_price);
        console.log("price: " + price);
        total = price + parseFloat(land);

        $('#price_plan_total_select').html(formatMoney(total));
        $('#hidden_price_plan_total_select').val(total);
        $('#input_hidden_price').val(total);
        $("#hidden_price_plan_land_emty").val(total);
        var x1 = parseFloat($('#hidden_price_plan_total_select').val());
        var x2 = parseFloat($('#input_hidden_price2').val().replace(/,/g, ""));
        
        console.log("xx: "  + x1);
        console.log("xx: "  + x2);
        var x3 = 0;
        x3 = x1 - x2;
        console.log("xx: "  + x3);
        $("#price_plan_land_emty").html(formatMoney(x3));
        
    } else {
        price = parseFloat(input_price);
        console.log("price: " + price);
        total = price - parseFloat(land);

        $('#price_plan_total_select').html(formatMoney(total));
        $('#hidden_price_plan_total_select').val(total);
        $('#input_hidden_price').val(total);
        $("#hidden_price_plan_land_emty").val(total);
        
        var x1 = parseFloat($('#hidden_price_plan_total_select').val());
        var x2 = parseFloat($('#input_hidden_price2').val().replace(/,/g, ""));
        console.log("xx: "  + x1);
        console.log("xx: "  + x2);
        var x3 = 0;
        x3 = x1 - x2;
        console.log("xx: "  + x3);
        $("#price_plan_land_emty").html(formatMoney(x3));
    }


}

function showImage(id) {

    var img = '';
    var img2 = '';
    var count = 0;
    $.ajax({
        type: 'GET',
        url: 'getData_imagelist/' + id,
        async: true,
        success: function (data) {
            console.log(data.data.length);
            var div = "<div class=\"carousel-item active text-center\" style=\"width: 100%;\">";
            var enddiv = "</div>";
            var div2 = "<div class=\"carousel-item text-center\" style=\"width: 100%;\" >";
            var enddiv2 = "</div>";
            var html = "";
            for (var i = 0; i < data.data.length; i++) {
                count++;
                //console.log("path: " + data.data[i].name + "/" + data.data[i].home_id);
                if (count === 1) {
                    img = "<img src=\"../upload/" + data.data[i].home_id + "/" + data.data[i].name + "\" width=\"800\" height=\"300\"  alt=\"Chicago\" class=\"boder-image\">";
                    html += div + img + enddiv;
                } else {
                    img2 = "<img src=\"../upload/" + data.data[i].home_id + "/" + data.data[i].name + "\" width=\"800\" height=\"300\"  alt=\"Chicago\" class=\"boder-image\">";
                    html += div2 + img2 + enddiv2;
                }


            }
            console.log("html: " + html);
            console.log("id: " + id);
            if (id === "1") {
                $('#slideshow1').html(html);
            } else if (id === "2") {
                $('#slideshow2').html(html);
            } else if (id === "3") {
                $('#slideshow3').html(html);
            } else if (id === "4") {
                $('#slideshow4').html(html);
            }
        }
    });
}

function getPrice(planid) {

    var output = '';
    $.ajax({
        url: 'projectplan/getplan/' + planid,
        type: 'GET',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = data[0].p_price;
    });

    return output;
}

function getHomePrice(id) {

    var output = '';
    $.ajax({
        url: 'gethome_by_id_tojson/' + id,
        type: 'GET',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = data[0].h_price;
    });

    return output;
}

function getHomeWH(id) {

    var output = [];
    $.ajax({
        url: 'gethome_by_id_tojson/' + id,
        type: 'GET',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = data;
    });

    return output;
}

function getSeq() {

    var seq = 0;
    $.ajax({
        url: 'get_order_seq',
        type: 'GET',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        seq = data;
    });

    return seq;
}

//get_order_no_by_plan_id
function get_order_no_by_plan_id(planid, master_plan) {

    console.log("get_order_no_by_plan_id planid: " + planid + " master_plan: " + master_plan);
    var order = [];
    $.ajax({
        url: 'getorder_by_planid',
        type: 'POST',
        data: {planid: planid, master_plan: master_plan},
        async: false,
        dataType: 'json',
    }).done(function (data) {
        console.log("data: " + data);
        if (data === null) {
            order = [];
        } else {
            order = data;
        }
    });

    return order;
}


function get_order_by_orderno(order_no) {

    console.log("get_order_by_orderno order_no: " + order_no);
    var order = [];
    $.ajax({
        url: 'getorder_by_orderno',
        type: 'POST',
        data: {order_no: order_no},
        async: false,
        dataType: 'json',
    }).done(function (data) {
        console.log("data: " + data);
        if (data === null) {
            order = [];
        } else {
            order = data;
        }
    });

    return order;
}

function pad(str, max) {
    str = str.toString().trim();
    return str.length < max ? pad("0" + str, max) : str;
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function getMonthFull(month) {
    var monthNames = ["มกราคม.", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
        "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }
}

function GetTodayDate() {
    var tdate = new Date();
    var dd = tdate.getDate(); //yields day
    var MM = tdate.getMonth(); //yields month
    var yyyy = tdate.getFullYear(); //yields year

    console.log("dd.length: " + pad(dd, 2));
    var currentDate = pad(dd, 2) + "/" + pad((MM + 1), 2) + "/" + yyyy;
    console.log("currentDate: " + currentDate);
    return currentDate;
}

function checkZero(data) {
    if (data.length == 1) {
        data = "0" + data;
    }
    return data;
}

function getCurrYear() {
    var today = new Date();
    var year = today.getFullYear() + "";
    year = checkZero(year);

    var result = parseInt(parseInt(year) + 543);
    return  result;
}

function get_data_plan_arr(plan_name, master_plan) {

    //console.log("get_order_by_orderno order_no: " + order_no);
    var order = [];
    $.ajax({
        url: '../projectplan/getDataPlan',
        type: 'POST',
        data: {plan_name: plan_name,
            master_plan: master_plan},
        async: false,
        dataType: 'json',
    }).done(function (data) {
        //console.log("data: " + data);
        if (data === null) {
            order = [];
        } else {
            order = data;
        }
    });

    return order;
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}