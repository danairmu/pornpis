
$(document).ready(function () {

    var rowId = 1;

    var table = $('#userlist').DataTable({
        responsive: true,
        "processing": false,
        "serverSide": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": true,
        "ordering": false,
        "info": false,
        pageLength: 10,
        "ajax": "register/userslist",
        "rowId": 'extn',
        "columns": [

            {targets: 0,
                data: null,
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return rowId++;
                }
            },

            {
                "className": "text-center",
                "data": "pid",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {"data": "first_name",
                "render": function (data, type, row, meta) {
                    return  row.first_name + ' ' + row.last_name;

                }
            },

            {"data": "phone"},

            {
                "className": "text-center",
                "data": "create_date",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-center",
                "data": "update_date",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-center",
                "data": "group_id",
                "render": function (data, type, row, meta) {
                    var result = '';
                    if (row.group_id === '1') {
                        result = '<span class="badge badge-success">Admin</span>';
                    } else if (row.group_id === '2') {
                        result = '<span class="badge badge-success">จัดการระบบ</span>';
                    } else {
                        result = '<span class="badge badge-success">ทั่วไป</span>';
                    }
                    return result;

                }
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="showOptionModalEdit(\'' + row.id + '\') "><i class="fa fa-edit" style="color:green"></i></a>';
                },
                "defaultContent": ''
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="deleteUser(\'' + row.id + '\') "><i class="fa fa-trash-o " style="color:green"></i></a>';
                },
                "defaultContent": ''
            },
        ],
        rowReorder: true
                //"pageLength": false,
    });

    var user_type = '';
    if ($("#radio1").is(':checked')) {

    } else {

    }





//insert user
    $("#btn_insert_user").on("click", function () {

        var password = $('#password').val();
        var confirm_password = $('#password_confirm').val();
        console.log("pid: " + $('#pid').val());

        if ($('#pid').val() === null || $('#pid').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลหมายเลยประจำตัวประชาชน");
            $("#mi-modal-message").modal('show');
        } else if ($('#user_name').val() === null || $('#user_name').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลชื่อเข้าใช้งานระบบ");
            $("#mi-modal-message").modal('show');
        } else if ($('#first_name').val() === null || $('#first_name').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลชื่อ");
            $("#mi-modal-message").modal('show');
        } else if ($('#last_name').val() === null || $('#last_name').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลนามสกุล");
            $("#mi-modal-message").modal('show');
        } else if ($('#phone').val() === null || $('#phone').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลเบอร์โทรศัพท์");
            $("#mi-modal-message").modal('show');
        } else if (password === null || password === '') {
            $("#idmessage").html("กรุณาใส่กรอกข้อมูลรหัสผ่าน");
            $("#mi-modal-message").modal('show');
        } else if (password !== confirm_password) {
            $("#idmessage").html("รหัสผ่านไม่ตรงกัน");
            $("#mi-modal-message").modal('show');
        } else if (password.length < 8) {
            $("#idmessage").html("รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวษร");
            $("#mi-modal-message").modal('show');
        } else {
            $("#identicalForm").submit();

        }

    });
//
//update user
    $("#btn_update_user").on("click", function () {

        var password = $('#password2').val();
        var confirm_password = $('#password_confirm2').val();
        console.log("password: " + password);
        console.log("password: " + confirm_password);

        if ($('#pid2').val() === null || $('#pid2').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลหมายเลยประจำตัวประชาชน");
            $("#mi-modal-message").modal('show');
        } else if ($('#user_name2').val() === null || $('#user_name2').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลชื่อเข้าใช้งานระบบ");
            $("#mi-modal-message").modal('show');
        } else if ($('#first_name2').val() === null || $('#first_name2').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลชื่อ");
            $("#mi-modal-message").modal('show');
        } else if ($('#last_name2').val() === null || $('#last_name2').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลนามสกุล");
            $("#mi-modal-message").modal('show');
        } else if ($('#phone2').val() === null || $('#phone2').val() === '') {
            $("#idmessage").html("กรุณากรอกข้อมูลเบอร์โทรศัพท์");
            $("#mi-modal-message").modal('show');
        } else if ((password !== null && password !== '') || (confirm_password !== null && confirm_password !== '')) {
            if (password !== confirm_password) {
                $("#idmessage").html("รหัสผ่านไม่ตรงกัน");
                $("#mi-modal-message").modal('show');
            } else if (password.length < 8) {
                $("#idmessage").html("รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวษร");
                $("#mi-modal-message").modal('show');
            } else {
                console.log("identicalForm else submit");
                $("#identicalForm2").submit();

            }
        } else {
            console.log("identicalForm2 submit");
            $("#identicalForm2").submit();

        }
        console.log("success update user");

    });

});

function deleteUser(id) {

    $("#hidden_user").val(id);
    $("#lablepopup").html("ต้องการยกเลิกผู้ใช้งานระบบ");
    $('#modal-btn-y-cancel').show();
    $('#modal-btn-n-cancel').show();
    $("#mi-modal").modal('show');
}



$("#modal-btn-y-cancel").on("click", function () {

    console.log("hidden_user: " + $("#hidden_user").val());
    $.ajax({
        type: 'GET',
        'url': 'register/delete/' + $("#hidden_user").val(),
        'success': function (data) {
            console.log("data: " + data);
            if (data) {
                $("#mi-modal").modal('hide');
                $("#idmessage").html("ยกเลิกผู้ใช้งานระบบเรียบร้อยแล้ว");
                $("#mi-modal-message").modal('show');
                var table = $('#userlist').DataTable();
                table.ajax.reload();

            } else {
                $("#mi-modal").modal('hide');
                $("#idmessage").html("เกิดข้อผิดพลาดในการยกเลิกผู้ใช้งานระบบ");
                $("#mi-modal-message").modal('show');
            }
        }
    });

});

$("#modal-btn-n-cancel").on("click", function () {
    $("#mi-modal").modal('hide');
});


$("#modal-btn-n-message").on("click", function (s) {
    $("#mi-modal-message").modal('hide');
});

function showOptionModalAdd() {
    $('#myModal').modal('show');
}


function showOptionModalEdit(id) {

    $.ajax({
        type: 'GET',
        url: 'register/userslistbyid/' + id,
        async: true,
        success: function (data) {
            //console.log(data[0].pid);
            $('#hiddenuserid').html('<input type="hidden" value="' + id + '" id="user_id" name="user_id">');
            console.log("get group id: " + data[0].group_id);
            if (data[0].group_id === '1') {
                $("#group_type").hide();
            } else {
                $("#group_type").show();
                if (data[0].group_id === '2') {
                    console.log("else if get group id: " + data[0].group_id);
                    $("#radio3").attr("checked", true);
                } else {
                    console.log("else else get group id: " + data[0].group_id);
                    $("#radio4").attr("checked", true);
                }
            }


            $('#pid2').val(data[0].pid);
            $('#pid2').val(data[0].pid);
            $('#user_name2').val(data[0].username);
            $('#first_name2').val(data[0].first_name);
            $('#last_name2').val(data[0].last_name);
            $('#email2').val(data[0].email);
            $('#phone2').val(data[0].phone);
        }
    });


    $('#myModalUpdate').modal('show');
}

function closeOptionModalAdd() {
    $('#myModal').modal('hide');
    cleatInsert();

//    $('password#password').maxlength({
//        alwaysShow: true,
//        placement: 'top-left'
//    });
}

function closeOptionModalEdit() {
    $('#myModalUpdate').modal('hide');
}

function numberOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
}

function cleatInsert() {
    $('#pid').val("");
    $('#user_name').val("");
    $('#first_name').val("");
    $('#last_name').val("");
    $('#phone').val("");
    $('#email').val("");
    $('#password').val("");
    $('#password_confirm').val("");
}

