
$(document).ready(function () {

    var master_plan = '1';
    $('#plan_master').val("1");
    loadPlan(master_plan);

});



$("#modal-btn-n-message").on("click", function (s) {
    $("#mi-modal-message").modal('hide');
});


function loadPlan(master_plan) {

    console.log("loadplan: " + master_plan);
    var rowId = 1;
    var table = $('#planlist').DataTable({
        responsive: true,
        "processing": false,
        "serverSide": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": true,
        "ordering": false,
        "info": false,
        pageLength: 25,
        ajax: {
            url: "managementplan/planslist/" + master_plan,
            type: "GET",
            async: true
        },
//        "ajax": "managementplan/planslist/" + master_plan,
        "rowId": 'extn',
        "columns": [

            {targets: 0,
                data: null,
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return rowId++;
                }
            },

            {
                "className": "text-center",
                "data": "p_name",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {"className": "text-center",
                "data": "p_ri",
                "render": function (data, type, row, meta) {
                    var p_ri = (row.p_ri === null) ? '0' : row.p_ri;
                    var p_ngan = (row.p_ngan === null) ? '0' : row.p_ngan;
                    var p_wa = (row.p_wa === null) ? '0' : row.p_wa;
                    return p_ri + '-' + p_ngan + '-' + p_wa;

                }
            },

            {
                "className": "text-center",
                "data": "p_width",
                "render": function (data, type, row, meta) {
                    return  (data === null) ? '0' : data;

                }
            },

            {
                "className": "text-center",
                "data": "p_height",
                "render": function (data, type, row, meta) {
                    return  (data === null) ? '0' : data;
                    ;

                }
            },

            {
                "className": "text-right",
                "data": "p_price",
                "render": function (data, type, row, meta) {

                    return  (data === null) ? '0' : addCommas(data);

                }
            },

            {
                "className": "text-right",
                "data": "p_price_tarangwa",
                "render": function (data, type, row, meta) {
                    return  (data === null) ? '0' : addCommas(data);

                }
            },

            {
                "className": "text-center",
                "data": "p_status",
                "render": function (data, type, row, meta) {
                    var result = '';
                    if (row.p_status === 'R') {
                        result = '<span class="badge badge-warning">จองแล้ว</span>';
                    } else if (row.p_status === 'S') {
                        result = '<span class="badge badge-success">ขายแล้ว</span>';
                    } else {
                        result = '';
                    }
                    return  result;

                }
            },

            {
                "className": "text-left",
                "data": "home_name",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-left",
                "data": "p_comment",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="showOptionModalEdit(\'' + row.p_name + '\',\'' + master_plan + '\') "><i class="fa fa-edit" style="color:green; cursor: pointer;"></i></a>';
                },
                "defaultContent": ''
            },
        ],
        rowReorder: true,
        //"pageLength": false,
    });

    $("#inputState").change(function () {
        master_plan = $("#inputState option:selected").val();
        console.log("master_plan: " + master_plan);
        if (master_plan === '1') {

            rowId = 1;
            table.clear().draw();
            table.ajax.url('managementplan/planslist/' + master_plan).load();


        } else if (master_plan === '2') {

            rowId = 1;
            table.clear().draw();
            table.ajax.url('managementplan/planslist/' + master_plan).load();

        } else {
            rowId = 1;
            table.clear().draw();
            table.ajax.url('managementplan/planslist/' + master_plan).load();

        }
    });

    $("#p_tarangwa").keyup(function () {

        var total_tarang = $('#p_tarangwa').val();
        var int_tarangwa = 0;
        var int_tarangwaSum = 0;
        var ri;
        var ngan;
        var wa;
        if (total_tarang.length === 0) {
            $('#price').val(formatMoney("0"));
            ri = '0';
            ngan = '0';
            wa = '0';
        } else {

            int_tarangwaSum = parseFloat(total_tarang);
            int_tarangwa = parseInt(total_tarang);
            if (int_tarangwa < 400) {
                ri = '0';
            } else {
                ri = parseInt(int_tarangwa / 400);
            }

            console.log("ri: " + ri);

            if (int_tarangwa < 100) {
                ngan = '0';
            } else {
                var _ri = (ri * 400);
                var _t = int_tarangwa;
                ngan = parseInt((_t - _ri) / 100);
            }
            console.log("ngan: " + ngan);
            wa = (parseFloat(total_tarang) % 100).toFixed(2);
            console.log("wa: " + wa);


        }

        $("#ri").val(ri);
        $("#ngan").val(ngan);
        $("#wa").val(wa);

        var price_tarangwa = $('#price_tarangwa').val().replace(/,/g, "");
        ;
        var _p = parseFloat(price_tarangwa);

        console.log("_p: " + _p);
        console.log("int_tarangwa: " + int_tarangwaSum);

        var result = (int_tarangwaSum * _p).toFixed(2);
        console.log("result: " + result);
        $('#price').val(addCommas(result));

    });

    $("#price_tarangwa").keyup(function () {
        var tarangwa = $('#p_tarangwa').val();
        if (tarangwa.length === 0) {
            $('#price').val('0');
        } else {
            var price_tarangwa = $('#price_tarangwa').val().replace(/,/g, "");
            ;
            var _t = parseFloat(tarangwa);
            var _p = parseFloat(price_tarangwa);
            var result = (_t * _p).toFixed(2);
            console.log("result: " + result);
            $('#price').val(addCommas(result));
        }
    });

    var timeout = null;
    $('#price_conner').keyup(function () {
        var hidden_setplan = $("#hidden_setplan").val();
        var hidden_setmasplan = $("#hidden_setmasplan").val();
        clearTimeout(timeout);
        var out = getPricePlan(hidden_setplan, hidden_setmasplan);
        $("#price").val(out);

        timeout = setTimeout(function () {
            var _inprice_conner = 0;
            var _inprice_float = 0;
            var result = 0;
            var _vprive_conner = $("#price_conner").val();
            var _vprive = $("#price").val().replace(/,/g, "");
            var hidden_vprive = $("#price").val();
            if (_vprive_conner.length === 0) {
                _inprice_conner = 0;
                var out = getPricePlan(hidden_setplan, hidden_setmasplan);
                $("#price").val(out);

            } else {
                _inprice_conner = parseFloat(_vprive_conner);
                _inprice_float = parseFloat(_vprive);
                result = (_inprice_float += _inprice_conner);
                $("#price").val(formatMoney(result));
            }

            console.log("result: " + result);
        }, 400);
    });



    $('#btn_update_plan').click(function () {
        console.log("btn_update_plan");
        console.log("plan_master: " + $('#plan_master').val());
        var form = $('#form');

        console.log("btn_update_plan price: " + $('#price').val());
        $.ajax({
            type: 'POST',
            data: form.serialize(),
            url: 'managementplan/update_plan',
            async: true,
            success: function (data) {
                console.log("data update: " + data);

                if (data == '1') {
                    rowId = 1;
                    var table = $('#planlist').DataTable();
                    table.ajax.reload();
                    closeOptionModalEdit();
                    $("#idmessage").html("แก้ไขข้อมูลแปลงเรียบร้อย");
                    $("#mi-modal-message").modal('show');
                } else {
                    closeOptionModalEdit();
                    $("#idmessage").html("เกิดข้อผิดพลาดในการแก้ไขข้อมูลแปลง");
                    $("#mi-modal-message").modal('show');
                }
            }
        });
    });
}



function showOptionModalEdit(id, master_plan) {

    $("#hidden_setplan").val(id);
    $("#hidden_setmasplan").val(master_plan);
    console.log("edit id: " + id + " master_plan: " + master_plan);
    var count = 0;
    var flag = getPlanConner(id, master_plan);
    if (flag === 'Y') {
        $("#div_conner").show();
    } else {
        $("#div_conner").hide();
    }
    console.log("flag: " + flag);
    $.ajax({
        type: 'POST',
        data: {master_plan: master_plan,
            plan_name: id
        },
        url: 'managementplan/check_plan',
        async: true,
        success: function (data) {
            count = data;
            $.ajax({
                type: 'POST',
                data: {master_plan: master_plan,
                    plan_name: id
                },
                url: 'managementplan/planslistbyid',
                async: true,
                success: function (data) {

                    console.log("count: " + count);
                    if (count == '1') {
                        $("#ri").attr("disabled", true);
                        $("#ngan").attr("disabled", true);
                        $("#wa").attr("disabled", true);
                        $("#p_width").attr("disabled", true);
                        $("#p_height").attr("disabled", true);
                        $("#price").attr("disabled", true);
                        $("#price_tarangwa").attr("disabled", true);
                        $("#comment").attr("disabled", true);
                        $("#btn_update_plan").attr("disabled", true);
                        $("#btn_close_plan").attr("disabled", true);
                        $("#p_tarangwa").attr("disabled", true);
                        $("#price_conner").attr("disabled", true);
                        $("#homes_id").attr("disabled", true);

                    } else {
                        $("#ri").attr("disabled", false);
                        $("#ngan").attr("disabled", false);
                        $("#wa").attr("disabled", false);
                        $("#p_width").attr("disabled", false);
                        $("#p_height").attr("disabled", false);
                        $("#price").attr("disabled", false);
                        $("#price_tarangwa").attr("disabled", false);
                        $("#comment").attr("disabled", false);
                        $("#btn_update_plan").attr("disabled", false);
                        $("#btn_close_plan").attr("disabled", false);
                        $("#p_tarangwa").attr("disabled", false);
                        $("#price_conner").attr("disabled", false);
                        $("#homes_id").attr("disabled", false);
                    }


                    $('#plan_master').val(master_plan);
                    $('#planidlabel').html(data[0].p_name);
                    $('#planid').val(data[0].p_name);
                    $('#ri').val((data[0].p_ri === null) ? '0' : data[0].p_ri);
                    $('#ngan').val((data[0].p_ngan === null) ? '0' : data[0].p_ngan);
                    $('#wa').val((data[0].p_wa === null) ? '0' : data[0].p_wa);
                    $('#p_tarangwa').val((data[0].p_tarangwa === null) ? '0' : data[0].p_tarangwa);
                    $('#p_width').val((data[0].p_width === null) ? '0' : data[0].p_width);
                    $('#p_height').val((data[0].p_price === null) ? '0' : data[0].p_height);
                    $('#price_conner').val((data[0].p_corner_price === null) ? '0' : formatMoney(data[0].p_corner_price));
                    $('#price').val(formatMoney(parseFloat(data[0].p_price_tarangwa * parseFloat(data[0].p_tarangwa))));
                    $('#price_tarangwa').val((data[0].p_price_tarangwa === null) ? '0' : addCommas(data[0].p_price_tarangwa));
                    $('#comment').val(data[0].p_comment);

                    //select drobdown
                    console.log("p_home_id: " + data[0].p_home_id);
                    if (data[0].p_home_id === null || data[0].p_home_id === '') {
                        $("#homes_id").val("0");
                    } else {
                        $("#homes_id").val(data[0].p_home_id);
                    }
                }
            });

        }
    });


    $('#myModalUpdate').modal('show');
}


function closeOptionModalEdit() {
    $('#myModalUpdate').modal('hide');
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function numberOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
}
function doubleOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9 .]/gi;
    element.value = element.value.replace(regex, "");
}

function getPlanConner(plan_name, masterplan) {
    console.log("masterplan: " + masterplan);
    var output = '';
    $.ajax({
        url: 'managementplan/getPlanConner',
        data: {
            plan_name: plan_name,
            master_plan: masterplan
        },
        type: 'POST',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = data[0].p_corner_flag;
    });
    return output;
}


function getPlanConner(plan_name, masterplan) {
    console.log("masterplan: " + masterplan);
    var output = '';
    $.ajax({
        url: 'managementplan/getPlanConner',
        data: {
            plan_name: plan_name,
            master_plan: masterplan
        },
        type: 'POST',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = data[0].p_corner_flag;
    });
    return output;
}

function getPricePlan(plan_name, masterplan) {
    console.log("masterplan: " + masterplan);
    var output = '';
    $.ajax({
        data: {master_plan: masterplan,
            plan_name: plan_name
        },
        url: 'managementplan/planslistbyid',
        type: 'POST',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = formatMoney(parseFloat(data[0].p_price_tarangwa * parseFloat(data[0].p_tarangwa)));
    });
    return output;
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function BAHTTEXT(num, suffix) {
    'use strict';

    if (typeof suffix === 'undefined') {
        suffix = 'บาทถ้วน';
    }

    num = num || 0;
    num = num.toString().replace(/[, ]/g, ''); // remove commas, spaces

    if (isNaN(num) || (Math.round(parseFloat(num) * 100) / 100) === 0) {
        return 'ศูนย์บาทถ้วน';
    } else {

        var t = ['', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน'],
                n = ['', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า'],
                len,
                digit,
                text = '',
                parts,
                i;

        if (num.indexOf('.') > -1) { // have decimal

            /* 
             * precision-hack
             * more accurate than parseFloat the whole number 
             */

            parts = num.toString().split('.');

            num = parts[0];
            parts[1] = parseFloat('0.' + parts[1]);
            parts[1] = (Math.round(parts[1] * 100) / 100).toString(); // more accurate than toFixed(2)
            parts = parts[1].split('.');

            if (parts.length > 1 && parts[1].length === 1) {
                parts[1] = parts[1].toString() + '0';
            }

            num = parseInt(num, 10) + parseInt(parts[0], 10);


            /* 
             * end - precision-hack
             */
            text = num ? BAHTTEXT(num) : '';

            if (parseInt(parts[1], 10) > 0) {
                text = text.replace('ถ้วน', '') + BAHTTEXT(parts[1], 'สตางค์');
            }

            return text;

        } else {

            if (num.length > 7) { // more than (or equal to) 10 millions

                var overflow = num.substring(0, num.length - 6);
                var remains = num.slice(-6);
                return BAHTTEXT(overflow).replace('บาทถ้วน', 'ล้าน') + BAHTTEXT(remains).replace('ศูนย์', '');

            } else {

                len = num.length;
                for (i = 0; i < len; i = i + 1) {
                    digit = parseInt(num.charAt(i), 10);
                    if (digit > 0) {
                        if (len > 2 && i === len - 1 && digit === 1 && suffix !== 'สตางค์') {
                            text += 'เอ็ด' + t[len - 1 - i];
                        } else {
                            text += n[digit] + t[len - 1 - i];
                        }
                    }
                }

                // grammar correction
                text = text.replace('หนึ่งสิบ', 'สิบ');
                text = text.replace('สองสิบ', 'ยี่สิบ');
                text = text.replace('สิบหนึ่ง', 'สิบเอ็ด');

                return text + suffix;
            }

        }

    }
}