$(document).ready(function () {

    var hidden_user = $('#hidden_user').val();
    if (hidden_user === 'general') {
        
    }
    
    console.log("start ready bookhome");
    //format price
    $('#input_home_price').val(formatMoney($('#input_home_price').val()));
    $("#input_home_price_char").html("(" + ThaiBaht($('#input_home_price').val()) + ")");

    $("#pricepay").keyup(function () {
        console.log("#pricepay: " + $("#pricepay").val());

        $('#pricepay').val(addCommas($("#pricepay").val()));
        var bath = ThaiBaht(CheckNumber($("#pricepay").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        console.log("bath: " + bath);
        $('#hidden_order_price_char').val(result);
        $('#order_price_char').html(result);
    });

    console.log("init bookhome");

    $("#dateorder").datepicker({
//                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                    thaiyear: true, //Set เป็นปี พ.ศ.
        }).datepicker("setDate");

    var date = $("#dateorder").data('datepicker').getFormattedDate('dd/mm/yyyy');
    console.log("date: " + date);
    if (date === '' || date === null) {
        console.log("date2: " + date);
    }
    $("#hidden_datepicker").val(date);
    var new_date = date.replace(/\//g, '');

    console.log("new_date " + new_date);
    var day = new_date.substr(0, 2);
    var month = new_date.substr(2, 2);
    var year = new_date.substr(4, 4);
    var srt_month = getMonth(month);
    var rs_month = day + " " + srt_month + " " + year;
    $("#dateorder").val(rs_month);

    $('#dateorder').change(function () {
        var date = $("#dateorder").data('datepicker').getFormattedDate('dd/mm/yyyy');
        $("#hidden_datepicker").val(date); // to save db
        var new_date = date.replace(/\//g, '');
//        console.log("new_date " + new_date);

        var day = new_date.substr(0, 2);
        var month = new_date.substr(2, 2);
        var year = new_date.substr(4, 4);
        var srt_month = getMonth(month);
        var rs_month = day + " " + srt_month + " " + year;
        $("#dateorder").val(rs_month);
    });

    var array = [];
    var names = [];
    var planid = '';
    var data = $("#hidden_planssave").val();
    console.log("data: " + data);
    array = data.split(',');
    var total = 0;
    for (var i = 0; i < array.length; i++) {
        names.push(array[i]);
        $('#input_hidden_plan_array').val(JSON.stringify(names));
        planid = array[0];
        total += i;
    }
    $("#plan_total").val(array.length);

    console.log("input_hidden_plan_array: " + $('#input_hidden_plan_array').val());

    var orders = [];
    var master_plan = $("#input_hidden_plan_master").val();
    orders = get_order_by_plan_id(planid, master_plan);

    console.log("else order_date: " + orders.order_date);
    var cnew_date = orders.order_date.replace(/\//g, '');
    console.log("cnew_date: " + cnew_date);
    var cday = cnew_date.substr(0, 2);
    var cmonth = cnew_date.substr(2, 2);
    var cyear = cnew_date.substr(4, 4);
    var csrt_month = getMonth(cmonth);
    var crs_month = cday + " " + csrt_month + " " + (parseInt(cyear) + 543);
    console.log("cnew_crs_mocrs_monthnthdate: " + crs_month);
    $("#order_date").val(crs_month);



    //getCheckbox
    var plans = $('#hidden_planssave').val();
    var array = [];
    array = plans.split(',');
    var d = "";
    var input = "";
    var p_name = "";

    for (var i = 0; i < array.length; i++) {
        p_name = array[i];
        console.log("array[i]: " + array[i]);
        input += '<label  class="form-check-label" for="exampleCheck' + p_name.trim() + '" style="color: #000000;">แปลงที่ ' + p_name.trim() + '</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
\n\                 <input  readonly="true" name="checks[]" type="checkbox" class="form-check-input group1" id="exampleCheck' + p_name.trim() + '" value="' + p_name.trim() + '">\n\
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $("#check_land").html(input);
    }
    $("input.group1").attr("disabled", true);
    var orders2 = [];
    console.log("orders.order_no: " + orders.order_no);
    orders2 = get_order_by_orderno(orders.order_no);
    console.log("orders.length*****: " + orders2.length);
    var price_land_empty = 3000;
    var count = 0;
    var plan_price_order = 0;
    for (var i = 0; i < orders2.length; i++) {
        plan_price_order += parseFloat(orders2[i].plan_price);
        console.log("plan_price_order: " + plan_price_order);
        if (orders2[i].p_empty_land === 'Y') {
            count++;
            $("input[type=checkbox][value=" + orders2[i].plan_name + "]").prop("checked", true);
        }
    }


    //End

    //set price planempty
    var result = parseFloat(price_land_empty) * parseFloat(count);
    $('#check_land_price').val(formatMoney(result));
    $('#plan_price_order').val(formatMoney(parseFloat(plan_price_order)));
    $("#plan_price_order_char").html("(" + BAHTTEXT(parseFloat(plan_price_order)) + ")");

    getDataFromDB(planid);

    $('#btn_save').click(function () {

        console.log("save agreement");
        var pid = $('#pid').val();
        var postcode = $('#postcode').val();
        var phone = $('#phone').val();
        var pricepay = $('#pricepay').val();

        if (pid === null || pid === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณากรอกข้อมูลบัตรประชาชน");
            $("#mi-modal-message").modal('show');
        } else if (postcode.length === 0) {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุรหัสไปรษณีย์");
            $("#mi-modal-message").modal('show');
        } else if (phone.length === 0) {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุหมายเลขโทรศัพท์");
            $("#mi-modal-message").modal('show');
        } else if (pricepay.length === 0) {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุเงินจอง");
            $("#mi-modal-message").modal('show');
        } else {

            var form = $('#form');
            $.ajax({
                'type': 'POST',
                'data': form.serialize(),
                'async': true,
                'url': 'save_agreenment',
                dataType: 'json',
                'success': function (data) {
                    //console.log(data);
                    //console.log("บันทึกข้อมูลสัญญาจองเรียบร้อย");
                    $("#btn_save").prop('disabled', true);
                    $("#btn_prommise").prop('disabled', false);
                    $("#btn_edit").prop('disabled', false);
                    $("#idmessage").html("บันทึกข้อมูลสัญญาจองเรียบร้อย");
                    $("#mi-modal-message").modal('show');


                }
            });

            console.log("master_plan : " + planid);
            console.log("planid : " + planid);
            var master_plan = $("#input_hidden_plan_master").val();
            var order_no = get_order_no_by_plan_id(planid, master_plan);
            $('#order_no').val(order_no);
            console.log("order_no: " + order_no);
            var agreementid = get_agreementid_by_order_no(order_no, master_plan);
            var agreementseq = get_agreementid_by_order_no(order_no, master_plan);
            $('#agreementid_check').val(agreementid);
            $("#input_hidden_plan_master").val(master_plan);
            $('#agreementid_seq').val(agreementseq);
        }

    });

    $('#btn_edit').click(function () {

        $("#lablepopup").html("ต้องการแก้ไขหมายเลขสัญญาการจอง  " + $('#order_no').val());
        $('#modal-btn-y-edit').show();
        $('#modal-btn-n-edit').show();
        $('#modal-btn-y-cancel').hide();
        $('#modal-btn-n-cancel').hide();
        $("#mi-modal").modal('show');

    });

    $('#modal-btn-y-edit').click(function () {
        console.log("edit agreement");
        var form = $('#form');

        console.log("edit input_hidden_plan_master: " + $("#input_hidden_plan_master").val());
        console.log("edit agreement order_no: " + $("#order_no").val());

        $.ajax({
            'type': 'POST',
            'data': form.serialize(),
            async: true,
            'url': 'set_update_agreement',
            'success': function (data) {
                console.log(data);
                if (data === 1) {
                    $("#mi-modal").modal('hide');
                    $("#idmessage").html("แก้ไขข้อมูลการจองเรียบร้อย");
                    $("#mi-modal-message").modal('show');
                }

            }
        });
    });

    $("#modal-btn-n-edit").on("click", function () {
        $("#mi-modal").modal('hide');
    });
    $("#modal-btn-n-message").on("click", function (s) {
        $("#mi-modal-message").modal('hide');
    });

    $('#smardCard').click(function () {
        var url = 'http://localhost:8444/getDataFromCardImage?name=Feitian SCR301 0';
        $.ajax({
            url: url,
        }).done(function (data) {
                    
            //alert(data.pid);
            if (data.pid === undefined) {
                $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> ไม่สามารถเชื่อมต่อเครื่องอ่าน smart card กรุณาตรวจสอบเครื่องอ่าน");
                $("#mi-modal-message").modal('show');
            } else {
                var srt = data.pid;
                var titleTh = data.titleTh;
                var firstnameTh = data.firstnameTh;
                var lastnameTh = data.lastnameTh;
                var age = data.age;
                var houseNumber = data.houseNumber;
                var group = data.group;
                var alley = data.alley;
                var street = data.street;
                var subDistrict = data.subDistrict;
                var district = data.district;
                var province = data.province;

                $('#img').prop('src', 'data:image/jpg;base64,' + data.image);
                $('#pid').val(srt);
                $('#title').val(titleTh);
                $('#firstname').val(firstnameTh);
                $('#lastname').val(lastnameTh);
                $('#age').val(age);
                $('#houseNumber').val(houseNumber);
                $('#alley').val(alley);
                $('#group').val(group.substr(7, group.lenght));
                $('#street').val(street);
                $("#subDistrict").val(subDistrict.substr(4, subDistrict.lenght));
                $("#district").val(district.substr(5, district.lenght));
                $("#province").val(province.substr(7, province.lenght));

            }
        }).fail(function (data) {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> ไม่สามารถเชื่อมต่อเครื่องอ่าน smart card กรุณาตรวจสอบเครื่องอ่าน");
            $("#mi-modal-message").modal('show');
        });
    });

//        $('#scan').click(function () {
//
//            scanner.scan(displayImagesOnPage,
//                    {
//                        "output_settings": [
//                            {
//                                "type": "return-base64",
//                                "format": "jpg"
//                            }
//                        ]
//                    }
//            );
//        });

    $("#print").click(function () {
        var mode = 'iframe'; //popup
        var close = mode == "popup";
        var options = {
            mode: mode,
            popClose: close,

        };
        $("div.printableArea").show();
        $("div.printableArea").printArea(options);
    });

    $("#ex").click(function () {

        //get value
        initview();
        initDiposit();
        var mode = 'iframe'; //popup
        var close = mode == "popup";
        var options = {
            mode: mode,
            popClose: close,
            header: false,
            fontsize: 10
        };
        $("div.printableArea").printArea(options);

        //testword
        //$("#printableAreatest").wordExport();
    });


    //สแกน
    $("#scan").on("click", function () {
        $('#redirectForm').attr('action', "../projectplan/scan?menu=projectplan&mode=1");
        $("#redirectForm").submit();
    });

    $("#scancard").on("click", function () {
        var pid = $('#pid').val();
        $('#redirectForm').attr('action', "../projectplan/scan?menu=projectplan&mode=2&pid=" + pid);
        $("#redirectForm").submit();
    });

    $("#crop").on("click", function () {
        $('#redirectForm').attr('action', "../projectplan/crop?menu=projectplan&mode=2");
        $("#redirectForm").submit();



    });
//    $("#crop").on("click", function () {
//        
//    });

    $("#btn_prommise").on("click", function () {
        $('#formredirec_promise').attr('action', "../promise/promiseselet?menu=projectplan");
        $("#formredirec_promise").submit();
    });
});

/** Processes the scan result */
function displayImagesOnPage(successful, mesg, response) {
    if (!successful) { // On error
        console.error('Failed: ' + mesg);
        return;
    }

    if (successful && mesg != null && mesg.toLowerCase().indexOf('user cancel') >= 0) { // User cancelled.
        console.info('User cancelled');
        return;
    }

    var scannedImages = scanner.getScannedImages(response, true, false); // returns an array of ScannedImage

    for (var i = 0; (scannedImages instanceof Array) && i < scannedImages.length; i++) {
        var scannedImage = scannedImages[i];
        processScannedImage(scannedImage);
    }
}

/** Images scanned so far. */
var imagesScanned = [];

/** Processes a ScannedImage */
function processScannedImage(scannedImage) {
    imagesScanned.push(scannedImage);

    createFolder();
    console.log("scannedImage src: " + scannedImage.src);
    createfilescan(scannedImage.src);

    var elementImg = scanner.createDomElementFromModel({
        'name': 'img',
        'attributes': {
            'id': 'img',
            'class': 'scanned',
            'src': scannedImage.src
        }
    });
    document.getElementById('images').appendChild(elementImg);
}

function createFolder() {
    var url = 'http://localhost:8444/creaatDir';
    $.ajax({
        url: url,
    }).done(function (data) {

    }).fail(function (data) {
        alert("พบข้อผิดพลาด");
    });
}

function createfilescan(dataimage) {
    var url = 'http://localhost:8444/creaatfile';
    $.ajax({
        url: url,
        type: 'POST',
        data: dataimage,
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (data) {

    }).fail(function (data) {
        alert("พบข้อผิดพลาด");
    });
}

function downloadImage(data) {
    download(data, "scan_test.jpg", "image/jpg");
}



function downloadx(img) {


    // atob to base64_decode the data-URI
    var image_data = atob(img.split(',')[1]);
    // Use typed arrays to convert the binary data to a Blob
    var arraybuffer = new ArrayBuffer(image_data.length);
    console.log("arraybuffer: " + arraybuffer);
    var view = new Uint8Array(arraybuffer);

    console.log("view: " + view);
    for (var i = 0; i < image_data.length; i++) {
        view[i] = image_data.charCodeAt(i) & 0xff;
    }

    var blob = new Blob([arraybuffer], {type: 'application/octet-stream'}); //image/jpeg

    console.log("blob: " + blob)
    // Use the URL object to create a temporary URL
    var url = (window.webkitURL || window.URL).createObjectURL(blob);
    console.log("url: " + url);
    location.href = url; // <-- Download!

}


function getDataFromDB(planid) {

    console.log("planid: " + planid);
    var master_plan = $("#input_hidden_plan_master").val();
    var order_no = get_order_no_by_plan_id(planid, master_plan);


    $("#agreenment_id").html(order_no);
    $("#agreenment_id1").html(order_no);
    $("#agreenment_id2").html(order_no);
    $('#agreementid').val(order_no);
    $('#order_no').val(order_no);
    $('#order_no_scan').val(order_no);

    console.log("planid: " + planid);
    console.log("master_plan: " + master_plan);
    console.log("order_no: " + order_no);

    var agreementid = get_agreementid_by_order_no(order_no, master_plan);
    console.log("agreementid: " + agreementid);
    var land_empty_price = $('#check_land_price').val().replace(/,/g, "");
    console.log("land_empty_price: " + land_empty_price);
    if (agreementid !== "0") {
        $("#btn_save").prop('disabled', true);
        $('#agreementid_check').val(agreementid);
        $("#btn_prommise").attr("disabled", false);


        $.ajax({
            'type': 'POST',
            'data': {order_no: order_no, plan_master: master_plan},
            async: true,
            'url': 'get_agreementid',
            'success': function (data) {
                console.log("data: " + data.id);
                console.log("data: " + data.agreement_id);
                console.log("data: " + data.order_no);
                console.log("data: " + data.pid);
                console.log("data: " + data.title);
                console.log("data: " + data.fname);
                console.log("data: " + data.lname);
                console.log("data: " + data.age);
                console.log("data: " + data.home_no);
                console.log("data: " + data.moo);
                console.log("data: " + data.soi);
                console.log("data: " + data.street);
                console.log("data: " + data.subdistrict);
                console.log("data: " + data.district);
                console.log("data: " + data.province);
                console.log("data: " + data.postcode);
                console.log("data: " + data.phone);
                console.log("data: " + data.email);
                console.log("data: " + data.plan);
                console.log("data: " + data.plan_total);
                console.log("data: " + data.price_tarangwa);
                console.log("data: " + data.price_total);
                console.log("data: " + data.price_pay);
                console.log("data: " + data.date_agreenment_thai);

                var date = data.date_agreenment_thai;
                var new_date = date.replace(/\//g, '');
                var day = new_date.substr(0, 2);
                var month = new_date.substr(2, 2);
                var year = new_date.substr(4, 4);
                var srt_month = getMonth(month);
                var rs_month = day + " " + srt_month + " " + year;


                $("#pid").val(data.pid);
                $("#title").val(data.title);
                $("#firstname").val(data.fname);
                $("#lastname").val(data.lname);
                $("#houseNumber").val(data.home_no);
                $("#age").val(data.age);
                $("#group").val(data.moo);
                $("#alley").val(data.soi);
                $("#street").val(data.street);
                $("#subDistrict").val(data.subdistrict);
                $("#district").val(data.district);
                $("#province").val(data.province);
                $("#postcode").val(data.postcode);
                $("#phone").val(data.phone);
                $("#email").val(data.email);
                $("#plan").val(data.plan);
                $("#plan_total").val(data.plan_total);
                $("#planprice").val(data.price_tarangwa);
                var price_tatal = parseFloat(data.price_total.replace(/,/g, ""));
                var total_price = formatMoney(parseFloat(price_tatal) + parseFloat(land_empty_price));
                $("#pricetotal").val(total_price);
                $("#pricepay").val(formatMoney(data.price_pay.replace(/,/g, "")));
                $("#pricetotal_char").html("(" + BAHTTEXT(total_price) + ")");
                $("#check_land_price_char").html("(" + BAHTTEXT(land_empty_price) + ")");

                var cdate = data.date_create_thai;
                var cnew_date = cdate.replace(/\//g, '');
                var cday = cnew_date.substr(0, 2);
                var cmonth = cnew_date.substr(2, 2);
                var cyear = cnew_date.substr(4, 4);
                var csrt_month = getMonthFull(cmonth);
                var crs_month = cday + " " + csrt_month + " พ.ศ. " + cyear;

                $("#bookdate").html("วันที่ " + crs_month);
                $("#hidden_bookdate").val("วันที่ " + crs_month);

                var price_pay = data.price_pay;
                $("#order_price_char").html(price_pay === '' ? '(ศูนย์บาทถ้วน)' : "(" + ThaiBaht(data.price_pay) + ")");
                $("#dateorder").val(rs_month);
                $("#hidden_datepicker").val(data.date_agreenment_thai);


                $("#hidden_order_price_char").val("(" + ThaiBaht(data.price_pay) + ")");
            }
        });
    } else {
        $("#btn_edit").prop('disabled', true);
        $("#bookdate").html($('#hidden_date_due').val());
        $("#date_view_1").html($('#hidden_date_due').val());
        $("#date_view_de_2").html($('#hidden_date_due').val());
        $("#hidden_bookdate").val("");
        $("#btn_prommise").attr("disabled", true);

        var price_tatal = parseFloat($("#pricetotal").val().replace(/,/g, ""));
        console.log("price_tatal: " + price_tatal);
        console.log("land_empty_price: " + land_empty_price);
        var result = parseFloat(price_tatal) + parseFloat(land_empty_price);
        $("#pricetotal").val(formatMoney(result));
        $("#pricetotal_char").html("(" + BAHTTEXT(result) + ")");
        $("#check_land_price_char").html("(" + ThaiBaht(land_empty_price) + ")");
    }
}

function get_order_no_by_plan_id(planid, master_plan) {

    console.log("init get_order_no_by_plan_id");
    console.log("planid: " + planid);
    console.log("master_plan: " + master_plan);
    var order_no = '';
    $.ajax({
        url: 'getorder_by_planid',
        type: 'POST',
        data: {
            planid: planid,
            master_plan: master_plan
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        //console.log("data: " + data);
        order_no = data.order_no;
    });

    return order_no;
}

function get_agreementid_by_order_no(order_no, plan_master) {

    console.log("init get_agreementid_by_order_no " + " order_no: " + order_no + " plan_master: " + plan_master);
    var agreement_id = '';
    $.ajax({
        url: 'get_agreementid',
        type: 'POST',
        data: {
            order_no: order_no,
            plan_master: plan_master
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        if (data === '0') {
            agreement_id = "0";
        } else {
            agreement_id = data.agreement_id;
        }

    });

    return agreement_id;
}

function get_agreementseq_by_order_no(order_no, plan_master) {

    console.log("init get_agreementseq_by_order_no " + " order_no: " + order_no + " plan_master: " + plan_master);
    var agreement_seq = '';
    $.ajax({
        url: 'get_agreementid',
        type: 'POST',
        data: {
            order_no: order_no,
            plan_master: plan_master
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        if (data === '0') {
            agreement_seq = "0";
        } else {
            agreement_seq = data.agreement_seq;
        }

    });

    return agreement_seq;
}

function get_order_by_plan_id(planid, master_plan) {

    console.log("get_order_no_by_plan_id planid: " + planid + " master_plan: " + master_plan);
    var order = [];
    $.ajax({
        url: 'getorder_by_planid',
        type: 'POST',
        data: {planid: planid, master_plan: master_plan},
        async: false,
        dataType: 'json',
    }).done(function (data) {
        console.log("data: " + data);
        if (data === null) {
            order = [];
        } else {
            order = data;
        }
    });

    return order;
}

function numberOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function initview() {

    var array = [];

//    array = $('#planprice').val().split(",");
//    var price_tarangwa = '';
//    for (var i = 0; i < array.length; i++) {
//        price_tarangwa += addCommas(array[i]) + "  ;  ";
//    }
//
//    price_tarangwa = price_tarangwa.substr(0, price_tarangwa.trim().length - 1);
    //view1
    var cdata = $("#hidden_bookdate").val();
    console.log("cdata: " + cdata);
    if (cdata !== null && cdata !== '') {
        $("#date_view_1").html(cdata);
    } else {
        $("#date_view_1").html($('#hidden_date_due').val());

    }

    var fullname = $("#title").val() + $("#firstname").val() + " " + $("#lastname").val();
    $("#view_ag_fullname").html(fullname);
    $("#view_ag_homeno").html($('#houseNumber').val());
    $("#view_ag_group").html($('#group').val());
    $("#view_ag_street").html($('#street').val() === "" ? '-' : $('#street').val());
    $("#view_ag_subDistrict").html($('#subDistrict').val());
    $("#view_ag_district").html($('#district').val());
    $("#view_ag_province").html($('#province').val());
    $("#view_ag_postcode").html($('#postcode').val() === "" ? '-' : $('#postcode').val());
    $("#view_ag_phone").html($('#phone').val() === "" ? '-' : $('#phone').val());
    $("#view_ag_plan").html($('#hidden_planssave').val());
    $("#view_ag_total_plan").html($('#plan_total').val());

    $("#view_pricetarangwa").html($("#planprice").val());
    $("#view_money").html($('#pricetotal').val());
    $("#view_chat_price").html("(" + $('#input_form_price_word').val() + ")");

    var pricepay = $('#pricepay').val().replace(/,/g, "");
    var _intpricepay = parseFloat(pricepay);
    var result = _intpricepay / 2;
    $("#view_price_order").html(formatMoney(result));

    var hidden_order_price_char = $('#hidden_order_price_char').val();
    console.log("hidden_order_price_char: " + hidden_order_price_char);
    $("#view_price_order_char").html(hidden_order_price_char === '(บาทถ้วน)' ? '(ศูนย์บาทถ้วน)' : hidden_order_price_char);

    var date = $('#dateorder').val();
    $("#view_price_order_date").html(date);

    //get image
    var file_name = get_filename_by_order_no($("#hidden_order_number").val(), '2');
    if (file_name !== '') {
        console.log("getImage: " + file_name);
        var img = "<img id=\"imagecorp\" src=\"../scancard/" + file_name + "\" style=\"width: 380px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
        console.log("image: " + img);
        $('#imagecardagreement').html(img);
    }

//    cropImage();

//    cropImage();

//    $('#watermarker ').watermarker({
//        onChange: function () {},
//        onInitialize: function () {},
//        imagePath: "../images/edid.png",
//        containerClass: "watermarker-wrapper",
//        watermarkerClass: "watermarker-container",
//        watermarkImageClass: "watermarker-image",
//        resizerClass: "resizer",
//        draggingClass: "dragging",
//        resizingClass: "resizing",
//        offsetLeft: 0,
//        offsetTop: 0,
//        aspectRatio: undefined,
//    });

}


function cropImage() {

    var $image = $('#imagecorp');

    $image.cropper({
        preview: '.preview',
        ready: function (e) {
            $(this).cropper('setData', {
                height: 467,
                rotate: 0,
                scaleX: 1,
                scaleY: 1,
                width: 573,
                x: 469,
                y: 19
            });
        }
    });


}
function initDiposit() {

//    console.log("order_no_scan: " + $('#order_no_scan').val());
//    var datahome = get_homedata_by_order_no($('#order_no_scan').val());
//    console.log("datahome: " + datahome);
//    var array = [];
//    array = datahome.split(',');

    //view2
    var cdata = $("#hidden_bookdate").val();
    if (cdata !== null && cdata !== '') {
        $("#date_view_de_2").html(cdata);
    } else {
        $("#date_view_de_2").html($('#hidden_date_due').val());
    }

    var fullname = $("#title").val() + $("#firstname").val() + " " + $("#lastname").val();
    $("#view_de_fullname").html(fullname);
    $("#view_de_homeno").html($('#houseNumber').val());
    $("#view_de_group").html($('#group').val());
    $("#view_de_street").html($('#street').val() === "" ? '-' : $('#street').val());
    $("#view_de_subDistrict").html($('#subDistrict').val());
    $("#view_de_district").html($('#district').val());
    $("#view_de_province").html($('#province').val());
    $("#view_de_postcode").html($('#postcode').val() === "" ? '-' : $('#postcode').val());
    $("#view_de_phone").html($('#phone').val() === "" ? '-' : $('#phone').val());
    $("#view_de_home").html($('#input_home_name').val());
    $("#view_home_price").html($('#input_home_price').val());
    $("#vview_home_price_char").html(ThaiBaht($('#input_home_price').val()));
    $("#view_de_plans").html($('#hidden_planssave').val());

    var pricepay = $('#pricepay').val().replace(/,/g, "");
    var _intpricepay = parseFloat(pricepay);
    var result = _intpricepay / 2;
    $("#view_de_money").html(formatMoney(result));

    //$("#view_de_money").html(addCommas($('#pricepay').val()));


    var view_de_chat_price = $('#hidden_order_price_char').val();
    $("#view_de_chat_price").html(view_de_chat_price === '(บาทถ้วน)' ? '(ศูนย์บาทถ้วน)' : view_de_chat_price);

    var date = $('#dateorder').val();
    $("#view_de_price_order").html(date);

    //get image
    var file_name = get_filename_by_order_no($("#hidden_order_number").val(), '2');
    if (file_name !== '') {
        console.log("getImage: " + file_name);
        var img = "<img src=\"../scancard/" + file_name + "\" style=\"width: 360px; height: 350px;\"  alt=\"Chicago\" class=\"boder-image\">";
        console.log("image de: " + img);
        $('#imagecarddeposit').html(img);
    }
}

function get_homedata_by_order_no(order_no) {


    var detailhome = '';
    $.ajax({
        url: 'getHomeDataByOrderNo',
        type: 'POST',
        data: {
            order_no: order_no
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        console.log(data);
        detailhome = data.data[0].h_name + "," + data.data[0].h_price;

    });

    return detailhome;
}

function get_order_by_orderno(order_no) {

    console.log("get_order_by_orderno order_no: " + order_no);
    var order = [];
    $.ajax({
        url: 'getorder_by_orderno',
        type: 'POST',
        data: {order_no: order_no},
        async: false,
        dataType: 'json',
    }).done(function (data) {
        console.log("data: " + data);
        if (data === null) {
            order = [];
        } else {
            order = data;
        }
    });

    return order;
}

function get_filename_by_order_no(order_no, type) {


    var file_name = '';
    $.ajax({
        url: 'getFilenameByOrderNo',
        type: 'POST',
        data: {
            order_no: order_no,
            type: type
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        console.log(data);
        if (data.data.length === 0) {
            file_name = '';
        } else {
            file_name = data.data[0].file_name_path;
        }


    });

    return file_name;
}

function ThaiBaht(Number) {
    var Number = CheckNumber(Number);
    console.log("Number: " + Number);
    var NumberArray = new Array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ");
    var DigitArray = new Array("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน", "สิบล้าน", "ร้อยล้าน");
    var BahtText = "";
    if (isNaN(Number))
    {
        return "ข้อมูลนำเข้าไม่ถูกต้อง";
    } else
    {
        if ((Number - 0) > 999999999.9999)
        {
            return "ข้อมูลนำเข้าเกินขอบเขตที่ตั้งไว้";
        } else
        {
            Number = Number.split(".");
            if (Number[1].length > 0)
            {
                Number[1] = Number[1].substring(0, 2);
            }
            var NumberLen = Number[0].length - 0;
            for (var i = 0; i < NumberLen; i++)
            {
                var tmp = Number[0].substring(i, i + 1) - 0;
                if (tmp != 0)
                {
                    if ((i == (NumberLen - 1)) && (tmp == 1)) {
                        if (NumberLen > 1) {
                            BahtText += "เอ็ด";
                        } else {
                            BahtText += "หนึ่ง";
                        }

                    } else
                    if ((i == (NumberLen - 2)) && (tmp == 2)) {
                        BahtText += "ยี่";
                    } else
                    if ((i == (NumberLen - 2)) && (tmp == 1)) {
                        BahtText += "";
                    } else {
                        BahtText += NumberArray[tmp];
                    }
                    BahtText += DigitArray[NumberLen - i - 1];
                }
            }
            BahtText += "บาท";
            if ((Number[1] == "0") || (Number[1] == "00"))
            {
                BahtText += "ถ้วน";
            } else
            {
                DecimalLen = Number[1].length - 0;
                for (var i = 0; i < DecimalLen; i++)
                {
                    var tmp = Number[1].substring(i, i + 1) - 0;
                    if (tmp != 0)
                    {
                        if ((i == (DecimalLen - 1)) && (tmp == 1))
                        {
                            BahtText += "เอ็ด";
                        } else
                        if ((i == (DecimalLen - 2)) && (tmp == 2))
                        {
                            BahtText += "ยี่";
                        } else
                        if ((i == (DecimalLen - 2)) && (tmp == 1))
                        {
                            BahtText += "";
                        } else
                        {
                            BahtText += NumberArray[tmp];
                        }
                        BahtText += DigitArray[DecimalLen - i - 1];
                    }
                }
                BahtText += "สตางค์";
            }
            return BahtText;
        }
    }
}

function CheckNumber(Number) {
    var decimal = false;
    Number = Number.toString();
    Number = Number.replace(/ |,|บาท|฿/gi, '');
    for (var i = 0; i < Number.length; i++)
    {
        if (Number[i] == '.') {
            decimal = true;
        }
    }
    if (decimal == false) {
        Number = Number + '.00';
    }
    return Number
}

function getMonth(month) {
    var monthNames = ["ม.ค.", "ก.พ", "มี.ค", "เม.ย", "พ.ค", "มิ.ย",
        "ก.ค", "ส.ค", "ก.ย", "ต.ค", "พ.ย", "ธ.ค"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }




}

function getMonthFull(month) {
    var monthNames = ["มกราคม.", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
        "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }
}

/**
 * @name BAHTTEXT.js
 * @version 1.1.5
 * @update May 1, 2017
 * @website: https://github.com/earthchie/BAHTTEXT.js
 * @author Earthchie http://www.earthchie.com/
 * @license WTFPL v.2 - http://www.wtfpl.net/
 **/
function BAHTTEXT(num, suffix) {
    'use strict';

    if (typeof suffix === 'undefined') {
        suffix = 'บาทถ้วน';
    }

    num = num || 0;
    num = num.toString().replace(/[, ]/g, ''); // remove commas, spaces

    if (isNaN(num) || (Math.round(parseFloat(num) * 100) / 100) === 0) {
        return 'ศูนย์บาทถ้วน';
    } else {

        var t = ['', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน'],
                n = ['', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า'],
                len,
                digit,
                text = '',
                parts,
                i;

        if (num.indexOf('.') > -1) { // have decimal

            /* 
             * precision-hack
             * more accurate than parseFloat the whole number 
             */

            parts = num.toString().split('.');

            num = parts[0];
            parts[1] = parseFloat('0.' + parts[1]);
            parts[1] = (Math.round(parts[1] * 100) / 100).toString(); // more accurate than toFixed(2)
            parts = parts[1].split('.');

            if (parts.length > 1 && parts[1].length === 1) {
                parts[1] = parts[1].toString() + '0';
            }

            num = parseInt(num, 10) + parseInt(parts[0], 10);


            /* 
             * end - precision-hack
             */
            text = num ? BAHTTEXT(num) : '';

            if (parseInt(parts[1], 10) > 0) {
                text = text.replace('ถ้วน', '') + BAHTTEXT(parts[1], 'สตางค์');
            }

            return text;

        } else {

            if (num.length > 7) { // more than (or equal to) 10 millions

                var overflow = num.substring(0, num.length - 6);
                var remains = num.slice(-6);
                return BAHTTEXT(overflow).replace('บาทถ้วน', 'ล้าน') + BAHTTEXT(remains).replace('ศูนย์', '');

            } else {

                len = num.length;
                for (i = 0; i < len; i = i + 1) {
                    digit = parseInt(num.charAt(i), 10);
                    if (digit > 0) {
                        if (len > 2 && i === len - 1 && digit === 1 && suffix !== 'สตางค์') {
                            text += 'เอ็ด' + t[len - 1 - i];
                        } else {
                            text += n[digit] + t[len - 1 - i];
                        }
                    }
                }

                // grammar correction
                text = text.replace('หนึ่งสิบ', 'สิบ');
                text = text.replace('สองสิบ', 'ยี่สิบ');
                text = text.replace('สิบหนึ่ง', 'สิบเอ็ด');

                return text + suffix;
            }

        }

    }
}
