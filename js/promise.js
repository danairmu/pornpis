$(document).ready(function () {

    //สัญญาที่ดิน
    var exampleCheckPlan1 = 0;
    var exampleCheckPlan2 = 0;
    var exampleCheckPlan3 = 0;

    //สัญญาบ้าน
    var exampleCheckHome1 = 0;
    var exampleCheckHome2 = 0;
    var exampleCheckHome3 = 0;
    var exampleCheckHome4 = 0;

    //ต่อท้าย
    var exampleCheckAttached1 = 0;
    var exampleCheckAttached2 = 0;
    var exampleCheckAttached3 = 0;
    var exampleCheckAttached4 = 0;
    var exampleCheckAttached5 = 0;

    var promise_seq = getSeqPromise();
    //console.log("promise_seq: " + promise_seq);
    //console.log("hiden_person_order_no: " + $('#hiden_person_order_no').val());
    var promiseid = pad(promise_seq, 3);
    var agreement_id = $('#hiden_person_order_no').val();
    var master_plan = $('#hiden_person_master_plan').val();

    var promiseid_db = get_promiseid_by_agreement_id(agreement_id);
    console.log("promiseid_db: " + promiseid_db);
    if (promiseid_db === 0) {
        //console.log("if promiseid: " + promiseid_db);
        //console.log("if agreement_id: " + agreement_id);
        //console.log("if master_plan: " + master_plan);
        $('#promiseid').html(agreement_id);
        $('#promisehomeid').html(agreement_id);
        $('#hidden_promiseid').val(agreement_id);
        $('#view_promise_pph').html("เลขที่ PPH-" + agreement_id);
        $('#view_promise_ppl').html("เลขที่ PPL-" + agreement_id);
        $('#view_promise_ppj').html(agreement_id);
        $("#btn_edit").attr("disabled", true);
    } else {
        console.log("else promiseid: " + promiseid_db);
        console.log("else agreement_id: " + promiseid_db);
        console.log("else master_plan: " + master_plan);
        $('#promiseid').html(promiseid_db);
        $('#promisehomeid').html(promiseid_db); //home 
        $('#hidden_promiseid').val(promiseid_db);
        $('#view_promise_pph').html("เลขที่ PPH-" + promiseid_db);
        $('#view_promise_ppl').html("เลขที่ PPL-" + promiseid_db);
        $('#view_promise_ppj').html(agreement_id);

        $("#btn_save").attr("disabled", true);
        $.ajax({
            url: 'get_promise_id_by_agreement_id',
            type: 'POST',
            data: {
                agreement_id: agreement_id
            },
            'success': function (data) {
                //console.log(data);
                $('#landid').val(data.data[0].land_no);
                $('#landid2').val(data.data[0].land_no);
                $('#pay_price_promise').val(data.data[0].pay_price_promise);
                $('#pay_price_promise_result').val(data.data[0].pay_price_promise_result);
                $('#pay_price_promise_result_char').html("(" + ThaiBaht($('#pay_price_promise_result').val()) + ")");

                $('#witness_name1').val(data.data[0].witness_name1);
                $('#witness_lastname1').val(data.data[0].witness_lastname1);
                $('#witness_name2').val(data.data[0].witness_name2);
                $('#witness_lastname2').val(data.data[0].witness_lastname2);

                if (data.data[0].promise_type1 !== null && data.data[0].promise_type1 !== '') {
                    $('#exampleCheckPlan1').prop("checked", true);
                    exampleCheckPlan1 = 1;
                }
                if (data.data[0].promise_type2 !== null && data.data[0].promise_type2 !== '') {
                    $('#exampleCheckPlan2').prop("checked", true);
                    exampleCheckPlan2 = 2;
                }
                if (data.data[0].promise_type3 !== null && data.data[0].promise_type3 !== '') {
                    $('#exampleCheckPlan3').prop("checked", true);
                    exampleCheckPlan3 = 3;
                }

            }
        });
    }

    //ดึงข้อมูลบ้าน
    var promiseidhome_db = get_home_promiseid_by_agreement_id(agreement_id);
    //console.log("promiseidhome_db: " + promiseidhome_db);
    if (promiseidhome_db === 0 && promiseid_db === 0) {
        $('#promisehomeid').html(agreement_id);
        if (promiseidhome_db === 0) {
            $("#btn_edit_home").attr("disabled", true);
        }
    } else {
        $('#promisehomeid').html(promiseid_db); //home 
        if (promiseidhome_db === 0) {
            $("#btn_save_home").attr("disabled", false);
        } else {
            $("#btn_save_home").attr("disabled", true);
            $.ajax({
                url: 'get_home_promise_id_by_agreement_id',
                type: 'POST',
                data: {
                    agreement_id: agreement_id
                },
                'success': function (data) {
                    //console.log(data);
                    $('#landid2').val(data.data[0].land_no);
                    $('#landid3').val(data.data[0].land_no2);
                    $('#pay_price_home').val(data.data[0].pay_price_home);
                    $('#pay_price_home_date').val(data.data[0].pay_price_home_date);
                    $('#pay_price_pirod1').val(data.data[0].pay_price_pirod1);
                    $('#pay_price_pirod2').val(data.data[0].pay_price_pirod2);
                    $('#pay_price_pirod3').val(data.data[0].pay_price_pirod3);
                    $('#pay_price_pirod4').val(data.data[0].pay_price_pirod4);
                    $('#pay_price_pirod5').val(data.data[0].pay_price_pirod5);

                    //char
                    $('#pay_price_create_home_char').html("(" + BAHTTEXT($('#pay_price_home').val()) + ")");
                    $('#pay_home_price_period1_char').html("(" + BAHTTEXT($('#pay_price_pirod1').val()) + ")");
                    $('#pay_home_price_period2_char').html("(" + BAHTTEXT($('#pay_price_pirod2').val()) + ")");
                    $('#pay_home_price_period3_char').html("(" + BAHTTEXT($('#pay_price_pirod3').val()) + ")");
                    $('#pay_home_price_period4_char').html("(" + BAHTTEXT($('#pay_price_pirod5').val()) + ")");
                    $('#pay_home_price_period5_char').html("(" + BAHTTEXT($('#pay_price_pirod5').val()) + ")");

                    $('#price_reduct').val(data.data[0].price_promotion);
                    $('#witness2_name1').val(data.data[0].witness_name1);
                    $('#witness2_lastname1').val(data.data[0].witness_lastname1);
                    $('#witness2_name2').val(data.data[0].witness_name2);
                    $('#witness2_lastname2').val(data.data[0].witness_lastname2);

                    //สัญญาสร้างบ้าน
                    if (data.data[0].promise_type1 !== null && data.data[0].promise_type1 !== '') {
                        $('#exampleCheckHome1').prop("checked", true);
                        exampleCheckHome1 = 1;
                    }
                    if (data.data[0].promise_type2 !== null && data.data[0].promise_type2 !== '') {
                        $('#exampleCheckHome2').prop("checked", true);
                        exampleCheckHome2 = 2;
                    }
                    if (data.data[0].promise_type3 !== null && data.data[0].promise_type3 !== '') {
                        $('#exampleCheckHome3').prop("checked", true);
                        exampleCheckHome3 = 3;
                    }
                    if (data.data[0].promise_type4 !== null && data.data[0].promise_type4 !== '') {
                        $('#exampleCheckHome4').prop("checked", true);
                        exampleCheckHome4 = 4;
                    }

                    //ต่อท้ายกรณี
                    if (data.data[0].attached1 !== null && data.data[0].attached1 !== '') {
                        $('#exampleCheckAttached1').prop("checked", true);
                        exampleCheckAttached1 = 1;
                    }
                    if (data.data[0].attached2 !== null && data.data[0].attached2 !== '') {
                        $('#exampleCheckAttached2').prop("checked", true);
                        exampleCheckAttached1 = 2;
                    }
                    if (data.data[0].attached3 !== null && data.data[0].attached3 !== '') {
                        $('#exampleCheckAttached3').prop("checked", true);
                        exampleCheckAttached1 = 3;
                    }
                    if (data.data[0].attached4 !== null && data.data[0].attached4 !== '') {
                        $('#exampleCheckAttached4').prop("checked", true);
                        exampleCheckAttached1 = 4;
                    }
                    if (data.data[0].attached5 !== null && data.data[0].attached5 !== '') {
                        $('#exampleCheckAttached5').prop("checked", true);
                        exampleCheckAttached1 = 5;
                    }


                }
            });
        }
    }

    //Start show chekbox tab1
    var plans = $('#hidden_planssave').val();
    var array = [];
    array = plans.split(',');
    var d = "";
    var input = "";
    var p_name = "";

    for (var i = 0; i < array.length; i++) {
        p_name = array[i];
        //console.log("array[i]: " + array[i]);
        input += '<label class="form-check-label" for="exampleCheck' + p_name.trim() + '" style="color: #000000;">แปลงที่ ' + p_name.trim() + '</label> &nbsp;&nbsp;&nbsp;&nbsp;\n\
                  <input  readonly="true" name="checks[]" type="checkbox" class="form-check-input group1" id="exampleCheck' + p_name.trim() + '" value="' + p_name.trim() + '">\n\
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $("#check_land").html(input);
        $("#check_land2").html(input);
    }
    $("input.group1").attr("disabled", true);
    var orders = [];
    orders = get_order_by_orderno(agreement_id);
    //console.log("orders.length*****: " + orders.length);

    var price_land_empty = 3000;
    var count = 0;
    for (var i = 0; i < orders.length; i++) {
        if (orders[i].p_empty_land === 'Y') {
            count++;
            $("input[type=checkbox][value=" + orders[i].plan_name + "]").prop("checked", true);
        }
    }

    var result = parseFloat(price_land_empty) * parseFloat(count);
    $('#check_land_price').val(formatMoney(result));
    $('#check_land_price2').val(formatMoney(result));
    //End

    $('#btn_insert_person').click(function () {
        //console.log("poppid: " + $("#poppid").val())
    });

    getDataAgreement(agreement_id, master_plan);

    //keyup
    $("#pay_price_promise").keyup(function () {

        $('#pay_price_promise').val(addCommas($("#pay_price_promise").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_promise").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_price_promise_char').html(result);
    });


    $("#pay_price_promise_result").keyup(function () {

        $('#pay_price_promise_result').val(addCommas($("#pay_price_promise_result").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_promise_result").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_price_promise_result_char').html(result);
    });

    $("#pay_price_promise_pirod").keyup(function () {

        $('#pay_price_promise_pirod').val(addCommas($("#pay_price_promise_pirod").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_promise_pirod").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_price_promise_pirod_char').html(result);
    });

    $("#pay_price_promise_tranfer").keyup(function () {

        $('#pay_price_promise_tranfer').val(addCommas($("#pay_price_promise_tranfer").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_promise_tranfer").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_price_promise_tranfer_char').html(result);
    });

    //ก่อสร้างบ้าน รายละเอียดการชำระเงิน

//    $("#price_reduct").keyup(function () {
//
//        $('#price_reduct').val(addCommas($("#price_reduct").val()));
//        var bath = ThaiBaht(CheckNumber($("#price_reduct").val()));
//        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
//        //console.log("bath: " + bath);
//        //$('#pay_price_create_home_char').html(result);
//    });

    $("#pay_price_home").keyup(function () {

        $('#pay_price_home').val(addCommas($("#pay_price_home").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_home").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_price_create_home_char').html(result);
    });


    $("#pay_price_home_date").keyup(function () {

        $('#pay_price_home_date').val(addCommas($("#pay_price_home_date").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_home_date").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_price_create_home_date_char').html(result);
    });

    $("#pay_price_pirod1").keyup(function () {

        $('#pay_price_pirod1').val(addCommas($("#pay_price_pirod1").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_pirod1").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_home_price_period1_char').html(result);
    });



    $("#pay_price_pirod2").keyup(function () {

        $('#pay_price_pirod2').val(addCommas($("#pay_price_pirod2").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_pirod2").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_home_price_period2_char').html(result);
    });


    $("#pay_price_pirod3").keyup(function () {

        $('#pay_price_pirod3').val(addCommas($("#pay_price_pirod3").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_pirod3").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_home_price_period3_char').html(result);
    });

    $("#pay_price_pirod4").keyup(function () {

        $('#pay_price_pirod4').val(addCommas($("#pay_price_pirod4").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_pirod4").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_home_price_period4_char').html(result);
    });

    $("#pay_price_pirod5").keyup(function () {

        $('#pay_price_pirod5').val(addCommas($("#pay_price_pirod5").val()));
        var bath = ThaiBaht(CheckNumber($("#pay_price_pirod5").val()));
        var result = bath === 'บาทถ้วน' ? "(" + 'ศูนย์' + "บาทถ้วน)" : "(" + bath + " )";
        //console.log("bath: " + bath);
        $('#pay_home_price_period5_char').html(result);
    });

    var timeout = null;

    $("#price_reduct").keyup(function () {


        var result = '';
        clearTimeout(timeout);
        result = getPriceAgreement(agreement_id, master_plan);
        $("#pricetotal2").val(result); //home_price2  pricetotal2

        var home_price2 = getPriceHome(agreement_id, master_plan);
        timeout = setTimeout(function () {
            var _inprice_conner = 0;
            var _inprice_float = 0;
            var result = 0;
            var _vprive_conner = $("#price_reduct").val();
            var _vprive = $("#home_price2").val().replace(/,/g, "");
            if (_vprive_conner.length === 0) {
                _inprice_conner = 0;
                var out = getPriceAgreement(agreement_id, master_plan);
                $("#home_price2").val(formatMoney(home_price2));

            } else {
                _inprice_conner = parseFloat(_vprive_conner);
                _inprice_float = parseFloat(_vprive);
                result = (_inprice_float - _inprice_conner);
                $("#home_price2").val(formatMoney(result));
            }

            //console.log("result: " + result);
        }, 400);


    });


    $("#modal-btn-n-message").on("click", function (s) {
        $("#mi-modal-message").modal('hide');
    });

    //load data table
    get_person_plan_list();
    var rowCount = $('#id-tbody tr').length;
    //console.log("rowCount: " + rowCount);


    //บันทึกบุคคลเกียวข้องสัญญาทึ่ดิน
    var number = [];
    $("#btn_pop_insert").click(function () {
        //console.log("####### btn_pop_insert #######");


        var hidden_promiseid = $('#hidden_promiseid').val();
        var poppid = $('#poppid').val();
        var poptitle = $('#poptitle').val();
        var popfirstname = $('#popfirstname').val();
        var poplastname = $('#poplastname').val();
        var popage = $('#popage').val();
        var pophouseNumber = $('#pophouseNumber').val();
        var popgroup = $('#popgroup').val();
        var popalley = $('#popalley').val();
        var popstreet = $('#popstreet').val();
        var popsubDistrict = $('#popsubDistrict').val();
        var popdistrict = $('#popdistrict').val();
        var popprovince = $('#popprovince').val();
        var poppostcode = $('#poppostcode').val();
        var popphone = $('#popphone').val();
        var popemail = $('#popemail').val();
        var plan_master = $('#hiden_person_master_plan').val();

        var result = chectk_personplan_promise(hidden_promiseid, poppid, plan_master);
        if (result >= 1) {
            $("#idmessage").html("พบข้อมูลของบุคคลดังกล่าวแล้ว");
            $("#mi-modal-message").modal('show');
        } else {
            $.ajax({
                type: 'POST',
                data: {
                    promiseid: hidden_promiseid,
                    pid: poppid,
                    title: poptitle,
                    firstname: popfirstname,
                    lastname: poplastname,
                    age: popage,
                    houseNumber: pophouseNumber,
                    group: popgroup,
                    alley: popalley,
                    street: popstreet,
                    subDistrict: popsubDistrict,
                    district: popdistrict,
                    province: popprovince,
                    postcode: poppostcode,
                    phone: popphone,
                    email: popemail,
                    plan_master: plan_master
                },
                url: 'insert_plan_person',
                async: true,
                success: function (data) {
                    //console.log("data: " + data);

                    //set to tr
                    //console.log("number: " + number.length);
                    var row;
                    if (number.length === 0) {
                        var rowCount = $('#id-tbody tr').length;
                        if (rowCount === 0) {
                            row = 1;
                        } else {
                            row = rowCount + 1;
                        }
                    } else {
                        row = number.length + 1;
                    }
                    var statusScan = getPerson_Status_Scan(hidden_promiseid, poppid, plan_master, '1', '1');
                    console.log("statusScan: " + statusScan);
                    var td_status = '';
                    if (statusScan === '1') {
                        td_status = '<td align="center"><i class="fa fa-check" style="color:green;" aria-hidden="true"></i></td>';
                    } else {
                        td_status = '<td align="center"><i class="fa fa-close" style="color:red;" aria-hidden="true"></i></td>';
                    }
                    var tr = '<tr>\n\
                        <td align="center" id="number"> ' + row + '</td>\n\
                        <td align="center" >' + poppid + '</td>\n\
                        <td>' + popfirstname + ' ' + poplastname + '</td>\n\
                        <td>' + popsubDistrict + ' ' + popdistrict + ' ' + popprovince + '</td>'
                            + td_status +
                            '<td align="center"><input type="checkbox" name="check1[]" id="check1" onclick="ischeckbox(this, \'' + poppid + '\')"></td>\n\
                        <td align="center"><a onclick="deleteperson(\'' + poppid + '\')"><i class="fa fa-trash" style="color:green"></i></a></td>\n\
                        <td align="center"><a style="cursor: pointer;" onclick="scanDataTab1(this,\'' + poppid + '\')"><span style="padding: 7px;" class="badge badge-success">สแกนสำเนาบัตร</span></a></td>\n\
                        <input type="hidden" name="hispid[]" value="' + poppid + '">\n\
                        <input type="hidden" name="histitle[]" value="' + poptitle + '">\n\
                        <input type="hidden" name="hisfirstname[]" value="' + popfirstname + '">\n\
                        <input type="hidden" name="hislastname[]" value="' + poplastname + '">\n\
                        <input type="hidden" name="hispopage[]" value="' + popage + '">\n\
                        <input type="hidden" name="hishouseNumber[]" value="' + pophouseNumber + '">\n\
                        <input type="hidden" name="hisgroup[]" value="' + popgroup + '">\n\
                        <input type="hidden" name="hisalley[]" value="' + popalley + '">\n\
                        <input type="hidden" name="hisstreet[]" value="' + popstreet + '">\n\
                        <input type="hidden" name="hissubDistrict[]" value="' + popsubDistrict + '">\n\
                        <input type="hidden" name="hisdistrict[]" value="' + popdistrict + '">\n\
                        <input type="hidden" name="hisprovince[]" value="' + popprovince + '">\n\
                        <input type="hidden" name="hispostcode[]" value="' + poppostcode + '">\n\
                        <input type="hidden" name="hisphone[]" value="' + popphone + '">\n\
                        <input type="hidden" name="hisemail[]" value="' + popemail + '">\n\
                    </tr>';

                    insert_person_home();

                    $("#id-tbody").append(tr);
                    closeOptionModalAdd();
                    var rowCount = $('#id-tbody tr').length;
                    number.push(rowCount);
                }
            });
        }
    });

    var promise_type1 = '';
    var promise_type2 = '';
    var promise_type3 = '';



    $('#exampleCheckPlan1').click(function () {
        if ($(this).is(':checked')) {
            console.log("exampleCheckPlan1 if");
            exampleCheckPlan1 = 1;
            promise_type1 = '11';
        } else {
            console.log("exampleCheckPlan1 else");
            exampleCheckPlan1 = 0;
            promise_type1 = '';
        }
    });

    $('#exampleCheckPlan2').click(function () {
        if ($(this).is(':checked')) {
            console.log("exampleCheckPlan2 if");
            exampleCheckPlan2 = 2;
            promise_type2 = '12';
        } else {
            console.log("exampleCheckPlan2 else");
            exampleCheckPlan2 = 0;
            promise_type2 = '';
        }
    });

    $('#exampleCheckPlan3').click(function () {
        if ($(this).is(':checked')) {
            console.log("exampleCheckPlan3 if");
            exampleCheckPlan3 = 3;
            promise_type3 = '13';
        } else {
            console.log("exampleCheckPlan3 else");
            exampleCheckPlan3 = 0;
            promise_type3 = '';
        }
    });


    //tab2 check 2 รูปแบบสัญญา
    var promise_type_tab2_1 = '';
    var promise_type_tab2_2 = '';
    var promise_type_tab2_3 = '';
    var promise_type_tab2_4 = '';

    var attached1_type1 = '';
    var attached1_type2 = '';
    var attached1_type3 = '';
    var attached1_type4 = '';
    var attached1_type5 = '';

//    $('#exampleCheckHome1').click(function () {
//        if ($(this).is(':checked')) {
//            console.log("exampleCheckHome1 if");
//            exampleCheckHome1 = 1;
//            promise_type_tab2_1 = '21';
//        } else {
//            console.log("exampleCheckHome1 else");
//            exampleCheckHome1 = 0;
//            promise_type_tab2_1 = '';
//        }
//    });

//    $('#exampleCheckHome2').click(function () {
//        if ($(this).is(':checked')) {
//            console.log("exampleCheckHome2 if");
//            exampleCheckHome2 = 2;
//            promise_type_tab2_2 = '22';
//        } else {
//            console.log("exampleCheckHome2 else");
//            exampleCheckHome2 = 0;
//            promise_type_tab2_2 = '';
//        }
//    });

//    $('#exampleCheckHome3').click(function () {
//        if ($(this).is(':checked')) {
//            console.log("exampleCheckHome3 if");
//            exampleCheckHome3 = 3;
//            promise_type_tab2_3 = '23';
//        } else {
//            console.log("exampleCheckHome3 else");
//            exampleCheckHome3 = 0;
//            promise_type_tab2_3 = '';
//        }
//    });

//    $('#exampleCheckHome4').click(function () {
//        if ($(this).is(':checked')) {
//            console.log("exampleCheckHome4 if");
//            exampleCheckHome4 = 4;
//            promise_type_tab2_1 = '24';
//        } else {
//            console.log("exampleCheckHome4 else");
//            exampleCheckHome4 = 0;
//            promise_type_tab2_4 = '';
//        }
//    });
//
//    //Attached
    $('#exampleCheckAttached1').click(function () {
        if ($(this).is(':checked')) {
            console.log("exampleCheckAttached1 if");
            exampleCheckAttached1 = 1;
            attached1_type1 = 'A1';
        } else {
            console.log("exampleCheckAttached1 else");
            exampleCheckAttached1 = 0;
            attached1_type1 = '';
        }
    });
//
//    $('#exampleCheckAttached2').click(function () {
//        if ($(this).is(':checked')) {
//            console.log("exampleCheckAttached2 if");
//            exampleCheckAttached2 = 2;
//            attached1_type1 = 'A2';
//        } else {
//            console.log("exampleCheckAttached2 else");
//            exampleCheckAttached2 = 0;
//            attached1_type2 = '';
//        }
//    });
//
//    $('#exampleCheckAttached3').click(function () {
//        if ($(this).is(':checked')) {
//            console.log("exampleCheckAttached3 if");
//            exampleCheckAttached3 = 3;
//            attached1_type1 = 'A3';
//        } else {
//            console.log("exampleCheckAttached3 else");
//            exampleCheckAttached3 = 0;
//            attached1_type3 = '';
//        }
//    });
//
//    $('#exampleCheckAttached4').click(function () {
//        if ($(this).is(':checked')) {
//            console.log("exampleCheckAttached4 if");
//            exampleCheckAttached4 = 4;
//            attached1_type4 = 'A4';
//        } else {
//            console.log("exampleCheckAttached4 else");
//            exampleCheckAttached4 = 0;
//            attached1_type4 = '';
//        }
//    });
//
//    $('#exampleCheckAttached5').click(function () {
//        if ($(this).is(':checked')) {
//            console.log("exampleCheckAttached5 if");
//            exampleCheckAttached5 = 5;
//            attached1_type5 = 'A5';
//        } else {
//            console.log("exampleCheckAttached5 else");
//            exampleCheckAttached5 = 0;
//            attached1_type5 = '';
//        }
//    });


    //บันทึกสัญญาที่ดิน
    $("#btn_save").click(function () {

        var pid = $('#pid').val();
        var landid = $('#landid').val();
        var pay_price_promise = $('#pay_price_promise').val();
        var witness_name1 = $('#witness_name1').val();
        var witness_lastname1 = $('#witness_lastname1').val();
        var witness_name2 = $('#witness_name2').val();
        var witness_lastname2 = $('#witness_lastname2').val();

        if (pid === null || pid === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาเลือกบุคคลทำสัญญา");
            $("#mi-modal-message").modal('show');
        } else if (landid === null || landid === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุโฉนดที่ดิน");
            $("#mi-modal-message").modal('show');
        } else if (pay_price_promise === null || pay_price_promise === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุเงินทำสัญญา");
            $("#mi-modal-message").modal('show');
        } else if (witness_name1 === null || witness_name1 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุชื่อพยานที่1");
            $("#mi-modal-message").modal('show');
        } else if (witness_lastname1 === null || witness_lastname1 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุนามสกุลพยานที่1");
            $("#mi-modal-message").modal('show');
        } else if (witness_name1 === null || witness_name2 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุชื่อพยานที่2");
            $("#mi-modal-message").modal('show');
        } else if (witness_lastname2 === null || witness_lastname2 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุนามสกุลพยานที่2");
            $("#mi-modal-message").modal('show');
        } else if (witness_name1.trim() === witness_name2.trim() && witness_lastname1.trim() === witness_lastname2.trim()) {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> ชื่อพยานต้องไม่เป็นบุคคลเดียวกัน");
            $("#mi-modal-message").modal('show');
        } else {



            var hidden_promiseid = $('#hidden_promiseid').val();
            var agreement_id = $('#hiden_person_order_no').val();
            var landid = $('#landid').val();
            var master_plan = $('#hiden_person_master_plan').val();
            var plans = $('#hidden_planssave').val();
            var plan_total = $('#plan_total').val(); //จำนวนแปลง
            var planprice = $('#planprice').val();//ราคาตารางวา
            var pricetotal = $('#pricetotal').val();
            var pricepay = $('#pricepay').val(); //ชำระเงินจอง
            var home_id = $('#home_id').val();
            var home_name = $('#home_name').val();
            var home_price = $('#home_price').val();
            var date_agreenment = $('#date_agreenment').val();
            var pay_price_promise = $('#pay_price_promise').val();
            var pay_price_promise_result = $('#pay_price_promise_result').val();

            var db_order_date_hidden = $('#db_order_date_hidden').val();
            var db_hidden_date_create_agreenment = $('#db_hidden_date_create_agreenment').val();
            var db_agerrment_date_hidden = $('#db_agerrment_date_hidden').val();
            var witness_name1 = $('#witness_name1').val();
            var witness_lastname1 = $('#witness_lastname1').val();
            var witness_name2 = $('#witness_name2').val();
            var witness_lastname2 = $('#witness_lastname2').val();

            //console.log("save promisd plan: " + date_agreenment);
            $.ajax({
                type: 'POST',
                data: {
                    promiseid: hidden_promiseid,
                    agreement_id: agreement_id,
                    landid: landid,
                    master_plan: master_plan,
                    plans: plans.trim(),
                    plan_total: plan_total,
                    planprice: planprice,
                    pricetotal: pricetotal,
                    pricepay: pricepay,
                    home_id: home_id,
                    home_name: home_name,
                    home_price: home_price,
                    dateorder: date_agreenment,
                    pay_price_promise: pay_price_promise,
                    pay_price_promise_result: pay_price_promise_result,

                    db_order_date_hidden: db_order_date_hidden,
                    db_hidden_date_create_agreenment: db_hidden_date_create_agreenment,
                    db_agerrment_date_hidden: db_agerrment_date_hidden,
                    witness_name1: witness_name1,
                    witness_lastname1: witness_lastname1,
                    witness_name2: witness_name2,
                    witness_lastname2: witness_lastname2,
                    promise_type1: promise_type1,
                    promise_type2: promise_type2,
                    promise_type3: promise_type3,
                },

                url: 'insert_primise_plan',
                async: true,
                success: function (data) {
                    //console.log("insert promise data: " + data);
                    $("#idmessage").html("บันทึกข้อมูลสัญญาซื้อขายที่ดินเรียบร้อย");
                    $("#mi-modal-message").modal('show');
                    $("#btn_save").attr("disabled", true);
                    $("#btn_edit").attr("disabled", false);
                    $('#landid2').val($('#landid').val());
                }
            });
        }
    });


    $("#btn_edit").click(function () {

        var pid = $('#pid').val();
        var landid = $('#landid').val();
        var pay_price_promise = $('#pay_price_promise').val();
        var witness_name1 = $('#witness_name1').val();
        var witness_lastname1 = $('#witness_lastname1').val();
        var witness_name2 = $('#witness_name2').val();
        var witness_lastname2 = $('#witness_lastname2').val();

        if (pid === null || pid === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาเลือกบุคคลทำสัญญา");
            $("#mi-modal-message").modal('show');
        } else if (landid === null || landid === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุโฉนดที่ดิน");
            $("#mi-modal-message").modal('show');
        } else if (pay_price_promise === null || pay_price_promise === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุเงินทำสัญญา");
            $("#mi-modal-message").modal('show');
        } else if (witness_name1 === null || witness_name1 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุชื่อพยานที่1");
            $("#mi-modal-message").modal('show');
        } else if (witness_lastname1 === null || witness_lastname1 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุนามสกุลพยานที่1");
            $("#mi-modal-message").modal('show');
        } else if (witness_name1 === null || witness_name2 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุชื่อพยานที่2");
            $("#mi-modal-message").modal('show');
        } else if (witness_lastname2 === null || witness_lastname2 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุนามสกุลพยานที่2");
            $("#mi-modal-message").modal('show');
        } else if (witness_name1.trim() === witness_name2.trim() && witness_lastname1.trim() === witness_lastname2.trim()) {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> ชื่อพยานต้องไม่เป็นบุคคลเดียวกัน");
            $("#mi-modal-message").modal('show');
        } else {


            var hidden_promiseid = $('#hidden_promiseid').val();
            var agreement_id = $('#hiden_person_order_no').val();
            var landid = $('#landid').val();
            var pay_price_promise = $('#pay_price_promise').val();
            var pay_price_promise_result = $('#pay_price_promise_result').val();

            var witness_name1 = $('#witness_name1').val();
            var witness_lastname1 = $('#witness_lastname1').val();
            var witness_name2 = $('#witness_name2').val();
            var witness_lastname2 = $('#witness_lastname2').val();

            //console.log("edit promisd plan");
            $.ajax({
                type: 'POST',
                data: {
                    promiseid: hidden_promiseid,
                    agreement_id: agreement_id,
                    landid: landid,
                    pay_price_promise: pay_price_promise,
                    pay_price_promise_result: pay_price_promise_result,

                    witness_name1: witness_name1,
                    witness_lastname1: witness_lastname1,
                    witness_name2: witness_name2,
                    witness_lastname2: witness_lastname2,
                },

                url: 'update_primise_plan',
                async: true,
                success: function (data) {
                    //console.log("update promise data: " + data);
                    $("#idmessage").html("แกไขข้อมูลสัญญาซื้อขายที่ดินเรียบร้อย");
                    $("#mi-modal-message").modal('show');
                }
            });
        }
    });



    //แสดงตัวอย่าง
    $("#ex").click(function () {


        //รูปแบบการออกแบบสัญญา
        console.log("exampleCheckPlan1: " + exampleCheckPlan1);
        console.log("exampleCheckPlan2: " + exampleCheckPlan2);
        console.log("exampleCheckPlan3: " + exampleCheckPlan3);

        var hidden_promiseid = $('#hidden_promiseid').val();
        get_data_view_plan(hidden_promiseid);



//        if (exampleCheckPlan1 == 0 && exampleCheckPlan2 == 0 && exampleCheckPlan3 == 0) {
//            $("#idmessage").html("กรุณาเลือกรูปแบบการออกสัญญา");
//            $("#mi-modal-message").modal('show');
//        } else {
//
//
//            var mode = 'iframe'; //popup
//            var close = mode == "popup";
//            var options = {
//                mode: mode,
//                popClose: close,
//                header: false,
//                fontsize: 10
//            };
//
//            if (exampleCheckPlan1 == 1) {
//
//                //get value
//                var hidden_promiseid = $('#hidden_promiseid').val();
//                get_data_view_plan(hidden_promiseid);
//                
//                $('#land_case1_page1').addClass('printableArea1');
//                $('#land_case1_page2').addClass('printableArea1');
//                $('#land_case1_page3').addClass('printableArea1');
//                $('#land_case1_page4').addClass('printableArea1');
//                $('#land_case1_page5').addClass('printableArea1');
//                $('#land_case1_page6').addClass('printableArea1');
//                $('#land_case1_page7').addClass('printableArea1');
//                $('#land_case1_page8').addClass('printableArea1');
//
//            } else {
//                $('#land_case1_page1').removeClass('printableArea1');
//                $('#land_case1_page2').removeClass('printableArea1');
//                $('#land_case1_page3').removeClass('printableArea1');
//                $('#land_case1_page4').removeClass('printableArea1');
//                $('#land_case1_page5').removeClass('printableArea1');
//                $('#land_case1_page6').removeClass('printableArea1');
//                $('#land_case1_page7').removeClass('printableArea1');
//                $('#land_case1_page8').removeClass('printableArea1');
//
//            }
//
//            if (exampleCheckPlan2 == 2) {
//
//                var hidden_promiseid = $('#hidden_promiseid').val();
//                get_data_view_plan(hidden_promiseid);
//
//                var mode = 'iframe'; //popup
//                var close = mode == "popup";
//                var options = {
//                    mode: mode,
//                    popClose: close,
//                    header: false,
//                    fontsize: 10
//                };
//                $('#land_case2_page1').addClass('printableArea1');
//                $('#land_case2_page2').addClass('printableArea1');
//                $('#land_case2_page3').addClass('printableArea1');
//                $('#land_case2_page4').addClass('printableArea1');
//                $('#land_case2_page5').addClass('printableArea1');
//                $('#land_case2_page6').addClass('printableArea1');
//                $('#land_case2_page7').addClass('printableArea1');
//                $('#land_case2_page8').addClass('printableArea1');
//
//            } else {
//                $('#land_case2_page1').removeClass('printableArea1');
//                $('#land_case2_page2').removeClass('printableArea1');
//                $('#land_case2_page3').removeClass('printableArea1');
//                $('#land_case2_page4').removeClass('printableArea1');
//                $('#land_case2_page5').removeClass('printableArea1');
//                $('#land_case2_page6').removeClass('printableArea1');
//                $('#land_case2_page7').removeClass('printableArea1');
//                $('#land_case2_page8').removeClass('printableArea1');
//            }
//
//            if (exampleCheckPlan3 == 3) {
//
//                var mode = 'iframe'; //popup
//                var close = mode == "popup";
//                var options = {
//                    mode: mode,
//                    popClose: close,
//                    header: false,
//                    fontsize: 10
//                };
//                //$("div.printableArea1").printArea(options);
//                $('#land_case3_page1').addClass('printableArea1');
//                $('#land_case3_page2').addClass('printableArea1');
//                $('#land_case3_page3').addClass('printableArea1');
//
//            } else {
//
//                $('#land_case3_page1').removeClass('printableArea1');
//                $('#land_case3_page2').removeClass('printableArea1');
//                $('#land_case3_page3').removeClass('printableArea1');
//            }
//
//            $("div.printableArea1").printArea(options);
//
//        }


    });

    //สแกน
    $("#scan1").click(function () {
        var master_plan = $('#hiden_person_master_plan').val();
        var promiseid = $('#hidden_promiseid').val();
        var orderno = $('#hiden_person_order_no').val();
        $('#hiddin_scan_promiseid').val(promiseid);
        $('#hiddin_scan_masterplan').val(master_plan);
        $('#hiddin_scan_orderno').val(orderno);
        $('#hiddin_scan_plan_save').val($('#hiden_to_scan_planssave').val());
        $('#hiddin_scan_due_date_order').val($('#hiden_to_scan_due_date_order').val());
        $('#hiddin_scan_due_date_display').val($('#hiden_to_scan_due_date_display').val());

        $('#scanForm1').attr('action', "../promise/scan?menu=projectplan&mode=1&tab=1&typescan=1");
        $('#scanForm1').submit();
    });

//    $("#scancard").on("click", function () {
//
//        var master_plan = $('#hiden_person_master_plan').val();
//        var promiseid = $('#hidden_promiseid').val();
//        var orderno = $('#hiden_person_order_no').val();
//        $('#hiddin_scan_promiseid').val(promiseid);
//        $('#hiddin_scan_masterplan').val(master_plan);
//        $('#hiddin_scan_orderno').val(orderno);
//        $('#hiddin_scan_plan_save').val($('#hiden_to_scan_planssave').val());
//        $('#hiddin_scan_due_date_order').val($('#hiden_to_scan_due_date_order').val());
//        $('#hiddin_scan_due_date_display').val($('#hiden_to_scan_due_date_display').val());
//
//        $('#scanForm1').attr('action', "../promise/scan?menu=projectplan&mode=2");
//        $("#scanForm1").submit();
//    });

//    END TAB1

    $("#scan2").click(function () {
        var master_plan = $('#hiden_person_master_plan').val();
        var promiseid = $('#hidden_promiseid').val();
        var orderno = $('#hiden_person_order_no').val();
        $('#hiddin_scan_promiseid2').val(promiseid);
        $('#hiddin_scan_masterplan2').val(master_plan);
        $('#hiddin_scan_orderno2').val(orderno);

        $('#hiddin_scan_plan_save2').val($('#hiden_to_scan_planssave').val());
        $('#hiddin_scan_due_date_order2').val($('#hiden_to_scan_due_date_order').val());
        $('#hiddin_scan_due_date_display2').val($('#hiden_to_scan_due_date_display').val());

        $('#scanForm2').attr('action', "../promise/scantab2?menu=projectplan&mode=2&tab=2&typescan=1");
        $('#scanForm2').submit();
    });

//    $("#scancard2").click(function () {
//        var master_plan = $('#hiden_person_master_plan').val();
//        var promiseid = $('#hidden_promiseid').val();
//        var orderno = $('#hiden_person_order_no').val();
//        $('#hiddin_scan_promiseid2').val(promiseid);
//        $('#hiddin_scan_masterplan2').val(master_plan);
//        $('#hiddin_scan_orderno2').val(orderno);
//
//        $('#hiddin_scan_plan_save2').val($('#hiden_to_scan_planssave').val());
//        $('#hiddin_scan_due_date_order2').val($('#hiden_to_scan_due_date_order').val());
//        $('#hiddin_scan_due_date_display2').val($('#hiden_to_scan_due_date_display').val());
//
//        $('#scanForm2').attr('action', "../promise/scantab2?menu=projectplan&mode=4");
//        $('#scanForm2').submit();
//    });

    //============================================TAB2==========================================
    //load tab2
    get_person_plan_list_tab2();

    function insert_person_home() {

        var hidden_promiseid = $('#hidden_promiseid').val();
        var poppid = $('#poppid').val();
        var poptitle = $('#poptitle').val();
        var popfirstname = $('#popfirstname').val();
        var poplastname = $('#poplastname').val();
        var popage = $('#popage').val();
        var pophouseNumber = $('#pophouseNumber').val();
        var popgroup = $('#popgroup').val();
        var popalley = $('#popalley').val();
        var popstreet = $('#popstreet').val();
        var popsubDistrict = $('#popsubDistrict').val();
        var popdistrict = $('#popdistrict').val();
        var popprovince = $('#popprovince').val();
        var poppostcode = $('#poppostcode').val();
        var popphone = $('#popphone').val();
        var popemail = $('#popemail').val();
        var popissuedDate = $('#popissuedDate').val();
        var popexpiredDate = $('#popexpiredDate').val();
        var plan_master = $('#hiden_person_master_plan').val();

        var result = chectk_personhome_promise(hidden_promiseid, poppid, plan_master);
        if (result >= 1) {
            $("#idmessage-tab2").html("พบข้อมูลของบุคคลดังกล่าวแล้ว");
            $("#mi-modal-message-tab2").modal('show');
        } else {
            $.ajax({
                type: 'POST',
                data: {
                    promiseid: hidden_promiseid,
                    pid: poppid,
                    title: poptitle,
                    firstname: popfirstname,
                    lastname: poplastname,
                    age: popage,
                    houseNumber: pophouseNumber,
                    group: popgroup,
                    alley: popalley,
                    street: popstreet,
                    subDistrict: popsubDistrict,
                    district: popdistrict,
                    province: popprovince,
                    postcode: poppostcode,
                    phone: popphone,
                    email: popemail,
                    issuedDate: popissuedDate,
                    expiredDate: popexpiredDate,
                    plan_master: plan_master
                },
                url: 'insert_home_person',
                async: true,
                success: function (data) {
                    //console.log("data: " + data);

                    //set to tr
                    //console.log("number: " + number.length);
                    var row;
                    if (number.length === 0) {
                        var rowCount = $('#id-tbody tr').length;
                        if (rowCount === 0) {
                            row = 1;
                        } else {
                            row = rowCount + 1;
                        }
                    } else {
                        row = number.length + 1;
                    }

                    var tr = '<tr>\n\
                        <td align="center" id="numbertab2"> ' + row + '</td>\n\
                        <td align="center" >' + poppid + '</td>\n\
                        <td>' + popfirstname + ' ' + poplastname + '</td>\n\
                        <td>' + popsubDistrict + ' ' + popdistrict + ' ' + popprovince + '</td>\n\
                        <td align="center"><i class="fa fa-close" style="color:red;" aria-hidden="true"></i></td>\n\
                        <td align="center"><input type="checkbox" name="check2[]" id="check2" onclick="ischeckboxtab2(this, \'' + poppid + '\')"></td>\n\
                        <input type="hidden" name="hispidtab2[]" value="' + poppid + '">\n\
                        <input type="hidden" name="histitletab2[]" value="' + poptitle + '">\n\
                        <input type="hidden" name="hisfirstnametab2[]" value="' + popfirstname + '">\n\
                        <input type="hidden" name="hislastnametab2[]" value="' + poplastname + '">\n\
                        <input type="hidden" name="hispopagetab2[]" value="' + popage + '">\n\
                        <input type="hidden" name="hishouseNumbertab2[]" value="' + pophouseNumber + '">\n\
                        <input type="hidden" name="hisgrouptab2[]" value="' + popgroup + '">\n\
                        <input type="hidden" name="hisalleytab2[]" value="' + popalley + '">\n\
                        <input type="hidden" name="hisstreettab2[]" value="' + popstreet + '">\n\
                        <input type="hidden" name="hissubDistricttab2[]" value="' + popsubDistrict + '">\n\
                        <input type="hidden" name="hisdistricttab2[]" value="' + popdistrict + '">\n\
                        <input type="hidden" name="hisprovincetab2[]" value="' + popprovince + '">\n\
                        <input type="hidden" name="hispostcodetab2[]" value="' + poppostcode + '">\n\
                        <input type="hidden" name="hisphonetab2[]" value="' + popphone + '">\n\
                        <input type="hidden" name="hisemailtab2[]" value="' + popemail + '">\n\
                    </tr>';

                    $("#id-tbody-tab2").append(tr);
                    closeOptionModalAddTab2();
                    var rowCount = $('#id-tbody-tab2 tr').length;
                    number.push(rowCount);
                }
            });
        }
    }


    //บันทึกบุคคลเกียวข้องสัญญาบ้าน 
    var number = [];
    $("#btn_pop_insert_tab2").click(function () {
        //console.log("####### btn_pop_insert #######");

        var hidden_promiseid = $('#hidden_promiseid').val();
        var poppid = $('#poppidtab2').val();
        var poptitle = $('#poptitletab2').val();
        var popfirstname = $('#popfirstnametab2').val();
        var poplastname = $('#poplastnametab2').val();
        var popage = $('#popagetab2').val();
        var pophouseNumber = $('#pophouseNumbertab2').val();
        var popgroup = $('#popgrouptab2').val();
        var popalley = $('#popalleytab2').val();
        var popstreet = $('#popstreettab2').val();
        var popsubDistrict = $('#popsubDistricttab2').val();
        var popdistrict = $('#popdistricttab2').val();
        var popprovince = $('#popprovincetab2').val();
        var poppostcode = $('#poppostcodetab2').val();
        var popphone = $('#popphonetab2').val();
        var popemail = $('#popemailtab2').val();
        var popissuedDate = $('#popissuedDate').val();
        var popexpiredDate = $('#popexpiredDate').val();
        var plan_master = $('#hiden_person_master_plan').val();

        var result = chectk_personhome_promise(hidden_promiseid, poppid, plan_master);
        if (result >= 1) {
            $("#idmessage-tab2").html("พบข้อมูลของบุคคลดังกล่าวแล้ว");
            $("#mi-modal-message-tab2").modal('show');
        } else {
            $.ajax({
                type: 'POST',
                data: {
                    promiseid: hidden_promiseid,
                    pid: poppid,
                    title: poptitle,
                    firstname: popfirstname,
                    lastname: poplastname,
                    age: popage,
                    houseNumber: pophouseNumber,
                    group: popgroup,
                    alley: popalley,
                    street: popstreet,
                    subDistrict: popsubDistrict,
                    district: popdistrict,
                    province: popprovince,
                    postcode: poppostcode,
                    phone: popphone,
                    email: popemail,
                    issuedDate: popissuedDate,
                    expiredDate: popexpiredDate,
                    plan_master: plan_master
                },
                url: 'insert_home_person',
                async: true,
                success: function (data) {
                    //console.log("data: " + data);

                    //set to tr
                    //console.log("number: " + number.length);
                    var row;
                    if (number.length === 0) {
                        var rowCount = $('#id-tbody tr').length;
                        if (rowCount === 0) {
                            row = 1;
                        } else {
                            row = rowCount + 1;
                        }
                    } else {
                        row = number.length + 1;
                    }

                    var tr = '<tr>\n\
                        <td align="center" id="numbertab2"> ' + row + '</td>\n\
                        <td align="center" >' + poppid + '</td>\n\
                        <td>' + popfirstname + ' ' + poplastname + '</td>\n\
                        <td>' + popsubDistrict + ' ' + popdistrict + ' ' + popprovince + '</td>\n\
                        <td><i class="fa fa-close" style="color:red;" aria-hidden="true"></i></td>\n\
                        <td align="center"><input type="checkbox" name="check2[]" id="check2" onclick="ischeckboxtab2(this, \'' + poppid + '\')"></td>\n\
                        <td align="center"><a onclick="deletepersontab2(\'' + poppid + '\')"><i class="fa fa-trash" style="color:green"></i></a></td>\n\
                        <td align="center"><a style="cursor: pointer;" onclick="scanDataTab2(this,\'' + poppid + '\')" ><span style="padding: 7px;" class="badge badge-success">สแกนสำเนาบัตร</span></a></td>\n\
                        <input type="hidden" name="hispidtab2[]" value="' + poppid + '">\n\
                        <input type="hidden" name="histitletab2[]" value="' + poptitle + '">\n\
                        <input type="hidden" name="hisfirstnametab2[]" value="' + popfirstname + '">\n\
                        <input type="hidden" name="hislastnametab2[]" value="' + poplastname + '">\n\
                        <input type="hidden" name="hispopagetab2[]" value="' + popage + '">\n\
                        <input type="hidden" name="hishouseNumbertab2[]" value="' + pophouseNumber + '">\n\
                        <input type="hidden" name="hisgrouptab2[]" value="' + popgroup + '">\n\
                        <input type="hidden" name="hisalleytab2[]" value="' + popalley + '">\n\
                        <input type="hidden" name="hisstreettab2[]" value="' + popstreet + '">\n\
                        <input type="hidden" name="hissubDistricttab2[]" value="' + popsubDistrict + '">\n\
                        <input type="hidden" name="hisdistricttab2[]" value="' + popdistrict + '">\n\
                        <input type="hidden" name="hisprovincetab2[]" value="' + popprovince + '">\n\
                        <input type="hidden" name="hispostcodetab2[]" value="' + poppostcode + '">\n\
                        <input type="hidden" name="hisphonetab2[]" value="' + popphone + '">\n\
                        <input type="hidden" name="hisemailtab2[]" value="' + popemail + '">\n\
                    </tr>';

                    $("#id-tbody-tab2").append(tr);
                    closeOptionModalAddTab2();
                    var rowCount = $('#id-tbody-tab2 tr').length;
                    number.push(rowCount);
                }
            });
        }
    });

    $("#modal-btn-n-message-tab2").on("click", function (s) {
        $("#mi-modal-message-tab2").modal('hide');
    });



    //บันทึกสัญญาบ้าน
    $("#btn_save_home").click(function () {

        var pid = $('#pid4').val();
        var landid = $('#landid2').val();
        var landid3 = $('#landid3').val();
        var pay_price_home = $('#pay_price_home').val();
        var witness_name1 = $('#witness2_name1').val();
        var witness_lastname1 = $('#witness2_lastname1').val();
        var witness_name2 = $('#witness2_name2').val();
        var witness_lastname2 = $('#witness2_lastname2').val();

        if (pid === null || pid === '') {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาเลือกบุคคลทำสัญญา");
            $("#mi-modal-message-tab2").modal('show');
        } else if (landid === null || landid === '') {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุโฉนดที่ดิน");
            $("#mi-modal-message-tab2").modal('show');
        } else if (landid3 === null || landid3 === '') {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุเลขที่ที่ดิน");
            $("#mi-modal-message-tab2").modal('show');
        } else if (pay_price_home === null || pay_price_home === '') {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุเงินทำสัญญา");
            $("#mi-modal-message-tab2").modal('show');
        } else if (witness_name1 === null || witness_name1 === '') {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุชื่อพยานที่1");
            $("#mi-modal-message-tab2").modal('show');
        } else if (witness_lastname1 === null || witness_lastname1 === '') {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุนามสกุลพยานที่1");
            $("#mi-modal-message-tab2").modal('show');
        } else if (witness_name1 === null || witness_name2 === '') {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุชื่อพยานที่2");
            $("#mi-modal-message-tab2").modal('show');
        } else if (witness_lastname2 === null || witness_lastname2 === '') {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุนามสกุลพยานที่2");
            $("#mi-modal-message-tab2").modal('show');
        } else if (witness_name1.trim() === witness_name2.trim() && witness_lastname1.trim() === witness_lastname2.trim()) {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> ชื่อพยานต้องไม่เป็นบุคคลเดียวกัน");
            $("#mi-modal-message-tab2").modal('show');
        } else {



            var hidden_promiseid = $('#hidden_promiseid').val();
            var agreement_id = $('#hiden_person_order_no').val();
            var landid = $('#landid2').val();
            var landid3 = $('#landid3').val();
            var master_plan = $('#hiden_person_master_plan').val();
            var plans = $('#hidden_planssave2').val();
            var plan_total = $('#plan_total2').val(); //จำนวนแปลง
            var planprice = $('#planprice2').val();//ราคาตารางวา
            var pricetotal = $('#pricetotal2').val();
            var pricepay = $('#pricepay2').val(); //ชำระเงินจอง
            var home_id = $('#home_id2').val();
            var home_name = $('#home_name2').val();
            var home_price = $('#home_price2').val();
            var date_agreenment = $('#date_agreenment2').val();
            var pay_price_home = $('#pay_price_home').val();
            var pay_price_home_date = $('#pay_price_home_date').val();
            var pay_price_pirod1 = $('#pay_price_pirod1').val();
            var pay_price_pirod2 = $('#pay_price_pirod2').val();
            var pay_price_pirod3 = $('#pay_price_pirod3').val();

            var db_order_date_hidden = $('#db_order_date_hidden2').val();
            var db_hidden_date_create_agreenment = $('#db_hidden_date_create_agreenment2').val();
            var db_agerrment_date_hidden = $('#db_agerrment_date_hidden2').val();
            var price_reduct = $("#price_reduct").val();
            var pay_price_pirod4 = $('#pay_price_pirod4').val();
            var pay_price_pirod5 = $('#pay_price_pirod5').val();
            var witness_name1 = $('#witness2_name1').val();
            var witness_lastname1 = $('#witness2_lastname1').val();
            var witness_name2 = $('#witness2_name2').val();
            var witness_lastname2 = $('#witness2_lastname2').val();

            //console.log("save promisd plan: " + date_agreenment);
            $.ajax({
                type: 'POST',
                data: {
                    promiseid: hidden_promiseid,
                    agreement_id: agreement_id,
                    landid: landid,
                    landid3: landid3,
                    master_plan: master_plan,
                    plans: plans.trim(),
                    plan_total: plan_total,
                    planprice: planprice,
                    pricetotal: pricetotal,
                    pricepay: pricepay,
                    home_id: home_id,
                    home_name: home_name,
                    home_price: home_price,
                    dateorder: date_agreenment,
                    pay_price_home: pay_price_home,
                    pay_price_home_date: pay_price_home_date,
                    pay_price_pirod1: pay_price_pirod1,
                    pay_price_pirod2: pay_price_pirod2,
                    pay_price_pirod3: pay_price_pirod3,

                    db_order_date_hidden: db_order_date_hidden,
                    db_hidden_date_create_agreenment: db_hidden_date_create_agreenment,
                    db_agerrment_date_hidden: db_agerrment_date_hidden,
                    price_reduct: price_reduct,
                    pay_price_pirod4: pay_price_pirod4,
                    pay_price_pirod5: pay_price_pirod5,
                    witness_name1: witness_name1,
                    witness_lastname1: witness_lastname1,
                    witness_name2: witness_name2,
                    witness_lastname2: witness_lastname2,
                },

                url: 'insert_primise_home',
                async: true,
                success: function (data) {
                    //console.log("insert promise data: " + data);
                    $("#idmessage-tab2").html("บันทึกข้อมูลสัญญาซื้อขายบ้านเรียบร้อยแล้ว");
                    $("#mi-modal-message-tab2").modal('show');
                    $("#btn_save_home").attr("disabled", true);
                    $("#btn_edit_home").attr("disabled", false);
                }
            });
        }
    });


    $("#btn_edit_home").click(function () {

        var pid = $('#pid4').val();
        var landid = $('#landid2').val();
        var landid3 = $('#landid3').val();
        var pay_price_home = $('#pay_price_home').val();
        var witness_name1 = $('#witness2_name1').val();
        var witness_lastname1 = $('#witness2_lastname1').val();
        var witness_name2 = $('#witness2_name2').val();
        var witness_lastname2 = $('#witness2_lastname2').val();

        if (pid === null || pid === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาเลือกบุคคลทำสัญญา");
            $("#mi-modal-message").modal('show');
        } else if (landid === null || landid === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุโฉนดที่ดิน");
            $("#mi-modal-message").modal('show');
        } else if (landid3 === null || landid3 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุเลขที่ที่ดิน");
            $("#mi-modal-message").modal('show');
        } else if (pay_price_home === null || pay_price_home === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุเงินทำสัญญา");
            $("#mi-modal-message").modal('show');
        } else if (witness_name1 === null || witness_name1 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุชื่อพยานที่1");
            $("#mi-modal-message").modal('show');
        } else if (witness_lastname1 === null || witness_lastname1 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุนามสกุลพยานที่1");
            $("#mi-modal-message").modal('show');
        } else if (witness_name1 === null || witness_name2 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุชื่อพยานที่2");
            $("#mi-modal-message").modal('show');
        } else if (witness_lastname2 === null || witness_lastname2 === '') {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> กรุณาระบุนามสกุลพยานที่2");
            $("#mi-modal-message").modal('show');
        } else if (witness_name1.trim() === witness_name2.trim() && witness_lastname1.trim() === witness_lastname2.trim()) {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> ชื่อพยานต้องไม่เป็นบุคคลเดียวกัน");
            $("#mi-modal-message").modal('show');
        } else {


            var hidden_promiseid = $('#hidden_promiseid').val();
            var agreement_id = $('#hiden_person_order_no').val();
            var landid = $('#landid2').val();
            var landid2 = $('#landid3').val();
            var pay_price_home = $('#pay_price_home').val();
            var pay_price_home_date = $('#pay_price_home_date').val();
            var pay_price_pirod1 = $('#pay_price_pirod1').val();
            var pay_price_pirod2 = $('#pay_price_pirod2').val();
            var pay_price_pirod3 = $('#pay_price_pirod3').val();

            var price_reduct = $("#price_reduct").val();
            var pay_price_pirod4 = $('#pay_price_pirod4').val();
            var pay_price_pirod5 = $('#pay_price_pirod5').val();
            var witness_name1 = $('#witness2_name1').val();
            var witness_lastname1 = $('#witness2_lastname1').val();
            var witness_name2 = $('#witness2_name2').val();
            var witness_lastname2 = $('#witness2_lastname2').val();

            //console.log("edit promisd home");
            $.ajax({
                type: 'POST',
                data: {
                    promiseid: hidden_promiseid,
                    agreement_id: agreement_id,
                    landid: landid,
                    landid2: landid2,
                    pay_price_home: pay_price_home,
                    pay_price_home_date: pay_price_home_date,
                    pay_price_pirod1: pay_price_pirod1,
                    pay_price_pirod2: pay_price_pirod2,
                    pay_price_pirod3: pay_price_pirod3,

                    price_reduct: price_reduct,
                    pay_price_pirod4: pay_price_pirod4,
                    pay_price_pirod5: pay_price_pirod5,
                    witness_name1: witness_name1,
                    witness_lastname1: witness_lastname1,
                    witness_name2: witness_name2,
                    witness_lastname2: witness_lastname2,
                },

                url: 'update_primise_home',
                async: true,
                success: function (data) {
                    //console.log("update promise data: " + data);
                    $("#idmessage-tab2").html("แกไขข้อมูลสัญญาซื้อขายที่ดินเรียบร้อย");
                    $("#mi-modal-message-tab2").modal('show');
                }
            });
        }
    });

    //แสดงตัวอย่าง 
    $("#ex_home").click(function () {

        var hidden_promiseid = $('#hidden_promiseid').val();
        get_data_view_plan(hidden_promiseid);

//        var hidden_promiseid = $('#hidden_promiseid').val();
//        if (exampleCheckHome1 == 1) {
//            get_data_view_home(hidden_promiseid);
//
//            //สัญญาบ้านแบบที่ 1
//            if (exampleCheckHome1 == 1) {
//
//            } else {
//
//            }
//
//            //สัญญาบ้านแบบที่ 2
//            if (exampleCheckHome2 == 1) {
//
//            } else {
//
//            }
//
//            //สัญญาบ้านแบบที่ 3
//            if (exampleCheckHome3 == 1) {
//
//            } else {
//
//            }
//
//            //สัญญาบ้านแบบที่ 4
//            if (exampleCheckHome4 == 1) {
//
//            } else {
//
//            }
//
//            //สัญญาบ้านต่อท้ายกรณีที่ 1
//            if (exampleCheckAttached1 == 1) {
//
//            } else {
//
//            }
//
//            //สัญญาบ้านต่อท้ายกรณีที่ 2
//            if (exampleCheckAttached2 == 1) {
//
//            } else {
//
//            }
//
//            //สัญญาบ้านต่อท้ายกรณีที่ 3
//            if (exampleCheckAttached3 == 1) {
//
//            } else {
//
//            }
//
//            //สัญญาบ้านต่อท้ายกรณีที่ 4
//            if (exampleCheckAttached4 == 1) {
//
//            } else {
//
//            }
//
//            //สัญญาบ้านต่อท้ายกรณีที่ 5
//            if (exampleCheckAttached5 == 1) {
//
//            } else {
//
//            }
//
//
//            var mode = 'iframe'; //popup
//            var close = mode == "popup";
//            var options = {
//                mode: mode,
//                popClose: close,
//                header: false,
//                fontsize: 10
//            };
//            $("div.printableArea2").printArea(options);
//        }


    });


    var el = document.getElementById('divscoretab1');
    el.tabIndex = 0;
    el.focus();

    var el = document.getElementById('divscoretab2');
    el.tabIndex = 0;
    el.focus();
});

//ยกเลิกสัญญาแปลง 
$("#canclepromise_plan").click(function () {

    $("#lablepopup").html("ต้องการยกเลิกสัญญาซื้อขายเลขที่  " + $('#hidden_promiseid').val());
    $("#mi-modal").modal('show');

});

$("#modal-btn-cancel-y").click(function () {
    var promise_id = $('#hidden_promiseid').val();
    var agreement_id = $('#hiden_person_order_no').val();
    var plan_master = $('#hiden_person_master_plan').val();
    var planid = $('#hidden_planssave').val();
    cancelPromisePlan(promise_id, agreement_id, planid, plan_master);

});


$("#modal-btn-cancel-n").on("click", function () {
    $("#mi-modal").modal('hide');
});

$("#modal-btn-n-message-cancel").on("click", function (s) {
    window.location.href = "../projectplan?menu=projectplan";
});

//ยกเลิกสัญญาบ้าน
$("#canclepromise_home").click(function () {

    $("#lablepopup-tab2").html("ต้องการยกเลิกสัญญาซื้อขายเลขที่  " + $('#hidden_promiseid').val());
    $("#mi-modal-tab2").modal('show');

});


$("#modal-btn-cancel-y-tab2").click(function () {

    var promise_id = $('#hidden_promiseid').val();
    var agreement_id = $('#hiden_person_order_no').val();
    var plan_master = $('#hiden_person_master_plan').val();
    var planid = $('#hidden_planssave').val();
    cancelPromisePlan(promise_id, agreement_id, planid, plan_master);

});


$("#modal-btn-cancel-n-tab2").on("click", function () {
    $("#mi-modal-tab2").modal('hide');
});


$("#modal-btn-n-message-cancel-tab2").on("click", function (s) {
    window.location.href = "../projectplan?menu=projectplan";
});



//========================================function TAB1 ===========================
function getInitPersonPlan() {

}

function ischeckbox(cb, pid) {

    var isChecked = $(cb).prop('checked');
    //console.log("isChecked: " + isChecked);
    //console.log("pid: " + pid);
    var promiseid = $('#hidden_promiseid').val();
    var status;
    if (isChecked) {
        status = 'Y';
    } else {
        status = 'N';
    }

    $.ajax({
        url: 'update_person_plan_flag',
        type: 'POST',
        data: {
            promiseid: promiseid,
            pid: pid,
            flag: status
        },
        async: true,
        dataType: 'json',
        success: function (data) {
            //console.log("data chekbox" + data);
            get_data_person_plan_detail(promiseid, pid);

        }
    });

}


function deleteperson(pid) {
    //console.log("##deleteperson : " + pid);
    var promiseid = $('#hidden_promiseid').val();
    $.ajax({
        url: 'deplte_person_plan',
        type: 'POST',
        data: {
            promiseid: promiseid,
            pid: pid
        },
        async: true,
        dataType: 'json',
        success: function (data) {
            //console.log("data chekbox" + data);
            $('#id-tbody tr').remove();
            get_person_plan_list();
            get_data_person_plan_detail(promiseid, pid);


            document.location.reload(true);

        }
    });

    //delete person_promise home2
    deletepersontab2(pid);
}

//======= function scan ==========
function scanDataTab1(th, pid) {

    //console.log("th: " + th);
    console.log("##scanDataTab1 : " + th);
    $("#mi-modal-promisepromise_pop").modal("show");

    //getDataScan getDataScanPopUp(order_no, plan_master_scan, promiseid, mode, tab)


    var promise_id = $('#hidden_promiseid').val();
    var order_no = $('#hiden_person_order_no').val();
    var plan_master = $('#hiden_person_master_plan').val();

    console.log("##order_no : " + order_no);
    console.log("##promise_id : " + promise_id);
    console.log("##plan_master : " + plan_master);

    var promistType = '1';
    var file_type = '2';
    getDataScanPopUp(order_no, plan_master, promise_id, "1", "1", promistType, file_type, th);

    $("#btn_scan").on("click", function (s) {
        var promiseid = $('#hidden_promiseid').val();
        var data = "data:image/jpeg;base64,/9j/";
        var b64Data = data.substring(23, data.length);
        //console.log("b64Data: " + b64Data);
        //console.log("pid: " + th);
        scanTab(th, "1", "1", "2", "1", "3");
    });

}

function scanDataTab2(th, pid) {

    console.log("##scanDataTab2 : " + pid);
    $("#mi-modal-promisepromise_pop2").modal("show");
    //getDataScan

    var promise_id = $('#hidden_promiseid').val();
    var order_no = $('#hiden_person_order_no').val();
    var plan_master = $('#hiden_person_master_plan').val();

    //console.log("##order_no : " + order_no);
    //console.log("##promise_id : " + promise_id);
    //console.log("##plan_master : " + plan_master);
    var promistType = '2';
    var file_type = '2';
    //getDataScanPopUp2(order_no, plan_master, promise_id, "4", "2", promistType, file_type, th);

    $("#btn_scan2").on("click", function (s) {
        var promiseid = $('#hidden_promiseid').val();
        var data = "data:image/jpeg;base64,/9j/";
        var b64Data = data.substring(23, data.length);
        //console.log("b64Data: " + b64Data);
        //console.log("pid: " + pid);
        scanTab(pid, "2", "2", "2", "2", "4");
    });

}



function scanTab(pid, type, tab, file_type, promise_type, mode) {

    scanner.scan(displayImagesOnPage, {
        "output_settings": [
            {
                "type": "return-base64",
                "format": "jpg"
            }
        ]
    }
    );
    /** Processes the scan result */
    function displayImagesOnPage(successful, mesg, response) {
        //console.log("successful: " + successful);
        if (!successful) { // On error
            console.error('Failed: ' + mesg);
            return;
        }

        if (successful && mesg != null && mesg.toLowerCase().indexOf('user cancel') >= 0) { // User cancelled.
            console.info('User cancelled');
            return;
        }

        var scannedImages = scanner.getScannedImages(response, true, false); // returns an array of ScannedImage

        for (var i = 0; (scannedImages instanceof Array) && i < scannedImages.length; i++) {
            var scannedImage = scannedImages[i];
            processScannedImage(scannedImage);
        }
    }

    function processScannedImage(scannedImage) {
        ////console.log("scannedImage src: " + scannedImage.src);
        var data = scannedImage.src;
        var b64Data = data.substring(23, data.length);
        var seq_san = getScanSeq();

        var filename = $('#hidden_promiseid').val();
        var promise_id = $('#hidden_promiseid').val();
        var plan_master = $('#hiden_person_master_plan').val();
        var path = "scanpromise/" + filename + "-" + seq_san + ".jpg";
        var filename_scan = $("#hidden_promiseid").val();
        var order_no = filename;
        var fullfile_name = promise_id + "-" + seq_san + "-" + plan_master;
        var filenameTopath = filename_scan.replace(/\//g, '-') + "-" + seq_san;
        //send to php
        //console.log("filename: " + filename);
        //console.log("promise_id: " + promise_id);
        //console.log("plan_master: " + plan_master);
        //console.log("filename_scan: " + filenameTopath);
        //console.log("fullfile_name: " + fullfile_name);
        //console.log("mode1: " + mode);

        $.ajax({
            'url': 'image_scan',
            'type': 'POST',
            async: true,
            'data': {
                promise_id: promise_id,
                filename: fullfile_name.trim(),
                pathfile: path,
                order_no: order_no,
                image_data: b64Data,
                plan_master: plan_master,
                mode: mode,
                filename_scan: filenameTopath,
                pid: pid,
                typescan: file_type,
                tab: tab
            },
            dataType: 'json',
            'success': function (data) {
                console.log("tab: " + tab);
                if (tab === '1') {
                    var table = $('#scanlist').DataTable();
                    table.ajax.reload();
                } else {
                    var table = $('#scan_list2').DataTable();
                    table.ajax.reload();
                }

            }, error: function (jqXHR, textStatus, errorThrown) {
                //console.log(errorThrown);
            }
        });
    }
}


function getDataScanPopUp(order_no, plan_master_scan, promiseid, mode, tab, prommise_type, file_type, pid) {

//    var tableid;
//    if (tab === '1') {
//        tableid = $('#scanlist');
//    } else {
//        tableid = $('#scanlist2');
//    }

    console.log("getDataScanPopUp");
    console.log("order_no: " + order_no);
    console.log("plan_master_scan: " + plan_master_scan);
    console.log("promiseid: " + promiseid);
    console.log("mode: " + mode);
    console.log("tab: " + tab);
    console.log('pid: ' + pid);
    $("#scanlist").dataTable().fnDestroy();
    var table = $('#scanlist').DataTable({

        fixedHeader: true,
        "processing": false,
        "serverSide": false,
        "searching": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": false,
        "ordering": false,
        "info": false,
        catch : false,
        pageLength: 10,
        "rowId": 'extn',

        "ajax": {
            "url": "scanlist",
            "type": "post",
            "data": {order_no: order_no,
                plan_master_scan: plan_master_scan,
                promiseid: promiseid,
                mode: mode,
                tab: tab,
                prommise_type: prommise_type,
                file_type: file_type,
                pid: pid,
                flag: '2' //smart card
            }
        },

        "columns": [

            {targets: 0,
                data: 'id',
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },

            {
                "className": "text-left",
                "data": "file_name",
                "render": function (data, type, row, meta) {
                    if (row.file_name.length > 0) {
                        var result = "เอกสารสำสำเนาบัตร " + pid;// + "-" +row.file_name.substring(0, row.file_name.length - 4);
                        return  result;
                    } else {
                        return  data;
                    }


                }
            },

            {"className": "text-center",
                "data": "create_date_thai",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="showOptionViewImage(\'' + row.order_no + '\', \'' + row.file_name_scan + '\') "><i class="fa fa-file-image-o" style="color:green; cursor: pointer;"></i></a>';
//                            &nbsp;&nbsp;\n\
//                            <a onclick="cancelImage(\'' + row.id + '\', \'' + row.order_no + '\', \'' + row.file_name + '\') "style="font-size: 18px; cursor: pointer" target="_blank"><i class="fa fa-close" style="color:red;" aria-hidden="true"></i></a>';
                },
                "defaultContent": ''
            },
        ],
        rowReorder: true
                //"pageLength": false,
    });
}

function showOptionViewImage(order_no, file_name) {

    var mode = 'iframe'; //popup
    var close = mode == "popup";
    var options = {
        mode: mode,
        popClose: close
    };


    console.log(" showOptionViewImage file_name: " + file_name);

    var img = "<img src=\"../scanpromisecard/" + file_name + "\" style=\"width: 100%\"  alt=\"Chicago\" class=\"boder-image\">";
    $('.printableAreaImage').html(img);
    $("div.printableAreaImage").show();
    $("div.printableAreaImage").printArea(options);


}

function getDataScanPopUp2(order_no, plan_master_scan, promiseid, mode, tab, prommise_type, file_type, pid) {


    var table = $('#scan_list2').DataTable({

        fixedHeader: true,
        "processing": false,
        "serverSide": false,
        "searching": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": false,
        "ordering": false,
        "info": false,
        pageLength: 10,
        "rowId": 'extn',

        "ajax": {
            "url": "scanlist",
            "type": "post",
            "data": {order_no: order_no,
                plan_master_scan: plan_master_scan,
                promiseid: promiseid,
                mode: mode,
                tab: tab,
                prommise_type: prommise_type,
                file_type: file_type,
                pid: pid
            }
        },

        "columns": [

            {targets: 0,
                data: 'id',
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },

            {
                "className": "text-left",
                "data": "file_name",
                "render": function (data, type, row, meta) {
                    if (row.file_name.length > 0) {
                        var result = "เอกสารสำสำเนาบัตร " + row.pid;// + "-" +row.file_name.substring(0, row.file_name.length - 4);
                        return  result;
                    } else {
                        return  data;
                    }


                }
            },

            {"className": "text-center",
                "data": "create_date_thai",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="showOptionViewImage(\'' + row.order_no + '\', \'' + row.file_name + '\') "><i class="fa fa-file-image-o" style="color:green; cursor: pointer;"></i></a>&nbsp;&nbsp;\n\
                            <a onclick="cancelImage(\'' + row.id + '\', \'' + row.order_no + '\', \'' + row.file_name + '\') "style="font-size: 18px; cursor: pointer" target="_blank"><i class="fa fa-close" style="color:red;" aria-hidden="true"></i></a>';
                },
                "defaultContent": ''
            },
        ],
        rowReorder: true
                //"pageLength": false,
    });
}

function closeOptionPop1(tab) {
    if (tab === '1') {
        $('#mi-modal-promisepromise_pop').modal('hide');
        document.location.reload(true);
    } else {
        $('#mi-modal-promisepromise_pop2').modal('hide');
        document.location.reload(true);
    }


}


function get_data_person_plan_detail(promiseid, pid) {

    //console.log("get_data_person_plan promiseid: " + promiseid);
    //console.log("get_data_person_plan pid: " + pid);
    var count;
    $.ajax({
        url: 'count_person_plan',
        type: 'POST',
        data: {
            promise_id: promiseid
        },
        async: true,
        dataType: 'json',
        success: function (data) {
            //console.log("data chekbox" + data);
            count = data;
            //console.log("count person: " + count);

            //show detail person
            $.ajax({
                url: 'get_person_plan_detail',
                type: 'POST',
                data: {
                    promise_id: promiseid
                },
                async: true,
                dataType: 'json',
                success: function (data) {
                    //console.log("show detail person data: " + data);
                    if (count === 0) {
                        $('#list-detail-person1').show();
                        $('#list-detail-person2').hide();
                        $('#list-detail-person3').hide();
                        set_detail_person_emty();
                    } else if (count === 1) {
                        $('#list-detail-person1').show();
                        $('#list-detail-person2').hide();
                        $('#list-detail-person3').hide();
                        set_detail_person1(data);

                    } else if (count === 2) {
                        $('#list-detail-person1').show();
                        $('#list-detail-person2').show();
                        $('#list-detail-person3').hide();
                        set_detail_person1(data);
                        set_detail_person2(data);
                    } else if (count === 3) {
                        set_detail_person1(data);
                        set_detail_person2(data);
                        set_detail_person3(data);
                        $('#list-detail-person1').show();
                        $('#list-detail-person2').show();
                        $('#list-detail-person3').show();
                    }
                }
            });
        }
    });
}


function get_person_plan_list() {

    var promiseid = $('#hidden_promiseid').val();
    var plan_master = $('#hiden_person_master_plan').val();
    //console.log("get_person_plan promiseid: " + promiseid);

    $.ajax({
        'type': 'POST',
        'data': {
            promise_id: promiseid
        },
        async: true,
        'url': 'get_person_plan_list',
        'success': function (data) {
            //list table
            var count = 0;
            for (var i = 0; i < data.data.length; i++) {



                count++;
                var tr = '<tr>\n\
                        <td align="center" id="number"> ' + count + '</td>\n\
                        <td align="center" >' + data.data[i].pid + '</td>\n\
                        <td>' + data.data[i].title + data.data[i].fname + ' ' + data.data[i].lname + '</td>\n\
                        <td>' + data.data[i].subdistrict + ' ' + data.data[i].district + ' ' + data.data[i].province + '</td>';

                var statusScan = getPerson_Status_Scan(promiseid, data.data[i].pid, plan_master, '1', '1');
                console.log("statusScan: " + statusScan);
                if (statusScan == '1') {
                    tr += '<td align="center"><i class="fa fa-check" style="color:green;" aria-hidden="true"></i></td>';
                } else {
                    tr += '<td align="center"><i class="fa fa-close" style="color:red;" aria-hidden="true"></i></td>';
                }

                if (data.data[i].flag === 'Y') {
                    var checked = "checked =\"checked\"";
                    tr += '<td align="center"><input type="checkbox" name="check1[]" id="check1" ' + checked + ' onclick="ischeckbox(this, \'' + data.data[i].pid + '\')"></td>';
                } else {
                    tr += '<td align="center"><input type="checkbox" name="check1[]" id="check1" onclick="ischeckbox(this, \'' + data.data[i].pid + '\')"></td>';
                }

                tr += '<td align="center"><a style="cursor: pointer;" onclick="deleteperson(\'' + data.data[i].pid + '\')"><i class="fa fa-trash" style="color:green"></i></a></td>\n\
                       <td align="center"><a style="cursor: pointer;" onclick="scanDataTab1(\'' + data.data[i].pid + '\')"><span style="padding: 7px;" class="badge badge-success">สแกนสำเนาบัตร</span></a></td>\n\
                    </tr>';

                $("#id-tbody").append(tr);
            }
            get_data_person_plan_detail(promiseid, "");

        }
    });
}

function get_promiseid_by_agreement_id(agreement_id) {

    //console.log("##get_promiseid_by_agreement_id agreement_id: " + agreement_id);
    var agreementid = '';
    $.ajax({
        url: 'get_promise_id_by_agreement_id',
        type: 'POST',
        data: {
            agreement_id: agreement_id
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        if (data.data.length > 0) {
            //console.log(data.data);
            agreementid = data.data[0].promise_id;
        } else {
            //console.log(data.data);
            agreementid = 0;
        }

    });

    return agreementid;
}

function getScanSeq() {

    var seq = 0;
    $.ajax({
        url: 'get_scan_seq',
        type: 'GET',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        seq = data;
    });

    return seq;
}

function showOptionModalManual() {
    clearData();
    $('#myModal').modal('show');
}

function showOptionModalSmartCard() {
    readDataFromSmartCard();
    $('#myModal').modal('show');
}

function closeOptionModalAdd() {
    $('#myModal').modal('hide');
}

function readDataFromSmartCard() {

    var url = 'http://localhost:8444/getDataFromCardImage?name=Feitian SCR301 0';
    $.ajax({
        url: url,
    }).done(function (data) {
        //console.log("data.pid: " + data.pid);
        if (data.pid === null || data.pid === '' || data.pid === undefined) {
            $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> ไม่สามารถเชื่อมต่อเครื่องอ่าน smart card กรุณาตรวจสอบเครื่องอ่าน");
            $("#mi-modal-message").modal('show');
            clearData();
        } else {
            clearData();
            var srt = data.pid;
            var titleTh = data.titleTh;
            var firstnameTh = data.firstnameTh;
            var lastnameTh = data.lastnameTh;
            var age = data.age;
            var houseNumber = data.houseNumber;
            var group = data.group;
            var alley = data.alley;
            var street = data.street;
            var subDistrict = data.subDistrict;
            var district = data.district;
            var province = data.province;

            //$('#img').prop('src', 'data:image/jpg;base64,' + data.image);
            $('#poppid').val(srt);
            $('#poptitle').val(titleTh);
            $('#popfirstname').val(firstnameTh);
            $('#poplastname').val(lastnameTh);
            $('#popage').val(age);
            $('#pophouseNumber').val(houseNumber);
            $('#popalley').val(alley);
            $('#popgroup').val(group.substr(7, group.lenght));
            $('#popstreet').val(street);
            $("#popsubDistrict").val(subDistrict.substr(4, subDistrict.lenght));
            $("#popdistrict").val(district.substr(5, district.lenght));
            $("#popprovince").val(province.substr(7, province.lenght));
        }



    }).fail(function (data) {
        $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> ไม่สามารถเชื่อมต่อเครื่องอ่าน smart card กรุณาตรวจสอบเครื่องอ่าน");
        $("#mi-modal-message").modal('show');
        clearData();
    });
}

function clearData() {
    $('#poppid').val("");
    $('#poptitle').val("");
    $('#popfirstname').val("");
    $('#poplastname').val("");
    $('#popage').val("");
    $('#pophouseNumber').val("");
    $('#popalley').val("");
    $('#popgroup').val("");
    $('#popstreet').val("");
    $("#popsubDistrict").val("");
    $("#popdistrict").val("");
    $("#popprovince").val("");
    $("#poppostcode").val("");
    $("#popphone").val("");
    $("#popemail").val("");

}

function set_detail_person_emty() {
    $('#pid').val("");
    $('#title').val("");
    $('#firstname').val("");
    $('#lastname').val("");
    $('#age').val("");
    $('#houseNumber').val("");
    $('#group').val("");
    $('#alley').val("");//ซอย
    $('#street').val("");
    $('#subDistrict').val("");
    $('#district').val("");
    $('#province').val("");
    $('#postcode').val("");
    $('#phone').val("");
    $('#email').val("");
}

function set_detail_person1(data) {
    $('#pid').val(data.data[0].pid);
    $('#title').val(data.data[0].title);
    $('#firstname').val(data.data[0].fname);
    $('#lastname').val(data.data[0].lname);
    $('#age').val(data.data[0].age);
    $('#houseNumber').val(data.data[0].home_no);
    $('#group').val(data.data[0].moo);
    $('#alley').val(data.data[0].soi);//ซอย
    $('#street').val(data.data[0].street);
    $('#subDistrict').val(data.data[0].subdistrict);
    $('#district').val(data.data[0].district);
    $('#province').val(data.data[0].province);
    $('#postcode').val(data.data[0].postcode);
    $('#phone').val(data.data[0].phone);
    $('#email').val(data.data[0].email);
}

function set_detail_person2(data) {
    $('#pid2').val(data.data[1].pid);
    $('#title2').val(data.data[1].title);
    $('#firstname2').val(data.data[1].fname);
    $('#lastname2').val(data.data[1].lname);
    $('#age2').val(data.data[1].age);
    $('#houseNumber2').val(data.data[1].home_no);
    $('#group2').val(data.data[1].moo);
    $('#alley2').val(data.data[1].soi);//ซอย
    $('#street2').val(data.data[1].street);
    $('#subDistrict2').val(data.data[1].subdistrict);
    $('#district2').val(data.data[1].district);
    $('#province2').val(data.data[1].province);
    $('#postcode2').val(data.data[1].postcode);
    $('#phone2').val(data.data[1].phone);
    $('#email2').val(data.data[1].email);
}

function set_detail_person3(data) {
    $('#pid3').val(data.data[2].pid);
    $('#title3').val(data.data[2].title);
    $('#firstname3').val(data.data[2].fname);
    $('#lastname3').val(data.data[2].lname);
    $('#age3').val(data.data[2].age);
    $('#houseNumber3').val(data.data[1].home_no);
    $('#group3').val(data.data[2].moo);
    $('#alley3').val(data.data[2].soi);//ซอย
    $('#street3').val(data.data[2].street);
    $('#subDistrict3').val(data.data[2].subdistrict);
    $('#district3').val(data.data[2].district);
    $('#province3').val(data.data[2].province);
    $('#postcode3').val(data.data[2].postcode);
    $('#phone3').val(data.data[2].phone);
    $('#email3').val(data.data[2].email);
}

//============================================function TAB2==========================================
function ischeckboxtab2(cb, pid) {

    var isChecked = $(cb).prop('checked');
    //console.log("isChecked: " + isChecked);
    //console.log("pid: " + pid);
    var promiseid = $('#hidden_promiseid').val();
    var status;
    if (isChecked) {
        status = 'Y';
    } else {
        status = 'N';
    }

    $.ajax({
        url: 'update_person_home_flag',
        type: 'POST',
        data: {
            promiseid: promiseid,
            pid: pid,
            flag: status
        },
        async: true,
        dataType: 'json',
        success: function (data) {
            //console.log("data chekbox" + data);
            get_data_person_plan_detail_tab2(promiseid, pid);

        }
    });

}

function deletepersontab2(pid) {
    //console.log("##deletepersontab2 : " + pid);
    var promiseid = $('#hidden_promiseid').val();
    $.ajax({
        url: 'deplte_person_home',
        type: 'POST',
        data: {
            promiseid: promiseid,
            pid: pid
        },
        async: true,
        dataType: 'json',
        success: function (data) {
            //console.log("data chekbox" + data);
            $('#id-tbody-tab2 tr').remove();
            get_person_plan_list_tab2();
            get_data_person_plan_detail_tab2(promiseid, pid);

            document.location.reload(true);
        }
    });
}

function get_data_person_plan_detail_tab2(promiseid, pid) {

    //console.log("get_data_person_plan promiseid: " + promiseid);
    //console.log("get_data_person_plan pid: " + pid);
    var count;
    $.ajax({
        url: 'count_person_home',
        type: 'POST',
        data: {
            promise_id: promiseid
        },
        async: true,
        dataType: 'json',
        success: function (data) {
            //console.log("data chekbox" + data);
            count = data;
            //console.log("count person: " + count);

            //show detail person
            $.ajax({
                url: 'get_person_home_detail',
                type: 'POST',
                data: {
                    promise_id: promiseid
                },
                async: true,
                dataType: 'json',
                success: function (data) {
                    //console.log("show detail person data: " + data);
                    if (count === 0) {
                        $('#list-detail-person_home1').show();
                        $('#list-detail-person_home2').hide();
                        $('#list-detail-person_home3').hide();
                        set_detail_person_emty_tab2();
                    } else if (count === 1) {
                        $('#list-detail-person_home1').show();
                        $('#list-detail-person_home2').hide();
                        $('#list-detail-person_home3').hide();
                        set_detail_person_home1(data);

                    } else if (count === 2) {
                        $('#list-detail-person_home1').show();
                        $('#list-detail-person_home2').show();
                        $('#list-detail-person_home3').hide();
                        set_detail_person_home1(data);
                        set_detail_person_home2(data);
                    } else if (count === 3) {
                        set_detail_person_home1(data);
                        set_detail_person_home2(data);
                        set_detail_person_home3(data);
                        $('#list-detail-person_home1').show();
                        $('#list-detail-person_home2').show();
                        $('#list-detail-person_home3').show();
                    }
                }
            });
        }
    });
}


function get_person_plan_list_tab2() {

    var promiseid = $('#hidden_promiseid').val();
    var plan_master = $('#hiden_person_master_plan').val();
    //console.log("get_person_plan_list_tab2 promiseid: " + promiseid);

    $.ajax({
        'type': 'POST',
        'data': {
            promise_id: promiseid
        },
        async: true,
        'url': 'get_person_home_list',
        'success': function (data) {
            //console.log("data get_person_home lenght: " + data.data.length);

            //list table
            var count = 0;
            for (var i = 0; i < data.data.length; i++) {
                count++;
                var tr = '<tr>\n\
                        <td align="center" id="number"> ' + count + '</td>\n\
                        <td align="center" >' + data.data[i].pid + '</td>\n\
                        <td>' + data.data[i].title + data.data[i].fname + ' ' + data.data[i].lname + '</td>\n\
                        <td>' + data.data[i].subdistrict + ' ' + data.data[i].district + ' ' + data.data[i].province + '</td>';


                var statusScan = getPerson_Status_Scan(promiseid, data.data[i].pid, plan_master, '1', '2');
                console.log("statusScan2: " + statusScan);
                if (statusScan == '1') {
                    tr += '<td align="center"><i class="fa fa-check" style="color:green;" aria-hidden="true"></i></td>';
                } else {
                    tr += '<td align="center"><i class="fa fa-close" style="color:red;" aria-hidden="true"></i></td>';
                }
                if (data.data[i].flag === 'Y') {
                    var checked = "checked =\"checked\"";
                    tr += '<td align="center"><input type="checkbox" name="check2[]" id="check2" ' + checked + ' onclick="ischeckboxtab2(this, \'' + data.data[i].pid + '\')"></td>';
                } else {
                    tr += '<td align="center"><input type="checkbox" name="check2[]" id="check2" onclick="ischeckboxtab2(this, \'' + data.data[i].pid + '\')"></td>';
                }

//                tr += '<td align="center"><a style="cursor: pointer;" onclick="deletepersontab2(\'' + data.data[i].pid + '\')"><i class="fa fa-trash" style="color:green"></i></a></td>\n\
//                      <td align="center"><a style="cursor: pointer;" onclick="scanDataTab2(this,\'' + data.data[i].pid + '\')" ><span style="padding: 7px;" class="badge badge-success">สแกนสำเนาบัตร</span></a></td>\n\
//                    </tr>';

                $("#id-tbody-tab2").append(tr);
            }
            get_data_person_plan_detail_tab2(promiseid, "");

        }
    });
}

function showOptionModalManualTab2() {
    clearDataTab2();
    $('#myModal-tab2').modal('show');
}
function showOptionModalSmartCardTab2() {
    readDataFromSmartCardTab2();
    $('#myModal-tab2').modal('show');
}
function closeOptionModalAddTab2() {
    $('#myModal-tab2').modal('hide');
}

function readDataFromSmartCardTab2() {

    var url = 'http://localhost:8444/getDataFromCardImage?name=Feitian SCR301 0';
    $.ajax({
        url: url,
    }).done(function (data) {
        //alert(data.pid);

        if (data.pid === null || data.pid === '' || data.pid === undefined) {
            $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> ไม่สามารถเชื่อมต่อเครื่องอ่าน smart card กรุณาตรวจสอบเครื่องอ่าน");
            $("#mi-modal-message-tab2").modal('show');
            clearDataTab2();
        } else {
            clearDataTab2();
            var srt = data.pid;
            var titleTh = data.titleTh;
            var firstnameTh = data.firstnameTh;
            var lastnameTh = data.lastnameTh;
            var age = data.age;
            var houseNumber = data.houseNumber;
            var group = data.group;
            var alley = data.alley;
            var street = data.street;
            var subDistrict = data.subDistrict;
            var district = data.district;
            var province = data.province;
            var issuedDate = data.issuedDate;
            var expiredDate = data.expiredDate;

            //$('#img').prop('src', 'data:image/jpg;base64,' + data.image);
            $('#poppidtab2').val(srt);
            $('#poptitletab2').val(titleTh);
            $('#popfirstnametab2').val(firstnameTh);
            $('#poplastnametab2').val(lastnameTh);
            $('#popagetab2').val(age);
            $('#pophouseNumbertab2').val(houseNumber);
            $('#popalleytab2').val(alley);
            $('#popgrouptab2').val(group.substr(7, group.lenght));
            $('#popstreettab2').val(street);
            $("#popsubDistricttab2").val(subDistrict.substr(4, subDistrict.lenght));
            $("#popdistricttab2").val(district.substr(5, district.lenght));
            $("#popprovincetab2").val(province.substr(7, province.lenght));
            $('#popissuedDate').val(issuedDate);
            $('#popexpiredDate').val(expiredDate);
        }
    }).fail(function (data) {
        $("#idmessage-tab2").html("<i class=\"fa fa-exclamation-circle\"></i> ไม่สามารถเชื่อมต่อเครื่องอ่าน smart card กรุณาตรวจสอบเครื่องอ่าน");
        $("#mi-modal-message-tab2").modal('show');
        clearDataTab2();
    });
}

function clearDataTab2() {
    $('#poppidtab2').val("");
    $('#poptitletab2').val("");
    $('#popfirstnametab2').val("");
    $('#poplastnametab2').val("");
    $('#popagetab2').val("");
    $('#pophouseNumbertab2').val("");
    $('#popalleytab2').val("");
    $('#popgrouptab2').val("");
    $('#popstreettab2').val("");
    $("#popsubDistricttab2").val("");
    $("#popdistricttab2").val("");
    $("#popprovincetab2").val("");
    $("#poppostcodetab2").val("");
    $("#poppostcodetab2").val("");
    $("#popphonetab2").val("");
    $("#popemailtab2").val("");


}

function set_detail_person_emty_tab2() {
    $('#pid4').val("");
    $('#title4').val("");
    $('#firstname4').val("");
    $('#lastname4').val("");
    $('#age4').val("");
    $('#houseNumber4').val("");
    $('#group4').val("");
    $('#alley4').val("");//ซอย
    $('#street4').val("");
    $('#subDistrict4').val("");
    $('#district4').val("");
    $('#province4').val("");
    $('#postcode4').val("");
    $('#phone4').val("");
    $('#email4').val("");
}

function set_detail_person_home1(data) {
    $('#pid4').val(data.data[0].pid);
    $('#title4').val(data.data[0].title);
    $('#firstname4').val(data.data[0].fname);
    $('#lastname4').val(data.data[0].lname);
    $('#age4').val(data.data[0].age);
    $('#houseNumber4').val(data.data[0].home_no);
    $('#group4').val(data.data[0].moo);
    $('#alley4').val(data.data[0].soi);//ซอย
    $('#street4').val(data.data[0].street);
    $('#subDistrict4').val(data.data[0].subdistrict);
    $('#district4').val(data.data[0].district);
    $('#province4').val(data.data[0].province);
    $('#postcode4').val(data.data[0].postcode);
    $('#phone4').val(data.data[0].phone);
    $('#email4').val(data.data[0].email);
}

function set_detail_person_home2(data) {
    $('#pid5').val(data.data[1].pid);
    $('#title5').val(data.data[1].title);
    $('#firstname5').val(data.data[1].fname);
    $('#lastname5').val(data.data[1].lname);
    $('#age5').val(data.data[1].age);
    $('#houseNumber5').val(data.data[1].home_no);
    $('#group5').val(data.data[1].moo);
    $('#alley5').val(data.data[1].soi);//ซอย
    $('#street5').val(data.data[1].street);
    $('#subDistrict5').val(data.data[1].subdistrict);
    $('#district5').val(data.data[1].district);
    $('#province5').val(data.data[1].province);
    $('#postcode5').val(data.data[1].postcode);
    $('#phone5').val(data.data[1].phone);
    $('#email5').val(data.data[1].email);
}

function set_detail_person_home3(data) {
    $('#pid6').val(data.data[2].pid);
    $('#title6').val(data.data[2].title);
    $('#firstname6').val(data.data[2].fname);
    $('#lastname6').val(data.data[2].lname);
    $('#age6').val(data.data[2].age);
    $('#houseNumber6').val(data.data[1].home_no);
    $('#group6').val(data.data[2].moo);
    $('#alley6').val(data.data[2].soi);//ซอย
    $('#street6').val(data.data[2].street);
    $('#subDistrict6').val(data.data[2].subdistrict);
    $('#district6').val(data.data[2].district);
    $('#province6').val(data.data[2].province);
    $('#postcode6').val(data.data[2].postcode);
    $('#phone6').val(data.data[2].phone);
    $('#email6').val(data.data[2].email);
}

function get_home_promiseid_by_agreement_id(agreement_id) {

    //console.log("##get_home_promiseid_by_agreement_id agreement_id: " + agreement_id);
    var agreementid = '';
    $.ajax({
        url: 'get_home_promise_id_by_agreement_id',
        type: 'POST',
        data: {
            agreement_id: agreement_id
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        if (data.data.length > 0) {
            //console.log(data.data);
            agreementid = data.data[0].promise_id;
        } else {
            //console.log(data.data);
            agreementid = 0;
        }

    });

    return agreementid;
}

// ==========================================COMMON=========================================
function getSeqPromise() {

    var seq = 0;
    $.ajax({
        url: 'get_promise_seq',
        type: 'GET',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        seq = data;
    });

    return seq;
}

function pad(str, max) {
    str = str.toString().trim();
    return str.length < max ? pad("0" + str, max) : str;
}

function getDataAgreement(oderno, masterplan) {

    console.log("##getDataAgreement");
    var land_empty_price = $('#check_land_price').val().replace(/,/g, "");
    var orders = [];
    $.ajax({
        'type': 'POST',
        'data': {

            oder_no: oderno,
            master_plan: masterplan
        },
        async: true,
        'url': 'get_data_agreement_by_order_no',
        'success': function (data) {
            //console.log("data: " + data);


            var date = data.date_agreenment_thai;
            var new_date = date.replace(/\//g, '');
            var day = new_date.substr(0, 2);
            var month = new_date.substr(2, 2);
            var year = new_date.substr(4, 4);
            var srt_month = getMonth(month);
            var rs_month = day + " " + srt_month + " " + year;
            var date_agreenment = data.date_agreenment;


            //tabplan
            console.log("##data.plan_price: " + data.plan_price);
            $("#planprice").val(data.price_tarangwa);
            $("#plan_total").val(data.plan_total);
            $("#plan_price").val(data.plan_price);
            $("#plan_price_tab2").val(data.plan_price);
            var price_tatal = parseFloat(data.price_total.replace(/,/g, ""));
            var total_price = parseFloat(price_tatal) + parseFloat(land_empty_price);
            $("#pricetotal").val(formatMoney(total_price));

            //$("#pricetotal").val(data.price_total);

            $("#total_price_char").html("(" + BAHTTEXT(total_price) + ")");
            $("#pricepay").val(formatMoney(parseInt(data.price_pay.replace(/,/g, "")) / 2));
            $("#order_price_char").html("(" + ThaiBaht(parseInt(data.price_pay.replace(/,/g, "")) / 2) + ")");
            $("#home_name").val(data.home_name);
            $("#home_id").val(data.home_id);
            $("#home_price").val(formatMoney(data.home_price));
            $("#home_price_char").html("(" + ThaiBaht(data.home_price) + ")");
            $('#dateorder').val(rs_month);
            $('#date_agreenment').val(date_agreenment);

            var date_create_thai = data.date_create_thai;
            var C_new_date = date_create_thai.replace(/\//g, '');
            var C_day = C_new_date.substr(0, 2);
            var C_month = C_new_date.substr(2, 2);
            var C_year = C_new_date.substr(4, 4);
            var C_srt_month = getMonth(C_month);
            var C_rs_month = C_day + " " + C_srt_month + " " + C_year;
            $('#date_create_agreenment').val(C_rs_month);
            $('#date_create_agreenment2').val(C_rs_month);

            //settab แบบบ้าน
            $("#planprice2").val(data.price_tarangwa);
            $("#plan_total2").val(data.plan_total);
            $("#pricetotal2").val(formatMoney(total_price));
            //$("#pricetotal2").val(data.price_total);
            $("#total_price_char2").html("(" + BAHTTEXT(data.price_total) + ")");
            $("#pricepay2").val(formatMoney(parseInt(data.price_pay.replace(/,/g, "")) / 2));
            $("#order_price_char2").html("(" + ThaiBaht(parseInt(data.price_pay.replace(/,/g, "")) / 2) + ")");
            $("#home_name2").val(data.home_name);
            $("#home_id2").val(data.home_id);
            $("#home_price2").val(formatMoney(data.home_price));
            $("#home_price_char2").html("(" + ThaiBaht(data.home_price) + ")");
            $('#dateorder2').val(rs_month);

            //การชำระเงินและการจดทะเบียนโอนกรรมสิทธิ์
            $('#detail_price_promise_char').html(formatMoney(total_price) + " (" + ThaiBaht(total_price) + ")");

            orders = get_order_by_orderno(oderno);
            var cnew_date = orders[0].order_date.replace(/\//g, '');
            //console.log("cnew_date: " + cnew_date);
            var cday = cnew_date.substr(0, 2);
            var cmonth = cnew_date.substr(2, 2);
            var cyear = cnew_date.substr(4, 4);
            var csrt_month = getMonth(cmonth);
            var crs_month = cday + " " + csrt_month + " " + (parseInt(cyear) + 543);

            $('#db_order_date').val(crs_month);
            $('#db_order_date2').val(crs_month);


            $('#db_hidden_date_create_agreenment').val(data.create_date);
            $('#db_order_date_hidden').val(orders[0].order_date_hidden);
            $('#db_agerrment_date_hidden').val(date_agreenment);

            $('#db_hidden_date_create_agreenment2').val(data.create_date);
            $('#db_order_date_hidden2').val(orders[0].order_date_hidden);
            $('#db_agerrment_date_hidden2').val(date_agreenment);


            //console.log("db_order_date_hidden: " + $('#db_order_date_hidden').val());
        }
    });
}

function getPriceAgreement(oderno, masterplan) {
    var output = '';
    var land_empty_price = $('#check_land_price').val().replace(/,/g, "");
    $.ajax({
        'type': 'POST',
        'data': {

            oder_no: oderno,
            master_plan: masterplan
        },
        async: false,
        'url': 'get_data_agreement_by_order_no',
    }).done(function (data) {
        var price_tatal = parseFloat(data.price_total.replace(/,/g, ""));
        var total_price = parseFloat(price_tatal) + parseFloat(land_empty_price);

        output = total_price;
    });
    return output;
}

function getPriceHome(oderno, masterplan) {
    var output = '';
    var land_empty_price = $('#check_land_price').val().replace(/,/g, "");
    $.ajax({
        'type': 'POST',
        'data': {

            oder_no: oderno,
            master_plan: masterplan
        },
        async: false,
        'url': 'get_data_agreement_by_order_no',
    }).done(function (data) {

        output = data.home_price;
    });
    return output;
}

function ThaiBaht(Number) {
    var Number = CheckNumber(Number);
    //console.log("Number: " + Number);
    var NumberArray = new Array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ");
    var DigitArray = new Array("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน", "สิบล้าน", "ร้อยล้าน");
    var BahtText = "";
    if (isNaN(Number))
    {
        return "ข้อมูลนำเข้าไม่ถูกต้อง";
    } else
    {
        if ((Number - 0) > 999999999.9999)
        {
            return "ข้อมูลนำเข้าเกินขอบเขตที่ตั้งไว้";
        } else
        {
            Number = Number.split(".");
            if (Number[1].length > 0)
            {
                Number[1] = Number[1].substring(0, 2);
            }
            var NumberLen = Number[0].length - 0;
            for (var i = 0; i < NumberLen; i++)
            {
                var tmp = Number[0].substring(i, i + 1) - 0;
                if (tmp != 0)
                {
                    if ((i == (NumberLen - 1)) && (tmp == 1)) {
                        if (NumberLen > 1) {
                            BahtText += "เอ็ด";
                        } else {
                            BahtText += "หนึ่ง";
                        }

                    } else
                    if ((i == (NumberLen - 2)) && (tmp == 2)) {
                        BahtText += "ยี่";
                    } else
                    if ((i == (NumberLen - 2)) && (tmp == 1)) {
                        BahtText += "";
                    } else {
                        BahtText += NumberArray[tmp];
                    }
                    BahtText += DigitArray[NumberLen - i - 1];
                }
            }
            BahtText += "บาท";
            if ((Number[1] == "0") || (Number[1] == "00"))
            {
                BahtText += "ถ้วน";
            } else
            {
                DecimalLen = Number[1].length - 0;
                for (var i = 0; i < DecimalLen; i++)
                {
                    var tmp = Number[1].substring(i, i + 1) - 0;
                    if (tmp != 0)
                    {
                        if ((i == (DecimalLen - 1)) && (tmp == 1))
                        {
                            BahtText += "เอ็ด";
                        } else
                        if ((i == (DecimalLen - 2)) && (tmp == 2))
                        {
                            BahtText += "ยี่";
                        } else
                        if ((i == (DecimalLen - 2)) && (tmp == 1))
                        {
                            BahtText += "";
                        } else
                        {
                            BahtText += NumberArray[tmp];
                        }
                        BahtText += DigitArray[DecimalLen - i - 1];
                    }
                }
                BahtText += "สตางค์";
            }
            return BahtText;
        }
    }
}

function CheckNumber(Number) {
    var decimal = false;
    Number = Number.toString();
    Number = Number.replace(/ |,|บาท|฿/gi, '');
    for (var i = 0; i < Number.length; i++)
    {
        if (Number[i] == '.') {
            decimal = true;
        }
    }
    if (decimal == false) {
        Number = Number + '.00';
    }
    return Number
}

function getMonth(month) {
    var monthNames = ["ม.ค.", "ก.พ", "มี.ค", "เม.ย", "พ.ค", "มิ.ย",
        "ก.ค", "ส.ค", "ก.ย", "ต.ค", "พ.ย", "ธ.ค"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function numberOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
}

function floatOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9-.]/gi;
    element.value = element.value.replace(regex, "");
}



//fnction แสดงตัวอย่างที่ดิน
function get_data_view_plan(promiseid) {
    console.log("get_data_view_plan: " + promiseid);
    var count;
    $.ajax({
        url: 'count_person_plan',
        type: 'POST',
        data: {
            promise_id: promiseid
        },
        'success': function (data) {
            count = data;
            //console.log("get_data_view_plan count: " + data);

            $.ajax({
                'type': 'POST',
                'data': {
                    promise_id: promiseid
                },
                async: true,
                'url': 'get_person_plan_view',
                'success': function (data) {
                    if (count === 1) {

                        console.log("count 1 fullname: " + data.data[0].title + data.data[0].fname + " " + data.data[0].lname);

                        $('#view_promise_date_plan').html($('#hidden_date_due').val());
                        $('#view_fullname').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname);
                        $('#view_homeno').html(data.data[0].home_no);
                        $('#view_street').html(data.data[0].street);
                        $('#view_subdistrict').html(data.data[0].subdistrict);
                        $('#view_district').html(data.data[0].district);
                        $('#view_province').html(data.data[0].province);
                        $('#view_postcode').html(data.data[0].postcode);
                        $('#view_phone').html(data.data[0].phone);

                        //plan
                        $('#view_total_plan').html($('#plan_total').val());
                        $('#view_plan_number').html($('#hidden_planssave').val());
                        $('#view_land_no').html($('#landid').val());
                        var order_no = $('#hiden_person_order_no').val();
                        var master_plan = $('#hiden_person_master_plan').val();
                        var planid = $('#hidden_planssave').val();
                        getDeatilplan(order_no, planid, master_plan);

                        //price
                        $('#view_pricetotal').html($('#pricetotal').val());
                        $('#view_pricetotal_char').html(BAHTTEXT($('#pricetotal').val()));
                        $('#view_pricepay').html($('#pricepay').val());
                        $('#view_pricepay_char').html(ThaiBaht($('#pricepay').val()));
                        $('#view_pay_price_promise').html($('#pay_price_promise').val());
                        $('#view_pay_price_promise_char').html(ThaiBaht($('#pay_price_promise').val()));
                        $('#view_pay_price_promise_result').html($('#pay_price_promise_result').val());
                        $('#view_pay_price_promise_result_char').html(ThaiBaht($('#pay_price_promise_result').val()));



                    } else if (count === 2) {


                        $('#view_promise_date_plan').html($('#hidden_date_due').val());
                        $('#view_fullname').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname + " และ " + data.data[1].title + data.data[1].fname + " " + data.data[1].lname);
                        $('#view_homeno').html(data.data[0].home_no);
                        $('#view_street').html(data.data[0].street);
                        $('#view_subdistrict').html(data.data[0].subdistrict);
                        $('#view_district').html(data.data[0].district);
                        $('#view_province').html(data.data[0].province);
                        $('#view_postcode').html(data.data[0].postcode);
                        $('#view_phone').html(data.data[0].phone);

                        $('#view_homeno2').html(data.data[1].home_no);
                        $('#view_street2').html(data.data[1].street);
                        $('#view_subdistrict2').html(data.data[1].subdistrict);
                        $('#view_district2').html(data.data[1].district);
                        $('#view_province2').html(data.data[1].province);
                        $('#view_postcode2').html(data.data[1].postcode);
                        $('#view_phone2').html(data.data[1].phone);

                        //plan
                        $('#view_total_plan').html($('#plan_total').val());
                        $('#view_plan_number').html($('#hidden_planssave').val());
                        $('#view_land_no').html($('#landid').val());

                        var order_no = $('#hiden_person_order_no').val();
                        var master_plan = $('#hiden_person_master_plan').val();
                        var planid = $('#hidden_planssave').val();
                        getDeatilplan(order_no, planid, master_plan);

                        //price
                        $('#view_pricetotal').html($('#pricetotal').val());
                        $('#view_pricetotal_char').html(BAHTTEXT($('#pricetotal').val()));
                        $('#view_pricepay').html($('#pricepay').val());
                        $('#view_pricepay_char').html(ThaiBaht($('#pricepay').val()));
                        $('#view_pay_price_promise').html($('#pay_price_promise').val());
                        $('#view_pay_price_promise_char').html(ThaiBaht($('#pay_price_promise').val()));
                        $('#view_pay_price_promise_result').html($('#pay_price_promise_result').val());
                        $('#view_pay_price_promise_result_char').html(ThaiBaht($('#pay_price_promise_result').val()));



                        $('#view_homeno2_text').show();
                        $('#view_street2_text').show();
                        $('#view_subdistrict2_text').show();
                        $('#view_district2_text').show();
                        $('#view_province2_text').show();
                        $('#view_postcode2_text').show();
                        $('#view_phone2_text').show();

                        $('#view_homeno2').show();
                        $('#view_street2').show();
                        $('#view_subdistrict2').show();
                        $('#view_district2').show();
                        $('#view_province2').show();
                        $('#view_postcode2').show();
                        $('#view_phone2').show();



                    } else if (count === 3) {

                        $('#view_promise_date_plan').html($('#hidden_date_due').val());
                        $('#view_fullname').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname + " , " + data.data[1].title + data.data[1].fname + " " + data.data[1].lname + " และ " + data.data[2].title + data.data[2].fname + " " + data.data[2].lname);
                        $('#view_homeno').html(data.data[0].home_no);
                        $('#view_street').html(data.data[0].street);
                        $('#view_subdistrict').html(data.data[0].subdistrict);
                        $('#view_district').html(data.data[0].district);
                        $('#view_province').html(data.data[0].province);
                        $('#view_postcode').html(data.data[0].postcode);
                        $('#view_phone').html(data.data[0].phone);

                        $('#view_homeno2').html(data.data[1].home_no);
                        $('#view_street2').html(data.data[1].street);
                        $('#view_subdistrict2').html(data.data[1].subdistrict);
                        $('#view_district2').html(data.data[1].district);
                        $('#view_province2').html(data.data[1].province);
                        $('#view_postcode2').html(data.data[1].postcode);
                        $('#view_phone2').html(data.data[1].phone);

                        $('#view_homeno3').html(data.data[2].home_no);
                        $('#view_street3').html(data.data[2].street);
                        $('#view_subdistrict3').html(data.data[2].subdistrict);
                        $('#view_district3').html(data.data[2].district);
                        $('#view_province3').html(data.data[2].province);
                        $('#view_postcode3').html(data.data[2].postcode);
                        $('#view_phone3').html(data.data[2].phone);

                        //plan
                        $('#view_total_plan').html($('#plan_total').val());
                        $('#view_plan_number').html($('#hidden_planssave').val());
                        $('#view_land_no').html($('#landid').val());
                        var order_no = $('#hiden_person_order_no').val();
                        var master_plan = $('#hiden_person_master_plan').val();
                        var planid = $('#hidden_planssave').val();
                        getDeatilplan(order_no, planid, master_plan);

                        //price
                        $('#view_pricetotal').html($('#pricetotal').val());
                        $('#view_pricetotal_char').html(BAHTTEXT($('#pricetotal').val()));
                        $('#view_pricepay').html($('#pricepay').val());
                        $('#view_pricepay_char').html(ThaiBaht($('#pricepay').val()));
                        $('#view_pay_price_promise').html($('#pay_price_promise').val());
                        $('#view_pay_price_promise_char').html(ThaiBaht($('#pay_price_promise').val()));
                        $('#view_pay_price_promise_result').html($('#pay_price_promise_result').val());
                        $('#view_pay_price_promise_result_char').html(ThaiBaht($('#pay_price_promise_result').val()));



                        $('#view_homeno2_text').show();
                        $('#view_street2_text').show();
                        $('#view_subdistrict2_text').show();
                        $('#view_district2_text').show();
                        $('#view_province2_text').show();
                        $('#view_postcode2_text').show();
                        $('#view_phone2_text').show();

                        $('#view_homeno2').show();
                        $('#view_street2').show();
                        $('#view_subdistrict2').show();
                        $('#view_district2').show();
                        $('#view_province2').show();
                        $('#view_postcode2').show();
                        $('#view_phone2').show();

                        $('#view_homeno3_text').show();
                        $('#view_street3_text').show();
                        $('#view_subdistrict3_text').show();
                        $('#view_district3_text').show();
                        $('#view_province3_text').show();
                        $('#view_postcode3_text').show();
                        $('#view_phone3_text').show();


                        $('#view_homeno3').show();
                        $('#view_street3').show();
                        $('#view_subdistrict3').show();
                        $('#view_district3').show();
                        $('#view_province3').show();
                        $('#view_postcode3').show();
                        $('#view_phone3').show();
                    }



                    get_data_view_home(promiseid);



                }
            });
        }
    });

}

function getDeatilplan(orderno, planid, master_plan) {

    var ri = '';
    var ngan = '';
    var wa = '';
    var price_tarngwa = '';
    var view_tarangwa_char = '';
    var tarngwaArray = [];
    var tarngwaArray1 = [];
    var tarngwaArray2 = [];
    var planarr = planid.split(',');
    //console.log("getDeatilplan orderno: " + orderno);
    //console.log("getDeatilplan planid: " + planid);
    //console.log("getDeatilplan master_plan: " + master_plan);
    for (var i = 0; i < planarr.length; i++) {
        $.ajax({
            url: 'getorder_by_plan',
            type: 'POST',
            data: {
                orderno: orderno,
                planid: planarr[i].trim(),
                master_plan: master_plan
            },
            async: false,
            dataType: 'json',
            'success': function (data) {

                ri = data.data[0].ri;
                ngan = data.data[0].ngan;
                wa = data.data[0].wa;
                var totaltatrnagwa = sumtarangwa(ri, ngan, wa);

                price_tarngwa = data.data[0].price_tarangwa;
                view_tarangwa_char = ThaiBaht(data.data[0].price_tarangwa);
                tarngwaArray.push(totaltatrnagwa);
                tarngwaArray1.push(price_tarngwa);
                tarngwaArray2.push(view_tarangwa_char);
            }
        });
    }

    var t1 = '';
    var t2 = '';
    var t3 = '';
    for (var i = 0; i < tarngwaArray.length; i++) {
        t1 += tarngwaArray[i] + ",";
    }

    for (var i = 0; i < tarngwaArray1.length; i++) {
        t2 += tarngwaArray1[i] + ",";
    }
    for (var i = 0; i < tarngwaArray2.length; i++) {
        t3 += "(" + tarngwaArray2[i] + ")" + ",";
    }

    $('#view_tarangwa').html(removeLastComma(t1));

    $('#view_home_tarangwa').html(removeLastComma(t1) + tarngwaArray.length > 1 ? " (ตามลำดับ) " : "");

    $('#view_price_tarangwa').html(removeLastComma(t2));
    $('#view_tarangwa_char').html(removeLastComma(t3));

}

function sumtarangwa(ri, ngan, wa) {

    var r = 400;
    var ng = 100;
    var total;
    if (ri === '0') {

        var _ngan = parseInt(ngan === '' ? 0 : ngan);
        var _wa = parseInt(wa);
        total = (ng * _ngan) + _wa;
    }
    if (ri === '1') {
        var _ri = parseInt(ri === '' ? 0 : ri);
        var _ngan = parseInt(ngan === '' ? 0 : ngan);
        var _wa = parseInt(wa);
        total = ((_ri * ri) + (_ngan * ng)) + _wa;
    }
    if (ri === '2') {
        var _ri = parseInt(ri === '' ? 0 : ri);
        var _ngan = parseInt(ngan === '' ? 0 : ngan);
        var _wa = parseInt(wa);
        total = ((_ri * ri) + (_ngan * ng)) + _wa;
    }

    return total;
}
function removeLastComma(strng) {
    var n = strng.lastIndexOf(",");
    var a = strng.substring(0, n)
    return a;
}
//fnction แสดงตัวอย่างบ้าน
function get_data_view_home(promiseid) {
    var count;
    $.ajax({
        url: 'count_person_home',
        type: 'POST',
        data: {
            promise_id: promiseid
        },
        'success': function (data) {
            count = data;
            //console.log("get_data_view_home count: " + data);

            $.ajax({
                'type': 'POST',
                'data': {
                    promise_id: promiseid
                },
                async: true,
                'url': 'get_person_home_view',
                'success': function (data) {
                    if (count === 1) {
                        $('#cardcen1').html("(" + data.data[0].title + data.data[0].fname + " " + data.data[0].lname + ")");
                        $('#view_fullname_home').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname);
                        $('#view_pid_home').html(data.data[0].pid);

                        var exdate = data.data[0].expiredDate;
                        setExDate(exdate);
                        $('#view_expridate_home').html(setExDate(exdate));
                        $('#view_address_home').html("บ้านเลขที่ " + data.data[0].home_no + " หมู่ " + data.data[0].moo + " ตำบล " + data.data[0].subdistrict + " อำเภอ " + data.data[0].district + " จังหวัด " + data.data[0].province + " รหัสไปรษณีษ์ " + data.data[0].postcode);
                        $('#view_phone_home').html("หมายเลขโทรศัพท์ " + data.data[0].phone);
                        $('#view_home_plan_number').html($('#plan2').val());
                        $('#view_home_plan_number2').html($('#plan2').val());
                        $('#view_home_land_no').html($('#landid2').val());
                        $('#view_home_land_no2').html($('#landid3').val());

                 
                        var _inprice_conner = 0;
                        var _inprice_float = 0;
                        var result = 0;
                        var _vprive_conner = $("#price_reduct").val();
                        var _vprive = $("#home_price2").val().replace(/,/g, "");

                        _inprice_conner = parseFloat(_vprive_conner);
                        _inprice_float = parseFloat(_vprive);
                        result = (_inprice_float - _inprice_conner);
                        $('#view_home_price').html(formatMoney(result));
                        $('#view_home_price_char').html(ThaiBaht(result));
                        //("#home_price2").val(formatMoney(result));
                        //console.log("result: " + result);
                        //$('#view_home_price').html($('#home_price2').val());
                        //$('#view_home_price_char').html(ThaiBaht($('#home_price2').val()));

                        ////console.log("xxxxz: " + $('#pay_price_home').val());
                        $('#view_pay_price_home').html($('#pay_price_home').val());
                        $('#view_pay_price_home_char').html(ThaiBaht($('#pay_price_home').val()));
                        $('#view_pay_price_pirod1').html($('#pay_price_pirod1').val());
                        $('#view_pay_price_pirod1_char').html(ThaiBaht($('#pay_price_pirod1').val()));
                        $('#view_pay_price_pirod2').html($('#pay_price_pirod2').val());
                        $('#view_pay_price_pirod2_char').html(ThaiBaht($('#pay_price_pirod2').val()));
                        $('#view_pay_price_pirod3').html($('#pay_price_pirod3').val());
                        $('#view_pay_price_pirod3_char').html(ThaiBaht($('#pay_price_pirod3').val()));

                    } else if (count === 2) {

                        $('#cardcen1').html("(" + data.data[0].title + data.data[0].fname + " " + data.data[0].lname);
                        $('#cardcen2').html("และ" + data.data[1].title + data.data[1].fname + " " + data.data[1].lname + ")");
                        $('#view_fullname_home').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname + " , " + data.data[1].title + data.data[1].fname + " " + data.data[1].lname + " ตามลำดับ ");

                        $('#view_pid_home').html(data.data[0].pid + " , " + data.data[1].pid + " ตามลำดับ ");

                        $('#view_expridate_home').html(setExDate(data.data[0].expiredDate) + " , " + setExDate(data.data[1].expiredDate) + " ตามลำดับ ");
                        $('#view_address_home').html("บ้านเลขที่ " + data.data[0].home_no + " หมู่ " + data.data[0].moo + " ตำบล " + data.data[0].subdistrict + " อำเภอ " + data.data[0].district + " จังหวัด " + data.data[0].province + " รหัสไปรษณีษ์ " + data.data[0].postcode
                                + ", " +
                                "บ้านเลขที่ " + data.data[1].home_no + " หมู่ " + data.data[1].moo + " ตำบล " + data.data[1].subdistrict + " อำเภอ " + data.data[1].district + " จังหวัด " + data.data[1].province + " รหัสไปรษณีษ์ " + data.data[1].postcode + " ตามลำดับ ");
                        $('#view_phone_home').html("หมายเลขโทรศัพท์ " + data.data[0].phone + " , " + data.data[1].phone + "");
                        $('#view_home_plan_number').html($('#plan2').val());
                        $('#view_home_plan_number2').html($('#plan2').val());
                        $('#view_home_land_no').html($('#landid2').val());
                        $('#view_home_land_no2').html($('#landid3').val());
                        $('#view_home_price').html($('#home_price2').val());
                        $('#view_home_price_char').html(ThaiBaht($('#home_price2').val()));

                        $('#view_pay_price_home').html($('#pay_price_home').val());
                        $('#view_pay_price_home_char').html(ThaiBaht($('#pay_price_home').val()));
                        $('#view_pay_price_pirod1').html($('#pay_price_pirod1').val());
                        $('#view_pay_price_pirod1_char').html(ThaiBaht($('#pay_price_pirod1').val()));
                        $('#view_pay_price_pirod2').html($('#pay_price_pirod2').val());
                        $('#view_pay_price_pirod2_char').html(ThaiBaht($('#pay_price_pirod2').val()));
                        $('#view_pay_price_pirod3').html($('#pay_price_pirod3').val());
                        $('#view_pay_price_pirod3_char').html(ThaiBaht($('#pay_price_pirod3').val()));

                    } else if (count === 3) {
                        $('#view_fullname_home').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname + " , " + data.data[1].title + data.data[1].fname + " " + data.data[1].lname + " , " + data.data[2].title + data.data[2].fname + " " + data.data[2].lname + " ตามลำดับ ");
                        $('#view_pid_home').html(data.data[0].pid + " , " + data.data[1].pid + " ตามลำดับ ");
                        $('#view_expridate_home').html(setExDate(data.data[0].expiredDate) + " , " + setExDate(data.data[1].expiredDate) + " , " + setExDate(data.data[2].expiredDate) + " ตามลำดับ ");
                        $('#view_address_home').html("บ้านเลขที่ " + data.data[0].home_no + " หมู่ " + data.data[0].moo + " ตำบล " + data.data[0].subdistrict + " อำเภอ " + data.data[0].district + " จังหวัด " + data.data[0].province + " รหัสไปรษณีษ์ " + data.data[0].postcode
                                + ", " +
                                "บ้านเลขที่ " + data.data[1].home_no + " หมู่ " + data.data[1].moo + " ตำบล " + data.data[1].subdistrict + " อำเภอ " + data.data[1].district + " จังหวัด " + data.data[1].province + " รหัสไปรษณีษ์ " + data.data[1].postcode
                                + ", " +
                                "บ้านเลขที่ " + data.data[2].home_no + " หมู่ " + data.data[2].moo + " ตำบล " + data.data[2].subdistrict + " อำเภอ " + data.data[2].district + " จังหวัด " + data.data[2].province + " รหัสไปรษณีษ์ " + data.data[2].postcode + " ตามลำดับ ");
                        $('#view_phone_home').html("หมายเลขโทรศัพท์ " + data.data[0].phone + " , " + data.data[1].phone + " , " + data.data[2].phone + "");
                        $('#view_home_plan_number').html($('#plan2').val());
                        $('#view_home_plan_number2').html($('#plan2').val());
                        $('#view_home_land_no').html($('#landid2').val());
                        $('#view_home_land_no2').html($('#landid3').val());
                        $('#view_home_price').html($('#home_price2').val());
                        $('#view_home_price_char').html(ThaiBaht($('#home_price2').val()));

                        $('#view_pay_price_home').html($('#pay_price_home').val());
                        $('#view_pay_price_home_char').html(ThaiBaht($('#pay_price_home').val()));
                        $('#view_pay_price_pirod1').html($('#pay_price_pirod1').val());
                        $('#view_pay_price_pirod1_char').html(ThaiBaht($('#pay_price_pirod1').val()));
                        $('#view_pay_price_pirod2').html($('#pay_price_pirod2').val());
                        $('#view_pay_price_pirod2_char').html(ThaiBaht($('#pay_price_pirod2').val()));
                        $('#view_pay_price_pirod3').html($('#pay_price_pirod3').val());
                        $('#view_pay_price_pirod3_char').html(ThaiBaht($('#pay_price_pirod3').val()));
                    }

                    var witness1_name1 = $("#witness_name1").val();
                    var witness1_lastname1 = $("#witness_lastname1").val();
                    var fullwiness1 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + witness1_name1 + " " + witness1_lastname1 + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                    var witness1_name2 = $("#witness_name2").val();
                    var witness1_lastname2 = $("#witness_lastname2").val();
                    var fullwiness2 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + witness1_name2 + " " + witness1_lastname2 + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                    $('#payantab1_1').html(fullwiness1);
                    $('#payantab1_2').html(fullwiness2);

                    var witness2_name1 = $("#witness2_name1").val();
                    var witness2_lastname1 = $("#witness2_lastname1").val();
                    var fullwiness2_1 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + witness2_name1 + " " + witness2_lastname1 + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                    var witness2_name2 = $("#witness2_name2").val();
                    var witness2_lastname2 = $("#witness2_lastname2").val();
                    var fullwiness2_2 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + witness2_name2 + " " + witness2_lastname2 + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                    $('#payantab2_1').html(fullwiness2_1);
                    $('#payantab2_2').html(fullwiness2_2);

                    //get image tab1
                    //get_filename_card_by_promise_id_tab1(promiseid, "2", "1");
                    //get_filename_card_by_promise_id_tab2(promiseid, "2", "2");




                    //popup
                    var mode = 'iframe'; //popup
                    var close = mode == "popup";
                    var options = {
                        mode: mode,
                        popClose: close,
                        header: false,
                        fontsize: 10
                    };

                    $("div.printableArea1").printArea(options);

                }
            });
        }
    });
}
function setExDate(exdate) {

    var result = "";
    if (exdate !== null && exdate !== '') {
        //console.log("exdate: " + exdate);
        var year = exdate.substr(0, 4);
        var month = exdate.substr(4, 2);
        var day = exdate.substr(6, 8);
        //console.log("year: " + year + " month: " + month + " day: " + day);
        var srt_month = getMonth(month);
        var result = day + " " + srt_month + " " + year;
    } else {
        result = "";
    }

    return result;
}



function chectk_personplan_promise(promise_id, pid, plan_master) {
    //console.log("masterplan: " + plan_master);
    var output = '';
    $.ajax({
        url: 'chectk_personplan_promise',
        data: {
            promise_id: promise_id,
            pid: pid,
            plan_master: plan_master,
        },
        type: 'POST',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = data;
    });
    return output;
}

function chectk_personhome_promise(promise_id, pid, plan_master) {
    //console.log("masterplan: " + plan_master);
    var output = '';
    $.ajax({
        url: 'chectk_personhome_promise',
        data: {
            promise_id: promise_id,
            pid: pid,
            plan_master: plan_master,
        },
        type: 'POST',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = data;
    });
    return output;
}

//ยกเลิกสัญญา
function  cancelPromisePlan(promise_id, agreement_id, planid, plan_master) {

    //console.log("cancelPromisePlan");
    //console.log("promiseid: " + promise_id);
    //console.log("agreement_id: " + agreement_id);
    //console.log("plan_master: " + plan_master);
    //console.log("planid: " + planid);


    $.ajax({
        url: 'cancel_promise_plan',
        type: 'POST',
        data: {
            promise_id: promise_id,
            agreement_id: agreement_id,
            plan_master: plan_master,
            plan_name: planid
        },
        async: false,
        dataType: 'json',
        'success': function (data) {

            //console.log("cancel plan data: " + data);
            if (data === 1) {
                cancelPromiseHome(promise_id, agreement_id, planid, plan_master);
            }
        }
    });
}


function  cancelPromiseHome(promise_id, agreement_id, planid, plan_master) {

    //console.log("cancelPromiseHome");
    //console.log("promiseid: " + promise_id);
    //console.log("agreement_id: " + agreement_id);
    //console.log("plan_master: " + plan_master);
    //console.log("planid: " + planid);


    $.ajax({
        url: 'cancel_promise_home',
        type: 'POST',
        data: {
            promise_id: promise_id,
            agreement_id: agreement_id,
            plan_master: plan_master
        },
        async: false,
        dataType: 'json',
        'success': function (data) {
            //console.log("cancel home data: " + data);
            if (data === 1) {

                $("#mi-modal").modal('hide');
                $("#idmessage-cancel").html("ยกเลยสัญญาเรียบร้อย");
                $("#mi-modal-message-cancel").modal('show');
            }


        }
    });
}


function get_order_by_orderno(order_no) {

    //console.log("get_order_by_orderno order_no: " + order_no);
    var order = [];
    $.ajax({
        url: '../projectplan/getorder_by_orderno',
        type: 'POST',
        data: {order_no: order_no},
        async: false,
        dataType: 'json',
    }).done(function (data) {
        //console.log("data: " + data);
        if (data === null) {
            order = [];
        } else {
            order = data;
        }
    });

    return order;
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}


function BAHTTEXT(num, suffix) {
    'use strict';

    if (typeof suffix === 'undefined') {
        suffix = 'บาทถ้วน';
    }

    num = num || 0;
    num = num.toString().replace(/[, ]/g, ''); // remove commas, spaces

    if (isNaN(num) || (Math.round(parseFloat(num) * 100) / 100) === 0) {
        return 'ศูนย์บาทถ้วน';
    } else {

        var t = ['', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน'],
                n = ['', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า'],
                len,
                digit,
                text = '',
                parts,
                i;

        if (num.indexOf('.') > -1) { // have decimal

            /* 
             * precision-hack
             * more accurate than parseFloat the whole number 
             */

            parts = num.toString().split('.');

            num = parts[0];
            parts[1] = parseFloat('0.' + parts[1]);
            parts[1] = (Math.round(parts[1] * 100) / 100).toString(); // more accurate than toFixed(2)
            parts = parts[1].split('.');

            if (parts.length > 1 && parts[1].length === 1) {
                parts[1] = parts[1].toString() + '0';
            }

            num = parseInt(num, 10) + parseInt(parts[0], 10);


            /* 
             * end - precision-hack
             */
            text = num ? BAHTTEXT(num) : '';

            if (parseInt(parts[1], 10) > 0) {
                text = text.replace('ถ้วน', '') + BAHTTEXT(parts[1], 'สตางค์');
            }

            return text;

        } else {

            if (num.length > 7) { // more than (or equal to) 10 millions

                var overflow = num.substring(0, num.length - 6);
                var remains = num.slice(-6);
                return BAHTTEXT(overflow).replace('บาทถ้วน', 'ล้าน') + BAHTTEXT(remains).replace('ศูนย์', '');

            } else {

                len = num.length;
                for (i = 0; i < len; i = i + 1) {
                    digit = parseInt(num.charAt(i), 10);
                    if (digit > 0) {
                        if (len > 2 && i === len - 1 && digit === 1 && suffix !== 'สตางค์') {
                            text += 'เอ็ด' + t[len - 1 - i];
                        } else {
                            text += n[digit] + t[len - 1 - i];
                        }
                    }
                }

                // grammar correction
                text = text.replace('หนึ่งสิบ', 'สิบ');
                text = text.replace('สองสิบ', 'ยี่สิบ');
                text = text.replace('สิบหนึ่ง', 'สิบเอ็ด');

                return text + suffix;
            }

        }

    }
}

function getPerson_Status_Scan(promise_id, pid, plan_master, promist_type, tab) {
    //console.log("getStatusScan: " + plan_master);
    var output = '';
    $.ajax({
        url: 'chectk_person_scan_card',
        data: {
            promise_id: promise_id,
            pid: pid,
            plan_master: plan_master,
            promist_type: promist_type,
            file_typ: '2',
            tab: tab,
            order_no: promise_id,
        },
        type: 'POST',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        output = data;
    });
    return output;
}

function get_filename_card_by_promise_id_tab1(promise_id, type, promise_type) {

    $.ajax({
        url: 'getFileSmartCardByPromiseId',
        type: 'POST',
        data: {
            promise_id: promise_id,
            type: type,
            promise_type: promise_type
        },
        async: false,
        dataType: 'json',
        'success': function (data) {
            console.log("#### get_filename_card_by_promise_id length:  " + data.data.length);

            var img1 = '';
            var img2 = '';
            //$('#imagetCardTab1_1').html(img1).append(img2);
            if (data.data.length === 1) {
                img1 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[0].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                $('#imagetCardTab1_1').html(img1);
            } else {
                img1 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[0].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                img2 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[1].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                $('#imagetCardTab1_1').html(img1);
                $('#imagetCardTab1_2').html(img2);
            }
        }
    });
}

function get_filename_card_by_promise_id_tab2(promise_id, type, promise_type) {

    console.log("#### 2 promise_id:  " + promise_id);
    console.log("#### 2 type:  " + type);
    console.log("#### 2 promise_type:  " + promise_type);
    $.ajax({
        url: 'getFileSmartCardByPromiseId',
        type: 'POST',
        data: {
            promise_id: promise_id,
            type: type,
            promise_type: promise_type
        },
        async: false,
        dataType: 'json',
        'success': function (data) {
            console.log("#### get_filename_card_by_promise_id_tab2 length:  " + data.data.length);

            var img1 = '';
            var img2 = '';

            //$('#imagetCardTab1_1').html(img1).append(img2);
            if (data.data.length === 1) {
                img1 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[0].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                $('#imagetCardTab2_1').html(img1);

            } else {
                img1 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[0].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                img2 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[1].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";

                $('#imagetCardTab2_1').html(img1);
                $('#imagetCardTab2_2').html(img2);
            }
        }
    });
}



