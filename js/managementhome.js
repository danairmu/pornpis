
$(document).ready(function () {

    loadhome();

    $('#file').change(function () {
        var base64;
        var inputElement = $("input[name=file]");
        var hiddenpath = $("#hiddenpath").val();
        var f = readImage(inputElement).done(function (base64Data) {
            
            base64 = base64Data;
            var contentType = 'image/png';
            var b64Data = base64.substring(23, base64.length);
            var blob = b64toBlob(b64Data, contentType);

            var path = $("#file").val();
            console.log("path: " + path);
            
            var seq = get_image_seq();
            console.log("seq: " + seq);
            
            var filename = path.split('\\').pop();
            console.log("filename: "+ filename);

            var output_file_name_path = filename.substr(0, filename.lastIndexOf('.')) || filename;
            console.log("output: " + output_file_name_path);
            
            var size = $("#file")[0].files[0].size;
            console.log("size: " + size);

            console.log("hiddenpath: " + hiddenpath);
            var homeid = $('#homeidimage').val();
            
            $.ajax({
                'url': 'upload_image',
                'type': 'POST',
                async: true,
                'data': {
                    name: filename,
                    file_name_path: output_file_name_path + "-" +seq+ ".jpg",
                    pathdata: path,
                    home_id: homeid,
                    image_data: b64Data,
                    hiddenpath: hiddenpath + "/" + homeid + "/" + filename
                },
                dataType: 'json',
                'success': function (data) {
                    console.log(data);
                    var table = $('#imagehomelist').DataTable();
                    table.ajax.reload();
                }, error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
    });

});

$("#modal-btn-n-message").on("click", function (s) {
    $("#mi-modal-message").modal('hide');
});

function get_image_seq() {

    var seq = 0;
    $.ajax({
        url: 'get_image_seq',
        type: 'GET',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        seq = data;
    });

    return seq;
}

function loadhome() {
    var rowId = 1;
    var table = $('#planhomelist').DataTable({
        responsive: true,
        fixedHeader: true,
        "processing": false,
        "serverSide": false,
        "searching": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": true,
        "ordering": false,
        "info": false,
        pageLength: 10,
        "ajax": "planshomelist",
        "rowId": 'extn',
        "columns": [

            {targets: 0,
                data: 'id',
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },

            {
                "className": "text-left",
                "data": "h_name",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {"className": "text-right",
                "data": "h_price",
                "render": function (data, type, row, meta) {
                    return  addCommas(data);

                }
            },

            {
                "className": "text-center",
                "data": "h_area",
                "render": function (data, type, row, meta) {
                    return  row.h_area + ' (ตร.เมตร)';

                }
            },

            {
                "className": "text-left",
                "data": "h_comment",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="showOptionModalEdit(\'' + row.id + '\') "><i class="fa fa-edit" style="color:green; cursor: pointer;"></i></a>';
                },
                "defaultContent": ''
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="showOptionImage(\'' + row.id + '\', \'' + row.h_name + '\') "><i class="fa fa-file-image-o" style="color:green; cursor: pointer;"></i></a>';
                },
                "defaultContent": ''
            },
        ],
        rowReorder: true
                //"pageLength": false,
    });

    $('#btn_update_home').click(function () {

        console.log("btn_update_home");
        var form = $('#form');

        $.ajax({
            type: 'POST',
            data: form.serialize(),
            url: 'update_home',
            async: true,
            success: function (data) {
                console.log("data update: " + data);

                if (data == '1') {
                    rowId = 1;
                    var table = $('#planhomelist').DataTable();
                    table.ajax.reload();
                    closeOptionModalEdit();
                    $("#idmessage").html("แก้ไขข้อมูลบ้านเรียบร้อย");
                    $("#mi-modal-message").modal('show');
                } else {
                    closeOptionModalEdit();
                    $("#idmessage").html("เกิดข้อผิดพลาดในการแก้ไขข้อมูลบ้าน");
                    $("#mi-modal-message").modal('show');
                }
            }
        });
    });
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

function readImage(inputElement) {
    var deferred = $.Deferred();

    var files = inputElement.get(0).files;
    if (files && files[0]) {
        var fr = new FileReader();
        fr.onload = function (e) {
            deferred.resolve(e.target.result);
        };
        fr.readAsDataURL(files[0]);
    } else {
        deferred.resolve(undefined);
    }

    return deferred.promise();
}

function getListImageByid(id) {


    var table = $('#imagehomelist').DataTable({

        fixedHeader: true,
        "processing": false,
        "serverSide": false,
        "searching": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": false,
        "ordering": false,
        "info": false,
        destroy: true,
        "ajax": "imagelist/" + id,
        "rowId": 'extn',
        "columns": [

            {targets: 0,
                data: "id",
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },

//            {"className": "text-center",
//                "data": "h_name",
//                "render": function (data, type, row, meta) {
//                    return  data;
//
//                }
//            },

            {
                "className": "text-left",
                "data": "name",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="showOptionViewImage(\'' + row.id + '\', \'' + row.home_id + '\' , \'' + row.name + '\') "><i class="fa fa-file-image-o" style="color:green; cursor: pointer;"></i></a>';
                },
                "defaultContent": ''
            },
            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="deleteimage(\'' + row.id + '\') " ><i class="fa fa-trash" style="color:green; cursor: pointer;"></i></a>';
                },
                "defaultContent": ''
            },
        ],
//        rowReorder: true
    });
}


function showOptionImage(id, homename) {

    $("#homeidimage").val(id);
    $('#planlabelimage').html(homename);
    getListImageByid(id);
    $('#myModalImage').modal('show');

}

function closeOptionModalImage() {

    $('#myModalImage').modal('hide');
}

function showOptionModalEdit(id) {

    console.log("edit id: " + id);
    $.ajax({
        type: 'GET',
        url: 'homelistbyid/' + id,
        async: true,
        success: function (data) {
            console.log(data[0].h_name);

            $('#planlabel').html(data[0].h_name);
            $('#homeid').val(data[0].id);
            $('#home_name').val(data[0].h_name);
            $('#price').val(addCommas(data[0].h_price));
            $('#area').val(data[0].h_area);
            $('#comment').val(data[0].h_comment);
            $('#home_width').val(data[0].h_width);
            $('#home_height').val(data[0].h_height);
        }
    });


    $('#myModalUpdate').modal('show');
}


function closeOptionModalEdit() {
    $('#myModalUpdate').modal('hide');
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function showOptionViewImage(id, homeid, imagename) {

    console.log("id: " + id + " homeid: " + homeid + " : " + imagename);


    var img = "<img src=\"../upload/" + homeid + "/" + imagename + "\" width=\"500\" height=\"300\"  alt=\"Chicago\" class=\"boder-image\">";
    $('#imagedata').html(img);
    $('#myModalViewImage').modal('show');

}

function closeOptionViewImage() {
    $('#myModalViewImage').modal('hide');
}

function deleteimage(id) {
    $.ajax({
        type: 'GET',
        url: 'delete_image/' + id,
        async: true,
        success: function (data) {
            var table = $('#imagehomelist').DataTable();
            table.ajax.reload();
        }
    });
}


function doubleOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9 .]/gi;
    element.value = element.value.replace(regex, "");
}

function numberOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
}