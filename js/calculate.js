$(document).ready(function () {

    $('#btn_cal').click(function () {
        console.log("Start Cal");
        var input_price = $("#input_price").val().replace(/,/g, "");
        var input_year = $("#input_year").val();
        var input_rate = $("#input_rate").val();
        if ((input_price !== null && input_price !== '')
                && (input_year !== null && input_year !== '')
                && (input_rate !== null && input_rate !== '')) {

            var year = parseInt(input_year);
            var total_month = year * 12;
            var int_price = parseFloat(input_price);
            var int_rate = parseFloat(input_rate);
            $("#result_price").html(": " + addCommas(input_price) + " บาท");
            $("#result_year").html(": " + input_year + " ปี (" + total_month + " เดือน)");
            $("#result_rate").html(": " + input_rate + "%");
            
            var a = int_price;
            var b = int_rate;
            var c = year;
            
            var n = c * 12;  //จำนวนเดือน
            var r = b / (12 * 100);
            console.log("r: "+ r);
            //price * ดอกเบี้ย
            console.log("cal1: " + (a * r * Math.pow((1 + r), n)));
            console.log("cal2: " + (Math.pow((1 + r), n) - 1));
            
            var p = (a * r * Math.pow((1 + r), n)) / (Math.pow((1 + r), n) - 1);
            
            console.log("p: " + p);
            
            var prin = Math.round(p * 100) / 100;
            $("#result_total").html(": " + addCommas(prin) + " บาท/เดือน");
            $("#display").show();
        } else {
            console.log("null value");

            var input_price = $('#input_price').val();
            var input_year = $("#input_year").val();
            var input_rate = $("#input_rate").val();

            if (input_price.length === 0) {
                $('#input_price').focus();
                $('#input_price').css('border', '2px solid #d39e00');
                $("#input_price").attr("placeholder", "กรุณากรอกข้อมูล");
            } else if (input_year.length === 0) {
                $('#input_year').focus();
                $('#input_year').css('border', '2px solid #d39e00');
                $("#input_year").attr("placeholder", "กรุณากรอกข้อมูล");
            } else if (input_rate.length === 0) {
                $('#input_rate').focus();
                $('#input_rate').css('border', '2px solid #d39e00');
                $("#input_rate").attr("placeholder", "กรุณากรอกข้อมูล");
            }

            $("#display").hide();
        }
    });
    $("#input_price").keyup(function () {

        var input_price = $('#input_price').val();
        if (input_price.length > 0) {
            $('#input_price').val(addCommas($('#input_price').val()));
            $('#input_price').css('border', '0px solid red');
            $("#input_price").attr("placeholder", "");
        }
    });

    $("#input_year").keyup(function () {

        var input_year = $("#input_year").val();
        if (input_year.length > 0) {
            $('#input_year').css('border', '0px solid red');
            $("#input_year").attr("placeholder", "");
        }
    });

    $("#input_rate").keyup(function () {

        var input_rate = $("#input_rate").val();
        if (input_rate.length > 0) {
            $('#input_rate').css('border', '0px solid red');
            $("#input_rate").attr("placeholder", "");
        }
    });
});



function numberOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
}

function floatOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9-.]/gi;
    element.value = element.value.replace(regex, "");
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}