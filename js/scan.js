
$(document).ready(function () {

    console.log("init scan");
    var rowId = 1;
    var order_no = $("#hidden_order").val();
    console.log("order_no: " + order_no);
    console.log("plan_master_scan: " + $('#plan_master_scan').val());
    var mode = getUrlParameter('mode');
    var v_pid = getUrlParameter('pid');
    console.log("mode: "+mode);
    console.log("v_pid: "+v_pid);
    if(mode === '1'){
        $("#labelscan").html("ข้อมูลเอกสารสแกนหมายเลขการจอง");
    }else{
        $("#labelscan").html("ข้อมูลสแกนสำเนาบัตรหมายเลขการจอง")
    }
    
    $("#scanlist").dataTable().fnDestroy();
    var table = $('#scanlist').DataTable({

        fixedHeader: true,
        "processing": false,
        "serverSide": false,
        "searching": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": true,
        "ordering": false,
        "info": false,
        pageLength: 10,
        "rowId": 'extn',

        "ajax": {
            "url": "scanlist",
            "type": "post",
            "data": {order_no: order_no,
                plan_master_scan: $('#plan_master_scan').val(),
                mode : mode
            }
        },

        "columns": [

            {targets: 0,
                data: 'id',
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },

            {
                "className": "text-left",
                "data": "file_name",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {"className": "text-center",
                "data": "create_date_thai",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="showOptionViewImage(\'' + row.order_no + '\', \'' + row.file_name_path + '\') "><i class="fa fa-file-image-o" style="color:green; cursor: pointer;"></i></a>';
                },
                "defaultContent": ''
            },
        ],
        rowReorder: true
                //"pageLength": false,
    });

    $('#scan').click(function () {
        console.log("scan");

        var data = "data:image/jpeg;base64,/9j/";
        var b64Data = data.substring(23, data.length);
        console.log("b64Data: " + b64Data);
        scanner.scan(displayImagesOnPage,
                {
                    "output_settings": [
                        {
                            "type": "return-base64",
                            "format": "jpg"
                        }
                    ]
                }
        );
    });

});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

/** Processes the scan result */
function displayImagesOnPage(successful, mesg, response) {

    if (!successful) { // On error
        console.error('Failed: ' + mesg);
        return;
    }

    if (successful && mesg != null && mesg.toLowerCase().indexOf('user cancel') >= 0) { // User cancelled.
        console.info('User cancelled');
        return;
    }

    var scannedImages = scanner.getScannedImages(response, true, false); // returns an array of ScannedImage

    for (var i = 0; (scannedImages instanceof Array) && i < scannedImages.length; i++) {
        var scannedImage = scannedImages[i];
        processScannedImage(scannedImage);
    }
}

function processScannedImage(scannedImage) {
    //console.log("scannedImage src: " + scannedImage.src);
    var data = scannedImage.src;
    var b64Data = data.substring(23, data.length);
    var seq_san = getScanSeq();

    
    var filename = $('#hidden_order').val().trim();
    var filenameTopath = filename.replace(/\//g, '-');
    var plan_master = $('#plan_master_scan').val();
    var path = "scan/" + filename + "-" + seq_san + ".jpg";
    var order_no = filename.trim();
    var fullfile_name = filename + "-" + seq_san + "-" + plan_master;
    //send to php

    var mode = getUrlParameter('mode');
    var v_pid = getUrlParameter('pid');
    console.log("mode1: " + mode + " v_pid: " + v_pid);
    console.log("$filenameTopath: " + filenameTopath);
    $.ajax({
        'url': 'image_scan',
        'type': 'POST',
        async: true,
        'data': {
            filenameTopath: filenameTopath + "-" + seq_san,
            filename: fullfile_name.trim(),
            pathfile: path,
            order_no: order_no,
            image_data: b64Data,
            plan_master: plan_master,
            mode: mode,
            pid: v_pid
        },
        dataType: 'json',
        'success': function (data) {
            console.log(data);
            var table = $('#scanlist').DataTable();
            table.ajax.reload();
        }, error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function getScanSeq() {

    var seq = 0;
    $.ajax({
        url: 'get_scan_seq',
        type: 'GET',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        seq = data;
    });

    return seq;
}

function showOptionViewImage(order_no, file_name) {

    var mode = 'iframe'; //popup
    var close = mode == "popup";
    var options = {
        mode: mode,
        popClose: close
    };

    var modeparam = getUrlParameter('mode');
    console.log("modeparam: " + modeparam);
    console.log("file_name: " + file_name);
    
    if (modeparam === '1') {
        var img = "<img src=\"../scan/" + file_name + "\" style=\"width: 100%\"  alt=\"Chicago\" class=\"boder-image\">";
        $('.printableAreaImage').html(img);
        $("div.printableAreaImage").show();
        $("div.printableAreaImage").printArea(options);
    } else {
        var img = "<img src=\"../scancard/" + file_name + "\" style=\"width: 100%\"  alt=\"Chicago\" class=\"boder-image\">";
        $('.printableAreaImage').html(img);
        $("div.printableAreaImage").show();
        $("div.printableAreaImage").printArea(options);
    }


//    $('#imagedata').show();
//    var img = "<img src=\"../scan/" + file_name + "\" style=\"width: 100%\"  alt=\"Chicago\" class=\"boder-image\">";
//    $('#imagedata').html(img);
//    $('#myModalViewImage').modal('show');

}
function closeOptionViewImage() {
    $('#myModalViewImage').modal('hide');
}

