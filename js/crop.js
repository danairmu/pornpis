$(document).ready(function () {

    testCrop();
    
});


function testCrop() {
    
    var image = document.querySelector('#image');
    var data = document.querySelector('#data');
    var button = document.getElementById('button');
    var result = document.getElementById('result');
    var minCroppedWidth = 420;
    var minCroppedHeight = 260;
    var maxCroppedWidth = 740;
    var maxCroppedHeight = 420;
    var cropper = new Cropper(image, {
        viewMode: 3,

        data: {
            width: (minCroppedWidth + maxCroppedWidth) / 2,
            height: (minCroppedHeight + maxCroppedHeight) / 2,
        },

        crop: function (event) {
            var width = 200;
            var height = 200;

            if (
                    width < minCroppedWidth
                    || height < minCroppedHeight
                    || width > maxCroppedWidth
                    || height > maxCroppedHeight
                    ) {
                cropper.setData({
                    width: Math.max(minCroppedWidth, Math.min(maxCroppedWidth, width)),
                    height: Math.max(minCroppedHeight, Math.min(maxCroppedHeight, height)),
                });
            }

            data.textContent = JSON.stringify(cropper.getData(true));
        },
    });
    console.log("cropper: " + cropper);
    console.log("cropper getCroppedCanvas: " + cropper.getCroppedCanvas);
   
    result.innerHTML = '';
    result.appendChild(cropper.getCroppedCanvas());
}