
$(document).ready(function () {

    console.log("init scan");
    var rowId = 1;
    var order_no = $("#hidden_order").val();
    var promiseid = $("#hidden_promiseid").val().trim();
    console.log("order_no: " + order_no);
    console.log("plan_master_id: " + $('#plan_master_id').val());
    console.log("promiseid: " + promiseid.trim());
    var mode = getUrlParameter('mode');
    var tab = getUrlParameter('tab');
    var prommise_type = '';
    if (tab === '1') {
        prommise_type = '1';
    } else {
        prommise_type = '2';
    }
    console.log("mode: " + mode + " tab: " + tab);
    var table = $('#scanlist').DataTable({

        fixedHeader: true,
        "processing": false,
        "serverSide": false,
        "searching": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": true,
        "ordering": false,
        "info": false,
        pageLength: 10,
        "rowId": 'extn',

        "ajax": {
            "url": "scanlist",
            "type": "post",
            "data": {
                order_no: order_no,
                plan_master_scan: $('#plan_master_id').val(),
                promiseid: promiseid,
                mode: mode,
                tab: tab,
                prommise_type: prommise_type,
                flag: '1' //document scan
            }
        },

        "columns": [

            {targets: 0,
                data: 'id',
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },

            {
                "className": "text-left",
                "data": "file_name",
                "render": function (data, type, row, meta) {
                    if (row.file_name.length > 0) {
                        var result = row.file_name.substring(0, row.file_name.length - 4);
                        return  result;
                    } else {
                        return  data;
                    }


                }
            },

            {"className": "text-center",
                "data": "create_date_thai",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },

            {
                "className": "text-center",
                "orderable": false,
                "data": "id",
                "render": function (data, type, row, meta) {
                    return '<a onclick="showOptionViewImage(\'' + row.order_no + '\', \'' + row.file_name_scan + '\') "><i class="fa fa-file-image-o" style="color:green; cursor: pointer;"></i></a>';
//                            &nbsp;&nbsp;\n\
//                            <a onclick="cancelImage(\'' + row.id + '\', \'' + row.order_no + '\', \'' + row.file_name_scan + '\') "style="font-size: 18px; cursor: pointer" target="_blank"><i class="fa fa-close" style="color:red;" aria-hidden="true"></i></a>';
                },
                "defaultContent": ''
            },
        ],
        rowReorder: true
                //"pageLength": false,
    });

    $('#scan').click(function () {
        //$("#myModalFileScan").modal("show");

        console.log("scan");
        var data = "data:image/jpeg;base64,/9j/";
        var b64Data = data.substring(23, data.length);
        console.log("b64Data: " + b64Data);
        scanner.scan(displayImagesOnPage,
                {
                    "output_settings": [
                        {
                            "type": "return-base64",
                            "format": "jpg"
                        }
                    ]
                }
        );

    });

});

//$("#btn_save_file_scan").click(function () {
//
//    $("#myModalFileScan").modal("hide");
//    var filescan = $("#filename_scan").val();
//    console.log("scan");
//    console.log("filescan: " + filescan);
//    var data = "data:image/jpeg;base64,/9j/";
//    var b64Data = data.substring(23, data.length);
//    console.log("b64Data: " + b64Data);
//    scanner.scan(displayImagesOnPage,
//            {
//                "output_settings": [
//                    {
//                        "type": "return-base64",
//                        "format": "jpg"
//                    }
//                ]
//            }
//    );
//});

function closeFileScan() {
    $('#myModalFileScan').modal('hide');
    $("#filename_scan").val("");
}

/** Processes the scan result */
function displayImagesOnPage(successful, mesg, response) {
    if (!successful) { // On error
        console.error('Failed: ' + mesg);
        return;
    }

    if (successful && mesg != null && mesg.toLowerCase().indexOf('user cancel') >= 0) { // User cancelled.
        console.info('User cancelled');
        $("#filename_scan").val("");
        return;
    }

    var scannedImages = scanner.getScannedImages(response, true, false); // returns an array of ScannedImage

    for (var i = 0; (scannedImages instanceof Array) && i < scannedImages.length; i++) {
        var scannedImage = scannedImages[i];
        processScannedImage(scannedImage);
    }
}

function processScannedImage(scannedImage) {
    //console.log("scannedImage src: " + scannedImage.src);
    var data = scannedImage.src;
    var b64Data = data.substring(23, data.length);
    var seq_san = getScanSeq();

    var filename = $('#hidden_promiseid').val().trim();
    var promise_id = $('#hidden_promiseid').val().trim();
    var plan_master = $('#plan_master_id').val();
    var path = "scanpromise/" + filename + "-" + seq_san + ".jpg";
    var filename_scan = $("#hidden_promiseid").val();
    var order_no = filename.trim();
    var fullfile_name = filename + "-" + seq_san + "-" + plan_master;
    var filenameTopath = filename_scan.replace(/\//g, '-');
    //send to php

    var mode = getUrlParameter('mode');
    var tab = getUrlParameter('tab');
    var typescan = getUrlParameter('typescan');
    console.log("mode1: " + mode);
    console.log("fullfile_name: " + fullfile_name);
    console.log("filenameTopath: " + filenameTopath);

    $.ajax({
        'url': 'image_scan',
        'type': 'POST',
        async: true,
        'data': {
            promise_id: promise_id,
            filename: fullfile_name.trim(),
            pathfile: path,
            order_no: order_no,
            image_data: b64Data,
            plan_master: plan_master,
            mode: mode,
            filename_scan: filenameTopath.trim() + "-" + seq_san,
            tab: tab,
            typescan: typescan
        },
        dataType: 'json',
        'success': function (data) {
            console.log(data);
            var table = $('#scanlist').DataTable();
            table.ajax.reload();
        }, error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function getScanSeq() {

    var seq = 0;
    $.ajax({
        url: 'get_scan_seq',
        type: 'GET',
        async: false,
        dataType: 'json',
    }).done(function (data) {
        seq = data;
    });

    return seq;
}

function showOptionViewImage(order_no, file_name) {

    var mode = 'iframe'; //popup
    var close = mode == "popup";
    var options = {
        mode: mode,
        popClose: close
    };

    var modeparam = getUrlParameter('mode');
    console.log("modeparam: " + modeparam);
    console.log("file_name: " + file_name);
    if (modeparam === '1' || modeparam === '2') {
        var img = "<img src=\"../scanpromise/" + file_name + "\" style=\"width: 100%\"  alt=\"Chicago\" class=\"boder-image\">";
        $('.printableAreaImage').html(img);
        $("div.printableAreaImage").show();
        $("div.printableAreaImage").printArea(options);
    } else {
        var img = "<img src=\"../scanpromisecard/" + file_name + "\" style=\"width: 100%\"  alt=\"Chicago\" class=\"boder-image\">";
        $('.printableAreaImage').html(img);
        $("div.printableAreaImage").show();
        $("div.printableAreaImage").printArea(options);
    }
//    $('#imagedata').show();
//    var img = "<img src=\"../scan/" + file_name + "\" style=\"width: 100%\"  alt=\"Chicago\" class=\"boder-image\">";
//    $('#imagedata').html(img);
//    $('#myModalViewImage').modal('show');

}

function cancelImage(id, order_no, file_name) {
    console.log("concelImage id: " + id + " order_no: " + order_no + " file_name: " + file_name);
}

function closeOptionViewImage() {
    $('#myModalViewImage').modal('hide');
}


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

