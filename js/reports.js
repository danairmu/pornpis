$(document).ready(function () {
    var t = $('#rstable').DataTable({
        ajax: 'reports/get_report_list',
        searching: true,
        ordering: false,
        lengthChange: false,
        "processing": false,
        "serverSide": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": true,
        "info": false,
        pageLength: 10,
        columns: [{
                'className': 'text-center align-middle',
                'data': null,
                'render': function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },
            {
                'className': 'text-left align-middle',
                'data': 'salename'

            },
            {
                'className': 'text-left align-middle',
                'data': 'agreement_no'
            },
            {
                'className': 'text-center',
                'data': 'create_date_thai'
            },
            {
                'className': 'text-center',
                'data': 'sale_date',
                'render': function (data, type, row, meta) {
                    return data === '' ? '' : data;
                }
            },
            {
                'className': 'text-right',
                'data': 'total_price',
                'render': function (data, type, row, meta) {
                    return formatMoney(data.replace(/,/g, ''));
                }
            },
            {
                'className': 'text-center align-middle pt-1 p-0',
                'data': null,
                'render': function (data, type, row, meta) {
                    return '<a onclick="showOptionAgreement(\'' + row.agreement_id + '\') " class="p-0" style="font-size: 18px; cursor: pointer" target="_blank"><i class="fa fa-file-text-o" style="color:green;" aria-hidden="true"></i></a>&nbsp;&nbsp;';
                    //<a style="font-size: 18px; cursor: pointer" target="_blank"><i class="fa fa-link" style="color:green;" aria-hidden="true"></i></a>

                }
            },
            {
                'className': 'text-center align-middle pt-1 p-0',
                'data': null,
                'render': function (data, type, row, meta) {
                    if (row.promise_id === null || row.promise_id === "") {
                        return '<a style="font-size: 18px; cursor: pointer" target="_blank"><i class="fa fa-file-text-o" aria-hidden="true"></i></a> ';
                        //<a style="font-size: 18px; cursor: pointer" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a>'
                        //;
                    } else {
                        return '<a onclick="showOptionPromise(\'' + row.promise_id + '\') " class="p-0" style="font-size: 18px; cursor: pointer" target="_blank"><i class="fa fa-file-text-o" aria-hidden="true" style="color:green;"></i></a>';
                        //<a style="font-size: 18px; cursor: pointer" target="_blank"><i class="fa fa-link" style="color:green;" aria-hidden="true"></i></a>'
                        //;
                    }

                }
            },
            {
                'className': 'd-none',
                'data': 'saleid'
            },
        ],
        rowReorder: true
                //'pageLength': false,
    });

    t.on('order.dt search.dt', function () {
        t.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    //-- Custom Filter
    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var match = true;
                var saleFilter = $('#saleFilter').val();
                console.log("saleFilter:  " + saleFilter);
                if (saleFilter) {
                    if (saleFilter !== data[8]) {
                        match = false;
                    }
                }

                if (match) {
                    match = isBetweenDate(data[3], $('#startDate').val(), $('#endDate').val());
                }

                if (match) {
                    if ($('#reportType1').prop('checked')) {
                        match = data[4] == '';
                    } else {
                        match = data[3] != '';
                    }
                }
                return match;
            }
    );

    $('#saleFilter').on('change', function () {
        t.draw();
    });

    $('#startDate').on('change', function () {
        t.draw();
    });

    $('#endDate').on('change', function () {
        t.draw();
    });

    $('#reportType1').on('change', function () {
        t.draw();
    });

    $('#reportType2').on('change', function () {
        t.draw();
    });
    //-- End Custom Filter


    $('#btn_export').on('click', function () {
        g_toExcelSummary("rstable");
    });

    $(".datepicker").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'th',
                    thaiyear: true,
    });
});

//Format: dd/mm/yyyy
function isBetweenDate(date, startDate, endDate) {
    var spl = '/';
    var yy = 2;
    var mm = 1;
    var dd = 0;

    if (!date) {
        return false;
    }

    var dateArr = date.split(spl);
    if (dateArr.length !== 3) {
        return false;
    }
    var current = new Date(dateArr[yy], parseInt(dateArr[mm]) - 1, dateArr[dd]);

    if (startDate) {
        startArr = startDate.split(spl);
        var from = new Date(startArr[yy], parseInt(startArr[mm]) - 1, startArr[dd]);

        if (current < from) {
            return false;
        }
    }

    if (endDate) {
        endArr = endDate.split(spl);
        var to = new Date(endArr[yy], parseInt(endArr[mm]) - 1, endArr[dd]);

        if (current > to) {
            return false;
        }
    }

    return true;
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

//แสดงเอกสารการจอง
function showOptionAgreement(order_no) {

    console.log("orderno: " + order_no);

    

    var plan_master = '';
    var orders2 = [];

    orders2 = get_order_by_orderno(order_no);
    for (var i = 0; i < orders2.length; i++) {
        plan_master = orders2[i].plan_id;
    }

    console.log("plan_master: " + plan_master);

    $.ajax({
        'type': 'POST',
        'data': {order_no: order_no, plan_master: plan_master},
        async: true,
        'url': 'projectplan/get_agreementid',
        'success': function (data) {

            var date = data.date_agreenment_thai;
            console.log("date: " + date);
            var new_date = date.replace(/\//g, '');
            var day = new_date.substr(0, 2);
            var month = new_date.substr(2, 2);
            var year = new_date.substr(4, 4);
            var srt_month = getMonth(month);
            var rs_month = day + " " + srt_month + " " + year;

            var cdate = data.date_create_thai;
            var cnew_date = cdate.replace(/\//g, '');
            var cday = cnew_date.substr(0, 2);
            var cmonth = cnew_date.substr(2, 2);
            var cyear = cnew_date.substr(4, 4);
            var csrt_month = getMonthFull(cmonth);
            var crs_month = cday + " " + csrt_month + " พ.ศ. " + cyear;



            $("#date_view_1").html(crs_month);
            $("#date_view_de_2").html(crs_month);

            $("#agreenment_id1").html(order_no);
            $('#view_ag_fullname').html(data.title + data.fname + " " + data.lname);
            $("#view_ag_homeno").html(data.home_no);
            $("#view_ag_group").html(data.moo);
            $("#view_ag_street").html(data.street);
            $("#view_ag_subDistrict").html(data.subdistrict);
            $("#view_ag_district").html(data.district);
            $("#view_ag_province").html(data.province);
            $("#view_ag_postcode").html(data.postcode);
            $("#view_ag_phone").html(data.phone);
            $("#view_ag_plan").html(data.plan);
            $("#view_ag_total_plan").html(data.plan_total);
            $("#view_pricetarangwa").html(data.price_tarangwa);
            $("#view_money").html(data.price_total);
            $("#view_chat_price").html("(" + ThaiBaht(data.price_total) + ")");

            var pricepay = data.price_pay.replace(/,/g, "");
            var _intpricepay = parseFloat(pricepay);
            var result = _intpricepay / 2;

            $("#view_price_order").html(formatMoney(result));
            $("#view_price_order_char").html("(" + ThaiBaht(data.price_pay) + ")");
            $("#view_price_order_date").html(rs_month);


            $("#agreenment_id2").html(order_no);
            $('#view_de_fullname').html(data.title + data.fname + " " + data.lname);
            $("#view_de_homeno").html(data.home_no);
            $("#view_de_group").html(data.moo);
            $("#view_de_street").html(data.street);
            $("#view_de_subDistrict").html(data.subdistrict);
            $("#view_de_district").html(data.district);
            $("#view_de_province").html(data.province);
            $("#view_de_postcode").html(data.postcode);
            $("#view_de_de_phone").html(data.phone);

            var datahome = get_homedata_by_order_no(order_no);
            console.log("datahome: " + datahome);
            var array = [];
            array = datahome.split(',');
            $("#view_de_home").html(array[0]);
            $("#view_home_price").html(addCommas(array[1]));
            $("#vview_home_price_char").html(ThaiBaht(array[1]));

            $("#view_de_plans").html(data.plan);
            $("#view_de_money").html(formatMoney(result));
            $("#view_de_chat_price").html("(" + ThaiBaht(data.price_total) + ")");
            $("#view_de_price_order").html(rs_month);




            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close,
                header: false,
                fontsize: 10
            };
            $("div.printableArea").printArea(options);
        }
    });


}

//function initview(order_no) {
//
//    
//
//    $.ajax({
//        'type': 'POST',
//        'data': {order_no: order_no, plan_master: plan_master},
//        async: true,
//        'url': 'projectplan/get_agreementid',
//        'success': function (data) {
//            
//        }
//    });
//    
//    
////    var cdata = $("#hidden_bookdate").val();
////    console.log("cdata: " + cdata);
////    if (cdata !== null && cdata !== '') {
////        $("#date_view_1").html(cdata);
////    } else {
////        $("#date_view_1").html($('#hidden_date_due').val());
////
////    }
//
//    var fullname = $("#title").val() + $("#firstname").val() + " " + $("#lastname").val();
//    $("#view_ag_fullname").html(fullname);
//    $("#view_ag_homeno").html($('#houseNumber').val());
//    $("#view_ag_group").html($('#group').val());
//    $("#view_ag_street").html($('#street').val() === "" ? '-' : $('#street').val());
//    $("#view_ag_subDistrict").html($('#subDistrict').val());
//    $("#view_ag_district").html($('#district').val());
//    $("#view_ag_province").html($('#province').val());
//    $("#view_ag_postcode").html($('#postcode').val() === "" ? '-' : $('#postcode').val());
//    $("#view_ag_phone").html($('#phone').val() === "" ? '-' : $('#phone').val());
//    $("#view_ag_plan").html($('#hidden_planssave').val());
//    $("#view_ag_total_plan").html($('#plan_total').val());
//
//    $("#view_pricetarangwa").html($("#planprice").val());
//    $("#view_money").html($('#pricetotal').val());
//    $("#view_chat_price").html("(" + $('#input_form_price_word').val() + ")");
//
//    var pricepay = $('#pricepay').val().replace(/,/g, "");
//    var _intpricepay = parseFloat(pricepay);
//    var result = _intpricepay / 2;
//    $("#view_price_order").html(formatMoney(result));
//
//    var hidden_order_price_char = $('#hidden_order_price_char').val();
//    console.log("hidden_order_price_char: " + hidden_order_price_char);
//    $("#view_price_order_char").html(hidden_order_price_char === '(บาทถ้วน)' ? '(ศูนย์บาทถ้วน)' : hidden_order_price_char);
//
//    var date = $('#dateorder').val();
//    $("#view_price_order_date").html(date);
//
//    //get image
//    var file_name = get_filename_by_order_no($("#hidden_order_number").val(), '2');
//    if (file_name !== '') {
//        console.log("getImage: " + file_name);
//        var img = "<img id=\"imagecorp\" src=\"../scancard/" + file_name + "\" style=\"width: 380px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
//        console.log("image: " + img);
//        $('#imagecardagreement').html(img);
//    }
//}


function showOptionPromise(promiseid) {

    console.log("showOptionPromise: " + promiseid);
    
    $('#view_promise_pph').html("เลขที่ PPH-" + promiseid);
    $('#view_promise_ppl').html("เลขที่ PPL-" + promiseid);
    
    if (promiseid === null || promiseid === "") {

        console.log("promiseid is null");
        $("#idmessage").html("<i class=\"fa fa-exclamation-circle\"></i> ยังไม่มีข้อมูลของการทำสัญญาซื้อขาย");
        $("#mi-modal-message").modal('show');

    } else {
        console.log("promiseid: " + promiseid);
        get_data_view_plan(promiseid);
        //get_data_view_home(promiseid);
    }
}

$("#modal-btn-n-message").on("click", function (s) {
    $("#mi-modal-message").modal('hide');
});

function ThaiBaht(Number) {
    var Number = CheckNumber(Number);
    var NumberArray = new Array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ");
    var DigitArray = new Array("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน", "สิบล้าน", "ร้อยล้าน");
    var BahtText = "";
    if (isNaN(Number))
    {
        return "ข้อมูลนำเข้าไม่ถูกต้อง";
    } else
    {
        if ((Number - 0) > 999999999.9999)
        {
            return "ข้อมูลนำเข้าเกินขอบเขตที่ตั้งไว้";
        } else
        {
            Number = Number.split(".");
            if (Number[1].length > 0)
            {
                Number[1] = Number[1].substring(0, 2);
            }
            var NumberLen = Number[0].length - 0;
            for (var i = 0; i < NumberLen; i++)
            {
                var tmp = Number[0].substring(i, i + 1) - 0;
                if (tmp != 0)
                {
                    if ((i == (NumberLen - 1)) && (tmp == 1))
                    {
                        BahtText += "เอ็ด";
                    } else
                    if ((i == (NumberLen - 2)) && (tmp == 2))
                    {
                        BahtText += "ยี่";
                    } else
                    if ((i == (NumberLen - 2)) && (tmp == 1))
                    {
                        BahtText += "";
                    } else
                    {
                        BahtText += NumberArray[tmp];
                    }
                    BahtText += DigitArray[NumberLen - i - 1];
                }
            }
            BahtText += "บาท";
            if ((Number[1] == "0") || (Number[1] == "00"))
            {
                BahtText += "ถ้วน";
            } else
            {
                DecimalLen = Number[1].length - 0;
                for (var i = 0; i < DecimalLen; i++)
                {
                    var tmp = Number[1].substring(i, i + 1) - 0;
                    if (tmp != 0)
                    {
                        if ((i == (DecimalLen - 1)) && (tmp == 1))
                        {
                            BahtText += "เอ็ด";
                        } else
                        if ((i == (DecimalLen - 2)) && (tmp == 2))
                        {
                            BahtText += "ยี่";
                        } else
                        if ((i == (DecimalLen - 2)) && (tmp == 1))
                        {
                            BahtText += "";
                        } else
                        {
                            BahtText += NumberArray[tmp];
                        }
                        BahtText += DigitArray[DecimalLen - i - 1];
                    }
                }
                BahtText += "สตางค์";
            }
            return BahtText;
        }
    }
}

function CheckNumber(Number) {
    var decimal = false;
    Number = Number.toString();
    Number = Number.replace(/ |,|บาท|฿/gi, '');
    for (var i = 0; i < Number.length; i++)
    {
        if (Number[i] == '.') {
            decimal = true;
        }
    }
    if (decimal == false) {
        Number = Number + '.00';
    }
    return Number
}

function getMonth(month) {
    var monthNames = ["ม.ค.", "ก.พ", "มี.ค", "เม.ย", "พ.ค", "มิ.ย",
        "ก.ค", "ส.ค", "ก.ย", "ต.ค", "พ.ย", "ธ.ค"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }
}


function get_homedata_by_order_no(order_no) {


    var detailhome = '';
    $.ajax({
        url: 'projectplan/getHomeDataByOrderNo',
        type: 'POST',
        data: {
            order_no: order_no
        },
        async: false,
        dataType: 'json'
    }).done(function (data) {
        console.log(data);
        detailhome = data.data[0].h_name + "," + data.data[0].h_price;

    });

    return detailhome;
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}




//fnction แสดงตัวอย่างที่ดิน
function get_data_view_plan(promiseid) {
    var count;
    var data;
    $.ajax({
        url: 'promise/count_person_plan',
        type: 'POST',
        data: {
            promise_id: promiseid
        },
        'success': function (data) {
            count = data;
            console.log("get_data_view_plan count: " + data);

            $.ajax({
                'type': 'POST',
                'data': {
                    promise_id: promiseid
                },
                async: true,
                'url': 'promise/get_person_plan_view',
                'success': function (data) {

                    console.log("get_data_view_plan count2: " + count);

                    if (count === 1) {

                        //$('#view_promise_date_plan').html($('#hidden_date_due').val());
                        console.log("xx: " + data.data[0].title + data.data[0].fname);
                        $('#view_fullname').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname);
                        $('#view_homeno').html(" " + data.data[0].home_no);
                        $('#view_street').html(data.data[0].street);
                        $('#view_subdistrict').html(data.data[0].subdistrict);
                        $('#view_district').html(data.data[0].district);
                        $('#view_province').html(data.data[0].province);
                        $('#view_postcode').html(data.data[0].postcode);
                        $('#view_phone').html(data.data[0].phone);
                    } else if (count === 2) {


                        $('#view_promise_date_plan').html($('#hidden_date_due').val());
                        $('#view_fullname').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname + " และ " + data.data[1].title + data.data[1].fname + " " + data.data[1].lname);
                        $('#view_homeno').html(" " + data.data[0].home_no);
                        $('#view_street').html(data.data[0].street);
                        $('#view_subdistrict').html(data.data[0].subdistrict);
                        $('#view_district').html(data.data[0].district);
                        $('#view_province').html(data.data[0].province);
                        $('#view_postcode').html(data.data[0].postcode);
                        $('#view_phone').html(data.data[0].phone);

                        $('#view_homeno2').html(" " + data.data[1].home_no);
                        $('#view_street2').html(data.data[1].street);
                        $('#view_subdistrict2').html(data.data[1].subdistrict);
                        $('#view_district2').html(data.data[1].district);
                        $('#view_province2').html(data.data[1].province);
                        $('#view_postcode2').html(data.data[1].postcode);
                        $('#view_phone2').html(data.data[1].phone);

                        $('#view_homeno2_text').show();
                        $('#view_street2_text').show();
                        $('#view_subdistrict2_text').show();
                        $('#view_district2_text').show();
                        $('#view_province2_text').show();
                        $('#view_postcode2_text').show();
                        $('#view_phone2_text').show();

                        $('#view_homeno2').show();
                        $('#view_street2').show();
                        $('#view_subdistrict2').show();
                        $('#view_district2').show();
                        $('#view_province2').show();
                        $('#view_postcode2').show();
                        $('#view_phone2').show();

                    } else if (count === 3) {
                        $('#view_promise_date_plan').html($('#hidden_date_due').val());
                        $('#view_fullname').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname + " , " + data.data[1].title + data.data[1].fname + " " + data.data[1].lname + " และ " + data.data[2].title + data.data[2].fname + " " + data.data[2].lname);
                        $('#view_homeno').html(data.data[0].home_no);
                        $('#view_street').html(data.data[0].street);
                        $('#view_subdistrict').html(data.data[0].subdistrict);
                        $('#view_district').html(data.data[0].district);
                        $('#view_province').html(data.data[0].province);
                        $('#view_postcode').html(data.data[0].postcode);
                        $('#view_phone').html(data.data[0].phone);

                        $('#view_homeno2').html(data.data[1].home_no);
                        $('#view_street2').html(data.data[1].street);
                        $('#view_subdistrict2').html(data.data[1].subdistrict);
                        $('#view_district2').html(data.data[1].district);
                        $('#view_province2').html(data.data[1].province);
                        $('#view_postcode2').html(data.data[1].postcode);
                        $('#view_phone2').html(data.data[1].phone);

                        $('#view_homeno3').html(data.data[2].home_no);
                        $('#view_street3').html(data.data[2].street);
                        $('#view_subdistrict3').html(data.data[2].subdistrict);
                        $('#view_district3').html(data.data[2].district);
                        $('#view_province3').html(data.data[2].province);
                        $('#view_postcode3').html(data.data[2].postcode);
                        $('#view_phone3').html(data.data[2].phone);

                        $('#view_homeno2_text').show();
                        $('#view_street2_text').show();
                        $('#view_subdistrict2_text').show();
                        $('#view_district2_text').show();
                        $('#view_province2_text').show();
                        $('#view_postcode2_text').show();
                        $('#view_phone2_text').show();

                        $('#view_homeno2').show();
                        $('#view_street2').show();
                        $('#view_subdistrict2').show();
                        $('#view_district2').show();
                        $('#view_province2').show();
                        $('#view_postcode2').show();
                        $('#view_phone2').show();

                        $('#view_homeno3_text').show();
                        $('#view_street3_text').show();
                        $('#view_subdistrict3_text').show();
                        $('#view_district3_text').show();
                        $('#view_province3_text').show();
                        $('#view_postcode3_text').show();
                        $('#view_phone3_text').show();


                        $('#view_homeno3').show();
                        $('#view_street3').show();
                        $('#view_subdistrict3').show();
                        $('#view_district3').show();
                        $('#view_province3').show();
                        $('#view_postcode3').show();
                        $('#view_phone3').show();
                    }

                    $.ajax({
                        'type': 'POST',
                        'data': {
                            promise_id: promiseid
                        },
                        async: true,
                        'url': 'promise/get_detail_by_promise_id',
                        'success': function (data2) {

                            if (count === 1) {

                                //plan
                                $('#view_total_plan').html(data2.data[0].plan_total);
                                $('#view_plan_number').html(data2.data[0].plan);
                                $('#view_land_no').html(data2.data[0].land_n0);
                                var order_no = data2.data[0].agreement_id;
                                var master_plan = data2.data[0].plan_master;
                                var planid = data2.data[0].plan;
                                getDeatilplan(order_no, planid, master_plan);

                                //price
                                $('#view_pricetotal').html(data2.data[0].price_total);
                                $('#view_pricetotal_char').html(ThaiBaht(data2.data[0].price_total));
                                $('#view_pricepay').html(data2.data[0].price_pay);
                                $('#view_pricepay_char').html(ThaiBaht(data2.data[0].price_pay));
                                $('#view_pay_price_promise').html(data2.data[0].pay_price_promise);
                                $('#view_pay_price_promise_char').html(ThaiBaht(data2.data[0].pay_price_promise));
                                $('#view_pay_price_promise_result').html(data2.data[0].pay_price_promise_result);
                                $('#view_pay_price_promise_result_char').html(ThaiBaht(data2.data[0].pay_price_promise_result));



                            } else if (count === 2) {

                                //plan
                                $('#view_total_plan').html($('#plan_total').val());
                                $('#view_plan_number').html($('#hidden_planssave').val());
                                $('#view_land_no').html($('#landid').val());

                                var order_no = $('#hiden_person_order_no').val();
                                var master_plan = $('#hiden_person_master_plan').val();
                                var planid = $('#hidden_planssave').val();
                                getDeatilplan(order_no, planid, master_plan);

                                //price
                                $('#view_pricetotal').html($('#pricetotal').val());
                                $('#view_pricetotal_char').html(ThaiBaht($('#pricetotal').val()));
                                $('#view_pricepay').html($('#pricepay').val());
                                $('#view_pricepay_char').html(ThaiBaht($('#pricepay').val()));
                                $('#view_pay_price_promise').html($('#pay_price_promise').val());
                                $('#view_pay_price_promise_char').html(ThaiBaht($('#pay_price_promise').val()));
                                $('#view_pay_price_promise_result').html($('#pay_price_promise_result').val());
                                $('#view_pay_price_promise_result_char').html(ThaiBaht($('#pay_price_promise_result').val()));



                                $('#view_homeno2_text').show();
                                $('#view_street2_text').show();
                                $('#view_subdistrict2_text').show();
                                $('#view_district2_text').show();
                                $('#view_province2_text').show();
                                $('#view_postcode2_text').show();
                                $('#view_phone2_text').show();

                                $('#view_homeno2').show();
                                $('#view_street2').show();
                                $('#view_subdistrict2').show();
                                $('#view_district2').show();
                                $('#view_province2').show();
                                $('#view_postcode2').show();
                                $('#view_phone2').show();



                            } else if (count === 3) {



                                //plan
                                $('#view_total_plan').html($('#plan_total').val());
                                $('#view_plan_number').html($('#hidden_planssave').val());
                                $('#view_land_no').html($('#landid').val());
                                var order_no = $('#hiden_person_order_no').val();
                                var master_plan = $('#hiden_person_master_plan').val();
                                var planid = $('#hidden_planssave').val();
                                getDeatilplan(order_no, planid, master_plan);

                                //price
                                $('#view_pricetotal').html($('#pricetotal').val());
                                $('#view_pricetotal_char').html(ThaiBaht($('#pricetotal').val()));
                                $('#view_pricepay').html($('#pricepay').val());
                                $('#view_pricepay_char').html(ThaiBaht($('#pricepay').val()));
                                $('#view_pay_price_promise').html($('#pay_price_promise').val());
                                $('#view_pay_price_promise_char').html(ThaiBaht($('#pay_price_promise').val()));
                                $('#view_pay_price_promise_result').html($('#pay_price_promise_result').val());
                                $('#view_pay_price_promise_result_char').html(ThaiBaht($('#pay_price_promise_result').val()));




                            }
                            get_data_view_home(promiseid);
                        }
                    });
                }
            });
        }
    });

}

function getDeatilplan(orderno, planid, master_plan) {
    var tarngwa = '';
    var price_tarngwa = '';
    var view_tarangwa_char = '';
    var tarngwaArray = [];
    var tarngwaArray1 = [];
    var tarngwaArray2 = [];
    var planarr = planid.split(',');
    console.log("getDeatilplan orderno: " + orderno);
    console.log("getDeatilplan planid: " + planid);
    console.log("getDeatilplan master_plan: " + master_plan);
    for (var i = 0; i < planarr.length; i++) {
        $.ajax({
            url: 'promise/getorder_by_plan',
            type: 'POST',
            data: {
                orderno: orderno,
                planid: planarr[i].trim(),
                master_plan: master_plan
            },
            async: false,
            dataType: 'json',
            'success': function (data) {
                tarngwa = data.data[0].wa;
                price_tarngwa = data.data[0].price_tarangwa;
                view_tarangwa_char = ThaiBaht(data.data[0].price_tarangwa);
                tarngwaArray.push(tarngwa);
                tarngwaArray1.push(price_tarngwa);
                tarngwaArray2.push(view_tarangwa_char);
            }
        });
    }

    var t1 = '';
    var t2 = '';
    var t3 = '';
    for (var i = 0; i < tarngwaArray.length; i++) {
        t1 += tarngwaArray[i] + ",";
    }

    for (var i = 0; i < tarngwaArray1.length; i++) {
        t2 += tarngwaArray1[i] + ",";
    }
    for (var i = 0; i < tarngwaArray2.length; i++) {
        t3 += "(" + tarngwaArray2[i] + ")" + ",";
    }

    $('#view_tarangwa').html(removeLastComma(t1));

    $('#view_home_tarangwa').html(removeLastComma(t1) + tarngwaArray.length > 1 ? " (ตามลำดับ) " : "");

    $('#view_price_tarangwa').html(removeLastComma(t2));
    $('#view_tarangwa_char').html(removeLastComma(t3));

}

function removeLastComma(strng) {
    var n = strng.lastIndexOf(",");
    var a = strng.substring(0, n)
    return a;
}

//fnction แสดงตัวอย่างบ้าน
function get_data_view_home(promiseid) {
    var count;
    $.ajax({
        url: 'promise/count_person_home',
        type: 'POST',
        data: {
            promise_id: promiseid
        },
        'success': function (data) {
            count = data;
            console.log("get_data_view_home count: " + data);

            $.ajax({
                'type': 'POST',
                'data': {
                    promise_id: promiseid
                },
                async: true,
                'url': 'promise/get_person_home_view',
                'success': function (data) {



                    if (count === 1) {
                        $('#cardcen1').html("(" + data.data[0].title + data.data[0].fname + " " + data.data[0].lname + ")");
                        $('#view_fullname_home').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname);
                        $('#view_pid_home').html(data.data[0].pid);

                        var exdate = data.data[0].expiredDate;
                        //setExDate(exdate);
                        $('#view_expridate_home').html(setExDate(exdate));
                        $('#view_address_home').html("บ้านเลขที่ " + data.data[0].home_no + " หมู่ " + data.data[0].moo + " ตำบล " + data.data[0].subdistrict + " อำเภอ " + data.data[0].district + " จังหวัด " + data.data[0].province + " รหัสไปรษณีษ์ " + data.data[0].postcode);
                        $('#view_phone_home').html("หมายเลขโทรศัพท์ " + data.data[0].phone);

                    } else if (count === 2) {

                        $('#cardcen1').html("(" + data.data[0].title + data.data[0].fname + " " + data.data[0].lname);
                        $('#cardcen2').html("และ" + data.data[1].title + data.data[1].fname + " " + data.data[1].lname + ")");
                        $('#view_fullname_home').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname + " , " + data.data[1].title + data.data[1].fname + " " + data.data[1].lname + " ตามลำดับ ");

                        $('#view_pid_home').html(data.data[0].pid + " , " + data.data[1].pid + " ตามลำดับ ");

                        $('#view_expridate_home').html(setExDate(data.data[0].expiredDate) + " , " + setExDate(data.data[1].expiredDate) + " ตามลำดับ ");
                        $('#view_address_home').html("บ้านเลขที่ " + data.data[0].home_no + " หมู่ " + data.data[0].moo + " ตำบล " + data.data[0].subdistrict + " อำเภอ " + data.data[0].district + " จังหวัด " + data.data[0].province + " รหัสไปรษณีษ์ " + data.data[0].postcode
                                + ", " +
                                "บ้านเลขที่ " + data.data[1].home_no + " หมู่ " + data.data[1].moo + " ตำบล " + data.data[1].subdistrict + " อำเภอ " + data.data[1].district + " จังหวัด " + data.data[1].province + " รหัสไปรษณีษ์ " + data.data[1].postcode + " ตามลำดับ ");
                        $('#view_phone_home').html("หมายเลขโทรศัพท์ " + data.data[0].phone + " , " + data.data[1].phone + "");




                    } else if (count === 3) {
                        $('#view_fullname_home').html(data.data[0].title + data.data[0].fname + " " + data.data[0].lname + " , " + data.data[1].title + data.data[1].fname + " " + data.data[1].lname + " , " + data.data[2].title + data.data[2].fname + " " + data.data[2].lname + " ตามลำดับ ");
                        $('#view_pid_home').html(data.data[0].pid + " , " + data.data[1].pid + " ตามลำดับ ");
                        $('#view_expridate_home').html(setExDate(data.data[0].expiredDate) + " , " + setExDate(data.data[1].expiredDate) + " , " + setExDate(data.data[2].expiredDate) + " ตามลำดับ ");
                        $('#view_address_home').html("บ้านเลขที่ " + data.data[0].home_no + " หมู่ " + data.data[0].moo + " ตำบล " + data.data[0].subdistrict + " อำเภอ " + data.data[0].district + " จังหวัด " + data.data[0].province + " รหัสไปรษณีษ์ " + data.data[0].postcode
                                + ", " +
                                "บ้านเลขที่ " + data.data[1].home_no + " หมู่ " + data.data[1].moo + " ตำบล " + data.data[1].subdistrict + " อำเภอ " + data.data[1].district + " จังหวัด " + data.data[1].province + " รหัสไปรษณีษ์ " + data.data[1].postcode
                                + ", " +
                                "บ้านเลขที่ " + data.data[2].home_no + " หมู่ " + data.data[2].moo + " ตำบล " + data.data[2].subdistrict + " อำเภอ " + data.data[2].district + " จังหวัด " + data.data[2].province + " รหัสไปรษณีษ์ " + data.data[2].postcode + " ตามลำดับ ");
                        $('#view_phone_home').html("หมายเลขโทรศัพท์ " + data.data[0].phone + " , " + data.data[1].phone + " , " + data.data[2].phone + "");



                    }

                    $.ajax({
                        'type': 'POST',
                        'data': {
                            promise_id: promiseid
                        },
                        async: true,
                        'url': 'promise/get_detail_by_promisehome_id',
                        'success': function (data) {

                            $('#view_home_plan_number').html(data.data[0].plan);
                            $('#view_home_plan_number2').html(data.data[0].plan);
                            $('#view_home_land_no').html(data.data[0].land_no);
                            $('#view_home_land_no2').html(data.data[0].land_no2);
                            $('#view_home_price').html(data.data[0].home_price);
                            $('#view_home_price_char').html(ThaiBaht(data.data[0].home_price));

                            //console.log("xxxxz: " + $('#pay_price_home').val());
                            $('#view_pay_price_home').html(data.data[0].pay_price_home);
                            $('#view_pay_price_home_char').html(ThaiBaht(data.data[0].pay_price_home));
                            $('#view_pay_price_pirod1').html(data.data[0].pay_price_pirod1);
                            $('#view_pay_price_pirod1_char').html(ThaiBaht(data.data[0].pay_price_pirod1));
                            $('#view_pay_price_pirod2').html(data.data[0].pay_price_pirod2);
                            $('#view_pay_price_pirod2_char').html(ThaiBaht(data.data[0].pay_price_pirod2));
                            $('#view_pay_price_pirod3').html(data.data[0].pay_price_pirod3);
                            $('#view_pay_price_pirod3_char').html(ThaiBaht(data.data[0].pay_price_pirod3));


                        }
                    });

                    get_filename_card_by_promise_id_tab1(promiseid, '2', '1');
                    get_filename_card_by_promise_id_tab2(promiseid, '2', '2');
                    var mode = 'iframe'; //popup
                    var close = mode == "popup";
                    var options = {
                        mode: mode,
                        popClose: close,
                        header: false,
                        fontsize: 10
                    };
                    $("div.printableArea1").printArea(options);
                }
            });
        }
    });
}


function setExDate(exdate) {

    var result = "";
    if (exdate !== null && exdate !== '') {
        console.log("exdate: " + exdate);
        var year = exdate.substr(0, 4);
        var month = exdate.substr(4, 2);
        var day = exdate.substr(6, 8);
        console.log("year: " + year + " month: " + month + " day: " + day);
        var srt_month = getMonth(month);
        var result = day + " " + srt_month + " " + year;
    } else {
        result = "";
    }

    return result;
}

function getMonthFull(month) {
    var monthNames = ["มกราคม.", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
        "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }
}

function g_toExcelSummary(i_id) {
    gg_toExcelSummary($("#" + i_id));
}
function gg_toExcelSummary(i_trArr, tableHtml) {
    var reportname = '';
    var tab_text = (tableHtml === undefined) ? $(i_trArr).html() : tableHtml;
    var th = $(i_trArr).find("thead tr");
    var id = table = ($(i_trArr).attr("id"));
    var tbody = $(i_trArr).find("tbody tr");
    var table = ($(i_trArr).attr("style") !== undefined ? "<table style'border-style: ridge;'>" : "<table>");


    if (th.length > 0) {


        reportname = 'รายงานการขาย' + " " + getDateTime();
        table += "<tr><td>รายงานการขาย</td></tr>";
        table += "<tr>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;' rowspan=\"2\" align=\"cener\">ลำดับ</td>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;' rowspan=\"2\" align=\"cener\">ชื่อ-นามสกุล</td>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;' rowspan=\"2\" align=\"cener\">เลขที่สัญญา</td>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;' colspan=\"3\" align=\"cener\">ข้อมูลการขาย</td>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;' rowspan=\"2\" align=\"cener\">สัญญาจอง</td>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;' rowspan=\"2\" align=\"cener\">สัญญาขาย</td>";

        table += "</tr>";

        table += "<tr>";
        table += "<td  style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;' align=\"cener\">วันที่จอง</td>";
        table += "<td  style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;' align=\"cener\">วันทีขาย</td>";
        table += "<td  style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;' align=\"cener\">ราคาขาย(บาท)</td>";
        table += "</tr>";


        $(i_trArr).find("tbody tr").each(function (i, ele) {
            table += "<tr>";

            $(ele).find("td").each(function (k, l) {

                var dnone = $(this).attr('class');
                //console.log("dnone: " + dnone);

                if ($(l).text() === 'No data available in table') {
                    table += "<td colspan=\"8\" style='background-color: #b6f4fc; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;'>" + 'ไม่พบข้อมูล' + "</td>";
                } else {
                    if (dnone !== ' d-none') {
                        table += "<td style='background-color: #b6f4fc; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;'>" + $(l).text() + "</td>";
                    }

                }

            });
            table += "</tr>";


        });

        table += "</table>";
        tab_text = table;
    }
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    var sa = null;
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        // If Internet Explorer    
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, "ABSExport.xls");
    } else {
        //other browser not tested on IE 11
        if ((navigator.userAgent + "").indexOf("Mozilla") < 0) {
            var link = document.createElement('a');
            link.download = reportname + ".xls";
            link.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
            link.click();

        } else {

            var link = document.createElement('a');
            link.download = reportname + ".xls";
            link.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
            link.click();
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
        }

    }

    return (sa);
}

function getDateTime() {
    var today = new Date();
    var day = today.getDate() + "";
    var month = (today.getMonth() + 1) + "";
    var year = today.getFullYear() + "";
    var hour = today.getHours() + "";
    var minutes = today.getMinutes() + "";

    var result = year + "-" + month + "-" + day;
    return result;
}

function get_order_by_orderno(order_no) {

    console.log("get_order_by_orderno order_no: " + order_no);
    var order = [];
    $.ajax({
        url: 'projectplan/getorder_by_orderno',
        type: 'POST',
        data: {order_no: order_no},
        async: false,
        dataType: 'json',
    }).done(function (data) {
        console.log("data: " + data);
        if (data === null) {
            order = [];
        } else {
            order = data;
        }
    });

    return order;
}


function get_filename_card_by_promise_id_tab1(promise_id, type, promise_type) {

    $.ajax({
        url: 'promise/getFileSmartCardByPromiseId',
        type: 'POST',
        data: {
            promise_id: promise_id,
            type: type,
            promise_type: promise_type
        },
        async: false,
        dataType: 'json',
        'success': function (data) {
            console.log("#### get_filename_card_by_promise_id length:  " + data.data.length);

            var img1 = '';
            var img2 = '';
            //$('#imagetCardTab1_1').html(img1).append(img2);
            if (data.data.length === 1) {
                img1 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[0].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                $('#imagetCardTab1_1').html(img1);
            } else {
                img1 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[0].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                img2 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[1].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                $('#imagetCardTab1_1').html(img1);
                $('#imagetCardTab1_2').html(img2);
            }
        }
    });
}

function get_filename_card_by_promise_id_tab2(promise_id, type, promise_type) {

    console.log("#### 2 promise_id:  " + promise_id);
    console.log("#### 2 type:  " + type);
    console.log("#### 2 promise_type:  " + promise_type);
    $.ajax({
        url: 'promise/getFileSmartCardByPromiseId',
        type: 'POST',
        data: {
            promise_id: promise_id,
            type: type,
            promise_type: promise_type
        },
        async: false,
        dataType: 'json',
        'success': function (data) {
            console.log("#### get_filename_card_by_promise_id_tab2 length:  " + data.data.length);

            var img1 = '';
            var img2 = '';

            //$('#imagetCardTab1_1').html(img1).append(img2);
            if (data.data.length === 1) {
                img1 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[0].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                $('#imagetCardTab2_1').html(img1);

            } else {
                img1 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[0].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";
                img2 = "<img id=\"imagecorp\" src=\"../\scanpromisecard/" + data.data[1].file_name_scan + "\" style=\"width: 360px; height: 350px;\" alt=\"Chicago\" class=\"boder-image\">";

                $('#imagetCardTab2_1').html(img1);
                $('#imagetCardTab2_2').html(img2);
            }
        }
    });
}