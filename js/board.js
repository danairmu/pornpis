$(document).ready(function () {

    loadChart();
    loadDatatable();


    $('#btn_export').on('click', function () {
        g_toExcelSummary("rstable");
    });
});


function loadChart() {
    var userId = $('#saleFilter').val();

    $.ajax({
        type: 'get',
        url: 'board/get_dashboard_data?user_id=' + userId,
        success: function (data) {
            var chart_data = generate_chart_data(data['data']);
            console.log(chart_data);
            init_chart(chart_data, data['data'].length);
        }
    });
}

function generate_chart_data(data) {
    var chart_data = [];
    chart_data['months'] = generate_months(data);
    chart_data['series'] = generate_series(data);
    return chart_data;
}

function generate_months(data) {
    var months = [];

    if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            months[i] = data[i].month;
        }
    } else {
        var today = new Date();
        var yyyy = today.getFullYear();
        var spl = '-';
        for (var m = 0; m < 12; m++) {
            months[m] = yyyy + spl + twoDigits(m + 1);
        }
    }

    return months;
}

function generate_series(data) {
    var result_arr = [];
    var order_arr = [];
    var agreement_arr = [];

    if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            order_arr[i] = data[i].order;
            agreement_arr[i] = data[i].agreement;
        }
    } else {
        for (var m = 0; m < 12; m++) {
            order_arr[m] = 0;
            agreement_arr[m] = 0;
        }
    }

    result_arr[0] = order_arr;
    result_arr[1] = agreement_arr;
    return result_arr;
}

function init_chart(chart_data, maxValue) {

    new Chartist.Bar('#dashboardChart', {
        labels: chart_data['months'],
        series: chart_data['series']
    }, {
        seriesBarDistance: 25,
        stackBars: false,
        high: getMaxValue(chart_data['series']),

        axisX: {
            showLabel: true,
            showGrid: false,
            seriesBarDistance: 25,

            
        },
        axisY: {
            labelInterpolationFnc: function (value) {
                return (value);
            },
            onlyInteger: true,
            showLabel: true,
            showGrid: true,
            seriesBarDistance: 25,

        },
        plugins: [
            Chartist.plugins.tooltip(),
            Chartist.plugins.legend({
                legendNames: ['จำนวนการจอง', 'จำนวนการขาย'],
            })
        ]
    }).on('draw', function (data) {
        console.log("maxValue: " + maxValue);
        if (data.type === 'bar') {

            data.element.attr({
                style: 'stroke-width: 20px',
                //x1: '250', 
                //x2: '250',
            });
        }
    });

}

function loadDatatable() {
    var t = $('#rstable').DataTable({
        ajax: 'board/get_dashboard_data',
//        searching: true,
//        ordering: false,
//        lengthChange: false,
//        paging: true,
        searching: true,
        ordering: false,
        lengthChange: false,
        "processing": false,
        "serverSide": false,
        "bFilter": false,
        "bLengthChange": false,
        "paging": true,
        "info": false,
        pageLength: 5,
        columns: [{
                'className': 'text-center',
                'data': null,
                'render': function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },
            {
                'className': 'text-center',
                'data': 'month'
            },
            {
                'data': 'salename'
            },
            {
                'className': 'text-right',
                'data': 'order'
            },
            {
                'className': 'text-right',
                'data': 'agreement'
            },
            {
                'className': 'd-none',
                'data': 'saleid'
            },
        ],
    });

    t.on('order.dt search.dt', function () {
        t.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    //-- Custom Filter
    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var match = true;
                var saleFilter = $('#saleFilter').val();
                if (saleFilter) {
                    if (saleFilter !== data[5]) {
                        match = false;
                    }
                }

                // if (match) {
                // 	match = isBetweenDate(data[2], $('#startDate').val(), $('#endDate').val());
                // }

                // if (match) {
                // 	if ($('#reportType1').prop('checked')) {
                // 		match = data[3] == '';
                // 	} else {
                // 		match = data[3] != '';
                // 	}
                // }
                return match;
            }
    );

    $('#saleFilter').on('change', function () {
        loadChart();
        t.draw();
    });
}

function getMaxValue(values) {
    var max = 0;
    for (var i = 0; i < values.length; i++) {
        for (var j = 0; j < values[i].length; j++) {
            var v = parseInt(values[i][j]);
            if (v > max) {
                max = v;
            }
        }
    }

    //default
    if (max == 0) {
        max = 10;
    }
    console.log("max: " + max);
    return max;
}

function twoDigits(n) {
    return n < 10 ? '0' + n : n;
}

function g_toExcelSummary(i_id) {
    gg_toExcelSummary($("#" + i_id));
}
function gg_toExcelSummary(i_trArr, tableHtml) {
    var reportname = '';
    var tab_text = (tableHtml === undefined) ? $(i_trArr).html() : tableHtml;
    var th = $(i_trArr).find("thead tr");
    var id = table = ($(i_trArr).attr("id"));
    var tbody = $(i_trArr).find("tbody tr");
    var table = ($(i_trArr).attr("style") !== undefined ? "<table style'border-style: ridge;'>" : "<table>");
    console.log("id: " + id);



    if (th.length > 0) {


        reportname = 'รายงานการจอง/การขาย ' + " " + getDateTime();
        table += "<tr><td>รายงานการจอง/การขาย</td></tr>";
        table += "<tr>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;'  align=\"cener\">ลำดับ</td>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;'  align=\"cener\">ปี-เดือน</td>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;'  align=\"cener\">ชื่อผู้ขาย</td>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;'  align=\"cener\">จำนวนการจอง</td>";
        table += "<td style='background-color: #28a745; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;'  align=\"cener\">จำนวนการขาย</td>";

        table += "</tr>";




        $(i_trArr).find("tbody tr").each(function (i, ele) {
            table += "<tr>";

            $(ele).find("td").each(function (k, l) {

                var dnone = $(this).attr('class');

                if ($(l).text() === 'No data available in table') {
                    table += "<td colspan=\"5\" style='background-color: #b6f4fc; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;'>" + 'ไม่พบข้อมูล' + "</td>";
                } else {
                    if (dnone !== ' d-none') {
                        table += "<td style='background-color: #b6f4fc; border-style: solid; border-width: thin; text-align: center; vertical-align: middle;'>" + $(l).text() + "</td>";
                    }

                }

            });
            table += "</tr>";


        });

        table += "</table>";
        tab_text = table;
    }
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    var sa = null;
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        // If Internet Explorer    
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, "ABSExport.xls");
    } else {
        //other browser not tested on IE 11
        if ((navigator.userAgent + "").indexOf("Mozilla") < 0) {
            var link = document.createElement('a');
            link.download = reportname + ".xls";
            link.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
            link.click();

        } else {

            var link = document.createElement('a');
            link.download = reportname + ".xls";
            link.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
            link.click();
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
        }

    }

    return (sa);
}
function getDateTime() {
    var today = new Date();
    var day = today.getDate() + "";
    var month = (today.getMonth() + 1) + "";
    var year = today.getFullYear() + "";
    var hour = today.getHours() + "";
    var minutes = today.getMinutes() + "";

    var result = year + "-" + month + "-" + day;
    return result;
}