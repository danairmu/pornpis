<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Board_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_dashboard_data($user_id = '')
    {
        $result_arr = array();
        $month_arr = $this->get_month_array();
        $user_arr = $this->get_user_arr($user_id);

        $index = 0;
        for ($m = 0; $m < count($month_arr); $m++) {
            $month = $month_arr[$m]['month'];

            for ($i = 0; $i < count($user_arr); $i++) {
                $id = $user_arr[$i]['id'];
                $fullname = $user_arr[$i]['fullname'];
                $order = $this->count_agreement_in_month($month, $id); //รายงานการจองง
                $agreement = $this->count_promise_in_month($month, $id); // รายงานการขาย
                if ($order > 0 || $agreement > 0) {
                    $row = array(
                        'month' => $month,
                        'saleid' => $id,
                        'salename' => $fullname,
                        'order' => $order,
                        'agreement' => $agreement,
                    );
                    $result_arr[$index++] = $row;
                }
            }
            
        }

        return $result_arr;
    }

    public function get_month_array()
    {
        $this->db->select('DISTINCT(DATE_FORMAT(ps_order.create_date, "%Y-%m")) as month');
        $this->db->from('ps_order');
        $this->db->where('ps_order.used','Y');
        $this->db->order_by('ps_order.create_date');
        return $this->db->get()->result_array();
    }

    //chart ps_agreement
    public function count_agreement_in_month($month, $user_id) {
        $this->db->select('count(DISTINCT ps_agreement.order_no) as total');
        $this->db->from('ps_agreement');
        $this->db->where('ps_agreement.used','Y');
        $this->db->where('date_format(ps_agreement.create_date, "%Y-%m") = "' . $month . '"');

        if (!empty($user_id)) {
            $this->db->where('ps_agreement.create_by', $user_id);
        }

        $result = $this->db->get()->row();
        return !empty($result) ? $result->total : 0; 
    }

    //chat promise
    public function count_promise_in_month($month, $user_id) {
        $this->db->select('COUNT(DISTINCT ps_promise.promise_id) as total');
        $this->db->from('ps_promise');
        $this->db->where('date_format(ps_promise.create_date, "%Y-%m") = "' . $month . '"');
        $this->db->join('ps_promise_home', 'ps_promise_home.promise_home_id = ps_promise.promise_id');
        $this->db->where('ps_promise.used','Y');
        
        if (!empty($user_id)) {
            $this->db->where('ps_promise.create_by', $user_id);
        }

        $result = $this->db->get()->row();
        return !empty($result) ? $result->total : 0; 
    }

    public function get_user_arr($user_id = '')
    {
        $this->db->select('users.id, concat(users.first_name, " ", users.last_name) as fullname');
        $this->db->from('users');

        if (!empty($user_id)) {
            $this->db->where('users.id', $user_id);
        }

        return $this->db->get()->result_array();
    }
}
