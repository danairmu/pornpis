<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Managemen_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->table_ps_area = 'ps_plan';
        $this->table_ps_home = 'ps_home';
    }

    public function get_data_plans_list($master_plan)
    {
        $this->db->select("ps_plan.id, ps_plan.p_name, ps_plan.p_ri, ps_plan.p_ngan, ps_plan.p_wa, ps_home.h_name as home_name");
        $this->db->select("ps_plan.p_width, ps_plan.p_height, ps_plan.p_status, ps_plan.p_comment, ps_plan.p_price, ps_plan.p_price_tarangwa");
        $this->db->from("ps_plan");
        $this->db->join('ps_home', 'ps_home.id = ps_plan.p_home_id ','left');
        $this->db->where("ps_plan.used", 'Y');
        $this->db->where("ps_plan.plan_id", $master_plan);
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }
    
    //home list
    public function get_data_homelist()
    {
        $this->db->select("ps_home.id, ps_home.h_name, ps_home.h_type, ps_home.h_price, ps_home.h_area, ps_home.h_comment");
        $this->db->from("ps_home");
        $this->db->where("ps_home.used", 'Y');
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }
    
    public function get_data_plan_by_id($plan_name, $plan_master)
    {
        $this->db->select("ps_plan.id, ps_plan.p_name, ps_plan.p_ri, ps_plan.p_ngan, ps_plan.p_wa, ps_plan.p_corner_price, ps_plan.p_home_id");
        $this->db->select("ps_plan.p_width, ps_plan.p_height, ps_plan.p_status, ps_plan.p_comment, ps_plan.p_price, ps_plan.p_price_tarangwa, ps_plan.p_tarangwa");
        $this->db->from("ps_plan");
        $this->db->where("ps_plan.used", 'Y');
        $this->db->where("ps_plan.p_name", $plan_name);
        $this->db->where("ps_plan.plan_id", $plan_master);
        
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
    
     public function get_data_home_by_id($id){
        $this->db->select("ps_home.id, ps_home.h_name, ps_home.h_type, ps_home.h_price, ps_home.h_area, ps_home.h_comment");
        $this->db->from("ps_home");
        $this->db->where("ps_home.used", 'Y');
        $this->db->where("ps_home.id", $id);
        
        
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
    public function update_plan($plan_id, $plan_master, $data) {

        $this->db->where('p_name', $plan_id);
        $this->db->where('plan_id', $plan_master);
        $this->db->update($this->table_ps_area, $data);
        return $this->db->affected_rows();
    }
    
    
    public function update_home($id, $data) {

        $this->db->where('id', $id);
        $this->db->update($this->table_ps_home, $data);
        return $this->db->affected_rows();
    }
    
}
