<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agreement_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->table_ps_argreement = 'ps_agreement';
        $this->table_ps_person = 'ps_person';
    }

    public function insertArgreement($data) {
       
        $this->db->insert($this->table_ps_argreement, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }
    
    public function insertPsPerson($data) {
       
        $this->db->insert($this->table_ps_person, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }
    
    public function get_agreementid_by_order_no($orderno, $master_plan) {
        $this->db->select("ps_agreement.id, ps_agreement.agreement_seq, ps_agreement.agreement_id, ps_agreement.pid, ps_agreement.order_no, ps_agreement.title, ps_agreement.fname, ps_agreement.create_date, ps_agreement.plan_price");
        $this->db->select("ps_agreement.lname, ps_agreement.age, ps_agreement.home_no, ps_agreement.moo, ps_agreement.soi, ps_agreement.street, ps_agreement.subdistrict, ps_agreement.district, ps_agreement.province, ps_agreement.postcode");
        $this->db->select("ps_agreement.phone, ps_agreement.email, ps_agreement.plan, ps_agreement.plan_total, ps_agreement.price_tarangwa, ps_agreement.price_total, ps_agreement.price_pay, ps_agreement.date_agreenment, DATE_FORMAT(DATE_ADD(ps_agreement.date_agreenment, INTERVAL 543 YEAR),'%d/%m/%Y') as date_agreenment_thai, DATE_FORMAT(DATE_ADD(ps_agreement.create_date, INTERVAL 543 YEAR),'%d/%m/%Y') as date_create_thai");
        $this->db->select("ps_agreement.home_id, ps_agreement.home_price, ps_agreement.home_name");
        
        $this->db->where("ps_agreement.order_no", $orderno);
        $this->db->where("ps_agreement.plan_master", $master_plan);
        $this->db->where('used', "Y");
        $query = $this->db->get($this->table_ps_argreement);
        return $query->row_array();
    }
    
    public function update_agreement($agreement_id, $plan_master, $data) {

        $this->db->where('agreement_id', $agreement_id);
        $this->db->where('plan_master', $plan_master);
        $this->db->where('used', "Y");
        $this->db->update($this->table_ps_argreement, $data);
        return $this->db->affected_rows();
    }
    
    public function get_seq_agreement_data_seq() {
        //Next seq
        $sql = "update ps_agreement_seq set seq=seq + 1 where id = 1";
        $this->db->query($sql);

        $this->db->flush_cache();
        $this->db->select('seq');
        $this->db->where('id', 1);
        return $this->db->get('ps_agreement_seq')->result_array()[0]["seq"];
    }
}
