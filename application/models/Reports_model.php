<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_report_list() {
        $reserve_arr = $this->get_agreement_list('R');
        $sale_arr = $this->get_agreement_list('S');
        return array_merge($reserve_arr);
    }

    private function get_agreement_list($status = '') {

        $sql = " SELECT DISTINCT ps_agreement.agreement_id, ps_promise.promise_id, (CONCAT(ps_agreement.agreement_id, \"(\", ps_agreement.plan ,\")\")) as agreement_no,";
        $sql .= " (CONCAT(users.first_name, \" (\", users.last_name ,\")\")) as salename, ";
        $sql .= " DATE_FORMAT(DATE_ADD(ps_agreement.create_date, INTERVAL 543 YEAR),\"%d/%m/%Y\") as create_date_thai,";
        $sql .= " DATE_FORMAT(DATE_ADD(ps_promise.create_date, INTERVAL 543 YEAR),\"%d/%m/%Y\") as sale_date,";
        $sql .= " users.id as saleid, ps_agreement.price_total as total_price";
        $sql .= " FROM ps_agreement ps_agreement";
        $sql .= " left join ps_promise  ps_promise on ps_agreement.agreement_id = ps_promise.agreement_id and ps_promise.used = \"Y\"";
        $sql .= " left join ps_promise_home ps_promise_home on  ps_agreement.agreement_id = ps_promise_home.agreement_id and ps_promise_home.used = \"Y\"";
        $sql .= " left join users users on users.id = ps_agreement.create_by";
        $sql .= " WHERE ps_agreement.used = \"Y\"";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_sale_filter_dropdown_data() {
        $arr = $this->array_to_options($this->get_sale_list(), 'id', 'salename', '-- All --');
        return $arr;
    }

    public function get_sale_list() {
        $this->db->select('users.id, concat(users.first_name, " ", users.last_name) as salename');
        return $this->db->get('users')->result_array();
    }

    public function array_to_options($arr = array(), $valueCol = '', $labelCol = '', $empty_item = false) {
        $options = array();

        if ($empty_item) {
            $options[''] = $empty_item;
        }

        for ($i = 0; $i < count($arr); $i++) {
            $options[$arr[$i][$valueCol]] = $arr[$i][$labelCol];
        }
        return $options;
    }

}
