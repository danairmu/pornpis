<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->table_ps_order = 'ps_order';
    }

    public function get_order_by_plan_id($id, $master_plan) {
        $this->db->select("ps_order.id, ps_order.plan_id, ps_order.order_no, ps_order.home_price, ps_order.home_type, ps_order.home_name, ps_order.comment, DATE_FORMAT(date(ps_order.order_date),\"%d/%m/%Y\") as order_date, ps_order.order_date as order_date_hidden, p_empty_land, p_empty_price");
        $this->db->where("ps_order.plan_name", $id);
        $this->db->where("ps_order.plan_id", $master_plan); //masterplan
        $this->db->where("ps_order.used", "Y");
        $query = $this->db->get($this->table_ps_order);
        $this->db->flush_cache();
        return $query->row_array();
    }

    public function get_plan_by_order_no($orderno, $master_plan) {
        $this->db->select("ps_order.id, ps_order.plan_name, ps_order.order_no");
        $this->db->where("ps_order.order_no", $orderno);
        $this->db->where("ps_order.plan_id", $master_plan); //masterplan
         $this->db->where("ps_order.used", "Y");
        $query = $this->db->get($this->table_ps_order);
        $this->db->flush_cache();
        return $query->result_array();
    }
    
    public function get_data_order_by_orderno($orderno) {
        $this->db->select("ps_order.id, ps_order.plan_id, ps_order.plan_name, ps_order.order_no, ps_order.home_price, ps_order.home_type, ps_order.home_name, ps_order.comment, DATE_FORMAT(date(ps_order.order_date),\"%d/%m/%Y\") as order_date, ps_order.order_date as order_date_hidden, p_empty_land, p_empty_price, ps_order.plan_price");
        $this->db->where("ps_order.order_no", $orderno);
        $this->db->where("ps_order.used", "Y");
        $query = $this->db->get($this->table_ps_order);
        $this->db->flush_cache();
        return $query->result_array();
    }

    public function get_seq_order_data_seq() {
        //Next seq
        $sql = "update ps_order_seq set seq=seq + 1 where id = 1";
        $this->db->query($sql);

        $this->db->flush_cache();
        $this->db->select('seq');
        $this->db->where('id', 1);
        return $this->db->get('ps_order_seq')->result_array()[0]["seq"];
    }

    public function update_order($order_no, $master_plan, $home_type, $home_price,$home_name, $comment, $userid) {
        $data['home_type'] = $home_type;
        $data['home_price'] = $home_price;
        $data['home_name'] = $home_name;
        $data['comment'] = $comment;
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('order_no', $order_no); 
        $this->db->where('plan_id', $master_plan);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_order, $data);
        return $this->db->affected_rows();
    }
    
    public function get_order_by_view_promise($order_no, $planid, $master_plan) {
        $this->db->select("ps_order.order_no, ps_order.ri, ps_order.ngan, ps_order.wa, ps_order.price_tarangwa");
        $this->db->where("ps_order.order_no", $order_no);
        $this->db->where("ps_order.plan_name", $planid);
        $this->db->where("ps_order.plan_id", $master_plan); //masterplan
        $this->db->where("ps_order.used", "Y");

        $query = $this->db->get($this->table_ps_order);
        $result['data'] = $query->result_array();
        return $result;
    }
    
    public function check_plan_data($id, $master_plan) {

        $this->db->where("ps_order.plan_name", $id);
        $this->db->where("ps_order.plan_id", $master_plan); //masterplan
        $this->db->where("ps_order.used", "Y");
        $query = $this->db->count_all_results($this->table_ps_order);
        return $query;
    }
    
    public function update_data_order($order_no, $plan_name, $master_plan, $data) {
        
        $this->db->where('order_no', $order_no); 
        $this->db->where('plan_id', $master_plan);
        $this->db->where("plan_name", $plan_name);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_order, $data);
        return $this->db->affected_rows();
    }
    
}
