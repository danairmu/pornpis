<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class File_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->table_ps_home = 'ps_scan';
        $this->table_ps_scan_promise = 'ps_scan_promise';
    }

   
    public function getFileByOrderNo($order_no, $type) {
        $this->db->select("ps_scan.order_no, ps_scan.file_name,  ps_scan.file_name_path");
        $this->db->from("ps_scan");
        $this->db->where("ps_scan.used", 'Y');
        $this->db->where("ps_scan.order_no", $order_no);
        $this->db->where("ps_scan.file_type", $type);


        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }
    
    public function getDataFileSmartCardByPromiseId($promise_id, $type, $promise_type) {
        $this->db->distinct();
        $this->db->select("ps_scan_promise.order_no, ps_scan_promise.file_name,  ps_scan_promise.file_name_scan, ps_scan_promise.file_path");
        $this->db->from("ps_plan_person");
        $this->db->join('ps_scan_promise', 'ps_scan_promise.promise_id = ps_plan_person.promise_id and ps_scan_promise.pid = ps_plan_person.pid');
        $this->db->where("ps_scan_promise.used", 'Y');
        $this->db->where("ps_scan_promise.promise_id", $promise_id);
        $this->db->where("ps_scan_promise.file_type", $type);
        $this->db->where("ps_scan_promise.prommise_type", $promise_type);
        $this->db->where("ps_plan_person.flag", 'Y');
       
        
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }
    
    public function getDataFileSmartCardByPromiseIdTab2($promise_id, $type, $promise_type) {
        $this->db->distinct();
        $this->db->select("ps_scan_promise.order_no, ps_scan_promise.file_name,  ps_scan_promise.file_name_scan, ps_scan_promise.file_path");
        $this->db->from("ps_home_person");
        $this->db->join('ps_scan_promise', 'ps_scan_promise.promise_id = ps_home_person.promise_home_id ','left');
        $this->db->where("ps_scan_promise.used", 'Y');
        $this->db->where("ps_scan_promise.promise_id", $promise_id);
        $this->db->where("ps_scan_promise.file_type", $type);
        $this->db->where("ps_scan_promise.prommise_type", $promise_type);
        $this->db->where("ps_home_person.flag", 'Y');
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }
    
    public function chectk_data_person_scan_card($promiseid, $pid, $plan_master, $promist_type, $file_typ) {

        $this->db->where('used', 'Y');
        $this->db->where('order_no', $promiseid);
        $this->db->where('pid', $pid);
        $this->db->where('plan_master', $plan_master);
        $this->db->where('file_type', $file_typ);
        $this->db->where('prommise_type', $promist_type);
        $query = $this->db->count_all_results($this->table_ps_scan_promise);
        return $query;
    }
    

}
