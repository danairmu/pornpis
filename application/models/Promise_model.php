<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Promise_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->table_ps_area = 'ps_plan';
        $this->table_ps_order = 'ps_order';
        $this->table_ps_scan = 'ps_scan';
        $this->table_ps_promise = 'ps_promise';
        $this->table_ps_promise_home = 'ps_promise_home';
        $this->table_ps_plan_person = 'ps_plan_person';
        $this->table_ps_home_person = 'ps_home_person';
        $this->table_ps_argreement = 'ps_agreement';
    }

    public function get_seq_promise_data_seq() {

        $sql = "update ps_promise_seq set seq=seq + 1 where id = 1";
        $this->db->query($sql);

        $this->db->flush_cache();
        $this->db->select('seq');
        $this->db->where('id', 1);
        return $this->db->get('ps_promise_seq')->result_array()[0]["seq"];
    }

    public function insert_plan_person($data) {

        $this->db->insert($this->table_ps_plan_person, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    public function insert_promise($data) {

        $this->db->insert($this->table_ps_promise, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    public function insert_promise_home($data) {

        $this->db->insert($this->table_ps_promise_home, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    public function update_promise($data, $promiseid) {

        $this->db->where('promise_id', $promiseid);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_promise, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    public function update_promise_home($data, $promiseid) {

        $this->db->where('promise_home_id', $promiseid);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_promise_home, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    public function insert_data_home_person($data) {

        $this->db->insert($this->table_ps_home_person, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    public function set_plan_person_status($promise_id, $pid, $status, $flag, $userid) {

        $used = 'Y';
        if ($flag == 0) {
            $used = 'Y';
        } else {
            $used = 'N';
            $status = 'N';
        }

        $data['used'] = $used;
        $data['flag'] = $status;
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('promise_id', $promise_id);
        $this->db->where('pid', $pid);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_plan_person, $data);
        return $this->db->affected_rows();
    }

    public function set_home_person_status($promise_id, $pid, $status, $flag, $userid) {

        $used = 'Y';
        if ($flag == 0) {
            $used = 'Y';
        } else {
            $used = 'N';
            $status = 'N';
        }

        $data['used'] = $used;
        $data['flag'] = $status;
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('promise_home_id', $promise_id);
        $this->db->where('pid', $pid);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_home_person, $data);
        return $this->db->affected_rows();
    }

    public function count_data_person_plan($promiseid) {

        $this->db->where('used', 'Y');
        $this->db->where('flag', 'Y');
        $this->db->where('promise_id', $promiseid);
        $query = $this->db->count_all_results($this->table_ps_plan_person);
        return $query;
    }

    public function count_data_person_home($promiseid) {

        $this->db->where('used', 'Y');
        $this->db->where('flag', 'Y');
        $this->db->where('promise_home_id', $promiseid);
        $query = $this->db->count_all_results($this->table_ps_home_person);
        return $query;
    }

    //getperson by promiseid บุคคลที่เกียวข้องกับสัญญาที่ดิน
    public function get_data_person_plan_list($promiseid) {
        $this->db->select("*");
        $this->db->from("ps_plan_person");
        $this->db->where("ps_plan_person.used", 'Y');
        $this->db->where("ps_plan_person.promise_id", $promiseid);
        $this->db->order_by("ps_plan_person.update_date", "asc");
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function get_data_person_plan_view($promiseid) {
        $this->db->select("*");
        $this->db->from("ps_plan_person");
        $this->db->where("ps_plan_person.used", 'Y');
        $this->db->where("ps_plan_person.flag", 'Y');
        $this->db->where("ps_plan_person.promise_id", $promiseid);
        $this->db->order_by("ps_plan_person.update_date", "asc");
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function get_data_person_home_view($promiseid) {
        $this->db->select("*");
        $this->db->from("ps_home_person");
        $this->db->where("ps_home_person.used", 'Y');
        $this->db->where("ps_home_person.flag", 'Y');
        $this->db->where("ps_home_person.promise_home_id", $promiseid);
        $this->db->order_by("ps_home_person.update_date", "asc");
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function get_data_person_home_list($promiseid) {
        $this->db->select("*");
        $this->db->from("ps_home_person");
        $this->db->where("ps_home_person.used", 'Y');
        $this->db->where("ps_home_person.promise_home_id", $promiseid);
        $this->db->order_by("ps_home_person.update_date", "asc");
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    //getperson by promiseid บุคคลที่เกียวข้องกับสัญญาที่ดิน
    public function get_data_person_plan_detail($promiseid) {
        $this->db->select("*");
        $this->db->from("ps_plan_person");
        $this->db->where("ps_plan_person.used", 'Y');
        $this->db->where("ps_plan_person.flag", 'Y');
        $this->db->where("ps_plan_person.promise_id", $promiseid);
        $this->db->order_by("ps_plan_person.update_date", "asc");
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function get_data_detail_by_promise_id($promiseid) {
        $this->db->select("*");
        $this->db->from("ps_promise");
        $this->db->where("ps_promise.used", 'Y');
        $this->db->where("ps_promise.promise_id", $promiseid);
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function get_data_detail_by_promisehome_id($promiseid) {
        $this->db->select("*");
        $this->db->from("ps_promise_home");
        $this->db->where("ps_promise_home.used", 'Y');
        $this->db->where("ps_promise_home.promise_home_id", $promiseid);
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function get_data_person_home_detail($promiseid) {
        $this->db->select("*");
        $this->db->from("ps_home_person");
        $this->db->where("ps_home_person.used", 'Y');
        $this->db->where("ps_home_person.flag", 'Y');
        $this->db->where("ps_home_person.promise_home_id", $promiseid);
        $this->db->order_by("ps_home_person.update_date", "asc");
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function get_data_promise_id_by_agreement_id($agreement_id) {
        $this->db->select("ps_promise.promise_id, ps_promise.land_no, ps_promise.pay_price_promise, ps_promise.pay_price_promise_result");
        $this->db->select("ps_promise.witness_name1, ps_promise.witness_lastname1, ps_promise.witness_name2, ps_promise.witness_lastname2");
        $this->db->select("ps_promise.promise_type1, ps_promise.promise_type2, ps_promise.promise_type3");
        $this->db->from("ps_promise");
        $this->db->where("ps_promise.used", 'Y');
        $this->db->where("ps_promise.agreement_id", $agreement_id);
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function get_data_home_promise_id_by_agreement_id($agreement_id) {
        $this->db->select("ps_promise_home.promise_home_id, ps_promise_home.land_no, ps_promise_home.land_no2,ps_promise_home.pay_price_home, ps_promise_home.pay_price_home_date");
        $this->db->select("ps_promise_home.pay_price_pirod1, ps_promise_home.pay_price_pirod2, ps_promise_home.pay_price_pirod3");
        $this->db->select("ps_promise_home.pay_price_pirod4, ps_promise_home.pay_price_pirod5, ps_promise_home.price_promotion");
        $this->db->select("ps_promise_home.witness_name1, ps_promise_home.witness_lastname1, ps_promise_home.witness_name2, ps_promise_home.witness_lastname2");
        $this->db->select("ps_promise_home.promise_type1, ps_promise_home.promise_type2, ps_promise_home.promise_type3, ps_promise_home.promise_type4");
        $this->db->select("ps_promise_home.attached1, ps_promise_home.attached2, ps_promise_home.attached3, ps_promise_home.attached4, ps_promise_home.attached5");
        $this->db->from("ps_promise_home");
        $this->db->where("ps_promise_home.used", 'Y');
        $this->db->where("ps_promise_home.agreement_id", $agreement_id);
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function count_data_promise($agreementid) {

        $this->db->where('used', 'Y');
        $this->db->where('agreement_id', $agreementid);
        $query = $this->db->count_all_results($this->table_ps_promise);
        return $query;
    }

    public function chectk_data_person_promise($promiseid, $pid, $plan_master) {

        $this->db->where('used', 'Y');
        $this->db->where('promise_id', $promiseid);
        $this->db->where('pid', $pid);
        $this->db->where('plan_master', $plan_master);
        $query = $this->db->count_all_results($this->table_ps_plan_person);
        return $query;
    }

    public function chectk_data_personhome_promise($promiseid, $pid, $plan_master) {

        $this->db->where('used', 'Y');
        $this->db->where('promise_home_id', $promiseid);
        $this->db->where('pid', $pid);
        $this->db->where('plan_master', $plan_master);
        $query = $this->db->count_all_results($this->table_ps_home_person);
        return $query;
    }

    //ยกเลิกสัญญา
    public function cancel_promise_plan($promiseid, $plan_master, $userid) {

        $data['used'] = 'N';
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('promise_id', $promiseid);
         $this->db->where('plan_master', $plan_master);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_promise, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    public function cancel_promise_home($promiseid, $plan_master, $userid) {

        $data['used'] = 'N';
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('promise_home_id', $promiseid);
        $this->db->where('plan_master', $plan_master);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_promise_home, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    //ยกเลิกบุคคลต่อจากยกเลิกสัญญา
    public function cancel_plan_person_status($promise_id, $plan_master, $userid) {


        $data['used'] = 'N';
        $data['flag'] = 'N';
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('promise_id', $promise_id);
        $this->db->where('plan_master', $plan_master);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_plan_person, $data);
        return $this->db->affected_rows();
    }

    public function cancel_home_person_status($promise_id, $plan_master, $userid) {


        $data['used'] = 'N';
        $data['flag'] = 'N';
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('promise_home_id', $promise_id);
         $this->db->where('plan_master', $plan_master);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_home_person, $data);
        return $this->db->affected_rows();
    }

    public function cancel_agreement($argreement_id, $plan_master, $userid) {


        $data['used'] = 'N';
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('agreement_id', $argreement_id);
        $this->db->where('plan_master', $plan_master);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_argreement, $data);
        return $this->db->affected_rows();
    }

    public function cancel_order($order_no, $plan_master, $userid) {


        $data['used'] = 'N';
        $data['order_status'] = 'N';
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('order_no', $order_no);
        $this->db->where('plan_id', $plan_master);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_order, $data);
        return $this->db->affected_rows();
    }

    public function cancel_plan($planid, $plan_master, $userid) {


        $data['p_status'] = 'N';
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('p_name', $planid);
        $this->db->where('plan_id', $plan_master);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_area, $data);
        return $this->db->affected_rows();
    }

}
