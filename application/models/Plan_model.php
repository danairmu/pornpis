<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Plan_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->table_ps_area = 'ps_plan';
        $this->table_ps_order = 'ps_order';
        $this->table_ps_scan = 'ps_scan';
        $this->table_ps_scan_promise = 'ps_scan_promise';
        
    }

    public function get_data_plan_by_id($id, $master_plan) {
        $this->db->select("ps_plan.id, ps_plan.p_name, ps_plan.p_ri, ps_plan.p_ngan, ps_plan.p_wa, ps_plan.p_width, ps_plan.p_height");
        $this->db->select("ps_plan.p_price, ps_plan.p_status, ps_plan.p_price_tarangwa");
        $this->db->select("ps_plan.p_left, ps_plan.p_right, ps_plan.p_top, ps_plan.p_buttom, ps_plan.p_corner_flag, ps_plan.p_home_id");
        $this->db->from("ps_plan");
        $this->db->where("ps_plan.p_name", $id);
        $this->db->where("ps_plan.plan_id", $master_plan);
        $this->db->where("ps_plan.used", 'Y');

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_status_plan_by_plan_id($planid) {
        $this->db->select("ps_plan.id, ps_plan.p_name, ps_plan.p_status");
        $this->db->select("ps_plan.p_price");
        $this->db->from("ps_plan");
        $this->db->where("ps_plan.plan_id", $planid);
        $this->db->where("ps_plan.used", 'Y');


        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function set_plan_stattus($id, $status, $plan_master, $userid) {
        $data['p_status'] = $status;
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        $this->db->where('p_name', $id);
        $this->db->where('plan_id', $plan_master);
        $this->db->where('used', 'Y');
        $this->db->update($this->table_ps_area, $data);
        return $this->db->affected_rows();
    }

    public function insertOrder($data) {
       
        $this->db->insert($this->table_ps_order, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    public function set_order_stattus($orderno, $planid, $status, $comment, $userid, $master_plan) {
        $data['order_status'] = $status;
        $data['used'] = $status;
        $data['comment'] = $comment;
        $data['update_date'] = date("Y-m-d h:m:s");
        $data['update_by'] = $userid;
        
        $this->db->where('order_no', $orderno);
        $this->db->where('plan_name', $planid);
        $this->db->where('plan_id', $master_plan);
        $this->db->where('used', 'Y');
        
        $this->db->update($this->table_ps_order, $data);
        return $this->db->affected_rows();
    }
    
    public function get_data_arr_plan_by_id($id) {
        $this->db->select("ps_plan.id, ps_plan.p_name, ps_plan.p_ri, ps_plan.p_ngan, ps_plan.p_wa");
        $this->db->select("ps_plan.p_price, ps_plan.p_status");
        $this->db->select("ps_plan.p_left, ps_plan.p_right, ps_plan.p_top, ps_plan.p_buttom");
        $this->db->from("ps_plan");
        $this->db->where("ps_plan.id", $id);
        $this->db->where("ps_plan.used", 'Y');


        $query = $this->db->get($this->table_ps_area);
        return $query->row_array();
    }

    public function get_data_check_plan_by_id($id, $master_plan) {
        $this->db->select("ps_plan.id, ps_plan.p_name, ps_plan.p_ri, ps_plan.p_ngan, ps_plan.p_wa");
        $this->db->select("ps_plan.p_price, ps_plan.p_status");
        $this->db->select("ps_plan.p_left, ps_plan.p_right, ps_plan.p_top, ps_plan.p_buttom, ps_plan.p_left_ex, ps_plan.p_right_ex");
        $this->db->from("ps_plan");
        $this->db->where("ps_plan.p_name", $id);
        $this->db->where("ps_plan.plan_id", $master_plan);
        $this->db->where("ps_plan.used", 'Y');
        $this->db->where("ps_plan.p_status", 'N');

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
    public function testInsertPlan($data) {
       
        $this->db->insert($this->table_ps_area, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }
    
     public function insertImageScan($data) {

        
        $this->db->insert($this->table_ps_scan, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }
    
    public function insertImageScanPromise($data) {

        
        $this->db->insert($this->table_ps_scan_promise, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }
    
    public function get_seq_scan_data_seq() {
        //Next seq
        $sql = "update ps_scan_seq set seq=seq + 1 where id = 1";
        $this->db->query($sql);

        $this->db->flush_cache();
        $this->db->select('seq');
        $this->db->where('id', 1);
        return $this->db->get('ps_scan_seq')->result_array()[0]["seq"];
    }
    public function get_data_scan_list($order_no , $plan_master, $type)
    {
        $this->db->select("ps_scan.id, ps_scan.file_name, DATE_FORMAT(DATE_ADD(ps_scan.create_date, INTERVAL 543 YEAR),'%d/%m/%Y %T') as create_date_thai, ps_scan.create_date, ps_scan.file_name_path");
        $this->db->from("ps_scan");
        $this->db->where("ps_scan.used", 'Y');
        $this->db->where("ps_scan.file_type", $type);
        $this->db->where("ps_scan.order_no", $order_no);
        $this->db->where("ps_scan.plan_master", $plan_master);
        $this->db->order_by("ps_scan.id", "asc");
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }
    
    public function get_data_scan_promise_list($promiseid, $order_no , $plan_master, $type, $prommise_type, $pid, $flag)
    {
        $_type = '';
        if($flag == '2'){
           $_type = '2'; 
        }else{
            $_type = '1'; 
        }
        
        $this->db->select("ps_scan_promise.id, ps_scan_promise.pid, ps_scan_promise.file_name, ps_scan_promise.file_name_scan, DATE_FORMAT(DATE_ADD(ps_scan_promise.create_date, INTERVAL 543 YEAR),'%d/%m/%Y %T') as create_date_thai, ps_scan_promise.create_date");
        $this->db->from("ps_scan_promise");
        $this->db->where("ps_scan_promise.used", 'Y');
        $this->db->where("ps_scan_promise.file_type", $_type);
        //$this->db->where("ps_scan_promise.promise_id", $promiseid);
        $this->db->where("ps_scan_promise.order_no", $order_no);
        $this->db->where("ps_scan_promise.plan_master", $plan_master);
        
        if($flag == '2'){
            $this->db->where("ps_scan_promise.pid", $pid);
        }
        
        if($flag == '1'){
            $this->db->where("ps_scan_promise.prommise_type", $prommise_type);
        }
        
        
        $this->db->order_by("ps_scan_promise.id", "asc");
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }
    
    
     public function update_scan_document($order_no, $plan_master, $data) {

        $this->db->where('order_no', $order_no);
        $this->db->where('plan_master', $plan_master);
        $this->db->where('used', "Y");
        $this->db->update($this->table_ps_scan, $data);
        return $this->db->affected_rows();
    }
    
    public function get_data_plan_by_plan_id($planid, $master_plan) {
        $this->db->select("ps_plan.id, ps_plan.p_name, ps_plan.p_ri, ps_plan.p_ngan, ps_plan.p_wa, ps_plan.p_width, ps_plan.p_height");
        $this->db->select("ps_plan.p_price, ps_plan.p_status, ps_plan.p_price_tarangwa");
        $this->db->select("ps_plan.p_left, ps_plan.p_right, ps_plan.p_top, ps_plan.p_buttom, ps_plan.p_corner_flag, ps_plan.p_home_id");
        $this->db->where("ps_plan.p_name", $planid);
        $this->db->where("ps_plan.plan_id", $master_plan);
        $this->db->where("ps_plan.used", 'Y');
        $query = $this->db->get($this->table_ps_area);
        $this->db->flush_cache();
        return $query->row_array();
    }
}
