<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->table_ps_home = 'ps_home';
        $this->table_ps_image = 'ps_image';
    }

    //home list
    public function get_data_homelist() {
        $this->db->select("ps_home.id, ps_home.h_name, ps_home.h_type, ps_home.h_price, ps_home.h_area, ps_home.h_comment");
        $this->db->from("ps_home");
        $this->db->where("ps_home.used", 'Y');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_data_home_by_id($id) {
        $this->db->select("ps_home.id, ps_home.h_name, ps_home.h_type, ps_home.h_price, ps_home.h_area, ps_home.h_comment, h_width, h_height");
        $this->db->from("ps_home");
        $this->db->where("ps_home.used", 'Y');
        $this->db->where("ps_home.id", $id);


        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
    public function get_data_home_by_orderno($order_no) {
        $this->db->select("ps_order.order_no, ps_home.h_name, ps_home.h_type, ps_home.h_price as h_price");
        $this->db->from("ps_order");
        $this->db->join('ps_home', 'ps_home.id = ps_order.home_type', 'left');
        $this->db->where("ps_home.used", 'Y');
        $this->db->where("ps_order.order_no", $order_no);


        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }
    
    public function insertImage($data) {

        
        $this->db->insert($this->table_ps_image, $data);
        $result_count = $this->db->affected_rows();
        return $result_count;
    }

    public function get_data_image_list($id) {
        $this->db->select("ps_image.id, ps_image.name, ps_image.file_name_path, ps_image.image_data, ps_home.h_name, ps_image.home_id, ps_image.path");
        $this->db->from("ps_image");
        $this->db->join('ps_home', 'ps_home.id = ps_image.home_id', 'left');
        $this->db->where("ps_image.used", 'Y');
        $this->db->where("ps_image.home_id", $id);
        $this->db->order_by("ps_image.id", "asc");
        $query = $this->db->get();
        $result['data'] = $query->result_array();
        return $result;
    }

    public function get_data_image_view($id) {
        $this->db->select("ps_image.id, ps_image.name, ps_image.image_data, ps_home.h_name, ps_image.home_id, ps_image.path");
        $this->db->from("ps_image");
        $this->db->join('ps_home', 'ps_home.id = ps_image.home_id', 'left');
        $this->db->where("ps_image.used", 'Y');
        $this->db->where("ps_image.id", $id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

//    public function delete_data_image($id) {
//        $this->db->where('id', $id);
//        return $this->db->delete($this->table_ps_image);
//        
//    }

    public function delete_data_image($id) {
        
        $data = array(
                'used' => 'N',
                'update_date' => date("Y-m-d h:m:s"),
            );
        
        $this->db->where('id', $id);
        $this->db->update($this->table_ps_image, $data);
        return $this->db->affected_rows();

        
    }
    
    public function get_data_seq_image() {
        //Next seq
        $sql = "update ps_image_seq set seq=seq + 1 where id = 1";
        $this->db->query($sql);

        $this->db->flush_cache();
        $this->db->select('seq');
        $this->db->where('id', 1);
        return $this->db->get('ps_image_seq')->result_array()[0]["seq"];
    }
}
