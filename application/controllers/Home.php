<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {


        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        $data = array();
        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $this->template->stylesheet->add('assets/css/news.css');
        $this->template->javascript->add('js/news.js');

        // Set the title
        // Set the title
        $this->template->title = 'Home';
        $this->template->content->view('news', $data);
        $this->template->publish();
    }

}
