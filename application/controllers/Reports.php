<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation'));
        $this->load->model(array('reports_model'));
    }

    public function index() {
        $data = array();
        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $data['sale_filter'] = 'sale_filter';
        $data['sale_filter_dropdown_data'] = $this->reports_model->get_sale_filter_dropdown_data();
        $data['sale_filter_dropdown_default'] = '';
        $data['report_type'] = 2;

        //-- === JS CSS === --//
        
        
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");
        
        //$this->template->javascript->add("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js");
        //$this->template->stylesheet->add("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css");
        
        $this->template->stylesheet->add("assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker-thai.js");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js");

        
        $this->template->javascript->add('js/reports.js');
        $this->template->stylesheet->add('assets/css/responsive.css');
        $this->template->javascript->add('js/jquery.PrintArea.js');
        //-- === TEMPLATE === --//
        $this->template->content->view('reports', $data);
        $this->template->publish();
    }

    public function get_report_list() {
        $data['data'] = $this->reports_model->get_report_list();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function getorder_by_orderno() {

        $order_no = $this->input->post('order_no');
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_order_by_orderno($order_no)));
    }
    
    public function get_order_by_orderno($order_no) {
        $this->load->model(array('order_model'));
        return $this->order_model->get_data_order_by_orderno($order_no);
    }

}
