<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Managementplan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
    }

    public function index() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        // Set the title
        $this->template->title = 'Managementplan!';
        // Dynamically add a css stylesheet

        $this->template->javascript->add('js/managementplan.js');
        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap.css');
        $this->template->javascript->add('assets/plugins/bootstrap/js/bootstrap.min.js');

        //$this->template->stylesheet->add("https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css");
        //$this->template->javascript->add("https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js");
        //new
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");

        $this->template->stylesheet->add('assets/css/responsive.css');

        
        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $this->load->model(array('home_model'));
        $data['homes'] =  $this->home_model->get_data_homelist(); 
        
        $this->template->stylesheet->add('assets/css/managementplan.css');
        $this->template->content->view('managementplan', $data);

        // Set a partial's content
        $this->template->footer = '';

        // Publish the template
        $this->template->publish();
    }

    //getlist plan
    public function planslist($master_plan) {

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_planlist($master_plan)));
    }

    public function get_planlist($master_plan) {
        $this->load->model(array('managemen_model'));
        return $this->managemen_model->get_data_plans_list($master_plan);
    }

    //gethomelist plan
    public function planshomelist() {

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_planshomelist()));
    }

    public function get_planshomelist() {
        $this->load->model(array('managemen_model'));
        return $this->managemen_model->get_data_homelist();
    }

    //getplan by id
    public function planslistbyid() {

        $this->load->model(array('home_model','managemen_model'));
        $plan_name = $this->input->post('plan_name');
        $plan_master = $this->input->post('master_plan');
         
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_planlistbyid($plan_name, $plan_master)));
    }

    public function get_planlistbyid($plan_name, $plan_master) {
        $this->load->model(array('managemen_model'));
        return $this->managemen_model->get_data_plan_by_id($plan_name, $plan_master);
    }

    //gethome by id
    public function homelistbyid($id) {

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_homelistbyid($id)));
    }

    public function get_homelistbyid($id) {
        $this->load->model(array('home_model'));
        return $this->home_model->get_data_home_by_id($id);
    }

     //getusers for table
    public function getPlanConner() {

        $id = $this->input->post('plan_name');
        $master_plan = $this->input->post('master_plan');
        $this->load->model(array('plan_model'));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->plan_model->get_data_plan_by_id($id, $master_plan)));
    }

    
    public function update_plan() {

        try {
            $this->load->model(array('managemen_model'));
            $plan_id = $this->input->post('planid');
            $plan_master = $this->input->post('plan_master');
            
            $data = array(
                'p_ri' => $this->input->post('ri') === '' ? '0' : $this->input->post('ri'),
                'p_ngan' => $this->input->post('ngan') === '' ? '0' : $this->input->post('ngan'),
                'p_wa' => $this->input->post('wa') === '' ? '0' : $this->input->post('wa'),
                'p_tarangwa' => $this->input->post('p_tarangwa') === '' ? '0' : $this->input->post('p_tarangwa'),
                'p_width' => $this->input->post('p_width') === '' ? '0' : $this->input->post('p_width'),
                'p_height' => $this->input->post('p_height') === '' ? '0' : $this->input->post('p_height'),
                'p_price' => $this->input->post('price') === '' ? '0' : str_replace(',', '', $this->input->post('price')),
                'p_price_tarangwa' => $this->input->post('price_tarangwa') === '' ? '0' : str_replace(',', '', $this->input->post('price_tarangwa')),
                'p_comment' => $this->input->post('comment'),
                'p_corner_price' => $this->input->post('price_conner') === '' ? '0' : str_replace(',', '', $this->input->post('price_conner')),
                'update_date' => date("Y-m-d h:m:s"),
                'p_home_id' => $this->input->post('homes_id'),
            );
            $manage = $this->managemen_model->update_plan($plan_id, $plan_master, $data);

            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($manage));
        } catch (Exception $e) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode("0"));
        }
    }

    public function update_home() {

        try {
            $this->load->model(array('managemen_model'));
            $id = $this->input->post('homeid');

            $data = array(
                'h_name' => $this->input->post('home_name'),
                'h_price' => str_replace(',', '', $this->input->post('price')),
                'h_area' => $this->input->post('area'),
                'h_comment' => $this->input->post('comment'),
                'h_width' => $this->input->post('home_width'),
                'h_height' => $this->input->post('home_height'),
                'update_date' => date("Y-m-d h:m:s"),
            );

            $home = $this->managemen_model->update_home($id, $data);
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($home));
        } catch (Exception $e) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode("0"));
        }
    }

    public function managhome() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        // Set the title
        $this->template->title = 'Managementplan!';
        // Dynamically add a css stylesheet

        $this->template->javascript->add('js/managementhome.js');
        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap.css');
        $this->template->stylesheet->add('assets/plugins/bootstrap/js/bootstrap.min.js');
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");
        $this->template->stylesheet->add('assets/css/responsive.css');


        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        //test image
//        $this->load->model(array('home_model'));
//        $result = $this->home_model->get_data_image_view('93');
//        $data['imgdata'] = base64_encode($result[0]['image_data']);

        $this->template->stylesheet->add('assets/css/managementplan.css');
        $this->template->content->view('managementhome', $data);

        // Set a partial's content
        $this->template->footer = '';

        // Publish the template
        $this->template->publish();
    }

    public function upload_image() {

        try {
            $name = $this->input->post('name');
            $home_id = $this->input->post('home_id');
            $image_data = $this->input->post('image_data');
            $path = $this->input->post('pathdata');
            $userid = $this->ion_auth->get_user_id();
            $file_name_path = $this->input->post('file_name_path');
            
            //local
            //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/upload/'. $home_id . '/' . $file_name_path;

            $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $home_id . '/' . $name;
            


            $data = array(
                'name' => $name,
                'file_name_path' => $file_name_path,
                'home_id' => $home_id,
                'path' => 'upload/' . $home_id . '/' . $name,
                'used' => 'Y',
                'create_by' => $userid,
                'create_date' => date("Y-m-d h:m:s"),
            );

            $outfile = $uploadfile;
            $result = file_put_contents($outfile, base64_decode($image_data));

            $this->load->model(array('home_model'));
            $db = $this->home_model->insertImage($data);

            $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
            $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
            $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
            $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");

            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($db));
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($e->getMessage()));
        }
    }

    function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return( $output_file );
    }

    public function imagelist($id) {

        $this->template->javascript->add('js/managementhome.js');
        $this->template->javascript->add("https://code.jquery.com/jquery-3.3.1.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_Imagelist($id)));
    }

    public function delete_image($id) {



        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");

        //delete file
        //$ims = $this->get_Imagelist($id);
        //$ims["data"]["path"];
        
        //server
        //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/scan/' . $filenameTopath . ".jpg";
        
        //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/upload/' . $home_id . '/' . $name;
        //unlink($uploadfile);

        $this->load->model(array('home_model'));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->home_model->delete_data_image($id)));
    }

    public function get_Imagelist($id) {
        $this->load->model(array('home_model'));
        return $this->home_model->get_data_image_list($id);
    }

    //gethome by id
    public function viewimagebyid($id) {

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_viewdataimagebyid($id)));
    }

    public function get_viewdataimagebyid($id) {
        $this->load->model(array('home_model'));
        return $this->home_model->get_data_image_view($id);
    }

    public function check_plan() {

        $id = $this->input->post('plan_name');
        $master_plan = $this->input->post('master_plan');

        $this->load->model(array('order_model'));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->order_model->check_plan_data($id, $master_plan)));
    }

    public function get_image_seq() {
        $this->load->model(array('home_model'));
        
        return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->home_model->get_data_seq_image()));
    }
    
}
