<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calculate extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
    }

    public function index() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

// Set the title


        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $this->template->javascript->add("js/calculate.js");
        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap.css');
        $this->template->stylesheet->add('assets/plugins/bootstrap/js/bootstrap.min.js');
        
        $this->template->content->view('calculate', $data);
        $this->template->footer = '';
        $this->template->publish();
    }


}
