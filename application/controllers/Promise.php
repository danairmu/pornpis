<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Promise extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
    }

    public function promiseselet() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $data['order_no'] = $this->input->post('hiden_person_order_no');
        $data['master_plan'] = $this->input->post('hiden_person_master_plan');
        $data['planssave'] = $this->input->post('person_plan_save');

        $due_date = time();
        $data['due_date_order'] = date("Y-m-d h:m:s");
        $data['due_date_display'] = $this->thai_date($due_date);


        $this->template->stylesheet->add('assets/css/responsive.css');
        $this->template->javascript->add('/js/promise.js');
        $this->template->javascript->add('js/jquery.PrintArea.js');

        $this->template->javascript->add('assets/scan/scanner.js');
        $this->template->javascript->add('assets/scan/download.js');

        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");


        $this->template->content->view('promise', $data);
        $this->template->footer = '';
        $this->template->publish();
    }

    public function scan() {


        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $data = array();
        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $data['promiseid'] = $this->input->post('hiddin_scan_promiseid');
        $data['plan_master'] = $this->input->post('hiddin_scan_masterplan');

        //to promise
        $data['order_no'] = $this->input->post('hiddin_scan_orderno');
        $data['master_plan'] = $this->input->post('hiddin_scan_masterplan');
        $data['planssave'] = $this->input->post('hiddin_scan_plan_save');
        $data['due_date_order'] = $this->input->post('hiddin_scan_due_date_order');
        $data['due_date_display'] = $this->input->post('hiddin_scan_due_date_display');

        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap.css');
        $this->template->javascript->add('assets/plugins/bootstrap/js/bootstrap.min.js');
        $this->template->javascript->add('assets/scan/scanner.js');
        $this->template->javascript->add('assets/scan/download.js');
        $this->template->stylesheet->add('assets/css/scan.css');
        $this->template->javascript->add('js/jquery.PrintArea.js');
        $this->template->javascript->add('js/scanpromise.js');
        //new
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");


        $this->template->stylesheet->add("assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js");


        $this->template->content->view('scanpromise', $data);
        $this->template->footer = '';
        $this->template->publish();
    }

    public function scantab2() {


        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $data = array();
        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $data['promiseid'] = $this->input->post('hiddin_scan_promiseid2');
        $data['plan_master'] = $this->input->post('hiddin_scan_masterplan2');

        //to promise
        $data['order_no'] = $this->input->post('hiddin_scan_orderno2');
        $data['master_plan'] = $this->input->post('hiddin_scan_masterplan2');
        $data['planssave'] = $this->input->post('hiddin_scan_plan_save2');
        $data['due_date_order'] = $this->input->post('hiddin_scan_due_date_order2');
        $data['due_date_display'] = $this->input->post('hiddin_scan_due_date_display2');

        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap.css');
        $this->template->javascript->add('assets/plugins/bootstrap/js/bootstrap.min.js');
        $this->template->javascript->add('assets/scan/scanner.js');
        $this->template->javascript->add('assets/scan/download.js');
        $this->template->stylesheet->add('assets/css/scan.css');
        $this->template->javascript->add('js/jquery.PrintArea.js');
        $this->template->javascript->add('js/scanpromise.js');
        //new
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");


        $this->template->stylesheet->add("assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js");


        $this->template->content->view('scanpromise', $data);
        $this->template->footer = '';
        $this->template->publish();
    }

    //getlist plan
    public function scanlist() {

        $order_no = $this->input->post('order_no');
        $mode = $this->input->post('mode');
        $tab = $this->input->post('tab');
        $plan_master = $this->input->post('plan_master_scan');
        $promiseid = $this->input->post('promiseid');
        $prommise_type = $this->input->post('prommise_type');
        $_pid = $this->input->post('pid');
        $_file_type = $this->input->post('file_type');
        $flag = $this->input->post('flag');
        
        $pid = '';
        if ($_pid === null || $_pid === '') {
            $pid = '0';
        } else {
            $pid = $_pid;
        }
        $type = '';

        if ($mode === '1') {
            $type = '1';
        }else {
            $type = '2';
        }


        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_scanlist($promiseid, $order_no, $plan_master, $_file_type, $prommise_type, $pid, $flag)));
    }

    public function get_scanlist($promiseid, $order_no, $plan_master, $type, $prommise_type, $pid, $flag) {
        $this->load->model(array('plan_model'));
        return $this->plan_model->get_data_scan_promise_list($promiseid, $order_no, $plan_master, $type, $prommise_type, $pid, $flag);
    }

    public function image_scan() {

        try {
            $filename = $this->input->post('filename');
            $pathfile = $this->input->post('pathfile');
            $image_data = $this->input->post('image_data');
            $promise_id = $this->input->post('promise_id');
            $order_no = $this->input->post('order_no');
            $plan_master = $this->input->post('plan_master');
            $mode = $this->input->post('mode');
            $pid = $this->input->post('pid');
            $filename_scan = $this->input->post('filename_scan');
            $tab = $this->input->post('tab');
            $typescan = $this->input->post('typescan');



            //tab 1 สแกนเอกสาร
            $uploadfile = '';
            if ($mode === '1') {
                //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/scanpromise/' . $filename_scan . ".jpg";
                
                $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/scanpromise/' . $filename . ".jpg";
            } //tab 2 สแกนเอกสาร
            else if ($mode === '2') {
                //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/scanpromise/' . $filename_scan . ".jpg";
                
                $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/scanpromisecard/' . $filename . ".jpg";
            }//tab 1 สแกนบัตร
            else if ($mode === '3') {
                //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/scanpromisecard/' . $filename_scan . ".jpg";
                $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/scanpromisecard/' . $filename . ".jpg";
            }//tab 2 สแกนบัตร
            else if ($mode === '4') {
                //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/scanpromisecard/' . $filename_scan . ".jpg";
                $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/scanpromisecard/' . $filename . ".jpg";
            }

            $prommise_type = '';
            if ($tab === '1') {
                $prommise_type = '1';
            } else {
                $prommise_type = '2';
            }

            $userid = $this->ion_auth->get_user_id();
            $data = array(
                'promise_id' => $promise_id,
                'order_no' => $order_no,
                'file_name' => $filename . ".jpg",
                'file_path' => $pathfile,
                'used' => 'Y',
                'create_date' => date("Y-m-d h:m:s"),
                'create_by' => $userid,
                'plan_master' => $plan_master,
                'file_name_scan' => $filename_scan . ".jpg",
                'pid' => $pid,
                'prommise_type' => $prommise_type,
                'file_type' => $typescan
            );


            $result = file_put_contents($uploadfile, base64_decode($image_data));

            $this->load->model(array('plan_model'));
            $db = $this->plan_model->insertImageScanPromise($data);

            //new
            $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
            $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
            $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
            $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");

            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($db));
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($e->getMessage()));
        }
    }

    public function get_promise_seq() {
        $this->load->model(array('promise_model'));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_seq_promise_data_seq()));
    }

    public function get_data_agreement_by_order_no() {
        $this->load->model(array('agreement_model'));

        $orderno = $this->input->post('oder_no');
        $masterplan = $this->input->post('master_plan');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->agreement_model->get_agreementid_by_order_no($orderno, $masterplan)));
    }

    public function get_scan_seq() {
        $this->load->model(array('plan_model'));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->plan_model->get_seq_scan_data_seq()));
    }

    //บันทึกบุคคลที่เกียวข้องที่ดิน
    public function insert_plan_person() {

        try {

            $this->load->model(array('promise_model'));
            $promiseid = $this->input->post('promiseid');
            $pid = $this->input->post('pid');
            $title = $this->input->post('title');
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $age = $this->input->post('age');
            $houseNumber = $this->input->post('houseNumber');
            $group = $this->input->post('group');
            $alley = $this->input->post('alley');
            $street = $this->input->post('street');
            $subDistrict = $this->input->post('subDistrict');
            $district = $this->input->post('district');
            $province = $this->input->post('province');
            $postcode = $this->input->post('postcode');
            $phone = $this->input->post('phone');
            $email = $this->input->post('email');
            $userid = $this->ion_auth->get_user_id();
            $plan_master = $this->input->post('plan_master');
            $issuedDate = $this->input->post('popissuedDate');
            $expiredDate = $this->input->post('popexpiredDate');
            $data = array(
                'promise_id' => $promiseid,
                'pid' => $pid,
                'title' => $title,
                'fname' => $firstname,
                'lname' => $lastname,
                'age' => $age,
                'home_no' => $houseNumber,
                'moo' => $group,
                'soi' => $alley,
                'street' => $street,
                'subdistrict' => $subDistrict,
                'district' => $district,
                'province' => $province,
                'postcode' => $postcode,
                'phone' => $phone,
                'email' => $email,
                'flag' => 'N',
                'used' => 'Y',
                'create_by' => $userid,
                'create_date' => date("Y-m-d h:m:s"),
                'plan_master' => $plan_master,
                'issuedDate' => $issuedDate,
                'expiredDate' => $expiredDate
            );

            $this->promise_model->insert_plan_person($data);
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    //บันทึกสัญญา
    public function insert_primise_plan() {

        try {

            $this->load->model(array('promise_model', 'plan_model'));
            $promiseid = $this->input->post('promiseid');
            $agreement_id = $this->input->post('agreement_id');
            $landid = $this->input->post('landid');
            $master_plan = $this->input->post('master_plan');
            $plans = $this->input->post('plans');
            $plan_total = $this->input->post('plan_total');
            $planprice = $this->input->post('planprice');
            $pricetotal = $this->input->post('pricetotal');
            $pricepay = $this->input->post('pricepay');
            $home_id = $this->input->post('home_id');
            $home_name = $this->input->post('home_name');
            $home_price = $this->input->post('home_price');
            $dateorder = $this->input->post('dateorder');
            $pay_price_promise = $this->input->post('pay_price_promise');
            $pay_price_promise_result = $this->input->post('pay_price_promise_result');
            $userid = $this->ion_auth->get_user_id();

            $db_order_date_hidden = $this->input->post('db_order_date_hidden');
            $db_hidden_date_create_agreenment = $this->input->post('db_hidden_date_create_agreenment');
            $db_agerrment_date_hidden = $this->input->post('db_agerrment_date_hidden');
            $witness_name1 = $this->input->post('witness_name1');
            $witness_lastname1 = $this->input->post('witness_lastname1');
            $witness_name2 = $this->input->post('witness_name2');
            $witness_lastname2 = $this->input->post('witness_lastname2');

            $promise_type1 = $this->input->post('promise_type1');
            $promise_type2 = $this->input->post('promise_type2');
            $promise_type3 = $this->input->post('promise_type3');
            
            $data = array(
                'promise_id' => $promiseid,
                'agreement_id' => $agreement_id,
                'land_no' => $landid,
                'plan_master' => $master_plan,
                'plan' => $plans,
                'plan_total' => $plan_total,
                'price_tarangwa' => $planprice,
                'price_total' => $pricetotal,
                'price_pay' => $pricepay,
                'home_price' => $home_price,
                'home_id' => $home_id,
                'home_name' => $home_name,
                //'date_promise' => $dateorder,
                'pay_price_promise' => $pay_price_promise, //ชำระเงินสัญญา
                'pay_price_promise_result' => $pay_price_promise_result, //ที่เหลือต้องชำระ
                'used' => 'Y',
                'create_by' => $userid,
                'create_date' => date("Y-m-d h:m:s"),
                'order_date' => $db_order_date_hidden,
                'argm_date' => $db_hidden_date_create_agreenment,
                'date_promise' => $db_agerrment_date_hidden,
                'witness_name1' => $witness_name1,
                'witness_lastname1' => $witness_lastname1,
                'witness_name2' => $witness_name2,
                'witness_lastname2' => $witness_lastname2,
                'promise_type1' => $promise_type1,
                'promise_type2' => $promise_type2,
                'promise_type3' => $promise_type3
            );

            $this->promise_model->insert_promise($data);
            $planarr = explode(",", $plans);
            for ($i = 0; $i < count($planarr); $i++) {
                $planname = str_replace(' ', '', $planarr[$i]);
                $this->plan_model->set_plan_stattus($planname, "S", $master_plan, $userid);
            }

            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    //แก้ไขสัญญาซื้อขาย 
    public function update_primise_plan() {

        try {

            $this->load->model(array('promise_model', 'plan_model'));
            $promiseid = $this->input->post('promiseid');
            $agreement_id = $this->input->post('agreement_id');
            $landid = $this->input->post('landid');
            $pay_price_promise = $this->input->post('pay_price_promise');
            $pay_price_promise_result = $this->input->post('pay_price_promise_result');
            $userid = $this->ion_auth->get_user_id();

            $witness_name1 = $this->input->post('witness_name1');
            $witness_lastname1 = $this->input->post('witness_lastname1');
            $witness_name2 = $this->input->post('witness_name2');
            $witness_lastname2 = $this->input->post('witness_lastname2');

            $data = array(
                'promise_id' => $promiseid,
                'agreement_id' => $agreement_id,
                'land_no' => $landid,
                'pay_price_promise' => $pay_price_promise, //ชำระเงินสัญญา
                'pay_price_promise_result' => $pay_price_promise_result, //ที่เหลือต้องชำระ
                'create_by' => $userid,
                'create_date' => date("Y-m-d h:m:s"),
                'witness_name1' => $witness_name1,
                'witness_lastname1' => $witness_lastname1,
                'witness_name2' => $witness_name2,
                'witness_lastname2' => $witness_lastname2,
            );

            $this->promise_model->update_promise($data, $promiseid);

            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    //บันทึกบุคคลที่เกียวข้องบ้าน
    public function insert_home_person() {

        try {

            $this->load->model(array('promise_model'));
            $promiseid = $this->input->post('promiseid');
            $pid = $this->input->post('pid');
            $title = $this->input->post('title');
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $age = $this->input->post('age');
            $houseNumber = $this->input->post('houseNumber');
            $group = $this->input->post('group');
            $alley = $this->input->post('alley');
            $street = $this->input->post('street');
            $subDistrict = $this->input->post('subDistrict');
            $district = $this->input->post('district');
            $province = $this->input->post('province');
            $postcode = $this->input->post('postcode');
            $phone = $this->input->post('phone');
            $email = $this->input->post('email');
            $issuedDate = $this->input->post('issuedDate');
            $expiredDate = $this->input->post('expiredDate');
            $userid = $this->ion_auth->get_user_id();
            $plan_master = $this->input->post('plan_master');

            $data = array(
                'promise_home_id' => $promiseid,
                'pid' => $pid,
                'title' => $title,
                'fname' => $firstname,
                'lname' => $lastname,
                'age' => $age,
                'home_no' => $houseNumber,
                'moo' => $group,
                'soi' => $alley,
                'street' => $street,
                'subdistrict' => $subDistrict,
                'district' => $district,
                'province' => $province,
                'postcode' => $postcode,
                'phone' => $phone,
                'email' => $email,
                'issuedDate' => $issuedDate,
                'expiredDate' => $expiredDate,
                'flag' => 'N',
                'used' => 'Y',
                'create_by' => $userid,
                'create_date' => date("Y-m-d h:m:s"),
                'plan_master' => $plan_master
            );

            $this->promise_model->insert_data_home_person($data);
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    public function update_person_plan_flag() {

        try {

            $this->load->model(array('promise_model'));
            $promiseid = $this->input->post('promiseid');
            $pid = $this->input->post('pid');
            $status = $this->input->post('flag');
            $userid = $this->ion_auth->get_user_id();
            $this->promise_model->set_plan_person_status($promiseid, $pid, $status, 0, $userid);
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    public function update_person_home_flag() {

        try {

            $this->load->model(array('promise_model'));
            $promiseid = $this->input->post('promiseid');
            $pid = $this->input->post('pid');
            $status = $this->input->post('flag');
            $userid = $this->ion_auth->get_user_id();
            $this->promise_model->set_home_person_status($promiseid, $pid, $status, 0, $userid);
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    //บันทึกสัญญาบ้าน
    public function insert_primise_home() {

        try {

            $this->load->model(array('promise_model', 'plan_model'));
            $promiseid = $this->input->post('promiseid');
            $agreement_id = $this->input->post('agreement_id');
            $landid = $this->input->post('landid');
            $landid2 = $this->input->post('landid3');
            $master_plan = $this->input->post('master_plan');
            $plans = $this->input->post('plans');
            $plan_total = $this->input->post('plan_total');
            $planprice = $this->input->post('planprice');
            $pricetotal = $this->input->post('pricetotal');
            $pricepay = $this->input->post('pricepay');
            $home_id = $this->input->post('home_id');
            $home_name = $this->input->post('home_name');
            $home_price = $this->input->post('home_price');
            //$dateorder = $this->input->post('dateorder');
            $pay_price_home = $this->input->post('pay_price_home');
            $pay_price_home_date = $this->input->post('pay_price_home_date');
            $pay_price_pirod1 = $this->input->post('pay_price_pirod1');
            $pay_price_pirod2 = $this->input->post('pay_price_pirod2');
            $pay_price_pirod3 = $this->input->post('pay_price_pirod3');

            $db_order_date_hidden = $this->input->post('db_order_date_hidden');
            $db_hidden_date_create_agreenment = $this->input->post('db_hidden_date_create_agreenment');
            $db_agerrment_date_hidden = $this->input->post('db_agerrment_date_hidden');
            $price_reduct = $this->input->post('price_reduct');
            $pay_price_pirod4 = $this->input->post('pay_price_pirod4');
            $pay_price_pirod5 = $this->input->post('pay_price_pirod5');
            $witness_name1 = $this->input->post('witness_name1');
            $witness_lastname1 = $this->input->post('witness_lastname1');
            $witness_name2 = $this->input->post('witness_name2');
            $witness_lastname2 = $this->input->post('witness_lastname2');

            $userid = $this->ion_auth->get_user_id();


            $data = array(
                'promise_home_id' => $promiseid,
                'agreement_id' => $agreement_id,
                'land_no' => $landid,
                'land_no2' => $landid2,
                'plan_master' => $master_plan,
                'plan' => $plans,
                'plan_total' => $plan_total,
                'price_tarangwa' => $planprice,
                'price_total' => $pricetotal,
                'price_pay' => $pricepay,
                'home_price' => $home_price,
                'home_id' => $home_id,
                'home_name' => $home_name,
                //'date_promise' => $dateorder,
                'pay_price_home' => $pay_price_home, //โดยชำระเงินค่าก่อสร้างบ้านเป็นเงิน
                'pay_price_home_date' => $pay_price_home_date, //และชำระเงินทำสัญญา ณ วันทำสัญญาเป็นเงิน
                'pay_price_pirod1' => $pay_price_pirod1,
                'pay_price_pirod2' => $pay_price_pirod2,
                'pay_price_pirod3' => $pay_price_pirod3,
                'used' => 'Y',
                'create_by' => $userid,
                'create_date' => date("Y-m-d h:m:s"),
                'order_date' => $db_order_date_hidden,
                'argm_date' => $db_hidden_date_create_agreenment,
                'date_promise' => $db_agerrment_date_hidden,
                'price_promotion' => $price_reduct,
                'pay_price_pirod4' => $pay_price_pirod4,
                'pay_price_pirod5' => $pay_price_pirod5,
                'witness_name1' => $witness_name1,
                'witness_lastname1' => $witness_lastname1,
                'witness_name2' => $witness_name2,
                'witness_lastname2' => $witness_lastname2,
            );

            $this->promise_model->insert_promise_home($data);

            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    //แก้ไขสัญญาซื้อขายบ้าน
    public function update_primise_home() {

        try {

            $this->load->model(array('promise_model', 'plan_model'));
            $promiseid = $this->input->post('promiseid');
            $agreement_id = $this->input->post('agreement_id');
            $landid = $this->input->post('landid');
            $landid2 = $this->input->post('landid2');
            $pay_price_home = $this->input->post('pay_price_home');
            $pay_price_home_date = $this->input->post('pay_price_home_date');
            $pay_price_pirod1 = $this->input->post('pay_price_pirod1');
            $pay_price_pirod2 = $this->input->post('pay_price_pirod2');
            $pay_price_pirod3 = $this->input->post('pay_price_pirod3');
            $userid = $this->ion_auth->get_user_id();

            $price_reduct = $this->input->post('price_reduct');
            $pay_price_pirod4 = $this->input->post('pay_price_pirod4');
            $pay_price_pirod5 = $this->input->post('pay_price_pirod5');
            $witness_name1 = $this->input->post('witness_name1');
            $witness_lastname1 = $this->input->post('witness_lastname1');
            $witness_name2 = $this->input->post('witness_name2');
            $witness_lastname2 = $this->input->post('witness_lastname2');


            $data = array(
                'promise_home_id' => $promiseid,
                'agreement_id' => $agreement_id,
                'land_no' => $landid,
                'land_no2' => $landid2,
                'pay_price_home' => $pay_price_home, //ชำระเงินสัญญา
                'pay_price_home_date' => $pay_price_home_date, //ที่เหลือต้องชำระ
                'pay_price_pirod1' => $pay_price_pirod1,
                'pay_price_pirod2' => $pay_price_pirod2,
                'pay_price_pirod3' => $pay_price_pirod3,
                'update_by' => $userid,
                'update_date' => date("Y-m-d h:m:s"),
                'price_promotion' => $price_reduct,
                'pay_price_pirod4' => $pay_price_pirod4,
                'pay_price_pirod5' => $pay_price_pirod5,
                'witness_name1' => $witness_name1,
                'witness_lastname1' => $witness_lastname1,
                'witness_name2' => $witness_name2,
                'witness_lastname2' => $witness_lastname2,
            );

            $this->promise_model->update_promise_home($data, $promiseid);

            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    public function deplte_person_plan() {

        try {

            $this->load->model(array('promise_model'));
            $promiseid = $this->input->post('promiseid');
            $pid = $this->input->post('pid');

            $userid = $this->ion_auth->get_user_id();
            $this->promise_model->set_plan_person_status($promiseid, $pid, 'N', 1, $userid);
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");


            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    public function deplte_person_home() {

        try {

            $this->load->model(array('promise_model'));
            $promiseid = $this->input->post('promiseid');
            $pid = $this->input->post('pid');

            $userid = $this->ion_auth->get_user_id();
            $this->promise_model->set_home_person_status($promiseid, $pid, 'N', 1, $userid);
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");


            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    public function get_person_plan_detail() {

        $this->load->model(array('promise_model'));
        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_person_plan_detail($promiseid)));
    }

    public function get_detail_by_promise_id() {

        $this->load->model(array('promise_model'));
        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_detail_by_promise_id($promiseid)));
    }

    public function get_detail_by_promisehome_id() {

        $this->load->model(array('promise_model'));
        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_detail_by_promisehome_id($promiseid)));
    }

    public function get_person_home_detail() {

        $this->load->model(array('promise_model'));
        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_person_home_detail($promiseid)));
    }

    public function get_person_plan_list() {

        $this->load->model(array('promise_model'));
        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_person_plan_list($promiseid)));
    }

    public function getorder_by_plan() {

        $this->load->model(array('order_model'));
        $orderno = $this->input->post('orderno');
        $planid = $this->input->post('planid');
        $master_plan = $this->input->post('master_plan');
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->order_model->get_order_by_view_promise($orderno, $planid, $master_plan)));
    }

    public function get_person_plan_view() {

        $this->load->model(array('promise_model'));
        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_person_plan_view($promiseid)));
    }

    public function get_person_home_view() {

        $this->load->model(array('promise_model'));
        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_person_home_view($promiseid)));
    }

    public function get_person_home_list() {

        $this->load->model(array('promise_model'));
        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_person_home_list($promiseid)));
    }

    public function count_person_plan() {
        $this->load->model(array('promise_model'));

        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->count_data_person_plan($promiseid)));
    }

    public function count_person_home() {
        $this->load->model(array('promise_model'));

        $promiseid = $this->input->post('promise_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->count_data_person_home($promiseid)));
    }

    function get_promise_id_by_agreement_id() {

        $this->load->model(array('promise_model'));
        $argreementid = $this->input->post('agreement_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_promise_id_by_agreement_id($argreementid)));
    }

    function get_home_promise_id_by_agreement_id() {

        $this->load->model(array('promise_model'));
        $argreementid = $this->input->post('agreement_id');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->get_data_home_promise_id_by_agreement_id($argreementid)));
    }

    public function chectk_personplan_promise() {
        $this->load->model(array('promise_model'));

        $promise_id = $this->input->post('promise_id');
        $pid = $this->input->post('pid');
        $plan_master = $this->input->post('plan_master');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->chectk_data_person_promise($promise_id, $pid, $plan_master)));
    }

    public function chectk_personhome_promise() {
        $this->load->model(array('promise_model'));

        $promise_id = $this->input->post('promise_id');
        $pid = $this->input->post('pid');
        $plan_master = $this->input->post('plan_master');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->promise_model->chectk_data_personhome_promise($promise_id, $pid, $plan_master)));
    }

    public function chectk_person_scan_card() {
        $this->load->model(array('file_model'));

        $promise_id = $this->input->post('promise_id');
        $pid = $this->input->post('pid');
        $plan_master = $this->input->post('plan_master');
        $promist_type = $this->input->post('promist_type');
        $file_typ = $this->input->post('file_typ');
        $order_no = $this->input->post('order_no');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->file_model->chectk_data_person_scan_card($order_no, $pid, $plan_master, $promist_type, $file_typ)));
    }

    function thai_date($time) {
        $thai_day_arr = array("อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์");
        $thai_month_arr = array(
            "0" => "",
            "1" => "มกราคม",
            "2" => "กุมภาพันธ์",
            "3" => "มีนาคม",
            "4" => "เมษายน",
            "5" => "พฤษภาคม",
            "6" => "มิถุนายน",
            "7" => "กรกฎาคม",
            "8" => "สิงหาคม",
            "9" => "กันยายน",
            "10" => "ตุลาคม",
            "11" => "พฤศจิกายน",
            "12" => "ธันวาคม"
        );

//        $thai_date_return = "วัน" . $thai_day_arr[date("w", $time)];
        $thai_date_return = "วันที่ " . date("j", $time);
        $thai_date_return .= " เดือน" . $thai_month_arr[date("n", $time)];
        $thai_date_return .= " พ.ศ." . (date("Y", $time) + 543);
        //$thai_date_return .= "  " . date("H:i", $time) . " น.";
        return $thai_date_return;
    }

    function conver_date($ttt) {
        $d1 = substr($ttt, 0, 2);
        $m1 = substr($ttt, 3, 2);
        $y = substr($ttt, 6, 4);
        $result = $y - 543;
        $date = $m1 . '-' . $d1 . '-' . $result;

        $ymd = DateTime::createFromFormat('m-d-Y', $date)->format('Y-m-d h:m:s');
        return $ymd;
    }

    //
    function cancel_promise_plan() {

        $this->load->model(array('promise_model', 'order_model'));
        $promise_id = $this->input->post('promise_id');
        $agreement_id = $this->input->post('agreement_id');
        $plan_master = $this->input->post('plan_master');
        $userid = $this->ion_auth->get_user_id();

        $success_plan = $this->promise_model->cancel_promise_plan($promise_id, $plan_master, $userid);
        if ($success_plan === 1) {
            $success_person = $this->promise_model->cancel_plan_person_status($promise_id, $plan_master, $userid);
            if ($success_person === 1) {
                $success_agreement = $this->promise_model->cancel_agreement($agreement_id, $plan_master, $userid);
                if ($success_agreement === 1) {

                    $ps = $this->order_model->get_plan_by_order_no($agreement_id, $plan_master);
                    for ($i = 0; $i < count($ps); $i++) {
                        $success_plan = $this->promise_model->cancel_plan($ps[$i]['plan_name'], $plan_master, $userid);
                    }

                    $success_order = $this->promise_model->cancel_order($agreement_id, $plan_master, $userid);

                    $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($success_order));
                }
            }
        }
    }

    function cancel_promise_home() {

        $this->load->model(array('promise_model', 'order_model'));
        $promise_id = $this->input->post('promise_id');
        $agreement_id = $this->input->post('agreement_id');
        $plan_master = $this->input->post('plan_master');
        $userid = $this->ion_auth->get_user_id();

        $success_plan = $this->promise_model->cancel_promise_home($promise_id, $plan_master, $userid);
        if ($success_plan === 1) {
            $success_person = $this->promise_model->cancel_home_person_status($promise_id, $plan_master, $userid);
            if ($success_person === 1) {

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($success_person));

//                $success_agreement = $this->promise_model->cancel_agreement($agreement_id, $plan_master, $userid);
//                if ($success_agreement === 1) {
//                    $ps = $this->order_model->get_plan_by_order_no($agreement_id, $plan_master);
//                    for ($i = 0; $i < count($ps); $i++) {
//                        $success_plan = $this->promise_model->cancel_plan($ps[$i]['plan_name'], $plan_master, $userid);
//                    }
//
//                    $success_order = $this->promise_model->cancel_order($agreement_id, $plan_master, $userid);
//
//                    $this->output
//                            ->set_content_type('application/json')
//                            ->set_output(json_encode($success_order));
//                }
            }
        }
    }

    public function getFileSmartCardByPromiseId() {

        $this->load->model(array('file_model'));
        $promise_id = $this->input->post('promise_id');
        $type = $this->input->post('type');
        $promise_type = $this->input->post('promise_type');

        if($promise_type == '1') {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($this->file_model->getDataFileSmartCardByPromiseId($promise_id, $type, $promise_type)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($this->file_model->getDataFileSmartCardByPromiseIdTab2($promise_id, $type, $promise_type)));
        }
    }

}
