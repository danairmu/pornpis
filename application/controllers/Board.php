<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Board extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation'));
        $this->load->model(array('reports_model', 'board_model'));
    }
    
    public function index() {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        
        $data = array();
        if($this->ion_auth->is_admin()){
            $data['userrole'] = "admin";
        }else{
            $data['userrole'] = "user";
        }

        $data['sale_filter'] = 'sale_filter';
		$data['sale_filter_dropdown_data'] = $this->reports_model->get_sale_filter_dropdown_data();
        $data['sale_filter_dropdown_default'] = '';
        $data['report_type'] = 1;

        //-- === CSS === --//
        $this->template->stylesheet->add("assets/plugins/chartist-js/dist/chartist.min.css");
        $this->template->stylesheet->add("assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"); 
        $this->template->stylesheet->add("assets/plugins/chartist-plugin-legend/chartist-plugin-legend.css"); 
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");
        $this->template->stylesheet->add("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css");

        //-- === JS === --//
        $this->template->javascript->add("js/board.js");
        $this->template->javascript->add("assets/plugins/chartist-js/dist/chartist.min.js");
        $this->template->javascript->add("assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js");
        $this->template->javascript->add("assets/plugins/chartist-plugin-legend/chartist-plugin-legend.js"); 
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js"); 
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js");

        
        
        // Set the title
        $this->template->title = 'Dashboard';
        $this->template->content->view('board', $data);
        $this->template->publish();
    }

    public function get_dashboard_data()
    {
        $user_id = $this->input->get('user_id');
        $data['data'] = $this->board_model->get_dashboard_data($user_id);
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

}
