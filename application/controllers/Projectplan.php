<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Projectplan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth'));
    }

    public function index() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        // Set the title
        $this->template->title = 'Register';
        $this->template->stylesheet->add('assets/css/projectplan.css');



        //arae
        $this->template->javascript->add('assets/slice/js/popper.min.js');
        $this->template->javascript->add('assets/slice/js/jquery.maphilight.min.js');
        $this->template->javascript->add('assets/slice/js/jquery.imagemapster.js');
        $this->template->stylesheet->add('assets/imageviewer/imageviewer.css');
        $this->template->javascript->add('assets/imageviewer/imageviewer.js');
        $this->template->javascript->add('js/projectplan.js');

        //zoom
        $this->template->javascript->add('assets/iviewer/jquery-ui.js');
        $this->template->javascript->add('assets/iviewer/imageMapResizer.min.js');
        $this->template->javascript->add('assets/iviewer/jquery.mousewheel.min.js');
        $this->template->javascript->add('assets/iviewer/popper.min.js');

        $data = array();
        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else if($this->ion_auth->is_management()){
            $data['userrole'] = "management";
        }else{
           $data['userrole'] = "general";
        }

//        $this->template->content->view('basic_note', $data);
        $this->template->content->view('projectplan', $data);
        $this->template->footer = '';
        $this->template->publish();
    }

    public function selecthome() {


        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data = array();

        
        
        $plan_type = $this->input->post('plan_type');
        $plan_master = $this->input->post('plan_master');
        $orderno = '';
        //edit select
        $flagagreement = '';
        if ($plan_type == '1') {
            $plan_array = $this->input->post('plans');
            $planarr = json_decode($plan_array);

            $planno = '';

            for ($i = 0; $i < count($planarr); $i++) {
                $order = $this->get_order_no_by_plan_id($planarr[$i], $plan_master);
                $orderno = $order['order_no'];
                $planno = $order['plan_id'];
                $home_price = $order['home_price'];
                $home_type = $order['home_type'];
                $home_name = $order['home_name'];
                $comment = $order['comment'];
            }
            $data['order_no'] = $orderno;
            $data['home_price'] = $home_price;
            $data['home_type'] = $home_type;
            $data['home_name'] = $home_name;
            $data['comment'] = $comment;

            $arr = array();
            $ps = $this->get_plan_by_order_no($orderno, $plan_master);
            for ($i = 0; $i < count($ps); $i++) {
                $arr[] = $ps[$i]['plan_name'];
                $planarr = $arr;
            }
        } else {
            $orderno = '0';

            $plan_array = $this->input->post('plans');
            $planarr = json_decode($plan_array);
            for ($i = 0; $i < count($planarr); $i++) {
                $this->load->model(array('plan_model'));
                $plans = $this->plan_model->get_data_plan_by_plan_id($planarr[$i], $plan_master);
                $p_home_id = $plans['p_home_id'];
                $home_price = $plans['p_price'];
            }
            $data['home_price'] = '';
            $data['home_type'] = $p_home_id;
            $data['order_no'] = '0';
            $data['comment'] = '';
            $data['home_name'] = '';
        }

        $result = 0.0;
        $dataPlan = '';
        $dataPlan2 = '';


        $items = array();
        $ris = array();
        $ngans = array();
        $was = array();
        $p_width = array();
        $p_height = array();
        $pricetarangwa = array();
        for ($i = 0; $i < count($planarr); $i++) {
            $dataarr = $this->get_plan_by_id($planarr[$i], $plan_master);
            $result = $dataarr[0]['p_price'];
            $items[] = $result;

            $ris[] = $dataarr[0]['p_ri'];
            $ngans[] = $dataarr[0]['p_ngan'];
            $was[] = $dataarr[0]['p_wa'];
            $p_width [] = $dataarr[0]['p_width'];
            $p_height [] = $dataarr[0]['p_height'];
            $pricetarangwa [] = $dataarr[0]['p_price_tarangwa'] == null ? '0' : $dataarr[0]['p_price_tarangwa'];

            $dataPlan .= $planarr[$i] .= ' , ';
            $dataPlan2 .= $planarr[$i];
        }

        //gethome_name
        $home = $this->get_home_list();
        $h_name1 = $home[0]['h_name'];
        $h_name2 = $home[1]['h_name'];
        $h_name3 = $home[2]['h_name'];
        $h_name4 = $home[3]['h_name'];
        $h_name5 = $home[4]['h_name'];
        $h_name6 = $home[5]['h_name'];
        $h_name7 = $home[6]['h_name'];
        $h_name8 = $home[7]['h_name'];

        $data['h_name1'] = $h_name1;
        $data['h_name2'] = $h_name2;
        $data['h_name3'] = $h_name3;
        $data['h_name4'] = $h_name4;
        $data['h_name5'] = $h_name5;
        $data['h_name6'] = $h_name6;
        $data['h_name7'] = $h_name7;
        $data['h_name8'] = $h_name8;


        //send show data
        $data['plans'] = rtrim($dataPlan, " , ");
        $data['prices'] = $this->sum_price($items);
        $data['status'] = $plan_type;
        $data['plan_id'] = $this->input->post('plan_id');



        $agreement = $this->get_data_agreementno_by_orderno($orderno, $plan_master);
        $agreementid = $agreement['agreement_id'];

        $this->load->model(array('promise_model'));
        $promise = $this->promise_model->count_data_promise($orderno);

        //new plan
        if ($promise !== 0) {
            $data['disableadd'] = "disabled";
            $data['disableedit'] = "disabled";
            $data['disablecancel'] = "disabled";
            $data['disableagreement'] = "disabled";
            $data['disableradio'] = "disabled";
        } else if ($plan_type === '0' || $plan_type === null || $plan_type === "") {
            $data['disableadd'] = "";
            $data['disableedit'] = "disabled";
            $data['disablecancel'] = "disabled";
            $data['disableagreement'] = "disabled";
            $data['disableradio'] = "";
        } else {
            $data['disableadd'] = "disabled";
            $data['disableedit'] = "";
            $data['disablecancel'] = "";
            $data['disableagreement'] = $flagagreement;
            $data['disableradio'] = "";
            //then mor data
        }

        if ($agreementid === null || $agreementid === '') {
            $data['disablepromise'] = "disabled";
        } else {
            $data['disablepromise'] = "";
        }
        //ri nang wa
        $total_ri = $this->sum_ri($ris);
        $total_ngan = $this->sum_ngan($ngans);
        $total_wa = $this->sum_wa($was);
        $total_tarangwa = $this->sum_tarangwa_result($pricetarangwa);

//        echo '$total_ri '. $total_ri;
//        echo '$total_ngan '. $total_ngan;
//        echo '$total_wa '. $total_wa;


        $sumdata = ($total_ri + $total_ngan + $total_wa);
        $data['sumAll'] = number_format($sumdata, 2);

        $sumAll = ((double) $total_ri + (double) $total_ngan + (double) $total_wa);
        $ri = (int) ($sumAll / 400);
        $data['ris'] = $ri;

        $ngan = (int) (($sumAll - ($ri * 400)) / 100);
        $data['ngan'] = $ngan;

        $wa = fmod($sumAll, 100);

        $data['wa'] = $wa;
        $width_value = $this->sum_width($p_width);
        $height_value = $this->sum_width($p_height);
        $data["p_width"] = $width_value;
        $data["p_height"] = $height_value;

        //sendata pass
        $data['data_plan'] = str_replace(' , ', '', $dataPlan2);
        $data['data_plan_save'] = rtrim($dataPlan, " , ");
        $data['data_price'] = $this->sum_price($items);
        $data['input_form_price_tarangwa'] = rtrim($total_tarangwa, " ; ");
        $data['plan_master'] = $plan_master;

        $this->template->title = 'Register!';
        $this->template->javascript->add('js/selecthome.js');
        $this->template->stylesheet->add('assets/css/selecthome.css');



        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else if($this->ion_auth->is_management()){
            $data['userrole'] = "management";
        }else{
           $data['userrole'] = "general";
        }


        $this->template->content->view('selecthome', $data);
        $this->template->footer = '';
        $this->template->publish();
    }

    public function sum_price($arr) {
        $sum = 0.0;
        for ($i = 0; $i < count($arr); $i++) {
            $sum += (int) ($arr[$i]);
        }
        return number_format($sum, 2);
    }

    public function sum_ri($arr) {
        $sum = 0.0;
        for ($i = 0; $i < count($arr); $i++) {
            $sum += (int) ($arr[$i]);
        }

        return number_format($sum, 2);
    }

    public function sum_ngan($arr) {
        $sum = 0.0;
        for ($i = 0; $i < count($arr); $i++) {
            $sum += (int) ($arr[$i]);
        }

        return number_format($sum * 100, 2);
    }

    public function sum_wa($arr) {
        $sum = 0.0;
        for ($i = 0; $i < count($arr); $i++) {
            $sum += (float) ($arr[$i]);
        }

        return number_format($sum, 2);
    }

    public function sum_tarangwa($arr) {
        $sum = 0.0;
        for ($i = 0; $i < count($arr); $i++) {
            $sum += (int) ($arr[$i]);
        }

        return number_format($sum, 2);
    }

    public function sum_tarangwa_result($arr) {
        $sum = 0.0;
        $result = '';
        for ($i = 0; $i < count($arr); $i++) {
            $sum = (int) $arr[$i];
            $result .= number_format($sum);
            $result .= ' ; ';
        }

        return $result;
    }

    public function sum_width($arr) {
        $sum = 0.0;
        for ($i = 0; $i < count($arr); $i++) {
            $sum += (double) ($arr[$i]);
        }

        return number_format($sum, 2);
    }

    public function sum_height($arr) {
        $sum = 0.0;
        for ($i = 0; $i < count($arr); $i++) {
            $sum += (double) ($arr[$i]);
        }

        return number_format($sum, 2);
    }

    public function bookhome() {


        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data = array();
         if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else if($this->ion_auth->is_management()){
            $data['userrole'] = "management";
        }else{
           $data['userrole'] = "general";
        }

        $data['input_form_plan'] = $this->input->post('input_form_plan');
        $plan_display = '';
        $disarr = $this->input->post('input_form_plan');
        for ($i = 0; $i < strlen($disarr); $i++) {
            $plan_display .= $disarr[$i] . ',';
        }
        $data['input_form_plan_display'] = rtrim($plan_display, ",");
        $data['input_form_price'] = $this->input->post('input_form_price');
        $data['input_form_price_tarangwa'] = $this->input->post('input_form_price_tarangwa');
        $piricebath = $this->input->post('input_form_price');
        $data['input_form_price_word'] = $this->num2thai(str_replace(',', '', $piricebath));
        $data['order_no'] = $this->input->post('hiden_order_no');

        $data['total_plan'] = strlen($this->input->post('input_form_plan'));
        $data['planssave'] = $this->input->post('planssave');
        $data['plan_master'] = $this->input->post('hiden_plan_master');
        $due_date = time();
        $data['due_date_order'] = date("Y-m-d h:m:s");
        $data['due_date_display'] = $this->thai_date($due_date);

        $data['home_type'] = $this->input->post('home_type2');
        $data['home_price'] = $this->input->post('home_price2');
        $data['home_name'] = $this->input->post('home_name2');

        $this->template->javascript->add('/js/bookhome.js');
        $this->template->javascript->add("/js/cropper.js");
        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap.css');
        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap-theme.css');
        $this->template->javascript->add('assets/plugins/bootstrap/js/bootstrap.js');

        //Calendar
//        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js");
//        $this->template->stylesheet->add("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css");
//        $this->template->javascript->add("assets/scan/scanner.js");
//        $this->template->javascript->add("assets/scan/download.js");
//        $this->template->javascript->add('assets/plugins/jquery/jquery-3.3.1.min.js');

        $this->template->stylesheet->add("assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker-thai.js");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js");

        $this->template->javascript->add('js/jquery.PrintArea.js');
        $this->template->javascript->add("assets/scan/scanner.js");
        $this->template->javascript->add("assets/scan/download.js");

        //crop
//        $this->template->javascript->add('js/crop/cropper.common.js');
//        $this->template->javascript->add('js/crop/cropper.esm.js');
        $this->template->javascript->add('js/crop/cropper.js');
//        $this->template->javascript->add('js/crop/cropper.min.js');
        $this->template->stylesheet->add("js/crop/cropper.css");
//        $this->template->stylesheet->add("js/crop/cropper.min.css");
        //BATHTEXT
//        $this->template->javascript->add('js/BAHTTEXT.js');
//        $this->template->javascript->add('js/BAHTTEXT.min.js');
        //test word
        $this->template->javascript->add("js/FileSaver.js");
        $this->template->javascript->add("js/jquery.wordexport.js");


        $this->template->javascript->add("js/watermarker.js");
        $this->template->stylesheet->add('assets/css/watermark.css');

        $this->template->content->view('bookhome', $data);
        $this->template->footer = '';
        $this->template->publish();
    }

    public function crop() {


        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data = array();
        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $this->template->javascript->add("js/crop.js");
        $this->template->javascript->add("js/cropper.js");
        $this->template->content->view('cropImage', $data);
        $this->template->footer = '';
        $this->template->publish();
    }

    public function scan() {


        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $data = array();
        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $data['order_no'] = $this->input->post('order_no_scan');
        $data['planssave'] = $this->input->post('input_form_plan_save_scan');
        $data['plan_master'] = $this->input->post('plan_master_scan');
        $data['input_form_price'] = $this->input->post('input_form_price_scan');
        $data['input_form_price_tarangwa'] = $this->input->post('input_form_price_tarangwa_scan');
        $data['input_form_plan'] = $this->input->post('input_form_plan_scan');
        $data['input_form_price_tarangwa'] = $this->input->post('input_form_price_tarangwa_scan');
        $data['input_form_plan'] = $this->input->post('input_form_plan_scan');
        $data['input_form_price_word'] = $this->input->post('input_form_price_word');

        $data['due_date_order'] = $this->input->post('due_date_order');
        $data['due_date_display'] = $this->input->post('due_date_display');

        $data['home_type'] = $this->input->post('scan_home_type');
        $data['home_name'] = $this->input->post('scan_home_name');
        $data['home_price'] = $this->input->post('scan_home_price');

        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap.css');
        $this->template->javascript->add('assets/plugins/bootstrap/js/bootstrap.min.js');
        $this->template->javascript->add('assets/scan/scanner.js');
        $this->template->javascript->add('assets/scan/download.js');
        $this->template->stylesheet->add('assets/css/scan.css');
        $this->template->javascript->add('js/jquery.PrintArea.js');
        $this->template->javascript->add('js/scan.js');
        //new
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");


        $this->template->stylesheet->add("assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js");

        $this->template->javascript->add("/js/cropper.js");
        $this->template->content->view('scan', $data);
        $this->template->footer = '';
        $this->template->publish();
    }

    public function backscan() {


        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data = array();
        if ($this->ion_auth->is_admin()) {
            $data['userrole'] = "admin";
        } else {
            $data['userrole'] = "user";
        }

        $data['order_no'] = $this->input->post('order_no_scan');
        $data['planssave'] = $this->input->post('input_form_plan_save_scan');
        $data['plan_master'] = $this->input->post('plan_master_scan');
        $data['input_form_price'] = $this->input->post('input_form_price_scan');
        $data['input_form_price_tarangwa'] = $this->input->post('input_form_price_tarangwa_scan');
        $data['input_form_plan'] = $this->input->post('input_form_plan_scan');
        $data['input_form_price_word'] = $this->input->post('input_form_price_word');
        $data['due_date_order'] = $this->input->post('due_date_order');
        $data['due_date_display'] = $this->input->post('due_date_display');

        $data['home_type'] = $this->input->post('scan_home_type');
        $data['home_name'] = $this->input->post('scan_home_name');
        $data['home_price'] = $this->input->post('scan_home_price');

        $this->template->javascript->add('/js/bookhome.js');
        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap.css');
        $this->template->stylesheet->add('assets/plugins/bootstrap/css/bootstrap-theme.css');
        $this->template->javascript->add('assets/plugins/bootstrap/js/bootstrap.js');

        $this->template->stylesheet->add("assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker-thai.js");
        $this->template->javascript->add("assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js");
        $this->template->javascript->add("/js/cropper.js");
        $this->template->javascript->add('js/jquery.PrintArea.js');
        $this->template->javascript->add("assets/scan/scanner.js");
        $this->template->javascript->add("assets/scan/download.js");
        $this->template->content->view('bookhome', $data);
        $this->template->footer = '';
        $this->template->publish();
    }

    public function set_plandata_status() {

        try {



            $this->load->model(array('plan_model', 'order_model'));

            $status = $this->input->post('status');
            $plans = $this->input->post('order_name');
            $master_plan = $this->input->post('master_plan');


            $planarr = json_decode($plans);
            $userid = $this->ion_auth->get_user_id();
            for ($i = 0; $i < count($planarr); $i++) {
                $id = str_replace(' ', '', $planarr[$i]);

                //setupdate status plan
                $this->plan_model->set_plan_stattus($id, $status, $master_plan, $userid);

                //get price by plan id
                $dataarr = $this->get_plan_by_id($id, $master_plan);


                $data = array(
                    'order_no' => $this->input->post('order_no'),
                    'plan_id' => $master_plan,
                    'plan_name' => $id,
                    'home_type' => $this->input->post('home_type'),
                    'home_price' => $this->input->post('home_price'),
                    'home_name' => $this->input->post('home_name'),
                    'plan_price' => $dataarr[0]['p_price'],
                    'order_status' => 'Y',
                    'used' => 'Y',
                    'comment' => $this->input->post('comment'),
                    'create_by' => $userid,
                    'create_date' => date("Y-m-d h:m:s"),
                    'ri' => $this->input->post('ri'),
                    'ngan' => $this->input->post('ngan'),
                    'wa' => $this->input->post('wa'),
                    'price_tarangwa' => $dataarr[0]['p_price_tarangwa'],
                    'order_date' => $this->input->post('order_date'),
                    'price_total_all' => $this->input->post('price_total_all'),
                );

                $order = $this->order_model->get_order_by_plan_id($id, $master_plan);
                if (count($order) <= 0) {
                    $this->plan_model->insertOrder($data);
                }
            }

            $checkdata = $this->input->post('check_data');
            $orderno = $this->input->post('order_no');
            $checkarr = json_decode($checkdata);

            for ($i = 0; $i < count($checkarr); $i++) {
                $planname = str_replace(' ', '', $checkarr[$i]);
                $data = array(
                    'p_empty_land' => 'Y',
                    'p_empty_price' => '3000'
                );

                $this->order_model->update_data_order($orderno, $planname, $master_plan, $data);
            }


            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    public function set_update_order() {

        try {
            $this->load->model(array('order_model'));
            $home_type = $this->input->post('home_type');
            $home_price = $this->input->post('home_price');
            $home_name = $this->input->post('home_name');
            $comment = $this->input->post('comment');
            $plans = $this->input->post('planid');
            $master_plan = $this->input->post('master_plan');
            $userid = $this->ion_auth->get_user_id();
            $planarr = json_decode($plans);

            $order_no = '';
            for ($i = 0; $i < count($planarr); $i++) {
                $planid = str_replace(' ', '', $planarr[$i]);
                $order = $this->get_order_no_by_plan_id($planid, $master_plan);
                $order_no = $order["order_no"];
                $this->order_model->update_order($order_no, $master_plan, $home_type, $home_price, $home_name, $comment, $userid);


                //unset check box
                $data = array(
                    'p_empty_land' => '',
                    'p_empty_price' => ''
                );

                $this->order_model->update_data_order($order_no, $planid, $master_plan, $data);
            }

            //start check bok
            $checkdata = $this->input->post('check_data');
            $orderno = $order_no;
            $checkarr = json_decode($checkdata);

            for ($i = 0; $i < count($checkarr); $i++) {
                $planname = str_replace(' ', '', $checkarr[$i]);
                $data = array(
                    'p_empty_land' => 'Y',
                    'p_empty_price' => '3000'
                );

                $this->order_model->update_data_order($orderno, $planname, $master_plan, $data);
            }
            //end check bok
            //update is has data in agreemant
            $agreement = $this->get_data_agreementno_by_orderno($order_no, $master_plan);
            $agreement_id = $agreement["agreement_id"];

            $data = array(
                'home_id' => $home_type,
                'home_price' => $home_price,
                'home_name' => $home_name,
                'update_by' => $userid,
                'update_date' => date("Y-m-d h:m:s")
            );
            if ($agreement_id !== null || $agreement_id !== '') {
                $this->agreement_model->update_agreement($agreement_id, $master_plan, $data);
            }


            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    public function set_cancel_plan() {

        try {
            $status = $this->input->post('status');
            $comment = $this->input->post('comment');
            $master_plan = $this->input->post('master_plan');
            $this->load->model(array('plan_model', 'order_model'));

            $plans = $this->input->post('planid');
            $userid = $this->ion_auth->get_user_id();

            $planarr = json_decode($plans);
            for ($i = 0; $i < count($planarr); $i++) {
                $id = str_replace(' ', '', $planarr[$i]);
                $this->plan_model->set_plan_stattus($id, $status, $master_plan, $userid);
                $order = $this->get_order_no_by_plan_id($id, $master_plan);
                $order_no = $order["order_no"];
                $this->plan_model->set_order_stattus($order_no, $id, $status, $comment, $userid, $master_plan);
            }

            $agreement = $this->get_data_agreementno_by_orderno($order_no, $master_plan);
            $agreement_id = $agreement["agreement_id"];


            $data = array(
                'used' => "N",
                'update_by' => $userid,
                'update_date' => date("Y-m-d h:m:s")
            );
            if ($agreement_id !== null && $agreement_id !== '') {
                $this->agreement_model->update_agreement($agreement_id, $master_plan, $data);
                $this->plan_model->update_scan_document($order_no, $master_plan, $data);
            }


            $this->output
                    ->set_content_type('application/json')
                    ->set_output("1");
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    //getusers for table
    public function getplan() {

        $id = $this->input->post('plan_name');
        $master_plan = $this->input->post('master_plan');
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_plan_by_id($id, $master_plan)));
    }

    public function getAearPlan() {

        $plan_array = $this->input->post('plans');
        $plan_master = $this->input->post('master_planid');

        $p_top = '';
        $p_buttom = '';
        $p_left = '';
        $p_right = '';
        $p_left_ex = '';
        $p_right_ex = '';

        $planarr = json_decode($plan_array);
        for ($i = 0; $i < count($planarr); $i++) {
            $plans = $this->get_plan_check_by_id($planarr[$i], $plan_master);
            $p_top .= $plans[0]['p_top'] === null ? "" : $plans[0]['p_top'] . ",";
            $p_buttom .= $plans[0]['p_buttom'] === null ? "" : $plans[0]['p_buttom'] . ",";
            $p_left .= $plans[0]['p_left'] === null ? "" : $plans[0]['p_left'] . ",";
            $p_right .= $plans[0]['p_right'] === null ? "" : $plans[0]['p_right'] . ",";
            $p_left_ex .= $plans[0]['p_left_ex'] === null ? "" : $plans[0]['p_left_ex'] . ",";
            $p_right_ex .= $plans[0]['p_right_ex'] === null ? "" : $plans[0]['p_right_ex'] . ",";
        }


        $result = $p_buttom . $p_top . $p_right . $p_left . $p_left_ex . $p_right_ex;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }

    public function getAearPlanCheck() {

        $planid = $this->input->post('planid');
        $plan_master = $this->input->post('master_planid');

        $p_top = '';
        $p_buttom = '';
        $p_left = '';
        $p_right = '';
        $p_left_ex = '';
        $p_right_ex = '';

        $plans = $this->get_plan_check_by_id($planid, $plan_master);
        $p_top .= $plans[0]['p_top'] === null ? "" : $plans[0]['p_top'] . ",";
        $p_buttom .= $plans[0]['p_buttom'] === null ? "" : $plans[0]['p_buttom'] . ",";
        $p_left .= $plans[0]['p_left'] === null ? "" : $plans[0]['p_left'] . ",";
        $p_right .= $plans[0]['p_right'] === null ? "" : $plans[0]['p_right'] . ",";
        $p_left_ex .= $plans[0]['p_left_ex'] === null ? "" : $plans[0]['p_left_ex'] . ",";
        $p_right_ex .= $plans[0]['p_right_ex'] === null ? "" : $plans[0]['p_right_ex'] . ",";
        $result = $p_buttom . $p_top . $p_right . $p_left . $p_left_ex . $p_right_ex;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }

    public function save_agreenment() {

        $plan_master = $this->input->post('plan_master');
        $this->load->model(array('agreement_model', 'promise_model'));

        $order_no = $this->input->post('order_no');
        $pid = $this->input->post('pid');
        $title = $this->input->post('title');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $age = $this->input->post('age');
        $houseNumber = $this->input->post('houseNumber');
        $group = $this->input->post('group');
        $alley = $this->input->post('alley');
        $street = $this->input->post('street');
        $subDistrict = $this->input->post('subDistrict');
        $district = $this->input->post('district');
        $province = $this->input->post('province');
        $postcode = $this->input->post('postcode');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $plan = $this->input->post('hidden_planssave');
        $plan_price_order = $this->input->post('plan_price_order');
        $plan_total = $this->input->post('plan_total');
        $planprice = $this->input->post('planprice');
        $pricetotal = $this->input->post('pricetotal_hidden');
        $check_land_price = $this->input->post('check_land_price');
        $pricepay = $this->input->post('pricepay');
        $dateorder = $this->input->post('hidden_datepicker');
        $agreement_id = $this->agreement_model->get_seq_agreement_data_seq();
        $pad_agreement_id = str_pad($agreement_id, 3, "0", STR_PAD_LEFT);
        $userid = $this->ion_auth->get_user_id();
        $plan_array = $this->input->post('input_hidden_plan_array');
        $date_for_database = $this->conver_date($dateorder);

        $home_type = $this->input->post('home_type');
        $home_name = $this->input->post('home_name');
        $home_price = $this->input->post('home_price');

        $planarr = json_decode($plan_array);

        $planid = '';
        $planidsearach = '';
        for ($i = 0; $i < count($planarr); $i++) {
            $planid .= str_replace(' ', '', $planarr[$i]) . ",";
            $planidsearach = str_replace(' ', '', $planarr[$i]);
        }
        $data = array(
            'agreement_seq' => $order_no . '-' . $pad_agreement_id,
            'agreement_id' => $order_no, //'P-' . $pad_agreement_id,
            'order_no' => $order_no,
            'pid' => $pid,
            'title' => $title,
            'fname' => $firstname,
            'lname' => $lastname,
            'age' => $age,
            'home_no' => $houseNumber,
            'moo' => $group,
            'soi' => $alley,
            'street' => $street,
            'subDistrict' => $subDistrict,
            'district' => $district,
            'province' => $province,
            'postcode' => $postcode,
            'phone' => $phone,
            'email' => $email,
            'plan' => $plan,
            'plan_total' => $plan_total,
            'price_tarangwa' => $planprice,
            'price_total' => $pricetotal,
            'land_empty_price' => $check_land_price,
            'price_pay' => $pricepay,
            'date_agreenment' => $date_for_database,
            'home_id' => $home_type,
            'home_name' => $home_name,
            'home_price' => $home_price,
            'used' => 'Y',
            'create_by' => $userid,
            'create_date' => date("Y-m-d h:m:s"),
            'plan_master' => $plan_master,
            'plan_price' => $plan_price_order
        );

        $dataplan_person = array(
            //'order_no' => $order_no,
            'promise_id' => $order_no,
            'plan_master' => $plan_master,
            'pid' => $pid,
            'title' => $title,
            'fname' => $firstname,
            'lname' => $lastname,
            'age' => $age,
            'home_no' => $houseNumber,
            'moo' => $group,
            'soi' => $alley,
            'street' => $street,
            'subdistrict' => $subDistrict,
            'district' => $district,
            'province' => $province,
            'postcode' => $postcode,
            'phone' => $phone,
            'email' => $email,
            'flag' => 'N',
            'used' => 'Y',
            'create_by' => $userid,
            'create_date' => date("Y-m-d h:m:s")
        );

        $datahome_person = array(
            //'order_no' => $order_no,
            'promise_home_id' => $order_no,
            'plan_master' => $plan_master,
            'pid' => $pid,
            'title' => $title,
            'fname' => $firstname,
            'lname' => $lastname,
            'age' => $age,
            'home_no' => $houseNumber,
            'moo' => $group,
            'soi' => $alley,
            'street' => $street,
            'subdistrict' => $subDistrict,
            'district' => $district,
            'province' => $province,
            'postcode' => $postcode,
            'phone' => $phone,
            'email' => $email,
            'flag' => 'N',
            'used' => 'Y',
            'create_by' => $userid,
            'create_date' => date("Y-m-d h:m:s")
        );

        $agreement = $this->get_data_agreementno_by_orderno($order_no, $plan_master);
        if (count($agreement) <= 0) {
            $this->agreement_model->insertArgreement($data);
            //$this->agreement_model->insertPsPerson($dataperson);
            $this->promise_model->insert_plan_person($dataplan_person);
            $this->promise_model->insert_data_home_person($datahome_person);
        }


        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode("1"));
    }

    public function set_update_agreement() {

        try {

            $plan_master = $this->input->post('plan_master');
            $this->load->model(array('agreement_model'));
            $userid = $this->ion_auth->get_user_id();
//            $agreementid = $this->input->post('agreementid_check');
            $order_no = $this->input->post('order_no');
            $agreementid = $this->input->post('order_no');
            $pid = $this->input->post('pid');
            $title = $this->input->post('title');
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $age = $this->input->post('age');
            $houseNumber = $this->input->post('houseNumber');
            $group = $this->input->post('group');
            $alley = $this->input->post('alley');
            $street = $this->input->post('street');
            $subDistrict = $this->input->post('subDistrict');
            $district = $this->input->post('district');
            $province = $this->input->post('province');
            $postcode = $this->input->post('postcode');
            $phone = $this->input->post('phone');
            $email = $this->input->post('email');
            $plan = $this->input->post('hidden_planssave');
            $plan_total = $this->input->post('plan_total');
            $planprice = $this->input->post('planprice');
            $pricetotal = $this->input->post('pricetotal_hidden');
            $check_land_price = $this->input->post('check_land_price');
            $pricepay = $this->input->post('pricepay');
            $dateorder = $this->input->post('hidden_datepicker');
            $home_type = $this->input->post('home_type');
            $home_name = $this->input->post('home_name');
            $home_price = $this->input->post('home_price');
            $date_for_database = $this->conver_date($dateorder);
            $plan_price_order = $this->input->post('plan_price_order');
//        for ($i = 0; $i < strlen($plan); $i++) {
            $data = array(
                'agreement_id' => $order_no, //$agreementid,
                'order_no' => $order_no,
                'pid' => $pid,
                'title' => $title,
                'fname' => $firstname,
                'lname' => $lastname,
                'age' => $age,
                'home_no' => $houseNumber,
                'moo' => $group,
                'soi' => $alley,
                'street' => $street,
                'subDistrict' => $subDistrict,
                'district' => $district,
                'province' => $province,
                'postcode' => $postcode,
                'phone' => $phone,
                'email' => $email,
                'plan' => $plan,
                'plan_total' => $plan_total,
                'price_tarangwa' => $planprice,
                'price_total' => $pricetotal,
                'land_empty_price' => $check_land_price,
                'price_pay' => $pricepay,
                'home_id' => $home_type,
                'home_name' => $home_name,
                'home_price' => $home_price,
                'date_agreenment' => $date_for_database,
                'update_by' => $userid,
                'update_date' => date("Y-m-d h:m:s"),
                'plan_price' => $plan_price_order,
            );

            $result = $this->agreement_model->update_agreement($agreementid, $plan_master, $data);
            $this->output
                    ->set_content_type('application/json')
                    ->set_output($result);
//        }
        } catch (Exception $exc) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output("0");
        }
    }

    function conver_date($ttt) {
        $d1 = substr($ttt, 0, 2);
        $m1 = substr($ttt, 3, 2);
        $y = substr($ttt, 6, 4);
        $result = $y - 543;
        $date = $m1 . '-' . $d1 . '-' . $result;

        $ymd = DateTime::createFromFormat('m-d-Y', $date)->format('Y-m-d h:m:s');
        return $ymd;
    }

    //getlist plan
    public function scanlist() {

        $order_no = $this->input->post('order_no');
        $plan_master = $this->input->post('plan_master_scan');
        $mode = $this->input->post('mode');

        $type = '';
        if ($mode === '1') {
            $type = '1';
        } else {
            $type = '2';
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_scanlist($order_no, $plan_master, $type)));
    }

    public function get_scanlist($order_no, $plan_master, $type) {
        $this->load->model(array('plan_model'));
        return $this->plan_model->get_data_scan_list($order_no, $plan_master, $type);
    }

    public function image_scan() {

        try {
            $filename = $this->input->post('filename');
            $pathfile = $this->input->post('pathfile');
            $image_data = $this->input->post('image_data');
            $order_no = $this->input->post('order_no');
            $plan_master = $this->input->post('plan_master');
            $mode = $this->input->post('mode');
            $pid = $this->input->post('pid');
            $filenameTopath = $this->input->post('filenameTopath');
            $type = '';
            //$uploadfile = 'scan/' . $filename . ".jpg";
            //local
            //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/scan/'. $filename . ".jpg";
            
            //เอกสารสแกน
            if ($mode === '1') {
                $type = '1';
                //local
                //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/scan/' . $filenameTopath . ".jpg";
                
                //server
                $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/scan/' . $filenameTopath . ".jpg";
            } else { //สแกนสำเนาบัตร
                //local
                //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/scancard/' . $filenameTopath . ".jpg";
               // $uploadfile2 = $_SERVER['DOCUMENT_ROOT'] . '/pornpis/scanpromisecard/' . $filenameTopath . ".jpg";
                
                //server
                $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/scancard/' . $filenameTopath . ".jpg";
                $uploadfile2 = $_SERVER['DOCUMENT_ROOT'] . '/scanpromisecard/' . $filenameTopath . ".jpg";
                
                $type = '2';
                //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/scancard/' . $filenameTopath . ".jpg";
            }


            $userid = $this->ion_auth->get_user_id();
            $this->load->model(array('plan_model'));

            $data = array(
                'order_no' => $order_no,
                'file_name' => $filename . ".jpg",
                'file_path' => $pathfile,
                'used' => 'Y',
                'create_date' => date("Y-m-d h:m:s"),
                'create_by' => $userid,
                'plan_master' => $plan_master,
                'file_name_path' => $filenameTopath . ".jpg",
                'file_type' => $type,
                'pid' => $pid
            );

            if ($type == '2') {
                $data2 = array(
                    //'promise_id' => $promise_id,
                    'order_no' => $order_no,
                    'file_name' => $filename . ".jpg",
                    'file_path' => $pathfile,
                    'used' => 'Y',
                    'create_date' => date("Y-m-d h:m:s"),
                    'create_by' => $userid,
                    'plan_master' => $plan_master,
                    'file_name_scan' => $filenameTopath . ".jpg",
                    'pid' => $pid,
                    'prommise_type' => '1',
                    'file_type' => $type
                );

                $outfile2 = $uploadfile2;
                $result = file_put_contents($outfile2, base64_decode($image_data));
                //สำเนาบัตรคนแรกทำรายการจอง
                $db = $this->plan_model->insertImageScanPromise($data2);
            }


            $outfile = $uploadfile;
            $result = file_put_contents($outfile, base64_decode($image_data));
            $db = $this->plan_model->insertImageScan($data);



            $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
            $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
            $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
            $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");

            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($db));
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($e->getMessage()));
        }
    }

    //get status plan
    public function getstatusplan($planid) {

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_plan_by_plan_id($planid)));
    }

    public function getHomeDataByOrderNo() {

        $this->load->model(array('home_model'));
        $order_no = $this->input->post('order_no');
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->home_model->get_data_home_by_orderno($order_no)));
    }

    public function getFilenameByOrderNo() {

        $this->load->model(array('file_model'));
        $order_no = $this->input->post('order_no');
        $type = $this->input->post('type');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->file_model->getFileByOrderNo($order_no, $type)));
    }

    public function get_agreementid() {

        $order_no = $this->input->post('order_no');
        $master_plan = $this->input->post('plan_master');
        $this->load->model(array('agreement_model'));

        $agreemant = $this->agreement_model->get_agreementid_by_order_no($order_no, $master_plan);
        if (count($agreemant) > 0) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($agreemant));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode("0"));
        }
    }

    public function getorder_by_planid() {

        $planid = $this->input->post('planid');
        $master_plan = $this->input->post('master_plan');
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_order_no_by_plan_id($planid, $master_plan)));
    }

    public function getorder_by_orderno() {

        $order_no = $this->input->post('order_no');
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_order_by_orderno($order_no)));
    }

    public function gethome_by_id_tojson($id) {

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_homelistbyid($id)));
    }

    public function get_plan_by_id($id, $master_plan) {
        $this->load->model(array('plan_model'));
        return $this->plan_model->get_data_plan_by_id($id, $master_plan);
    }

    public function get_plan_check_by_id($id, $master_plan) {
        $this->load->model(array('plan_model'));
        return $this->plan_model->get_data_check_plan_by_id($id, $master_plan);
    }

    public function get_plan_by_plan_id($planid) {
        $this->load->model(array('plan_model'));
        return $this->plan_model->get_status_plan_by_plan_id($planid);
    }

    public function get_order_no_by_plan_id($id, $master_plan) {
        $this->load->model(array('order_model'));
        return $this->order_model->get_order_by_plan_id($id, $master_plan);
    }

    public function get_order_by_orderno($order_no) {
        $this->load->model(array('order_model'));
        return $this->order_model->get_data_order_by_orderno($order_no);
    }

    public function get_plan_by_order_no($id, $master_plan) {
        $this->load->model(array('order_model'));
        return $this->order_model->get_plan_by_order_no($id, $master_plan);
    }

    public function get_order_seq() {
        $this->load->model(array('order_model'));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->order_model->get_seq_order_data_seq()));
    }

    public function get_scan_seq() {
        $this->load->model(array('plan_model'));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->plan_model->get_seq_scan_data_seq()));
    }

    public function get_data_agreementno_by_orderno($id, $master_plan) {
        $this->load->model(array('agreement_model'));
        return $this->agreement_model->get_agreementid_by_order_no($id, $master_plan);
    }

    public function get_home_list() {
        $this->load->model(array('home_model'));
        return $this->home_model->get_data_homelist();
    }

    public function get_homelistbyid($id) {
        $this->load->model(array('home_model'));
        return $this->home_model->get_data_home_by_id($id);
    }

    public function getData_imagelist($id) {

        $this->template->javascript->add('js/managementhome.js');
        $this->template->javascript->add("https://code.jquery.com/jquery-3.3.1.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
        $this->template->javascript->add("https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js");
        $this->template->stylesheet->add("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        $this->template->stylesheet->add("https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css");

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_Imagelist($id)));
    }

    public function get_Imagelist($id) {
        $this->load->model(array('home_model'));
        return $this->home_model->get_data_image_list($id);
    }

    public function getDataPlan() {

        $planid = $this->input->post('plan_name');
        $master_plan = $this->input->post('master_plan');
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->get_plan_by_id($planid, $master_plan)));
    }

    function thai_date($time) {
        $thai_day_arr = array("อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์");
        $thai_month_arr = array(
            "0" => "",
            "1" => "มกราคม",
            "2" => "กุมภาพันธ์",
            "3" => "มีนาคม",
            "4" => "เมษายน",
            "5" => "พฤษภาคม",
            "6" => "มิถุนายน",
            "7" => "กรกฎาคม",
            "8" => "สิงหาคม",
            "9" => "กันยายน",
            "10" => "ตุลาคม",
            "11" => "พฤศจิกายน",
            "12" => "ธันวาคม"
        );

//        $thai_date_return = "วัน" . $thai_day_arr[date("w", $time)];
        $thai_date_return = "วันที่ " . date("j", $time);
        $thai_date_return .= " เดือน" . $thai_month_arr[date("n", $time)];
        $thai_date_return .= " พ.ศ." . (date("Y", $time) + 543);
        //$thai_date_return .= "  " . date("H:i", $time) . " น.";
        return $thai_date_return;
    }

    function num2thai($number) {
        $t1 = array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $t2 = array("เอ็ด", "ยี่", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
        $zerobahtshow = 0; // ในกรณีที่มีแต่จำนวนสตางค์ เช่น 0.25 หรือ .75 จะให้แสดงคำว่า ศูนย์บาท หรือไม่ 0 = ไม่แสดง, 1 = แสดง
        (string) $number;
        $number = explode(".", $number);
        if (!empty($number[1])) {
            if (strlen($number[1]) == 1) {
                $number[1] .= "0";
            } else if (strlen($number[1]) > 2) {
                if ($number[1]{2} < 5) {
                    $number[1] = substr($number[1], 0, 2);
                } else {
                    $number[1] = $number[1]{0} . ($number[1]{1} + 1);
                }
            }
        }

        for ($i = 0; $i < count($number); $i++) {
            $countnum[$i] = strlen($number[$i]);
            if ($countnum[$i] <= 7) {
                $var[$i][] = $number[$i];
            } else {
                $loopround = ceil($countnum[$i] / 6);
                for ($j = 1; $j <= $loopround; $j++) {
                    if ($j == 1) {
                        $slen = 0;
                        $elen = $countnum[$i] - (($loopround - 1) * 6);
                    } else {
                        $slen = $countnum[$i] - ((($loopround + 1) - $j) * 6);
                        $elen = 6;
                    }
                    $var[$i][] = substr($number[$i], $slen, $elen);
                }
            }

            $nstring[$i] = "";
            for ($k = 0; $k < count($var[$i]); $k++) {
                if ($k > 0)
                    $nstring[$i] .= $t2[7];
                $val = $var[$i][$k];
                $tnstring = "";
                $countval = strlen($val);
                for ($l = 7; $l >= 2; $l--) {
                    if ($countval >= $l) {
                        $v = substr($val, -$l, 1);
                        if ($v > 0) {
                            if ($l == 2 && $v == 1) {
                                $tnstring .= $t2[($l)];
                            } elseif ($l == 2 && $v == 2) {
                                $tnstring .= $t2[1] . $t2[($l)];
                            } else {
                                $tnstring .= $t1[$v] . $t2[($l)];
                            }
                        }
                    }
                }
                if ($countval >= 1) {
                    $v = substr($val, -1, 1);
                    if ($v > 0) {
                        if ($v == 1 && $countval > 1 && substr($val, -2, 1) > 0) {
                            $tnstring .= $t2[0];
                        } else {
                            $tnstring .= $t1[$v];
                        }
                    }
                }

                $nstring[$i] .= $tnstring;
            }
        }
        $rstring = "";
        if (!empty($nstring[0]) || $zerobahtshow == 1 || empty($nstring[1])) {
            if ($nstring[0] == "")
                $nstring[0] = $t1[0];
            $rstring .= $nstring[0] . "บาท";
        }
        if (count($number) == 1 || empty($nstring[1])) {
            $rstring .= "ถ้วน";
        } else {
            $rstring .= $nstring[1] . "สตางค์";
        }
        return $rstring;
    }

    //test init plan
    public function testInitPlan() {

        $this->load->model(array('plan_model'));
        for ($i = 3; $i < 142; $i++) {
            $data = array(
                'p_name' => $i,
                'p_status' => 'N',
                'plan_id' => '2',
                'used' => 'Y',
                'create_date' => date("Y-m-d h:m:s"),
                'create_by' => 'Admin',
            );

            //$this->plan_model->testInsertPlan($data);
        }
    }

}
