<style type='text/css'>
    .boder-plan{
        border-style: solid; 
        color: white;
        border-width: 8px;
    }
</style>
<div class="container-fluid" style="min-height: 1400px;">
    <div class="pt-3"></div>
    <div class="card text-white" style="background-color: #2ca535;">
        <div class="card-header">
            <label class="font-weight-bold">ข้อมูลแปลง</label>
        </div>
        <div class="card-body text-dark" style="background-color: #5ebb6d;">
            <div class="row pt-2 text-right">
                <div class="col-md-2">
                    <div class="form-group pl-0">
                        <select id="inputState" class="form-control">
                            <option id="v1" selected value="1" style="background-color: white; color: black;">ผังที่ 1</option>
                            <option id="v2" value="2" style="background-color: white; color: black;">ผังที่ 2</option>
                            <option id="v3" value="3" style="background-color: white; color: black;">ผังที่ 3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="pt-2"></div>

            <table id="planlist" class="display responsive nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead class="thead-light bg-success">
                    <tr >
                        <th style="vertical-align : middle;text-align:center;" rowspan="2">ลำดับที่</th>
                        <th style="vertical-align : middle;text-align:center;" scope="col" rowspan="2">หมายเลขแปลง</th>
                        <th style="vertical-align : middle;text-align:center;">เนื้อที่</th>
                        <th style="vertical-align : middle;text-align:center;" scope="col" rowspan="2">กว้าง(เมตร)</th>
                        <th style="vertical-align : middle;text-align:center;" scope="col" rowspan="2">ยาว(เมตร)</th>
                        <th style="vertical-align : middle;text-align:center;" scope="col" rowspan="2">ราคา/แปลง(บาท)</th>
                        <th style="vertical-align : middle;text-align:center;" scope="col" rowspan="2"> ราคา/ตารางวา(บาท) </th>
                        <th style="vertical-align : middle;text-align:center;" class="text-center" scope="col" rowspan="2" >สถานะ</th>
                        <th style="vertical-align : middle;text-align:center;" class="text-center" scope="col" rowspan="2" >แบบบ้าน</th>
                        <th style="vertical-align : middle;text-align:center;" class="text-center" scope="col" rowspan="2" >หมายเหตุ</th>
                        <th style="vertical-align : middle;text-align:center;" scope="col" rowspan="2" >แก้ไข</th>
                    </tr>
                    <tr>
                        <th scope="col" >ไร่-งาน-วา</th>
                    </tr>
                </thead>
            </table>

        </div>
    </div>

    <div id="myModalUpdate" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="max-width: 550px;">
            <div class="modal-content w-100">
                <div class="modal-content p-3 " >
                    <div class="modal-header">
                        <h6 class="modal-title" id="myModalLabel"><i class="icon-star"></i> แก้ไขข้อมูลแปลงที่&nbsp;</a><label  id="planidlabel"></label></h6>
                        <button onclick="closeOptionModalEdit()" type="button" class="close">×</button>
                    </div>
                    <div class="modal-body ">
                        <form id="form">
                            <input type="hidden" name="planid" id="planid" />
                            <input type="hidden" name="plan_master" id="plan_master" />
                            <div class="row  pt-1" style="display: none;">
                                <div class="col-12">
                                    <table >
                                        <tr>
                                            <td width="105">เนื้อที่(ไร่)</td>
                                            <td width="75">
                                                <input type="hidden" name="ri" id="ri" class="form-control" required style="width:72px;  height: 30px;" oninput="numberOnly(this.id);" /> 
                                            </td>
                                            <td width="90" align="right">เนื้อที่(งาน)</td>
                                            <td width="80">
                                                <input type="hidden" name="ngan" id="ngan" class="form-control" required style="width:70px;  height: 30px;" oninput="numberOnly(this.id);" /> 
                                            </td>
                                            <td width="80" align="right">เนื้อที่(วา)</td>
                                            <td width="80">
                                                <input type="hidden" name="wa" id="wa" class="form-control" required style="width:75px;  height: 30px;" oninput="doubleOnly(this.id);"/> 
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-1">
                                <div class="col-12">
                                    <table >
                                        <tr>
                                            <td align="right" style="width: 100px;">ตารางวา</td>
                                            <td width="75">
                                                <input type="text" name="p_tarangwa" id="p_tarangwa" class="form-control" required style="width:230px;  height: 30px;" oninput="doubleOnly(this.id);" /> 
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 ">
                                <div class="col-12">
                                    <table border="0">    
                                        <tr>
                                            <td align="right" style="width: 100px;">เนื้อที่(กว้าง)</td>
                                            <td style="width: 140px;">
                                                <input type="text" name="p_width" id="p_width" class="form-control" style="width:129px;  height: 30px;" required oninput="doubleOnly(this.id);"/> 
                                            </td>
                                            <td style="width: 91px;" align="right">เนื้อที่(ยาว)</td>
                                            <td style="width: 132px;">
                                                <input type="text" name="p_height" id="p_height" class="form-control" style="width:134px;  height: 30px;" required oninput="doubleOnly(this.id);"/> 
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 ">
                                <div class="col-12">
                                    <table border="0">    
                                        <tr>
                                            <td align="right" style="width: 100px;">ราคา/แปลง</td>
                                            <td style="width: 100px;">
                                                <input type="text" name="price" id="price" class="form-control" style="width:130px;  height: 30px;" readonly="true" oninput="doubleOnly(this.id);"/> 
                                                <input type="hidden" name="price_hidden" id="price_hidden" > 
                                            </td>
                                            <td style="width: 100px;" align="right">ราคา/ตารางวา</td>
                                            <td style="width: 100px;">
                                                <input type="text" name="price_tarangwa" id="price_tarangwa" class="form-control" style="width:135px;  height: 30px;" required oninput="doubleOnly(this.id);"/> 
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 " id="div_conner">
                                <div class="col-12">
                                    <table border="0">    
                                        <tr>
                                            <td align="right" style="width: 100px;">ราคาแปลงมุม</td>
                                            <td width="150">
                                                <input type="hidden" name="hidden_setplan" id="hidden_setplan" > 
                                                <input type="hidden" name="hidden_setmasplan" id="hidden_setmasplan" > 
                                                <input type="text" name="price_conner" id="price_conner" class="form-control" style="width:130px;  height: 30px;" oninput="doubleOnly(this.id);"/> 
                                                <input type="hidden" name="price_conner_hidden" id="price_conner_hidden" > 
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row  pt-1">
                                <div class="col-12">
                                    <table >
                                        <tr>
                                            <td align="right" style="width: 100px;">แบบบ้าน</td>
                                            <td width="250">
                                                <select name="homes_id" class="form-control custom-select" id="homes_id" required >
                                                    <option value="0"> - Select - </option>
                                                    <?php foreach ($homes as $key => $value) { ?>
                                                        <option 
                                                            value="<?php echo $value['id'] ?>"><?php echo $value['h_name'] ?></option>
                                                        <?php } ?> 
                                                </select>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row  pt-2 ">
                                <div class="col-12">
                                    <table border="0">    
                                        <tr>
                                            <td align="right" style="width: 100px;">หมายเหตุ</td>
                                            <td><textarea class="form-control" name="comment" id="comment" cols="50"></textarea></td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-12 text-center">
                                    <div class="m-auto">
                                        <button type = "button" class="btn btn-success fa fa-edit p-1" id="btn_update_plan">แก้ไขข้อมูล</button>
                                        <button onclick="closeOptionModalEdit()" type="button" id="btn_close_plan" class="btn btn-success fa fa-close p-1">ยกเลิก</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--modal information-->
    <div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message">
        <div class="modal-dialog modal-sm" style="max-width: 400px;">
            <div class="modal-content">
                <div style="border-bottom: 1px solid #e9ecef;">
                    <div class="text-center">
                        <dd id="idmessage" class="pt-3"></dd>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <button type="button" class="btn btn-warning" id="modal-btn-n-message" style="width: 30%;" >ตกลง</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>