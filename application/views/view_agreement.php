<style>
    #xx.item:empty:before {
        content: "\200b"; 
    }
    @page{
        margin: 0cm;
    }

    /*    @font-face {
            font-family: "THSarabunNew";
            src: url("assets/fonts/THSarabunNew.ttf");
        }*/

    .font-title{
        /*        font-size:32px;
                font-family:"Angsana New";*/
    }

    .font-report{
        /*        font-size:18px;
                font-family:"Angsana New";*/
    }


    /*    @media print {
            table { font-size: 28pt }
        }
        @media screen {
            table { font-size: 28px }
        }
        @media screen, print {
            table { line-height: 1.2 }
        }*/
    /*    .printableArea{
            font-family: "Angsana New" !important;
            font-size:38px !important;
        }*/

    .preview {
        overflow: hidden;
        width: 200px; 
        height: 200px;
    }

</style>

<div class="container" style="display: none;">
    <div class="pt-1"></div>
    <div class="printableArea" id="printableAreatest">
        <style>
            table, tr, td{
                font-size:18px;
                /*font-family:"Angsana New";*/
            }
        </style>
        <div style="width: 100%; 
             border-style: solid; 
             color: #000;
             border-width: 8px;
             border-radius: 1px;" >
            <table style="width: 100%; height: 100%"  border="0" align="center" >
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/logo.jpg') ?>" width="150" height="120" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="47" colspan="5"><div align="center"  class="font-weight-bold" style="font-size: 32px;">ใบจอง</div></td>
                </tr>

                <tr>
                    <td width="70" height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 60px; font-size:30px; font-family:'Angsana New';" class="font-report">เลขที่ PPR-<label style="font-size:30px; font-family:'Angsana New';" class="font-report" id="agreenment_id1"></label></td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="70" height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td align="right" class="font-report"></td> <!-- <span id="date_view_1"></span> -->
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="86" height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 60px; font-size:30px; font-family:'Angsana New';" class="font-report">ข้าพเจ้า&nbsp;</td>
                                <td valign="bottom" class="font-report" style="font-size:30px; font-family:'Angsana New';">
                                    <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_fullname"></span></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50">&nbsp;</td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 109px; font-size:30px; font-family:'Angsana New';">อยู่บ้านเลขที่&nbsp;</td>
                                <td valign="bottom" style="width: 70px; font-size:30px; font-family:'Angsana New';" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_homeno"></span></div></td>
                                <td style="width: 50px; font-size:30px; font-family:'Angsana New';">หมู่ที่&nbsp;</td>
                                <td valign="bottom" style="width: 50px; font-size:30px; font-family:'Angsana New';" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_group"></span></div></td>
                                <td style="width: 50px; font-size:30px; font-family:'Angsana New';">ถนน&nbsp;</td>
                                <td valign="bottom" style="width: 130px; font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_street"></span></div></td>
                                <td style="width: 50px; font-size:30px; font-family:'Angsana New'" ;>ตำบล&nbsp;</td>
                                <td valign="bottom" style="font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_subDistrict"></span></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 35px; font-size:30px; font-family:'Angsana New'">อำเภอ&nbsp;</td>
                                <td valign="bottom" style="font-size:30px; font-family:'Angsana New'"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_district"></span></div></td>
                                <td style="width: 54px; font-size:30px; font-family:'Angsana New'">จังหวัด&nbsp;</td>
                                <td valign="bottom" style="width: 210px; font-size:30px; font-family:'Angsana New'"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_province"></span></div></td>
                                <td style="width: 120px; font-size:30px; font-family:'Angsana New'">รหัสไปรษณีย์&nbsp;</td>
                                <td valign="bottom" style="width: 193px; font-size:30px; font-family:'Angsana New'"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_postcode"></span></div></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td height="50">&nbsp;</td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New'">โทรศัพท์&nbsp;</td>
                                <td valign="bottom" style="width: 213px; font-size:30px; font-family:'Angsana New'" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_phone"></span></div></td>
                                <td style="width: 194px; font-size:30px; font-family:'Angsana New'">ซื้อที่ดินแปลงหมายเลข&nbsp;</td>
                                <td align="center" valign="bottom" style="font-size:30px; font-family:'Angsana New'"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_plan"></span></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New'">จำนวน&nbsp;</td>
                                <td style="width: 140px; font-size:30px; font-family:'Angsana New'" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_ag_total_plan"></span></div></td>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New'">แปลง&nbsp;</td>
                                <td style="width: 140px; font-size:30px; font-family:'Angsana New'">ราคาตารางวาละ&nbsp;</td>
                                <td align="center" style="font-size:30px; font-family:'Angsana New'"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_pricetarangwa"></span></div></td>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New'">บาท</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 70px; font-size:30px; font-family:'Angsana New'">เป็นเงิน&nbsp;</td>
                                <td style="width: 265px; font-size:30px; font-family:'Angsana New'" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_money"></span></div></td>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New'">บาท&nbsp;</td>
                                <td align="center" style="font-size:30px; font-family:'Angsana New'"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_chat_price"></span></div></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 209px; font-size:30px; font-family:'Angsana New'">โดยชำระเงินจองเป็นเงิน&nbsp;</td>
                                <td style="width: 222px; font-size:30px; font-family:'Angsana New'" valign="bottom" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_price_order"></span></div></td>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New'">บาท&nbsp;</td>
                                <td style="font-size:30px; font-family:'Angsana New'" valign="bottom" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_price_order_char"></span></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50">&nbsp;</td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 259px; font-size:30px; font-family:'Angsana New'">และนัดมาทำสัญญาภายในวันที่&nbsp;</td>
                                <td style="width: 380px; font-size:30px; font-family:'Angsana New' " valign="bottom" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_price_order_date"></span></div></tview_price_orderd>
                                <td style="font-size:30px; font-family:'Angsana New';">หากพ้นกำหนดดังกล่าว</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="29">&nbsp;</td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 130px; font-size:30px; font-family:'Angsana New'">ให้ถือว่าใบจองนี้เป็นอันยกเลิกและข้าพเจ้ายินยอมให้ค่าจองนี้ตกเป็นกรรมสิทธิ์ของบริษัท  ตติเชษฐ์ จำกัดทันทีโดยไม่โต้แย้งใด ๆ ทั้งสิ้น</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td height="50" colspan="2">&nbsp;</td>
                    <td width="200">&nbsp;</td>
                    <td colspan="2">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New'">ลงชื่อ&nbsp;</td>
                                <td style="width: 194px; font-size:30px; font-family:'Angsana New'" valign="bottom"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_price_order"></span></div></td>
                                <td style="font-size:30px; font-family:'Angsana New'">ผู้จอง</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" colspan="2">&nbsp;</td>
                    <td width="445">&nbsp;</td>
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New'">ลงชื่อ&nbsp;</td>
                                <td style="width: 194px; font-size:30px; font-family:'Angsana New'" valign="bottom"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_price_order"></span></div></td>
                                <td style="font-size:30px; font-family:'Angsana New'">ผู้รับจอง/ผู้รับเงิน</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5"><div align="center"><small>การชำระเงินด้วยเช็ค  จะถือว่าชำระเสร็จแล้วต่อเมื่อเช็คนั้นขึ้นเงิน  และผ่านเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว</small></div></td>
                </tr>

                <tr>
                    <td  colspan="5">
                        <div align="center" class="pt-2" style="width: 100%">
                            <div style="width: 380px; height: 350px;" id="imagecardagreement"></div>


                        </div>
                    </td>
                </tr>
<!--                <tr>
                    <td>
                        <div class="preview">xxx</div>
                    </td>
                </tr>-->
                <tr>
                    <td  height="86" colspan="5" align="center" valign="bottom">88  หมู่ที่ 2 ต.บ้านกรด อ.บางปะอิน จ.พระนครศรีอยุธยา 13160</td>
                </tr>
            </table>
        </div>
    </div>
</div>
