<div class="container-fluid" style="min-height: 1400px;">
    <div class="pt-3"></div>
    <div class="card" style="background-color: #2ca535;">
        <div class="card-header text-white">
            <label class="font-weight-bold">จัดการข้อมูลแบบบ้าน</label>
        </div>
        <div class="card-body text-dark" style="background-color: #5ebb6d;">
            <table id="planhomelist" class="display responsive nowrap table table-hover table-striped table-bordered " cellspacing="0" width="100%" >
                <thead class="thead-light bg-success">
                    <tr>
                        <th scope="col" width="8%">ลำดับที่</th>
                        <th scope="col" class="text-center">รูปแบบ</th>
                        <th scope="col" class="text-center">ราคา(บาท)</th>
                        <th scope="col" class="text-center">พื้นทีใช้สอย</th>
                        <th scope="col" class="text-center">หมายเหตุ</th>
                        <th scope="col" width="8%">แก้ไข</th>
                        <th scope="col" width="8%">ภาพ</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
<!--<img id='base64image' src='<//?= $imgdata ?>' width="800" height="400"/>-->

    <div id="myModalUpdate" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="max-width: 600px;">
            <div class="modal-content w-100">
                <div class="modal-content p-3 " >
                    <div class="modal-header">
                        <h6 class="modal-title" id="myModalLabel"><i class="icon-star"></i> แก้ไขข้อมูลบ้าน <label  id="planlabel"></label></a></h6>
                        <button onclick="closeOptionModalEdit()" type="button" class="close">×</button>
                    </div>
                    <div class="modal-body">

                        <form id="form">
                            <div id="hiddenuserid"></div>

                            <div class="row  pt-2 ">
                                <div class="col-12">
                                    <table border="0">    
                                        <tr>
                                            <td width="100">รูปแบบ</td>
                                            <td width="180">
                                                <input type="hidden"  name="homeid" id="homeid" />
                                                <input type="text" name="home_name" id="home_name" class="form-control" style="width:170px;  height: 30px;" required/> 
                                            </td>
                                            <td width="80" align="right">ราคา</td>
                                            <td width="125">
                                                <input type="text" name="price" id="price" class="form-control" style="width:120px;  height: 30px;" oninput="doubleOnly(this.id);" required/> 
                                            </td>
                                            <td width="10">(บาท)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 ">
                                <div class="col-12">
                                    <table border="0">    
                                        <tr>
                                            <td width="100">พื้นทีใช้สอย</td>
                                            <td width="180">
                                                <input type="text" name="area" id="area" class="form-control" style="width:170px;  height: 30px;" oninput="doubleOnly(this.id);" required/> 
                                            </td> 
                                            <td width="15">(ตร.เมตร)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 ">
                                <div class="col-12">
                                    <table border="0">    
                                        <tr>
                                            <td width="100">เนื้อที่(กว้าง)</td>
                                            <td width="80">
                                                <input type="text" name="home_width" id="home_width" class="form-control" style="width:70px;  height: 30px;" required oninput="doubleOnly(this.id);"/> 
                                            </td>
                                            <td width="80" align="right">เนื้อที่(ยาว)</td>
                                            <td width="70">
                                                <input type="text" name="home_height" id="home_height" class="form-control" style="width:70px;  height: 30px;" required oninput="doubleOnly(this.id);"/> 
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 ">
                                <div class="col-12">
                                    <table border="0">    
                                        <tr>
                                            <td width="100">หมายเหตุ</td>
                                            <td width="382">
                                                <textarea class="form-control" name="comment" id="comment" width="382"></textarea> 
                                            </td>                                           
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-12 text-center">
                                    <div class="m-auto">
                                        <button type = "button" id="btn_update_home" class="btn btn-success fa fa-edit p-1">แก้ไขข้อมูล</button>
                                        <button onclick="closeOptionModalEdit()" type="button" class="btn btn-success fa fa-close p-1">ยกเลิก</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="myModalImage" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="max-width: 800px;">
            <div class="modal-content w-100">
                <div class="modal-content p-3 " >
                    <div class="modal-header">
                        <h6 class="modal-title" id="myModalLabel"><i class="icon-star"></i> แก้ไขรูปภาพ <label  id="planlabelimage"></label></a></h6>
                        <button onclick="closeOptionModalImage()" type="button" class="close">×</button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-12 text-right">
                                <div class="m-auto">

                                    <label class="btn btn-success waves-effect p-1" >
                                        เพิ่มรูปภาพ <input type="file" style="display: none;" name="file" id="file">
                                    </label>

                                    <input type="hidden"  name="homeidimage" id="homeidimage" />
                                    <input type="hidden"  name="hiddenpath" id="hiddenpath" value="<?= base_url('upload') ?>"/>

                                </div>
                            </div>
                        </div>
                        <div class="pt-2"></div>
                        <table id="imagehomelist" class="display nowrap table table-hover table-striped table-bordered p-1" cellspacing="0" width="100%" >
                            <thead class="thead-light bg-success">
                                <tr>
                                    <th scope="col" width="15%">ลำดับที่</th>
                                    <!--<th scope="col">แบบบ้าน</th>-->
                                    <th scope="col" class="text-center">รูปภาพ</th>
                                    <th scope="col" width="8%">View</th>
                                    <th scope="col" width="8%">Delete</th>
                                </tr>
                            </thead>
                        </table>            
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="myModalViewImage" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px; height: 400px;">

            <button onclick="closeOptionViewImage()" type="button" class="close">×</button>

            <div id="imagedata"></div>


        </div>
    </div>


    <!--modal information-->
    <div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message">
        <div class="modal-dialog modal-sm" style="max-width: 400px;">
            <div class="modal-content">
                <div style="border-bottom: 1px solid #e9ecef;">
                    <div class="text-center">
                        <dd id="idmessage" class="pt-3"></dd>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <button type="button" class="btn btn-warning" id="modal-btn-n-message" style="width: 30%;" >ตกลง</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>