<!-- Grid row -->
<div class="container-fluid" style="min-height: 1000px;">

    <div class="row pt-3">

        <!-- Grid column -->
        <div class="col-md-12 d-flex justify-content-center mb-2" >
            <div style="width: 100%;" align="center">
<!--                <button type="button" class="btn btn-outline-black waves-effect filter" style="margin: 3px; width: 100px; background-color: #28a745; color: white; border: 2px solid white;" data-rel="all">ทั้งหมด</button>
                <button type="button" class="btn btn-outline-black waves-effect filter" style="margin: 3px; width: 100px; background-color: #28a745; color: white; border: 2px solid white;" data-rel="1">Modern</button>
                <button type="button" class="btn btn-outline-black waves-effect filter" style="margin: 3px; width: 100px; background-color: #28a745; color: white; border: 2px solid white;" data-rel="2">Standard</button>-->
            </div>

        </div>
        <!-- Grid column -->

    </div>
    <!-- Grid row -->

    <!-- Grid row -->
    <div class="gallery" id="gallery">

        <div class="row">
            <div class="col-md-12">
                <div class="mb-3 pics">
                    <div class="section-box-ten">
                        <figure>
                            <h5>แบบบ้านที่ 1</h5>
                            <p>รายละเอียดข้อมูล</p>
                            <a href="#" class="btn btn-read" id="detail1">รายละเอียด...</a>
                        </figure>
                        <a href="#" title="image 5" class="thumb pics animation all 1" id="image1">
                            <img class="img-homes1" src="<?= base_url('assets/images/homes1.jpg') ?>" alt="Card image cap" width="560" height="300" style="width: 650px; height: 300px;">
                        </a>
                    </div>
                </div>

                <div class="mb-3 pics">
                    <div class="section-box-ten">
                        <figure>
                            <h5>แบบบ้านที่ 2</h5>
                            <p>รายละเอียดข้อมูล</p>
                            <a href="#" class="btn btn-read" id="detail2" >รายละเอียด...</a>
                        </figure>
                        <a href="#" title="image 5" class="thumb pics animation all 1" id="image2">
                            <img class="img-homes2"  src="<?= base_url('assets/images/homes2.jpg') ?>" alt="Card image cap2" width="560" height="300" style="width: 650px; height: 300px;">
                        </a>
                    </div>
                </div>

                <div class="mb-3 pics">
                    <div class="section-box-ten">
                        <figure>
                            <h5>แบบบ้านที่ 3</h5>
                            <p>รายละเอียดข้อมูล</p>
                            <a href="#" class="btn btn-read" id="detail3">รายละเอียด...</a>
                        </figure>
                        <a href="#" title="image 5" class="thumb pics animation all 1" id="image3">
                            <img class="img-fluid" src="<?= base_url('assets/images/homes3.jpg') ?>" alt="Card image cap" width="560" height="300" style="width: 650px; height: 300px;">
                        </a>
                    </div>
                </div>    

            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="mb-3 pics">
                    <div class="section-box-ten">
                        <figure>
                            <h5>แบบบ้านที่ 4</h5>
                            <p>รายละเอียดข้อมูล</p>
                            <a href="#" class="btn btn-read" id="detail4">รายละเอียด...</a>
                        </figure>
                        <a href="#" title="image 5" class="thumb pics animation all 2" id="image4">
                            <img class="img-fluid" src="<?= base_url('assets/images/homem1.jpg') ?>" alt="Card image cap" width="560" height="300" style="width: 650px; height: 300px;">
                        </a>
                    </div>
                </div>

                <div class="mb-3 pics">
                    <div class="section-box-ten">
                        <figure>
                            <h5>แบบบ้านที่ 5</h5>
                            <p>รายละเอียดข้อมูล</p>
                            <a href="#" class="btn btn-read" id="detail5">รายละเอียด...</a>
                        </figure>
                        <a href="#" title="image 5" class="thumb pics animation all 2" id="image5">
                            <img class="img-fluid" src="<?= base_url('assets/images/homem2.jpg') ?>" alt="Card image cap" width="560" height="300" style="width: 650px; height: 300px;">
                        </a>
                    </div>
                </div>

                <div class="mb-3 pics">
                    <div class="section-box-ten">
                        <figure>
                            <h5>แบบบ้านที่ 6</h5>
                            <p>รายละเอียดข้อมูล</p>
                            <a href="#" class="btn btn-read" id="detail6">รายละเอียด...</a>
                        </figure>
                        <a href="#" title="image 5" class="thumb pics animation all 2" id="image6">
                            <img class="img-fluid" src="<?= base_url('assets/images/homem3.jpg') ?>" alt="Card image cap" width="560" height="300" style="width: 650px; height: 300px;" >
                        </a>
                    </div>
                </div>    

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="mb-3 pics">
                    <div class="section-box-ten">
                        <figure>
                            <h5>แบบบ้านที่ 7</h5>
                            <p>รายละเอียดข้อมูล</p>
                            <a href="#" class="btn btn-read" id="detail7">รายละเอียด...</a>
                        </figure>
                        <a href="#" title="image 5" class="thumb pics animation all 1" id="image7">
                            <img class="img-fluid" src="<?= base_url('assets/images/homev1.jpg') ?>" alt="Card image cap" width="560" height="300" style="width: 650px; height: 300px;">
                        </a>
                    </div>
                </div>

                <div class="mb-3 pics">
                    <div class="section-box-ten">
                        <figure>
                            <h5>แบบบ้านที่ 8</h5>
                            <p>รายละเอียดข้อมูล</p>
                            <a href="#" class="btn btn-read" id="detail8">รายละเอียด...</a>
                        </figure>
                        <a href="#" title="image 5" class="thumb pics animation all 2" id="image8">
                            <img class="img-fluid" src="<?= base_url('assets/images/homev2.jpg') ?>" alt="Card image cap" width="560" height="300" style="width: 650px; height: 300px;">
                        </a>
                    </div>
                </div>

                <div class="mb-3 pics">
                    <div class="section-box-ten">
                        <figure>
                            <h5>แบบบ้านที่ 9</h5>
                            <p>รายละเอียดข้อมูล</p>
                            <a href="#" class="btn btn-read" id="detail9">รายละเอียด...</a>
                        </figure>
                        <a href="#" title="image 5" class="thumb pics animation all 1" id="image9">
                            <img class="img-fluid" src="<?= base_url('assets/images/homev3.jpg') ?>" alt="Card image cap" width="560" height="300" style="width: 650px; height: 300px;">
                        </a>
                    </div>
                </div>    

            </div>
        </div>


    </div>
    <!-- Grid row -->
</div>

<!-- .modal-profile -->
<div class="modal fade modal-profile" tabindex="-1" role="dialog" aria-labelledby="modalProfile" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-height: 900px; max-width: 1100px;">
        <div class="modal-content">
            <!--            <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal">×</button>
                            <h3 class="modal-title"></h3>
                        </div>-->
            <div class="row">
                <div class="col-md-8">
                    <div class="modal-body" >

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pt-3">
                        <table>
                            <tr>
                                <td class="font-weight-bold">รายละเอียด</td>
                            </tr>
                            <tr>
                                <td><span id="detail1">รอการ update ข้อมูล</span></td>
                                <td><span id="detail2"></span></td>
                            </tr>
                            
                        </table>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>