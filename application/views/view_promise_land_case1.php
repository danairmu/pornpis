

<div class="container"  style="display: none;">
    <div class="pt-1"></div>
    <div  id="land_case1_page1" class="printableArea1">
        <style>
            .report-font{
                font-size:27px; 
                font-family:'Angsana New'; 
                font-style: normal;
            }
        </style>
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_ppl"></span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/logo.jpg') ?>" width="150" height="120" />
                        </div>
                    </td>
                </tr>


                <tr>
                    <td height="47" colspan="5">
                        <div align="center"  style="font-size: 14px;">
                            บริษัท ตติเชษฐ์ จำกัด เลขที่ 88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160
                            โทร 035-881888-9 โทรสาร 035-881881
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td  class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 60px; font-size:30px; font-family:'Angsana New';" >
                                    <div align="center">สัญญาจะซื้อจะขายที่ดินจัดสรร(เฉพาะที่ดิน)</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date_plan"> </span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        สัญญานี้ทำขึ้นระหว่างบริษัท ตติเชษฐ์ จำกัด โดยผู้มีอำนาจลงนามท้ายสัญญา ที่ตั้งสำนักงานเลขที่ 
                                        88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160 ซึ่งต่อไปในสัญญานี้เรียกว่า

                                        “ผู้จะขาย” ฝ่ายหนึ่ง กับ 
                                        <span id="view_fullname"></span>
                                        <span id="view_homeno1_text" >อยู่บ้านเลขที่</span><span id="view_homeno"></span>
                                        <span id="view_street1_text" >ถนน&nbsp;</span><span id="view_street"></span>
                                        <span id="view_subdistrict1_text" >ตำบล&nbsp;</span><span id="view_subdistrict"></span>
                                        <span id="view_district1_text" >อำเภอ&nbsp;</span><span id="view_district"></span>
                                        <span id="view_province1_text" >จังหวัด&nbsp;</span><span id="view_province"></span>
                                        <span id="view_postcode1_text" >รหัสไปรษณีย์&nbsp;</span><span id="view_postcode"></span>
                                        <span id="view_phone1_text" >โทรศัพท์&nbsp;</span><span id="view_phone"></span>

                                        <span id="view_homeno2_text" style="display: none;">อยู่บ้านเลขที่</span><span id="view_homeno2"></span>
                                        <span id="view_street2_text" style="display: none;">ถนน&nbsp;</span><span id="view_street2"></span>
                                        <span id="view_subdistrict2_text" style="display: none;">ตำบล&nbsp;</span><span id="view_subdistrict2"></span>
                                        <span id="view_district2_text" style="display: none;">อำเภอ&nbsp;</span><span id="view_district2"></span>
                                        <span id="view_province2_text" style="display: none;">จังหวัด&nbsp;</span><span id="view_province2"></span>
                                        <span id="view_postcode2_text" style="display: none;">รหัสไปรษณีย์&nbsp;</span><span id="view_postcode2"></span>
                                        <span id="view_phone2_text" style="display: none;">โทรศัพท์&nbsp;</span><span id="view_phone2"></span>


                                        <span id="view_homeno3_text" style="display: none;">อยู่บ้านเลขที่</span> <span id="view_homeno3" style="display: none;"></span>
                                        <span id="view_street3_text" style="display: none;">ถนน&nbsp;</span><span id="view_street3" style="display: none;"> </span>
                                        <span id="view_subdistrict3_text" style="display: none;">ตำบล&nbsp;</span><span id="view_subdistrict3" style="display: none;"></span>
                                        <span id="view_district3_text" style="display: none;">อำเภอ&nbsp;</span><span id="view_district3" style="display: none;"></span>
                                        <span id="view_province3_text" style="display: none;">จังหวัด&nbsp;</span><span id="view_province3" style="display: none;"></span>
                                        <span id="view_postcode3_text" style="display: none;">รหัสไปรษณีย์&nbsp;</span><span id="view_postcode3" style="display: none;"></span>
                                        <span id="view_phone3_text" style="display: none;">โทรศัพท์&nbsp;</span><span id="view_phone3" style="display: none;"></span>

                                        ซึ่งต่อไปในสัญญานี้เรียกว่า “ผู้จะซื้อ” อีกฝ่ายหนึ่ง
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        โดยที่ผู้จะขายเป็นเจ้าของโครงการจัดสรรที่ดินชื่อ “บ้านพรพิศ 2” ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 
                                        ซึ่งต่อไปในสัญญานี้เรียกว่า “โครงการ” โดยได้รับอนุญาตให้ทำการจัดสรรที่ดินตามกฎหมายว่าด้วยการจัดสรรที่ดิน 
                                        ตามใบอนุญาตเลขที่  9/2551   ลงวันที่ 21 พฤษภาคม  2551 ดังปรากฏตามเอกสารแนบท้ายสัญญาฉบับนี้ 
                                        และทั้งสองฝ่ายตกลงทำสัญญาจะซื้อจะขายที่ดินจัดสรร ดังต่อไปนี้
                                    </div>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 1. รายละเอียดราคาที่ดิน
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ผู้จะขายตกลงจะขายและผู้จะซื้อตกลงจะซื้อที่ดินของโครงการจำนวน  <span id="view_total_plan"></span>  แปลง 
                                        เป็นที่ดินแปลงหมายเลข   <span id="view_plan_number"></span>  เนื้อที่  <span id="view_tarangwa"></span>  
                                        ตารางวา ตามลำดับ โดยประมาณ ซึ่งภายหลังจากการรังวัดแบ่งแยกโฉนดแล้วเนื้อที่เพิ่มขึ้นหรือลดลงทั้งสองฝ่ายตกลงเพิ่มหรือลดราคาตามส่วนโดยให้ถือราคาตารางวาละ     
                                        <span id="view_price_tarangwa"></span> บาท <span id="view_tarangwa_char"></span> 
                                        ผู้จะซื้อได้ตรวจที่ดิน สภาพที่ดิน การคมนาคมและรายละเอียดอื่น ๆ ในพื้นที่แล้ว 
                                        เห็นว่าถูกวัตถุประสงค์ที่ดินดังกล่าว ซึ่งตั้งอยู่บนที่ดินโฉนดเลขที่  <span id="view_land_no"></span> ตั้งอยู่ที่หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน 
                                        จังหวัดพระนครศรีอยุธยา ตามแผนผังแนบท้ายและให้ถือเป็นสัญญาส่วนหนึ่งด้วย
                                    </div>                         
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/ข้อ 2. การชำระเงิน..........</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="5" height="20" >
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>


            </table>
        </div>
    </div>

    <!--page2-->
    <div id="land_case1_page2" class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;" >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/logo.jpg') ?>" width="150" height="120" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <div align="center"  style="font-size: 14px;">
                            บริษัท ตติเชษฐ์ จำกัด เลขที่ 88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160
                            โทร 035-881888-9 โทรสาร 035-881881
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-2-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 2. การชำระเงินและการจดทะเบียนโอนกรรมสิทธิ์
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        คู่สัญญาทั้งสองฝ่ายตกลงที่จะซื้อจะขายทรัพย์สินตามข้อ 1. ในราคา    <span id="view_pricetotal"></span>    บาท (<span id="view_pricetotal_char"></span>)  
                                        โดยผู้จะซื้อได้ชำระเงินจองเป็นเงิน       		          <span id="view_pricepay"></span>    บาท 
                                        <span id="view_pricepay_char"></span>    ในวันจองซื้อ              และเงินทำสัญญาเป็นเงิน                           <span id="view_pay_price_promise"></span>   บาท 
                                        (<span id="view_pay_price_promise_char"></span>)    ให้ถือว่าเงินจองและเงินทำสัญญาเป็นส่วนหนึ่งของการชำระราคาด้วย    
                                        คงเหลือเงินที่ต้องชำระอีกเป็นเงิน                      <span id="view_pay_price_promise_result"></span>           บาท                             (<span id="view_pay_price_promise_result_char"></span>)
                                        โดยจะชำระ เมื่อออกโฉนดที่ดินเรียบร้อยแล้ว    ซึ่งผู้จะขายจะแจ้งกำหนดวันชำระเงินโอนกรรมสิทธิ์ และจดทะเบียนโอนกรรมสิทธิ์ที่ดินตามข้อ 
                                        1.  ให้ผู้จะซื้อทราบล่วงหน้าอย่างน้อย 30 (สามสิบ) วัน และผู้จะซื้อจะต้องไปดำเนินการรับโอนกรรมสิทธิ์ และชำระเงินค่าที่ดินในวันดังกล่าวโดยไม่มีข้อโต้แย้งใด ๆ ทั้งสิ้น 
                                        โดยการชำระเงินทั้งหมดจะชำระด้วยเงินสดหรือแคชเชียร์เช็คหรือดร๊าฟขีดคร่อมเฉพาะในนาม “บริษัท ตติเชษฐ์ จำกัด” เท่านั้น

                                    </div> 
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 3. การผิดนัดชำระเงิน
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ในการชำระเงินแต่ละงวดตามข้อ 2. ของสัญญานี้ ถ้าผู้จะซื้อผิดนัดไม่ชำระตามกำหนดงวดหนึ่งงวดใด ผู้จะซื้อจะต้องเสียดอกเบี้ยร้อยละ 1.25 
                                        (หนึ่งจุดสองห้า) ต่อเดือนของเงินที่จะต้องชำระแต่ละงวดให้แก่ผู้จะขาย หรือผู้จะขายจะใช้สิทธิ์บอกเลิกสัญญา 
                                        และริบเงินที่ผู้จะซื้อได้ชำระให้แก่ผู้จะขายแล้วทั้งหมดก็ได้
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ในกรณีที่ผู้จะขายผิดสัญญาในสาระสำคัญ ผู้จะซื้อมีสิทธิ์บอกเลิกสัญญาได้และผู้จะขายต้องคืนเงินทั้งหมดที่ได้รับจากผู้จะซื้อ 
                                        พร้อมดอกเบี้ยตามกฎหมายนับแต่วันที่ได้รับเงินดังกล่าวไป เพื่อเป็นการทดแทนความเสียหายให้แก่ผู้จะซื้อ 
                                        โดยผู้จะซื้อจะไม่เรียกร้องค่าเสียหายอื่นใดอีก
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ในกรณีที่ไม่สามารถดำเนินการโครงการต่อไปได้ เนื่องจากภาวะสงคราม หรือกฎหมายไม่เปิดช่องให้ดำเนินการผู้จะขายจะคืนเงินทั้งหมดที่ได้
                                        รับจากผู้จะซื้อให้แก่ผู้จะซื้อจนครบ โดยทั้งสองฝ่ายจะไม่เรียกร้องดอกเบี้ย หรือค่าเสียหายอื่นใดต่อกันอีก
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/ข้อ 4. หลักเกณฑ์การ..........</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="70">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--page3-->
    <div id="land_case1_page3" class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/logo.jpg') ?>" width="150" height="120" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <div align="center"  style="font-size: 14px;">
                            บริษัท ตติเชษฐ์ จำกัด เลขที่ 88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160
                            โทร 035-881888-9 โทรสาร 035-881881
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-3-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 4. หลักเกณฑ์การกู้เงินจากสถาบันการเงิน (กรณีที่ผู้จะซื้อได้แสดงความประสงค์ขอกู้เงิน)
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ถ้าผู้จะซื้อมีความประสงค์จะขอกู้เงินจากสถาบันการเงิน เพื่อนำมาชำระเงินโอนกรรมสิทธิ์ โดยได้แจ้งความประสงค์ 
                                        เป็นลายลักษณ์อักษรต่อผู้จะขายในวันทำสัญญา ผู้จะขายจะติดต่อสถาบันการเงิน เพื่อให้ผู้จะซื้อกู้เงินจำนวนดังกล่าวมาชำระให้แก่ผู้จะขาย โ
                                        ดยในการกู้เงินนี้ ผู้จะขายจะโอนกรรมสิทธิ์ที่ดินตามสัญญาให้แก่ผู้จะซื้อในวัน และเวลาเดียวกัน ผู้จะซื้อก็จดทะเบียนจำนองเป็นประกันต่อสถาบันการเงิน
                                    </div> 
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        4.1. เพื่อให้ผู้จะซื้อสามารถชำระเงินโอนกรรมสิทธิ์ได้ตามกำหนดผู้จะซื้อต้องยอมรับข้อกำหนดของสถาบันการเงิน และปฏิบัติตามเงื่อนไขในการขอกู้เงิน 
                                        และการจำนอง ถ้าหากสถาบันการเงินต้องการผู้กู้ร่วมหรือค้ำประกัน หรือเอกสารอื่น ๆ ที่จำเป็นต่อการติดต่อขอกู้เงิน ผู้จะซื้อจะต้องจัดหามาให้แก่ผู้จะขายภายใน 7 
                                        วันนับแต่วันที่ผู้จะซื้อได้ลงลายมือชื่อในหนังสือขอกู้ ณ สถาบันการเงินในวันทำสัญญาจำนอง
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        4.2. ผู้จะซื้อจะต้องนำหลักฐานซึ่งแสดงว่ามีคุณสมบัติตามหลักเกณฑ์การขอกู้เงินกับสถาบันการเงิน มามอบให้แก่ผู้จะขายภายใน 30 (สามสิบ) วัน นับแต่วันทำสัญญานี้คือ
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ก.	บัตรประจำตัวประชาชน หรือบัตรข้าราชการหรือบัตรพนักงานรัฐวิสาหกิจ (ถ่ายเอกสารรับรองสำเนาถูกต้อง)
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ข.	สำเนาทะเบียนบ้านทุกหน้า (ถ่ายเอกสารรับรองสำเนาถูกต้อง)
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ค.	ทะเบียนสมรส ทะเบียนหย่า แล้วแต่กรณี (ถ่ายเอกสารรับรองสำเนาถูกต้อง)
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ง.	ใบรับรองอัตราเงินเดือน (สำหรับข้าราชการใช้แบบฟอร์มของราชการ) มีผู้ลงนามรับรองระดับหัวหน้ากองขึ้นไป สำหรับลูกจ้างเอกชน 
                                        ใช้แบบฟอร์มของบริษัท ห้างร้าน โดยมีผู้จัดการเป็นผู้ลงนามรับรอง
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        สำหรับอาชีพอิสระให้นำหลักฐานการประกอบอาชีพนั้น ๆ เช่นทะเบียนการค้า ทะเบียนพาณิชย์ 
                                        หนังสือรับรองกระทรวงพาณิชย์ หลักฐานการเสียภาษีเงินได้ และบัญชีเงินฝากแสดงฐานะการเงินไม่น้อยกว่า 6 เดือน
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        4.3. ผู้จะซื้อ ขอรับรองว่าผู้จะซื้อมีความสามารถในการชำระเงินตามสัญญากู้เงินกับ
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/สถาบันการเงิน...........</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>

                    <td colspan="5" height="200">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>

                </tr>
            </table>
        </div>
    </div>

    <!--page4-->
    <div id="land_case1_page4" class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/logo.jpg') ?>" width="150" height="120" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <div align="center"  style="font-size: 14px;">
                            บริษัท ตติเชษฐ์ จำกัด เลขที่ 88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160
                            โทร 035-881888-9 โทรสาร 035-881881
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-4-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block;">
                                        สถาบันการเงิน โดยมีรายได้อยู่ในเกณฑ์ตามที่สถาบันการเงินกำหนด และมีความสามารถในการทำนิติกรรมตลอดจนเอกสารหลักฐานครบถ้วนตามที่กฎหมาย 
                                        หรือระเบียบของทางราชการกำหนดไว้ หากเอกสารใด ๆ ที่ผู้จะซื้อได้นำมามอบให้แก่ผู้จะขาย หรือสถาบันการเงิน ที่ผู้จะขายกำหนดหรือหากข้อเท็จจริงตามสัญญาข้อ 
                                        4.2. หรือ 4.3. ข้างต้นนี้ตามลำดับเพื่อประกอบการพิจารณาขอกู้เงินผิดไปจากความจริง ซึ่งทำให้สถาบันการเงินไม่ยอมให้ผู้จะซื้อกู้เงิน 
                                        จนเป็นเหตุให้ผู้จะซื้อผิดนัดชำระเงินแก่ผู้จะขาย ผู้จะขายจะใช้สิทธิบอกเลิกสัญญา และริบเงินที่ผู้จะซื้อได้ชำระให้ผู้จะขายแล้วทั้งหมด
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        แต่ทั้งนี้สถาบันการเงินดังกล่าว จะอนุมัติให้เงินกู้แก่ผู้จะซื้อหรือไม่ก็ดี หรืออนุมัติให้กู้ไม่เต็มตามจำนวนที่ขอกู้ก็ดี ย่อมไม่ผูกพันผู้จะขาย 
                                        และหากมีเหตุขัดข้องทำให้สถาบันการเงินไม่อนุมัติให้ผู้จะซื้อกู้เงิน หรืออนุมัติให้กู้เงิน ไม่เต็มตามจำนวนที่ขอกู้ 
                                        เป็นหน้าที่ของผู้จะซื้อจะต้องจัดหาเงินมาชำระตามราคาทรัพย์สินที่จะซื้อขายดังกล่าวในข้อ 2. ข้างต้นให้แก่ผู้จะขายตามกำหนดด้วย
                                    </div> 

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td class="report-font">
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 5. หลักฐานที่ใช้ในการโอนกรรมสิทธิ์และค่าใช้จ่าย
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        ผู้จะซื้อจะต้องนำหลักฐานที่ใช้ในการโอนกรรมสิทธิ์ ดังมีรายละเอียดทั้งหมดตามข้อ 4.2. (ยกเว้นใบรับรองอัตราเงินเดือน และหลักฐานการประกอบอาชีพ) 
                                        มามอบให้แก่ผู้จะขายภายใน 30 (สามสิบ) วัน นับตั้งแต่วันทำสัญญานี้ ส่วนค่าใช้จ่าย และค่าธรรมเนียมในการทำนิติกรรมซื้อขาย 
                                        ทั้งสองฝ่ายตกลงชำระฝ่ายละครึ่ง ส่วนค่าธรรมเนียมในการทำนิติกรรมจำนองผู้จะซื้อเป็นผู้มีหน้าที่ออกเงินเองทั้งสิ้น โดยจะต้องนำเงินค่าใช้จ่าย 
                                        และค่าธรรมเนียมนี้มาชำระเงินสดภายในวันโอนกรรมสิทธิ์
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td class="report-font">
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 6. การโอนสิทธิ์และหน้าที่ตามสัญญา
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        สิทธิตามสัญญานี้ ผู้จะซื้อจะโอนให้แก่บุคคลอื่นมิได้ เว้นแต่ได้รับความยินยอมจากผู้จะขายเป็นลายลักษณ์อักษรก่อน 
                                        และหากมีกรณีที่ผู้จะขายยินยอมให้ผู้จะซื้อโอนสิทธิของตนให้แก่บุคคลใด หรือผู้จะซื้อมีความประสงค์จะเปลี่ยนแปลงรายละเอียดและ/หรือ เงื่อนไขต่าง ๆ 
                                        ในสัญญาฉบับนี้ โดยผู้จะขายสามารถตกลงยินยอมในรายละเอียด และ/หรือเงื่อนไขนั้นได้ ผู้จะซื้อยินยอมเสียค่าธรรมเนียม 
                                        และค่าใช้จ่ายในการเปลี่ยนสัญญาให้ผู้จะขายเป็นจำนวนเงิน 20,000 บาท (สองหมื่นบาทถ้วน)
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/ข้อ 7. ด้วยเป็นความประสงค์...........</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--page5-->
    <div id="land_case1_page5" class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/logo.jpg') ?>" width="150" height="120" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <div align="center"  style="font-size: 14px;">
                            บริษัท ตติเชษฐ์ จำกัด เลขที่ 88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160
                            โทร 035-881888-9 โทรสาร 035-881881
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-5-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 7. ด้วยเป็นความประสงค์ของทั้งผู้จะซื้อ และผู้จะขายที่ต้องการให้ที่ดินในโครงการดังกล่าวแห่งนี้เป็นชุมชนแห่งที่พักอาศัยชั้นดีที่มีทัศนียภาพสวยงามมีความเป็นระเบียบเรียบร้อย 
                                        และเพื่อประโยชน์สุขอันเป็นส่วนรวมของผู้จะซื้อ และผู้อยู่อาศัยทั้งหลาย รวมทั้งเพื่อรักษาชื่อเสียงของโครงการนี้ 
                                        ผู้จะซื้อจึงตกลงที่จะปฎิบัติตามข้อกำหนดของผู้จะขายดังต่อไปนี้ตลอดไป
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        7.1. ผู้จะซื้อจะใช้ที่ดินเพื่อปลูกสร้างอาคารเป็นที่อยู่อาศัยเท่านั้น การใช้ที่ดินและ/หรืออาคารเพื่อการอื่น เช่น อุตสาหกรรม พาณิชยกรรม 
                                        เกษตรกรรม เพื่อประโยชน์ทางด้านการค้า จะกระทำไม่ได้ เว้นแต่ได้รับอนุญาตจากผู้จะขายเป็นลายลักษณ์อักษรก่อน
                                    </div> 
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        7.2. ผู้จะซื้อสัญญาว่าจะเข้าออกโครงการโดยใช้ประตูเข้า-ออกของโครงการเท่านั้นห้ามมิให้ผู้จะซื้อเจาะรั้ว เพื่อใช้เป็นทางเข้า-ออก ภายนอกโครงการเอง
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        7.3. กรณีที่ผู้จะซื้อมีความประสงค์จะสร้างสิ่งปลูกสร้างใด ๆ ภายในโครงการ ผู้จะซื้อจะต้องไม่กองวัสดุ อุปกรณ์ก่อสร้างใด ๆ 
                                        ล้ำเข้ามาในถนน นอกจากมีความจำเป็น ผู้จะขายอนุญาตให้กองล้ำเข้ามาในถนนได้ไม่เกิน 1 เมตร จากรั้ว 
                                        ทั้งจะต้องรักษาความสะอาดให้เป็นที่เรียบร้อยทุกวันหลังเลิกงาน และจะต้องขุดลอกทำความสะอาดบ่อพักและท่อระบายน้ำตรงจุดที่เกียวข้อง 
                                        เมื่อเสร็จสิ้นการก่อสร้างต่อเติม (ทั้งนี้ผู้จะซื้อยินดีที่จะวางเงินประกันความเสียหายเป็นเงิน 10,000 บาท (หนึ่งหมื่นบาทถ้วน))
                                    </div>

                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        7.4. ผู้จะซื้อมีหน้าที่กระทำการใด ๆ เพื่อรักษาทัศนียภาพอันสวยงาม ความเป็นระเบียบเรียบร้อย ประโยชน์สุขอันร่วมกันของผู้จะซื้อ 
                                        และผู้อยู่อาศัยทั้งหลาย และรักษาชื่อเสียงของโครงการแห่งนี้
                                    </div>

                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        7.5. หากข้อกำหนดดังกล่าวนี้จะต้องมีการจดทะเบียนตามกฎหมายในรูปของการจำยอม หรือสิทธิอื่นใด เมื่อผู้จะขายได้แจ้งให้แก่ผู้จะซื้อได้ทราบแล้ว 
                                        ทั้งสองฝ่ายจะดำเนินการจดทะเบียนกันโดยทันที 
                                        ซึ่งผู้จะขายมีหน้าที่ออกค่าฤชาธรรมเนียม และค่าใช้จ่ายต่าง ๆ ในการจดทะเบียนทั้งสิ้น
                                    </div>

                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        7.6. หากผู้จะซื้อปฎิบัติผิดข้อกำหนดดังกล่าวข้างต้นนี้ ไม่ว่าข้อหนึ่งข้อใด ผู้จะซื้อจำต้องรับผิดชอบในความเสียหายที่เกิดขึ้นกับผู้จะซื้อรายอื่น 
                                        ผู้เข้าอยู่อาศัยทั้งหลายหรือผู้จะขายก็ตามและในกรณีที่ผู้จะซื้อได้ปลูกสร้างสิ่งปลูกสร้างใด ซึ่งผิดข้อกำหนดดังกล่าวข้างต้น 
                                        ไม่ว่าข้อหนึ่งข้อใดผู้จะซื้อตกลงจะรื้อถอนสิ่งปลูกสร้างนั้นออกทันทีที่จะได้รับแจ้งจากผู้จะขายหรือมิฉะนั้นยินยอมให้ผู้จะขายมีอำนาจรื้อถอนโดยผู้จะซื้อเป็นผู้
                                        ออกค่าใช้จ่ายทั้งสิ้น
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/ข้อ 8. การบอกกล่าว</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้จะขาย</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--page6-->
    <div id="land_case1_page6" class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/logo.jpg') ?>" width="150" height="120" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <div align="center"  style="font-size: 14px;">
                            บริษัท ตติเชษฐ์ จำกัด เลขที่ 88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160
                            โทร 035-881888-9 โทรสาร 035-881881
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-6-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 8. การบอกกล่าว
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        การบอกกล่าวใด ๆ ตามสัญญานี้ จะต้องทำเป็นหนังสือ และส่งทางไปรษณีย์ไปยังผู้จะซื้อตามที่อยู่ข้างต้น ในกรณีที่ผู้จะซื้อเปลี่ยนที่อยู่/ย้ายที่อยู่ 
                                        ทำให้ส่งหนังสือถึงผู้จะซื้อไม่ได้ ให้ถือว่าหลักฐานทางไปรษณีย์ที่คืนกลับมา เป็นการส่งถึงผู้จะซื้อโดยสมบูรณ์แล้ว 
                                        การแจ้งไปยังผู้จะขายให้แจ้งไป ณ สำนักงานเลขที่ 88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 
                                        หรือไปยังที่อยู่อื่นตามที่คู่สัญญาฝ่ายหนึ่งฝ่ายใดจะได้แจ้งเป็นหนังสือให้อีกฝ่ายหนึ่งทราบ แต่สำหรับการแจ้งเปลี่ยนที่อยู่นั้นให้ถือเอาวันที่ได้รับหนังสือการแจ้งเปลี่ยนที่อยู่เป็นวันที่แจ้งได้
                                    </div> 
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 9. ผู้จะซื้อตกลง ชำระค่าสาธารณูปโภคในโครงการแห่งนี้ โดยให้เป็นไปตามบันทึกข้อตกลงต่อท้ายสัญญานี้ และให้ถือว่าบันทึกต่อท้ายสัญญาดังกล่าวเป็นส่วนหนึ่ง และเป็นสาระสำคัญของสัญญาฉบับนี้ดัวย
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 10. การผิดสัญญา
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        หากผู้จะซื้อผิดสัญญาข้อใดข้อหนึ่ง ผู้จะขายมีสิทธิ์ที่จะบอกเลิกสัญญาและริบเงินที่ผู้จะซื้อได้ชำระเงินให้แก่ผู้จะขายแล้วทั้งหมด
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        หนังสือสัญญานี้มีทั้งหมด 7 หน้า ทำขึ้นเป็นสองฉบับมีข้อความถูกต้องตรงกัน คู่สัญญาทั้งสองฝ่ายต่างเข้าใจในข้อความนี้โดยตลอดแล้ว 
                                        จึงได้ลงลายมือชื่อไว้เป็นสำคัญต่อหน้าพยาน และเก็บไว้ฝ่ายละหนึ่งฉบับ
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>


                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>


                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>  
                <tr>
                    <td colspan="5" height="220">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--page7-->
    <div id="land_case1_page7" class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/logo.jpg') ?>" width="150" height="120" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <div align="center"  style="font-size: 14px;">
                            บริษัท ตติเชษฐ์ จำกัด เลขที่ 88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160
                            โทร 035-881888-9 โทรสาร 035-881881
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-7-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td  class="report-font">

                                    <div style="width: 100%; display: inline-block;">
                                        เอกสารแนบท้าย ซึ่งถือเป็นส่วนหนึ่งของสัญญา
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        1.	แผนผังแสดงตำแหน่งของแปลงหมายเลข.....................................
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        2.	บันทึกข้อตกลงท้ายสัญญาจะซื้อจะขายที่ดินเลขที่.............................
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        3.	เอกสารประกอบของคู่สัญญา
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        4.	สำเนาโฉนดของแปลงหมายเลข.................................หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>



                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="70">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--page8-->
    <div id="land_case1_page8" class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/logo.jpg') ?>" width="150" height="120" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <div align="center"  style="font-size: 14px;">
                            บริษัท ตติเชษฐ์ จำกัด เลขที่ 88 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160
                            โทร 035-881888-9 โทรสาร 035-881881
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-8-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td align="left" class="report-font" >
                                    <div style="width: 450px; display: inline-block; ">
                                        ลงชื่อ...........................................................ผู้จะขาย
                                    </div>
                                    <div style="width: 390px; display: inline-block; ">
                                        ลงชื่อ......................................................ผู้จะซื้อ

                                    </div>
                                    <div style="width: 450px; display: inline-block; text-indent: 3.0em; ">
                                        <span id="cardcen1"></span>	
                                    </div>
                                    <div style="width: 390px; display: inline-block; text-indent: 1.8em;">
                                        (บริษัท พรพิศโฮม คอนสตรัคชั่น จำกัด

                                    </div>
                                    <div style="width: 450px; display: inline-block; text-indent: 3.0em;">
                                        <span id="cardcen2"></span>	
                                    </div>
                                    <div style="width: 390px; display: inline-block; text-indent: 2.2em;">
                                        โดยนางสมพิศ  ด่านชัยวิจิตร)	
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td align="left" class="report-font" >
                                    <div style="width: 450px; display: inline-block; ">
                                        ลงชื่อ <span id="payantab1_1"></span>พยาน
                                    </div>
                                    <div style="width: 390px; display: inline-block; ">
                                        ลงชื่อ<span id="payantab1_2"></span>พยาน

                                    </div>
                                    <div style="width: 450px; display: inline-block; text-indent: 2.0em;">
                                        (......................................................)
                                    </div>
                                    <div style="width: 390px; display: inline-block; text-indent: 2.0em;">
                                        (......................................................)

                                    </div>
                                    <div style="width: 450px; display: inline-block; text-indent: 3.0em;">

                                    </div>
                                    <div style="width: 390px; display: inline-block; ">


                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td  colspan="5">
                        <table style="width: 100%">
                            <tr>
                                <td align="center" class="report-font" >
                                    <div style="width: 450px; display: inline-block; ">
                                        <div style="width: 380px; height: 350px; display: inline-block;" id="imagetCardTab1_1"></div>
                                    </div>
                                    <div style="width: 390px; display: inline-block; ">
                                        <div style="width: 380px; height: 350px; display: inline-block;" id="imagetCardTab1_2"></div>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>

                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50" height="30"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="30"><p>&nbsp;</p></td>
                </tr>
                
                
                
            </table>
        </div>
    </div>
</div>


