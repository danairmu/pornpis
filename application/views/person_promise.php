<div class="row pt-2 pl-1">
    <label class="text-dark pl-2 font-weight-bold">บุคคลที่เกี่ยวข้องกับสัญญา</label>
</div>
<div class="row pt-0">
    <div style="width: 100%;" class="pl-2 pr-2" >
        <table id="personlistx" class="display responsive nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead class="thead-light bg-success">
                <tr>
                    <th scope="col" width="8%" class="text-center">ลำดับที่</th>
                    <th scope="col" class="text-center">บัตรประจำตัวประชาชน</th>
                    <th scope="col" class="text-center">ชื่อ-นามสกุล</th>
                    <th scope="col" class="text-center">ที่อยู่</th>
                    <th scope="col" width="8%" class="text-center">เลือก</th>
                    <th scope="col" width="8%" class="text-center">ยกเลิก</th>
                </tr>
            </thead>
            <tbody id="id-tbody"></tbody>

        </table>
    </div>
</div>