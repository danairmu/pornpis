<style type="text/css">
    .table-wrapper .fg-toolbar {
        background-color: #d8ffb5;
        border: none;
    }

    .table-bordered {
        border-bottom: 1px solid #fff !important;
    }

    #rstable_wrapper > .fg-toolbar:first-child {
        display: none;
    }

    .table .thead-light th { 
        background-color: #28a745;
        text-align: center;
        font-weight: bold;
        font-size: 15px;
        top: 0;
        color: white;

    }
</style>
<br>
<div class="container-fluid" style="min-height: 1400px;">
    <div class="card text-dark" style="background-color: #2ca535;">
        <div class="card-header">
            <label class="font-weight-bold text-white">รายงานการขาย</label>
        </div>
        <div class="card-body" style="background-color: #5ebb6d;">
            <!-- === CRITERIA === -->
            <table style="width: 100%;" >
                <tr>
                    <td>
                    </td>
                    <td colspan="3" class="text-white">
                        <input type="radio" name="reportType" id="reportType1" value="1" <?php echo $report_type == 1 ? 'checked' : ''; ?>/>
                        <label for="reportType1">ข้อมูลการจอง</label>
                        <input type="radio" name="reportType" id="reportType2" value="2" <?php echo $report_type == 2 ? 'checked' : ''; ?>/>
                        <label for="reportType2">ข้อมูลการขาย</label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 70px;" class="text-white">
                        ชื่อผู้ขาย
                    </td>
                    <td colspan="3">
                        <?php echo form_dropdown($sale_filter, $sale_filter_dropdown_data, $sale_filter_dropdown_default, ['class' => 'form-control', 'id' => 'saleFilter', 'style' => 'width: 235px;']); ?>
                    </td>
                </tr>

                <tr>
                    <td class="text-white">
                        จากวันที่
                    </td>
                    <td style="width: 130px;">
                        <input data-format="dd/mm/yyyy" type="datetime" id="startDate" name="startDate" class="form-control datepicker"
                               style="width: 235px;" />
                    </td>
                    <td style="width: 73px; text-align: center;" class="text-white">
                        ถึงวันที่
                    </td>
                    <td>
                        <input data-format="dd/mm/yyyy" type="datetime" id="endDate" name="endDate" class="form-control datepicker" style="width: 235px;" />
                    </td>
                </tr>
            </table>
            <br>
            <!-- === END CRITERIA === -->
            <div class="pt-2"></div>
            <div align="right">
                <button type = "button" id="btn_export"  class="btn btn-success fa fa-file-excel-o  waves-effect p-1">Export Excel</button>
            </div>
            <div class="pt-1"></div>
            <div class="row">
                <div class="col-12">
                    <table id="rstable" class="display responsive nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="thead-light bg-success">
                            <tr>
                                <th scope="col" rowspan="2" width="8%" class="text-center">ลำดับที่</th>
                                <th scope="col" rowspan="2" class="text-center bg-success text-white">ชื่อ-นามสกุล</th>
                                <th scope="col" rowspan="2" class="text-center bg-success text-white">เลขที่สัญญา</th>
                                <th scope="col" colspan="3" class="text-center bg-success text-white">ข้อมูลการขาย</th>
                                <th scope="col" rowspan="2" class="text-center bg-success text-white">สัญญาจอง</th>
                                <th scope="col" rowspan="2" class="text-center bg-success text-white">สัญญาขาย</th>
                            </tr>
                            <tr class="bg-success">
                                <th scope="col" class="text-center bg-success text-white" style="border-left: none; border-top: none;">วันที่จอง</th>
                                <th scope="col" class="text-center bg-success text-white" style="border-left: none; border-top: none;">วันทีขาย</th>
                                <th scope="col" class="text-center bg-success text-white" style="border-left: none; border-top: none;">ราคาขาย(บาท)</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <br><br>

</div>

<!--modal information-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message">
    <div class="modal-dialog modal-sm" style="max-width: 470px;">
        <div class="modal-content">

            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <pre class="text-center pt-3" id="idmessage"></pre>
                </div>

            </div>
            <div class="modal-body p-2">
                <div class="text-center">
                    <button type="button" class="btn btn-warning" id="modal-btn-n-message" style="width: 30%;" >ตกลง</button>
                </div>

            </div>
        </div>
    </div>
</div>

<?php include 'view_agreement.php'; ?>
<?php include 'view_deposit.php'; ?>

<?php include 'view_promise_land_case1.php'; ?>
<?php include 'view_promise_home_case3.php'; ?>