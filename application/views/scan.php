<div class="container" style="height: 650px;">

    <div class="card">
        <div class="card-header">
            <label class="font-weight-bold" id="labelscan"></label> <label class="font-weight-bold"><?= $order_no ?> </label> 
            <input name="hidden_order" type="hidden" id="hidden_order" value="<?= $order_no ?> "/>
            <input type="hidden" id="plan_master_id" name="plan_master_id"  value="<?= $plan_master ?>"/>



        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="m-auto">
                        <button type = "button" id="scan" class="btn btn-success fa fa-print  waves-effect p-1">Scan เอกสาร</button>
                    </div>
                </div>
            </div>
            <div class="pt-2"></div>
            <div class="table table-bordered">
                <table id="scanlist" class="display nowrap table table-hover table-striped table-bordered " cellspacing="0" width="100%" >
                    <thead class="thead-light bg-success">
                        <tr>
                            <th scope="col" width="8%">ลำดับที่</th>
                            <th scope="col" class="text-center">เอกสาร</th>
                            <th scope="col" width="25%">วันที่สแกน</th>
                            <th scope="col" width="8%">View</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <form id='redirectForm' method='POST' action='<?= base_url('projectplan/backscan?menu=projectplan') ?>'>

                        <input name="hidden_order" type="hidden" id="hidden_order" value="<?= $order_no ?>"/>
                        <input type="hidden" name="order_no_scan" id="order_no_scan" value="<?= $order_no ?>"/>
                        <input type="hidden" name="order_no" id="order_no"  value="<?= $order_no ?>"/>
                        <input type="hidden" id="input_form_plan_save_scan"  name="input_form_plan_save_scan"  value="<?= $planssave ?>"/>
                        <input type="hidden" id="hidden_planssave" name="hidden_planssave" value="<?= $planssave ?>">
                        <input type="hidden" id="plan_master_scan" name="plan_master_scan"  value="<?= $plan_master ?>"/>
                        <input type="hidden" id="input_form_price_scan"  name="input_form_price_scan"  value="<?= $input_form_price ?>"/>
                        <input type="hidden" id="input_form_price_tarangwa_scan"  name="input_form_price_tarangwa_scan"  value="<?= $input_form_price_tarangwa ?>"/>
                        <input type="hidden" id="input_form_plan_scan"  name="input_form_plan_scan"  value="<?= $input_form_plan ?>"/>
                        <input type="hidden" id="due_date_order"  name="due_date_order"  value="<?= $due_date_order ?>"/>
                        <input type="hidden" id="due_date_display"  name="due_date_display"  value="<?= $due_date_display ?>"/>
                        <input type="hidden" id="input_form_price_word"  name="input_form_price_word"  value="<?= $input_form_price_word ?>"/>

                        <input type="hidden" id="scan_home_type"  name="scan_home_type"  value="<?= $home_type ?>"/>
                        <input type="hidden" id="scan_home_name"  name="scan_home_name"  value="<?= $home_name ?>"/>
                        <input type="hidden" id="scan_home_price" name="scan_home_price"  value="<?= $home_price ?>"/>

                        <button type = "submit"  class="btn btn-success fa fa-arrow-left waves-effect p-1">ย้อนกลับ</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <!--<div class="printableAreaImage">-->
    <!--        <div id="myModalViewImage" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 800px; height: 400px;">
    
                    <button onclick="closeOptionViewImage()" type="button" class="close">×</button>-->

    <!--<div id="imagedata" style="display: none;"></div>-->


    <!--            </div>
            </div>-->
    <!--</div>-->
</div>
<div class="container" style="display: none;">
    <div class="printableAreaImage">

    </div>
</div>
