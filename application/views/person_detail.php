<div>
    <div id="list1">
        <form id="formlist1">
            <div class="pt-3 pl-3"> 
                <table>
                    <tr>
                        <td style="width: 0px;"></td>
                        <td><label class="font-weight-bold"><u>ข้อมูลบุคคล</u></label></td>
                    </tr>
                </table>
            </div>

            <div class="row  pt-1 pl-3">
                <div class="col-12">
                    <table >
                        <tr>
                            <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน</td>
                            <td width="330">
                                <input type="text" name="pid" id="pid" class="form-control " style="width:320px;  height: 30px;"  required="true" /> 
                            </td>

                        </tr>
                    </table>
                </div>
            </div>

            <div class="row  pt-2 pl-3">
                <div class="col-12">
                    <table border="0">
                        <tr>
                            <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                            <td width="123">
                                <input type="text" name="title" id="title" class="form-control" style="width:90px; height: 30px;" required="true" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">ชื่อ</td>
                            <td width="230">
                                <input type="text" name="firstname" id="firstname" class="form-control" style="width:220px; height: 30px; " required="true" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">นามสกุล</td>
                            <td width="230">
                                <input type="text" name="lastname" id="lastname" class="form-control" style="width:220px;  height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">อายุ</td>
                            <td >
                                <input type="text" name="age" id="age" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                            </td>
                            <td align="left" colspan="2" class="label-custom" >ปี</td>
                            <td width="10"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row  pt-2 pl-3">
                <div class="col-12">
                    <table border="0">                
                        <tr class="pt-2">
                            <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                            <td width="110">
                                <input type="text" name="houseNumber" id="houseNumber" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);" readonly="true"/>
                            </td>
                            <td class="label-custom">หมู่ที่</td>
                            <td width="120">
                                <input type="text" name="group" id="group" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">ซอย</td>
                            <td width="225">
                                <input type="text" name="alley" id="alley" class="form-control" style="width:180px; height: 30px;" readonly="true"/>
                            </td>

                            <td align="right" class="label-custom">ถนน</td>
                            <td  colspan="2">
                                <input type="text" name="street" id="street" class="form-control" style="width:260px; height: 30px;" readonly="true"/>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>

            <div class="row pt-2 pl-3">
                <div class="col-12">
                    <table border="0">
                        <tr>
                            <td width="100" align="right" class="label-custom">ตำบล</td>
                            <td width="230">
                                <input type="text" name="subDistrict" id="subDistrict" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">อำเภอ</td>
                            <td width="230">
                                <input type="text" name="district" id="district" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">จังหวัด</td>
                            <td width="260">
                                <input type="text" name="province" id="province" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                            </td>
                            <td width="18"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row  pt-2 pl-3 pb-4">
                <div class="col-12">
                    <table border="0">
                        <tr>
                            <td width="100" align="right" class="label-custom">รหัสไปรษณีย์</td>
                            <td width="215">
                                <input type="text" name="postcode" id="postcode" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">โทรศัพท์</td>
                            <td width="240">
                                <input type="text" name="phone" id="phone" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">อีเมล์</td>
                            <td >
                                <input type="email" name="email" id="email" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>
    <div id="list2" style="display: none;">
        <hr>
        <form id="formlist2">
            <div class="row  pt-1 pl-3">
                <div class="col-12">
                    <table >
                        <tr>
                            <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน</td>
                            <td width="330">
                                <input type="text" name="pid2" id="pid2" class="form-control " style="width:320px;  height: 30px;"  required="true" readonly="true"/> 
                            </td>

                        </tr>
                    </table>
                </div>
            </div>

            <div class="row  pt-2 pl-3">
                <div class="col-12">
                    <table border="0">
                        <tr>
                            <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                            <td width="123">
                                <input type="text" name="title2" id="title2" class="form-control" style="width:90px; height: 30px;" required="true" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">ชื่อ</td>
                            <td width="230">
                                <input type="text" name="firstname2" id="firstname2" class="form-control" style="width:220px; height: 30px; " required="true" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">นามสกุล</td>
                            <td width="230">
                                <input type="text" name="lastname2" id="lastname2" class="form-control" style="width:220px;  height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">อายุ</td>
                            <td >
                                <input type="text" name="age2" id="age2" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                            </td>
                            <td align="left" colspan="2" class="label-custom" >ปี</td>
                            <td width="10"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row  pt-2 pl-3">
                <div class="col-12">
                    <table border="0">                
                        <tr class="pt-2">
                            <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                            <td width="110">
                                <input type="text" name="houseNumber2" id="houseNumber2" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);" readonly="true"/>
                            </td>
                            <td class="label-custom">หมู่ที่</td>
                            <td width="120">
                                <input type="text" name="group2" id="group2" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">ซอย</td>
                            <td width="225">
                                <input type="text" name="alley2" id="alley2" class="form-control" style="width:180px; height: 30px;" readonly="true"/>
                            </td>

                            <td align="right" class="label-custom">ถนน</td>
                            <td  colspan="2">
                                <input type="text" name="street2" id="street2" class="form-control" style="width:260px; height: 30px;" readonly="true"/>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>

            <div class="row pt-2 pl-3">
                <div class="col-12">
                    <table border="0">
                        <tr>
                            <td width="100" align="right" class="label-custom">ตำบล</td>
                            <td width="230">
                                <input type="text" name="subDistrict2" id="subDistrict2" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">อำเภอ</td>
                            <td width="230">
                                <input type="text" name="district2" id="district2" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">จังหวัด</td>
                            <td width="260">
                                <input type="text" name="province2" id="province2" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                            </td>
                            <td width="18"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row  pt-2 pl-3 pb-4">
                <div class="col-12">
                    <table border="0">
                        <tr>
                            <td width="100" align="right" class="label-custom">รหัสไปรษณีย์</td>
                            <td width="215">
                                <input type="text" name="postcode2" id="postcode2" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">โทรศัพท์</td>
                            <td width="240">
                                <input type="text" name="phone2" id="phone2" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">อีเมล์</td>
                            <td >
                                <input type="email" name="email2" id="email2" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>
    
    <div id="list3" style="display: none;">
        <hr>
        <form id="formlist3">
            <div class="row  pt-1 pl-3">
                <div class="col-12">
                    <table >
                        <tr>
                            <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน</td>
                            <td width="330">
                                <input type="text" name="pid3" id="pid3" class="form-control " style="width:320px;  height: 30px;"  required="true" readonly="true"/> 
                            </td>

                        </tr>
                    </table>
                </div>
            </div>

            <div class="row  pt-2 pl-3">
                <div class="col-12">
                    <table border="0">
                        <tr>
                            <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                            <td width="123">
                                <input type="text" name="title3" id="title3" class="form-control" style="width:90px; height: 30px;" required="true" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">ชื่อ</td>
                            <td width="230">
                                <input type="text" name="firstname3" id="firstname3" class="form-control" style="width:220px; height: 30px; " required="true" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">นามสกุล</td>
                            <td width="230">
                                <input type="text" name="lastname3" id="lastname3" class="form-control" style="width:220px;  height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">อายุ</td>
                            <td >
                                <input type="text" name="age3" id="age3" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                            </td>
                            <td align="left" colspan="2" class="label-custom" >ปี</td>
                            <td width="10"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row  pt-2 pl-3">
                <div class="col-12">
                    <table border="0">                
                        <tr class="pt-2">
                            <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                            <td width="110">
                                <input type="text" name="houseNumber3" id="houseNumber3" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);" readonly="true"/>
                            </td>
                            <td class="label-custom">หมู่ที่</td>
                            <td width="120">
                                <input type="text" name="group3" id="group3" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">ซอย</td>
                            <td width="225">
                                <input type="text" name="alley3" id="alley3" class="form-control" style="width:180px; height: 30px;" readonly="true"/>
                            </td>

                            <td align="right" class="label-custom">ถนน</td>
                            <td  colspan="2">
                                <input type="text" name="street3" id="street3" class="form-control" style="width:260px; height: 30px;" readonly="true"/>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>

            <div class="row pt-2 pl-3">
                <div class="col-12">
                    <table border="0">
                        <tr>
                            <td width="100" align="right" class="label-custom">ตำบล</td>
                            <td width="230">
                                <input type="text" name="subDistrict3" id="subDistrict3" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">อำเภอ</td>
                            <td width="230">
                                <input type="text" name="district3" id="district3" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">จังหวัด</td>
                            <td width="260">
                                <input type="text" name="province3" id="province3" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                            </td>
                            <td width="18"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row  pt-2 pl-3 pb-4">
                <div class="col-12">
                    <table border="0">
                        <tr>
                            <td width="100" align="right" class="label-custom">รหัสไปรษณีย์</td>
                            <td width="215">
                                <input type="text" name="postcode3" id="postcode3" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">โทรศัพท์</td>
                            <td width="240">
                                <input type="text" name="phone3" id="phone3" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                            </td>
                            <td align="right" class="label-custom">อีเมล์</td>
                            <td >
                                <input type="email" name="email3" id="email3" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>

