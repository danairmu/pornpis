<style type='text/css'>
    .boder-plan{
        border-style: solid; 
        color: white;
        border-width: 8px;
    }
    .div-detail{
        background-color: #edffe1; 
        border-radius: 5px;
    }
</style>
<div class="container" style="min-height: 800px">



    <div class="pt-3"></div>
    <div class="card text-dark" style="background-color: #2ca535;">

        <input type="hidden" id="hiden_person_order_no" name="hiden_person_order_no"  value="<?= $order_no ?>"/>
        <input type="hidden" id="hiden_person_master_plan" name="hiden_person_master_plan"  value="<?= $master_plan ?>"/>
        
        <!--for scan-->
        <input type="hidden" id="hiden_to_scan_planssave" name="hiden_to_scan_planssave"  value="<?= $planssave ?>"/>
        <input type="hidden" id="hiden_to_scan_due_date_order" name="hiden_to_scan_due_date_order"  value="<?= $due_date_order ?>"/>
        <input type="hidden" id="hiden_to_scan_due_date_display" name="hiden_to_scan_due_date_display"  value="<?= $due_date_display ?>"/>
        
        <div class="card-body bg-white" >

            <!--tab-->
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">สัญญาซื้อขายที่ดิน</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">สัญญาซื้อขายบ้าน</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div id="page-land">
                        <?php include_once 'promise_tab1.php'; ?>
                    </div>
                </div>

                <!-- ---------------------------tab2------------------------------------------------------------------------------------->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div id="page-home">
                        <?php include_once 'promise_tab2.php'; ?>
                    </div>
                    
                    
                </div>
            </div>

        </div>
    </div>
    <div id="p1_land1">
        <?php include 'view_promise_land_case1.php'; ?>
    </div>
      
    <div id="p1_land3">
        <?php  //include 'view_promise_land_emty.php'; ?>
    </div>
   
    <div id="p1_land2">
        <?php include 'view_promise_home_case3.php'; ?>
    </div> 
    
</div>

