<style type='text/css'>
    .boder-plan{
        border-style: solid; 
        color: #dcdedd;
        border-width: 8px;
        border-radius: 15px;
    }

</style>
<link href="<?php echo base_url('assets/plugins/aos/css/aos.css'); ?>" rel="stylesheet">
<div class="container-fluid" style="min-height: 1400px;" >
    <div class="pt-3"></div>
    <!--box-shadow: 5px 2px 5px 2px rgba(255, 255, 255, 255);-->
    <div class="card text-white" style="background-color: #2ca535; width: 100%">
        <div id="target" style="display: none;"></div>
        <div class="card-header">
            <label class="font-weight-bold">แผนผังโครงการ</label>
        </div>
        <input type="hidden" id="hidden_user" name="hidden_user"  value="<?= $userrole ?>"/>
        <div class="card-body" style="background-color: #5ebb6d;">

            <div class="row p-2 pt-4">
                <div class="col-2">
                    <div class="form-group pl-0">
                        <select id="inputState" class="form-control border-success">
                            <option id="v1" selected value="1">ผังที่ 1</option>
                            <option id="v2" value="2" >ผังที่ 2</option>
                            <option id="v3" value="3">ผังที่ 3</option>
                        </select>
                    </div>
                </div>
                <div class="col-2">    
                    <div class="custom-control custom-radio form-check">
                        <input type="radio" id="cusRadios" name="exampleRadios" value="1" class="custom-control-input" checked>
                        <label class="custom-control-label check-radio text-white font-weight-bold" for="cusRadios">รูปผังสำหรับลูกค้า</label>
                    </div>
                </div>
                <div class="col-2 text-left">    
                    <div class="custom-control custom-radio form-check">
                        <input type="radio" id="saleplan" name="exampleRadios" value="2" class="custom-control-input" >
                        <label class="custom-control-label check-radio text-white font-weight-bold" for="saleplan">รูปผังจริง</label>
                    </div>
                </div>

                <div class="col-6 m-0 text-right" id="bt_selecthome" style="display: none;"> 
                    <div id="bt_selecthome">
                        <form id="form" action="<?= base_url('projectplan/selecthome?menu=projectplan') ?>" method="POST">
                            <button type="submit" id="bt_selecthome"  class="btn btn-warning">ทำรายการจอง</button>
                            <input type="hidden" id="input_hidden_plan" name="plans"  />
                            <input type="hidden" id="input_hidden_price" name="prices"  />
                            <input type="hidden" id="input_hidden_plan_type" name="plan_type"  />
                            <input type="hidden" id="input_hidden_plan_master" name="plan_master"  />
                            <input type="hidden" id="plan_id" name="plan_id"  />
                        </form>
                    </div>

                    <input type="hidden" id="zoomScale" value="1.0">
                    <!--<button class="btn btn-info" onclick="zoomIn()">+</button>
                    <button class="btn btn-danger" onclick="zoomOut()">-</button>-->

                </div>
            </div>

            <!--image-->
            <div class="row">
                <div style="width: 100%;">
                    <div id="mapContainer_p1" style="display: none;" >
                        <div style="width: 100%; min-height: 657px; overflow: hidden; background-color: white;" class="boder-plan rounded">
                            <div id="mapObject" >
                                <img id="m_main" src="<?= base_url('assets/images/pornpit-plancus1.jpg') ?>" usemap="#image-map"  width="930" height="657">
                                <map name="image-map" >

                                    <input type="hidden" name="master_plancus" id="master_plancus" value="1">
                                    <area target="_blank" alt="213" title="213"  coords="1739,3684,1833,3673,1860,3794,1744,3816" shape="poly"  >
                                    <area target="_blank" alt="212" title="212"  coords="1744,3810,1865,3800,1875,3868,1917,3868,1917,3910,1760,3905" shape="poly">
                                    <area target="_blank" alt="211" title="211"  coords="1912,3863,1939,3858,1933,3816,2033,3800,2065,3910,1917,3916" shape="poly">
                                    <area target="_blank" alt="210" title="210"  coords="1907,3705,2007,3689,2044,3794,1923,3821" shape="poly">
                                    <area target="_blank" alt="209" title="209"  coords="2007,3684,2112,3668,2165,3773,2044,3800" shape="poly">
                                    <area target="_blank" alt="208" title="208"  coords="2049,3810,2159,3773,2186,3847,2228,3863,2244,3916,2070,3905" shape="poly">
                                    <area target="_blank" alt="207" title="207"  coords="2217,3842,2259,3837,2238,3789,2338,3763,2391,3910,2254,3910" shape="poly">
                                    <area target="_blank" alt="206" title="206"  coords="2181,3647,2280,3626,2344,3763,2228,3789" shape="poly">
                                    <area target="_blank" alt="205" title="205"  coords="2233,3463,2344,3421,2375,3521,2254,3568" shape="poly">
                                    <area target="_blank" alt="204" title="204"  coords="2154,3352,2291,3305,2344,3421,2191,3479" shape="poly">
                                    <area target="_blank" alt="203" title="203"  coords="2291,3616,2407,3579,2454,3700,2333,3737" shape="poly">
                                    <area target="_blank" alt="202" title="202"  coords="2333,3737,2454,3705,2475,3784,2370,3826" shape="poly">
                                    <area target="_blank" alt="201" title="201"  coords="2359,3837,2470,3789,2517,3889,2544,3873,2554,3910,2396,3910" shape="poly">

                                    <area target="_blank" alt="200" title="200"  coords="2538,3868,2565,3858,2554,3810,2670,3784,2712,3910,2549,3910"  shape="poly">
                                    <area target="_blank" alt="199" title="199"  coords="2522,3726,2649,3679,2675,3789,2559,3816" shape="poly">
                                    <area target="_blank" alt="198" title="198"  coords="2486,3621,2612,3589,2638,3684,2528,3737" shape="poly">
                                    <area target="_blank" alt="197" title="197"  coords="2444,3516,2570,3473,2617,3589,2486,3621" shape="poly">
                                    <area target="_blank" alt="196" title="196"  coords="2380,3347,2507,3305,2570,3468,2438,3516" shape="poly">
                                    <area target="_blank" alt="195" title="195"  coords="2517,3300,2622,3258,2675,3368,2549,3416" shape="poly">
                                    <area target="_blank" alt="194" title="194"  coords="2559,3416,2664,3379,2707,3473,2580,3516" shape="poly">
                                    <area target="_blank" alt="193" title="193"  coords="2591,3510,2701,3473,2743,3579,2622,3616" shape="poly">
                                    <area target="_blank" alt="192" title="192"  coords="2628,3621,2733,3579,2775,3679,2659,3710" shape="poly">
                                    <area target="_blank" alt="191" title="191"  coords="2659,3716,2785,3673,2817,3773,2696,3810" shape="poly">
                                    <area target="_blank" alt="190" title="190"  coords="2686,3821,2801,3779,2843,3884,2875,3868,2880,3910,2717,3910" shape="poly">

                                    <area target="_blank" alt="189" title="189"  coords="2854,3879,2896,3863,2880,3826,3006,3789,3043,3910,2864,3910" shape="poly">
                                    <area target="_blank" alt="188" title="188"  coords="2854,3726,2975,3689,3001,3789,2885,3831" shape="poly">
                                    <area target="_blank" alt="187" title="187"  coords="2822,3637,2938,3600,2975,3689,2854,3737" shape="poly">
                                    <area target="_blank" alt="186" title="186"  coords="2791,3542,2896,3500,2943,3589,2822,3642" shape="poly">
                                    <area target="_blank" alt="185" title="185"  coords="2759,3447,2870,3416,2901,3500,2791,3547" shape="poly">
                                    <area target="_blank" alt="184" title="184"  coords="2722,3352,2843,3326,2880,3410,2754,3442" shape="poly">
                                    <area target="_blank" alt="183" title="183"  coords="2686,3231,2801,3200,2849,3316,2717,3352" shape="poly">
                                    <area target="_blank" alt="182" title="182"  coords="2807,3195,2922,3163,2959,3273,2843,3316" shape="poly">
                                    <area target="_blank" alt="181" title="181"  coords="2849,3316,2959,3284,3001,3373,2875,3416" shape="poly">
                                    <area target="_blank" alt="180" title="180"  coords="2880,3410,2991,3379,3033,3473,2912,3516" shape="poly">
                                    <area target="_blank" alt="179" title="179"  coords="2912,3521,3033,3479,3070,3573,2943,3616" shape="poly">
                                    <area target="_blank" alt="178" title="178"  coords="2949,3621,3064,3573,3106,3673,2991,3710" shape="poly">
                                    <area target="_blank" alt="177" title="177"  coords="2991,3716,3101,3679,3133,3773,3012,3826" shape="poly">
                                    <area target="_blank" alt="176" title="176"  coords="3017,3823,3138,3775,3170,3886,3217,3875,3227,3910,3043,3912"  shape="poly">

                                    <area target="_blank" alt="175" title="175"  coords="3206,3868,3275,3852,3259,3816,3333,3789,3375,3900,3212,3910"  shape="poly">
                                    <area target="_blank" alt="174" title="174"  coords="3185,3731,3296,3689,3333,3784,3212,3821" shape="poly">
                                    <area target="_blank" alt="173" title="173"  coords="3138,3631,3259,3605,3301,3684,3191,3737" shape="poly">
                                    <area target="_blank" alt="172" title="172"  coords="3117,3542,3227,3500,3269,3595,3154,3631" shape="poly">
                                    <area target="_blank" alt="171" title="171"  coords="3091,3452,3206,3416,3233,3489,3112,3542" shape="poly">
                                    <area target="_blank" alt="170" title="170"  coords="3059,3358,3170,3316,3206,3410,3085,3447" shape="poly">
                                    <area target="_blank" alt="169" title="169"  coords="3022,3252,3133,3221,3164,3326,3059,3352" shape="poly">
                                    <area target="_blank" alt="168" title="168"  coords="2980,3131,3096,3105,3133,3216,3017,3252" shape="poly">
                                    <area target="_blank" alt="167" title="167"  coords="3101,3089,3212,3063,3254,3173,3133,3221" shape="poly">
                                    <area target="_blank" alt="166" title="166"  coords="3148,3216,3259,3174,3291,3274,3175,3311" shape="poly">
                                    <area target="_blank" alt="165" title="165"  coords="3180,3321,3296,3279,3327,3379,3212,3410" shape="poly">
                                    <area target="_blank" alt="163" title="163"  coords="3201,3416,3327,3379,3401,3573,3275,3605" shape="poly">
                                    <area target="_blank" alt="162" title="162"  coords="3275,3605,3390,3573,3433,3679,3306,3710" shape="poly">
                                    <area target="_blank" alt="161" title="161"  coords="3312,3710,3433,3679,3469,3773,3348,3810" shape="poly">
                                    <area target="_blank" alt="160" title="160"  coords="3348,3821,3464,3773,3511,3910,3385,3905" shape="poly">

                                    <area target="_blank" alt="159" title="159"  coords="3532,3763,3648,3731,3690,3852,3564,3858" shape="poly">
                                    <area target="_blank" alt="158" title="158"  coords="3490,3668,3617,3621,3648,3726,3532,3763" shape="poly">
                                    <area target="_blank" alt="157" title="157"  coords="3454,3552,3575,3526,3611,3626,3485,3658" shape="poly">
                                    <area target="_blank" alt="156" title="156"  coords="3417,3447,3532,3410,3585,3521,3454,3558" shape="poly">
                                    <area target="_blank" alt="155" title="155"  coords="3385,3347,3506,3310,3538,3405,3417,3458" shape="poly">
                                    <area target="_blank" alt="154" title="154"  coords="3354,3242,3454,3205,3511,3316,3380,3342" shape="poly">
                                    <area target="_blank" alt="153" title="153"  coords="3312,3131,3427,3100,3459,3205,3348,3247" shape="poly">
                                    <area target="_blank" alt="152" title="152"  coords="3275,3026,3390,2995,3427,3095,3312,3126" shape="poly">
                                    <area target="_blank" alt="151" title="151"  coords="3227,2905,3348,2868,3390,2989,3275,3026" shape="poly">
                                    <area target="_blank" alt="150" title="150"  coords="3185,2768,3306,2737,3354,2863,3233,2905" shape="poly">
                                    <area target="_blank" alt="149" title="149"  coords="3296,2726,3427,2679,3480,2821,3359,2863" shape="poly">
                                    <area target="_blank" alt="148" title="148"  coords="3354,2868,3469,2837,3517,2947,3390,2995" shape="poly">
                                    <area target="_blank" alt="147" title="147"  coords="3396,2989,3522,2947,3559,3063,3433,3105" shape="poly">
                                    <area target="_blank" alt="146" title="146"  coords="3443,3105,3564,3068,3601,3189,3475,3237" shape="poly">
                                    <area target="_blank" alt="145" title="145"  coords="3485,3231,3596,3200,3643,3305,3522,3358" shape="poly">
                                    <area target="_blank" alt="144" title="144"  coords="3527,3358,3643,3310,3675,3410,3559,3458" shape="poly">
                                    <area target="_blank" alt="143" title="143"  coords="3564,3452,3675,3410,3711,3510,3596,3552" shape="poly">
                                    <area target="_blank" alt="141" title="141"  coords="3606,3558,3717,3516,3780,3721,3659,3752" shape="poly">
                                    <area target="_blank" alt="140" title="140"  coords="3664,3752,3785,3721,3838,3858,3696,3852" shape="poly">
                                    <area target="_blank" alt="139" title="139"  coords="3880,3805,4001,3758,4064,3910,3911,3905" shape="poly">
                                    <area target="_blank" alt="138" title="138"  coords="3843,3695,3964,3647,4001,3747,3880,3800" shape="poly">
                                    <area target="_blank" alt="137" title="137"  coords="3806,3579,3927,3531,3964,3642,3843,3689" shape="poly">
                                    <area target="_blank" alt="136" title="136"  coords="3764,3468,3885,3410,3922,3531,3796,3573" shape="poly">
                                    <area target="_blank" alt="135" title="135"  coords="3722,3342,3843,3295,3885,3410,3759,3468" shape="poly">
                                    <area target="_blank" alt="134" title="134"  coords="3675,3205,3801,3168,3843,3295,3711,3337" shape="poly">
                                    <area target="_blank" alt="133" title="133"  coords="3811,3163,3932,3121,3974,3237,3843,3273" shape="poly">
                                    <area target="_blank" alt="132" title="132"  coords="3848,3279,3964,3247,4001,3337,3880,3384" shape="poly">
                                    <area target="_blank" alt="131" title="131"  coords="3885,3395,4006,3342,4048,3447,3927,3489" shape="poly">
                                    <area target="_blank" alt="130" title="130"  coords="3927,3500,4038,3452,4080,3558,3953,3605" shape="poly">
                                    <area target="_blank" alt="129" title="129"  coords="3959,3605,4080,3558,4111,3663,3990,3705" shape="poly">
                                    <area target="_blank" alt="128" title="128"  coords="4001,3710,4111,3668,4153,3779,4032,3810" shape="poly">
                                    <area target="_blank" alt="127" title="127"  coords="4032,3805,4153,3784,4174,3847,4211,3852,4232,3905,4064,3916" shape="poly">
                                    <area target="_blank" alt="126" title="126"  coords="4206,3842,4290,3821,4264,3784,4364,3752,4400,3905,4232,3905" shape="poly">
                                    <area target="_blank" alt="125" title="125"  coords="4185,3684,4327,3631,4364,3742,4222,3789" shape="poly">
                                    <area target="_blank" alt="124" title="124"  coords="4143,3558,4306,3516,4343,3631,4185,3679" shape="poly">
                                    <area target="_blank" alt="123" title="123"  coords="4106,3458,4274,3400,4311,3516,4143,3563" shape="poly">
                                    <area target="_blank" alt="122" title="122"  coords="4064,3337,4222,3284,4274,3395,4101,3452" shape="poly">
                                    <area target="_blank" alt="121" title="121"  coords="4022,3210,4180,3158,4227,3295,4064,3331" shape="poly">
                                    <area target="_blank" alt="120" title="120"  coords="3980,3079,4116,3031,4174,3158,4016,3205" shape="poly">
                                    <area target="_blank" alt="119" title="119"  coords="3932,2942,4059,2895,4127,3026,3974,3073" shape="poly">
                                    <area target="_blank" alt="118" title="118"  coords="3880,2821,3985,2752,4059,2895,3922,2931" shape="poly">
                                    <area target="_blank" alt="117" title="117"  coords="3801,2674,3785,2595,3864,2621,3932,2679,3974,2752,3880,2816,3817,2742,3848,2710" shape="poly">
                                    <area target="_blank" alt="116" title="116"  coords="3611,2626,3701,2595,3796,2600,3801,2679,3748,2763,3675,2789,3643,2705" shape="poly">
                                    <area target="_blank" alt="115" title="115"  coords="3664,2779,3743,2768,3785,2810,3838,2868,3869,2926,3738,2979" shape="poly">
                                    <area target="_blank" alt="114" title="114"  coords="3748,2979,3859,2931,3901,3063,3780,3110" shape="poly">
                                    <area target="_blank" alt="113" title="113"  coords="3611,3031,3727,2984,3790,3100,3653,3147" shape="poly">
                                    <area target="_blank" alt="112" title="112"  coords="3575,2910,3696,2868,3738,2973,3606,3031" shape="poly">
                                    <area target="_blank" alt="111" title="111"  coords="3538,2805,3653,2763,3701,2874,3569,2910" shape="poly">
                                    <area target="_blank" alt="110" title="110"  coords="3480,2663,3601,2621,3659,2758,3527,2800" shape="poly">

                                    <area target="_blank" alt="109" title="109"  coords="3406,2416,3590,2358,3638,2421,3638,2468,3601,2495,3454,2552"  shape="poly">
                                    <area target="_blank" alt="108" title="108"  coords="3364,2300,3496,2252,3585,2358,3406,2410" shape="poly">
                                    <area target="_blank" alt="107" title="107"  coords="3327,2195,3448,2152,3496,2252,3359,2295" shape="poly">
                                    <area target="_blank" alt="106" title="106"  coords="3264,2100,3401,2047,3443,2152,3338,2184,3322,2152,3291,2163" shape="poly">
                                    <area target="_blank" alt="105" title="105"  coords="3106,2158,3259,2105,3291,2147,3254,2168,3269,2210,3143,2258" shape="poly">
                                    <area target="_blank" alt="104" title="104"  coords="3148,2258,3269,2210,3312,2326,3191,2374" shape="poly">
                                    <area target="_blank" alt="103" title="103"  coords="3191,2374,3301,2342,3354,2437,3227,2479" shape="poly">
                                    <area target="_blank" alt="102" title="102"  coords="3227,2484,3348,2452,3390,2574,3275,2610" shape="poly">
                                    <area target="_blank" alt="101" title="101"  coords="3101,2531,3217,2489,3269,2616,3138,2658" shape="poly">
                                    <area target="_blank" alt="100" title="100"  coords="3054,2416,3180,2379,3227,2484,3106,2521" shape="poly">
                                    <area target="_blank" alt="99" title="99"  coords="3022,2295,3133,2263,3185,2379,3054,2410" shape="poly">
                                    <area target="_blank" alt="98" title="98"  coords="2959,2205,3096,2163,3138,2258,3033,2289,3006,2247,2975,2252" shape="poly">
                                    <area target="_blank" alt="97" title="97"  coords="2822,2252,2954,2195,2970,2242,2943,2268,2959,2310,2849,2347" shape="poly">
                                    <area target="_blank" alt="96" title="96"  coords="2859,2347,2959,2316,3001,2421,2885,2458" shape="poly">
                                    <area target="_blank" alt="95" title="95"  coords="2891,2458,3001,2426,3038,2537,2917,2579" shape="poly">
                                    <area target="_blank" alt="94" title="94"  coords="2938,2579,3043,2537,3075,2674,2975,2716" shape="poly">
                                    <area target="_blank" alt="93" title="93"  coords="2817,2610,2922,2574,2970,2710,2854,2758" shape="poly">
                                    <area target="_blank" alt="92" title="92"  coords="2770,2500,2880,2474,2928,2574,2812,2605" shape="poly">
                                    <area target="_blank" alt="91" title="91"  coords="2738,2395,2854,2352,2885,2463,2780,2500" shape="poly">
                                    <area target="_blank" alt="90" title="90"  coords="2680,2310,2812,2247,2843,2347,2738,2389,2733,2347,2696,2347" shape="poly">
                                    <area target="_blank" alt="89" title="89"  coords="2549,2352,2670,2300,2691,2342,2664,2363,2686,2421,2565,2458" shape="poly">
                                    <area target="_blank" alt="88" title="88"  coords="2580,2463,2680,2426,2722,2531,2607,2568" shape="poly">
                                    <area target="_blank" alt="87" title="87"  coords="2612,2574,2728,2526,2759,2642,2638,2684" shape="poly">
                                    <area target="_blank" alt="86" title="86"  coords="2638,2684,2754,2647,2812,2779,2675,2816" shape="poly">

                                    <area target="_blank" alt="85" title="85"  coords="2517,2721,2633,2689,2680,2816,2559,2858" shape="poly">
                                    <area target="_blank" alt="84" title="84"  coords="2486,2605,2601,2579,2643,2684,2517,2721" shape="poly">
                                    <area target="_blank" alt="83" title="83"  coords="2454,2500,2580,2463,2607,2574,2486,2610" shape="poly">
                                    <area target="_blank" alt="82" title="82"  coords="2423,2379,2549,2352,2580,2474,2459,2495" shape="poly">
                                    <area target="_blank" alt="81" title="81"  coords="2412,2274,2538,2242,2559,2352,2423,2379" shape="poly">
                                    <area target="_blank" alt="80" title="80"  coords="2386,2158,2512,2137,2538,2242,2407,2268" shape="poly">
                                    <area target="_blank" alt="79" title="79"  coords="2365,2053,2486,2031,2512,2137,2380,2158" shape="poly">
                                    <area target="_blank" alt="78" title="78"  coords="2349,1958,2470,1926,2491,2021,2359,2053" shape="poly">
                                    <area target="_blank" alt="77" title="77"  coords="2323,1847,2444,1816,2465,1926,2338,1958" shape="poly">
                                    <area target="_blank" alt="76" title="76"  coords="2296,1721,2417,1705,2454,1816,2323,1842" shape="poly">
                                    <area target="_blank" alt="75" title="75"  coords="2275,1595,2391,1574,2428,1695,2296,1726" shape="poly">
                                    <area target="_blank" alt="74" title="74"  coords="2238,1463,2365,1437,2401,1568,2270,1589" shape="poly">
                                    <area target="_blank" alt="73" title="73"  coords="2217,1326,2333,1305,2365,1431,2238,1458" shape="poly">
                                    <area target="_blank" alt="72" title="72"  coords="2186,1200,2317,1174,2333,1310,2212,1326" shape="poly">
                                    <area target="_blank" alt="71" title="71"  coords="2159,1095,2280,1074,2307,1174,2191,1205" shape="poly">
                                    <area target="_blank" alt="70" title="70"  coords="2144,995,2265,974,2280,1063,2165,1095" shape="poly">
                                    <area target="_blank" alt="69" title="69"  coords="2112,895,2249,874,2265,974,2144,1000" shape="poly">
                                    <area target="_blank" alt="68" title="68"  coords="2091,800,2233,779,2244,868,2117,889" shape="poly">
                                    <area target="_blank" alt="67" title="67"  coords="2065,695,2207,674,2228,774,2096,795" shape="poly">
                                    <area target="_blank" alt="66" title="66"  coords="2044,584,2159,553,2191,663,2075,695" shape="poly">
                                    <area target="_blank" alt="65" title="65"  coords="2017,463,2123,442,2159,553,2044,574" shape="poly">
                                    <area target="_blank" alt="64" title="64"  coords="1981,353,2096,332,2123,432,2017,463" shape="poly">
                                    <area target="_blank" alt="63" title="63"  coords="1928,221,2065,195,2086,316,1991,342,1991,300,1954,295"  shape="poly">

                                    <area target="_blank" alt="62" title="62"  coords="1796,253,1933,226,1949,284,1917,300,1923,342,1818,358" shape="poly">
                                    <area target="_blank" alt="61" title="61"  coords="1823,368,1923,347,1954,458,1849,474" shape="poly">
                                    <area target="_blank" alt="60" title="60"  coords="1844,474,1954,458,1991,568,1875,600" shape="poly">
                                    <area target="_blank" alt="59" title="59"  coords="1891,658,1991,637,2023,753,1917,774" shape="poly">
                                    <area target="_blank" alt="58" title="58"  coords="1917,779,2023,763,2054,853,1939,874" shape="poly">
                                    <area target="_blank" alt="57" title="57"  coords="1933,884,2054,853,2070,947,1960,974" shape="poly">
                                    <area target="_blank" alt="56" title="56"  coords="1965,974,2070,958,2091,1042,1981,1068" shape="poly">
                                    <area target="_blank" alt="55" title="55"  coords="1986,1063,2086,1053,2112,1147,2002,1174" shape="poly">
                                    <area target="_blank" alt="54" title="54"  coords="2012,1179,2112,1153,2144,1295,2038,1316" shape="poly">
                                    <area target="_blank" alt="53" title="53"  coords="2133,1789,2238,1763,2280,1910,2159,1931" shape="poly">
                                    <area target="_blank" alt="52" title="52"  coords="2159,1931,2270,1900,2291,2010,2175,2037" shape="poly">
                                    <area target="_blank" alt="51" title="51"  coords="2181,2037,2296,2021,2312,2110,2196,2137" shape="poly">
                                    <area target="_blank" alt="50" title="50"  coords="2196,2137,2317,2121,2328,2210,2217,2237" shape="poly">
                                    <area target="_blank" alt="49" title="49"  coords="2217,2231,2328,2216,2349,2305,2238,2326" shape="poly">
                                    <area target="_blank" alt="48" title="48"  coords="2244,2331,2349,2316,2375,2410,2254,2431" shape="poly">
                                    <area target="_blank" alt="47" title="47"  coords="2265,2431,2370,2410,2391,2510,2280,2542" shape="poly">
                                    <area target="_blank" alt="46" title="46"  coords="2291,2542,2401,2516,2423,2631,2312,2652" shape="poly">
                                    <area target="_blank" alt="45" title="45"  coords="2312,2658,2428,2642,2459,2747,2338,2768" shape="poly">
                                    <area target="_blank" alt="44" title="44"  coords="2344,2774,2454,2752,2491,2884,2380,2916" shape="poly">

                                    <area target="_blank" alt="43" title="43" coords="2217,2805,2338,2779,2375,2921,2259,2963" shape="poly">
                                    <area target="_blank" alt="42" title="42" coords="2186,2684,2307,2668,2333,2774,2217,2800" shape="poly">
                                    <area target="_blank" alt="41" title="41" coords="2170,2568,2280,2552,2312,2668,2191,2679" shape="poly">
                                    <area target="_blank" alt="40" title="40" coords="2144,2452,2254,2426,2286,2542,2170,2563" shape="poly">
                                    <area target="_blank" alt="39" title="39" coords="2128,2352,2228,2331,2244,2421,2154,2452" shape="poly">
                                    <area target="_blank" alt="38" title="38" coords="2112,2258,2212,2237,2228,2326,2128,2347" shape="poly">
                                    <area target="_blank" alt="37" title="37" coords="2091,2163,2191,2137,2217,2231,2112,2252" shape="poly">
                                    <area target="_blank" alt="36" title="36" coords="2070,2058,2170,2037,2196,2137,2091,2163" shape="poly">
                                    <area target="_blank" alt="35" title="35" coords="2044,1953,2159,1937,2181,2031,2065,2053" shape="poly">
                                    <area target="_blank" alt="34" title="34" coords="2017,1810,2123,1795,2159,1931,2044,1958" shape="poly">
                                    <area target="_blank" alt="33" title="33" coords="1896,1200,2002,1184,2028,1316,1923,1337" shape="poly">
                                    <area target="_blank" alt="32" title="32" coords="1875,1089,1975,1068,2012,1174,1896,1195" shape="poly">
                                    <area target="_blank" alt="31" title="31" coords="1854,989,1954,979,1981,1068,1875,1084" shape="poly">
                                    <area target="_blank" alt="30" title="30" coords="1833,900,1933,874,1965,974,1844,995" shape="poly">
                                    <area target="_blank" alt="29" title="29" coords="1812,805,1912,784,1939,868,1833,900" shape="poly">
                                    <area target="_blank" alt="28" title="28" coords="1781,674,1886,658,1912,774,1802,800" shape="poly">
                                    <area target="_blank" alt="27" title="27" coords="1733,500,1844,479,1875,600,1760,621" shape="poly">
                                    <area target="_blank" alt="26" title="26" coords="1712,384,1818,368,1844,468,1739,500" shape="poly">
                                    <area target="_blank" alt="25" title="25" coords="1691,268,1791,258,1823,363,1712,384" shape="poly">

                                    <area target="_blank" id="a_24" title="24" coords="1539,326,1639,305,1670,447,1565,474"  shape="poly" >
                                    <area target="_blank" id="a_23" title="23" coords="1560,480,1665,453,1686,553,1576,574"   shape="poly">
                                    <area target="_blank" id="a_22" title="22" coords="1570,574,1686,553,1712,663,1591,689" shape="poly">
                                    <area target="_blank" id="a_21" title="21" coords="1591,689,1712,663,1739,768,1612,800"   shape="poly">
                                    <area target="_blank" id="a_20" title="20" coords="1614,800,1736,768,1760,874,1633,900" shape="poly">
                                    <area target="_blank" id="a_19" title="19" coords="1640,900,1760,879,1786,974,1665,1005"  shape="poly">
                                    <area target="_blank" alt="18" title="18" coords="1664,1003,1691,1103,1805,1070,1785,977" shape="poly">
                                    <area target="_blank" alt="17" title="17" coords="1697,1100,1807,1074,1833,1168,1712,1200" shape="poly">
                                    <area target="_blank" alt="16" title="16" coords="1712,1200,1833,1174,1849,1274,1728,1295"  shape="poly">
                                    <area target="_blank" alt="15" title="15" coords="1728,1300,1850,1273,1875,1400,1744,1421" shape="poly">
                                    <area target="_blank" alt="14" title="14" coords="1744,1426,1875,1400,1898,1535,1770,1558" shape="poly">
                                    <area target="_blank" alt="13" title="13" coords="1758,1560,1896,1531,1923,1658,1796,1690" shape="poly">
                                    <area target="_blank" alt="12" title="12" coords="1796,1689,1923,1664,1952,1795,1823,1821"  shape="poly">
                                    <area target="_blank" alt="11" title="11" coords="1828,1821,1952,1800,1981,1916,1854,1947" shape="poly">
                                    <area target="_blank" alt="10" title="10" coords="1854,1947,1975,1921,1996,2021,1875,2047" shape="poly">
                                    <area target="_blank" alt="9" title="9" coords="1875,2047,1996,2026,2017,2121,1896,2142" shape="poly">
                                    <area target="_blank" alt="8" title="8" coords="1896,2147,2017,2126,2038,2221,1917,2247" shape="poly">
                                    <area target="_blank" alt="7" title="7" coords="1917,2252,2033,2224,2060,2321,1933,2352" shape="poly">
                                    <area target="_blank" alt="6" title="6" coords="1939,2352,2054,2331,2081,2426,1960,2452" shape="poly">
                                    <area target="_blank" alt="5" title="5" coords="1960,2458,2075,2431,2100,2526,1981,2558" shape="poly">
                                    <area target="_blank" alt="4" title="4" coords="1981,2560,2102,2540,2117,2652,2007,2674" shape="poly">
                                    <area target="_blank" id="ap1_3" title="3" coords="2012,2675,2123,2658,2149,2784,2033,2800" shape="poly">
                                    <area target="_blank" id="ap1_2" title="2" coords="2033,2800,2144,2784,2170,2900,2049,2926" shape="poly">
                                    <area target="_blank" id="ap1_1" title="1" coords="2044,2927,2175,2905,2223,3089,2181,3105,2202,3326,2154,3347" shape="poly">
                                </map>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 100%">
                    <div id="mapContainer_p2"  style="display: none;" >
                        <div style="width: 100%; height: 657px; overflow: hidden; background-color: white;" class="boder-plan rounded">
                            <div id="mapObject2">
                                <img  id="m_main2" src="<?= base_url('assets/images/pornpit-plansale1.jpg') ?>" usemap="#image-map2" width="930" height="657">
                                <map name="image-map2" >
                                    <area target="_blank" alt="213" title="213"  coords="1739,3684,1833,3673,1860,3794,1744,3816" shape="poly">
                                    <area target="_blank" alt="212" title="212"  coords="1744,3810,1865,3800,1875,3868,1917,3868,1917,3910,1760,3905" shape="poly">
                                    <area target="_blank" alt="211" title="211"  coords="1912,3863,1939,3858,1933,3816,2033,3800,2065,3910,1917,3916" shape="poly">
                                    <area target="_blank" alt="210" title="210"  coords="1907,3705,2007,3689,2044,3794,1923,3821" shape="poly">
                                    <area target="_blank" alt="209" title="209"  coords="2007,3684,2112,3668,2165,3773,2044,3800" shape="poly">
                                    <area target="_blank" alt="208" title="208"  coords="2049,3810,2159,3773,2186,3847,2228,3863,2244,3916,2070,3905" shape="poly">
                                    <area target="_blank" alt="207" title="207"  coords="2217,3842,2259,3837,2238,3789,2338,3763,2391,3910,2254,3910" shape="poly">
                                    <area target="_blank" alt="206" title="206"  coords="2181,3647,2280,3626,2344,3763,2228,3789" shape="poly">
                                    <area target="_blank" alt="205" title="205"  coords="2233,3463,2344,3421,2375,3521,2254,3568" shape="poly">
                                    <area target="_blank" alt="204" title="204"  coords="2154,3352,2291,3305,2344,3421,2191,3479" shape="poly">
                                    <area target="_blank" alt="203" title="203"  coords="2291,3616,2407,3579,2454,3700,2333,3737" shape="poly">
                                    <area target="_blank" alt="202" title="202"  coords="2333,3737,2454,3705,2475,3784,2370,3826" shape="poly">
                                    <area target="_blank" alt="201" title="201"  coords="2359,3837,2470,3789,2517,3889,2544,3873,2554,3910,2396,3910" shape="poly">

                                    <area target="_blank" alt="200" title="200"  coords="2538,3868,2565,3858,2554,3810,2670,3784,2712,3910,2549,3910"  shape="poly">
                                    <area target="_blank" alt="199" title="199"  coords="2522,3726,2649,3679,2675,3789,2559,3816" shape="poly">
                                    <area target="_blank" alt="198" title="198"  coords="2486,3621,2612,3589,2638,3684,2528,3737" shape="poly">
                                    <area target="_blank" alt="197" title="197"  coords="2444,3516,2570,3473,2617,3589,2486,3621" shape="poly">
                                    <area target="_blank" alt="196" title="196"  coords="2380,3347,2507,3305,2570,3468,2438,3516" shape="poly">
                                    <area target="_blank" alt="195" title="195"  coords="2517,3300,2622,3258,2675,3368,2549,3416" shape="poly">
                                    <area target="_blank" alt="194" title="194"  coords="2559,3416,2664,3379,2707,3473,2580,3516" shape="poly">
                                    <area target="_blank" alt="193" title="193"  coords="2591,3510,2701,3473,2743,3579,2622,3616" shape="poly">
                                    <area target="_blank" alt="192" title="192"  coords="2628,3621,2733,3579,2775,3679,2659,3710" shape="poly">
                                    <area target="_blank" alt="191" title="191"  coords="2659,3716,2785,3673,2817,3773,2696,3810" shape="poly">
                                    <area target="_blank" alt="190" title="190"  coords="2686,3821,2801,3779,2843,3884,2875,3868,2880,3910,2717,3910" shape="poly">

                                    <area target="_blank" alt="189" title="189"  coords="2854,3879,2896,3863,2880,3826,3006,3789,3043,3910,2864,3910" shape="poly">
                                    <area target="_blank" alt="188" title="188"  coords="2854,3726,2975,3689,3001,3789,2885,3831" shape="poly">
                                    <area target="_blank" alt="187" title="187"  coords="2822,3637,2938,3600,2975,3689,2854,3737" shape="poly">
                                    <area target="_blank" alt="186" title="186"  coords="2791,3542,2896,3500,2943,3589,2822,3642" shape="poly">
                                    <area target="_blank" alt="185" title="185"  coords="2759,3447,2870,3416,2901,3500,2791,3547" shape="poly">
                                    <area target="_blank" alt="184" title="184"  coords="2722,3352,2843,3326,2880,3410,2754,3442" shape="poly">
                                    <area target="_blank" alt="183" title="183"  coords="2686,3231,2801,3200,2849,3316,2717,3352" shape="poly">
                                    <area target="_blank" alt="182" title="182"  coords="2807,3195,2922,3163,2959,3273,2843,3316" shape="poly">
                                    <area target="_blank" alt="181" title="181"  coords="2849,3316,2959,3284,3001,3373,2875,3416" shape="poly">
                                    <area target="_blank" alt="180" title="180"  coords="2880,3410,2991,3379,3033,3473,2912,3516" shape="poly">
                                    <area target="_blank" alt="179" title="179"  coords="2912,3521,3033,3479,3070,3573,2943,3616" shape="poly">
                                    <area target="_blank" alt="178" title="178"  coords="2949,3621,3064,3573,3106,3673,2991,3710" shape="poly">
                                    <area target="_blank" alt="177" title="177"  coords="2991,3716,3101,3679,3133,3773,3012,3826" shape="poly">
                                    <area target="_blank" alt="176" title="176"  coords="3017,3823,3138,3775,3170,3886,3217,3875,3227,3910,3043,3912"  shape="poly">

                                    <area target="_blank" alt="175" title="175"  coords="3206,3868,3275,3852,3259,3816,3333,3789,3375,3900,3212,3910"  shape="poly">
                                    <area target="_blank" alt="174" title="174"  coords="3185,3731,3296,3689,3333,3784,3212,3821" shape="poly">
                                    <area target="_blank" alt="173" title="173"  coords="3138,3631,3259,3605,3301,3684,3191,3737" shape="poly">
                                    <area target="_blank" alt="172" title="172"  coords="3117,3542,3227,3500,3269,3595,3154,3631" shape="poly">
                                    <area target="_blank" alt="171" title="171"  coords="3091,3452,3206,3416,3233,3489,3112,3542" shape="poly">
                                    <area target="_blank" alt="170" title="170"  coords="3059,3358,3170,3316,3206,3410,3085,3447" shape="poly">
                                    <area target="_blank" alt="169" title="169"  coords="3022,3252,3133,3221,3164,3326,3059,3352" shape="poly">
                                    <area target="_blank" alt="168" title="168"  coords="2980,3131,3096,3105,3133,3216,3017,3252" shape="poly">
                                    <area target="_blank" alt="167" title="167"  coords="3101,3089,3212,3063,3254,3173,3133,3221" shape="poly">
                                    <area target="_blank" alt="166" title="166"  coords="3148,3216,3259,3174,3291,3274,3175,3311" shape="poly">
                                    <area target="_blank" alt="165" title="165"  coords="3180,3321,3296,3279,3327,3379,3212,3410" shape="poly">
                                    <area target="_blank" alt="164_163" title="164_163"  coords="3201,3416,3327,3379,3401,3573,3275,3605" shape="poly">
                                    <area target="_blank" alt="162" title="162"  coords="3275,3605,3390,3573,3433,3679,3306,3710" shape="poly">
                                    <area target="_blank" alt="161" title="161"  coords="3312,3710,3433,3679,3469,3773,3348,3810" shape="poly">
                                    <area target="_blank" alt="160" title="160"  coords="3348,3821,3464,3773,3511,3910,3385,3905" shape="poly">

                                    <area target="_blank" alt="159" title="159"  coords="3532,3763,3648,3731,3690,3852,3564,3858" shape="poly">
                                    <area target="_blank" alt="158" title="158"  coords="3490,3668,3617,3621,3648,3726,3532,3763" shape="poly">
                                    <area target="_blank" alt="157" title="157"  coords="3454,3552,3575,3526,3611,3626,3485,3658" shape="poly">
                                    <area target="_blank" alt="156" title="156"  coords="3417,3447,3532,3410,3585,3521,3454,3558" shape="poly">
                                    <area target="_blank" alt="155" title="155"  coords="3385,3347,3506,3310,3538,3405,3417,3458" shape="poly">
                                    <area target="_blank" alt="154" title="154"  coords="3354,3242,3454,3205,3511,3316,3380,3342" shape="poly">
                                    <area target="_blank" alt="153" title="153"  coords="3312,3131,3427,3100,3459,3205,3348,3247" shape="poly">
                                    <area target="_blank" alt="152" title="152"  coords="3275,3026,3390,2995,3427,3095,3312,3126" shape="poly">
                                    <area target="_blank" alt="151" title="151"  coords="3227,2905,3348,2868,3390,2989,3275,3026" shape="poly">
                                    <area target="_blank" alt="150" title="150"  coords="3185,2768,3306,2737,3354,2863,3233,2905" shape="poly">
                                    <area target="_blank" alt="149" title="149"  coords="3296,2726,3427,2679,3480,2821,3359,2863" shape="poly">
                                    <area target="_blank" alt="148" title="148"  coords="3354,2868,3469,2837,3517,2947,3390,2995" shape="poly">
                                    <area target="_blank" alt="147" title="147"  coords="3396,2989,3522,2947,3559,3063,3433,3105" shape="poly">
                                    <area target="_blank" alt="146" title="146"  coords="3443,3105,3564,3068,3601,3189,3475,3237" shape="poly">
                                    <area target="_blank" alt="145" title="145"  coords="3485,3231,3596,3200,3643,3305,3522,3358" shape="poly">
                                    <area target="_blank" alt="144" title="144"  coords="3527,3358,3643,3310,3675,3410,3559,3458" shape="poly">
                                    <area target="_blank" alt="143" title="143"  coords="3564,3452,3675,3410,3711,3510,3596,3552" shape="poly">
                                    <area target="_blank" alt="142_141" title="142_141"  coords="3606,3558,3717,3516,3780,3721,3659,3752" shape="poly">
                                    <area target="_blank" alt="140" title="140"  coords="3664,3752,3785,3721,3838,3858,3696,3852" shape="poly">
                                    <area target="_blank" alt="139" title="139"  coords="3880,3805,4001,3758,4064,3910,3911,3905" shape="poly">
                                    <area target="_blank" alt="138" title="138"  coords="3843,3695,3964,3647,4001,3747,3880,3800" shape="poly">
                                    <area target="_blank" alt="137" title="137"  coords="3806,3579,3927,3531,3964,3642,3843,3689" shape="poly">
                                    <area target="_blank" alt="136" title="136"  coords="3764,3468,3885,3410,3922,3531,3796,3573" shape="poly">
                                    <area target="_blank" alt="135" title="135"  coords="3722,3342,3843,3295,3885,3410,3759,3468" shape="poly">
                                    <area target="_blank" alt="134" title="134"  coords="3675,3205,3801,3168,3843,3295,3711,3337" shape="poly">
                                    <area target="_blank" alt="133" title="133"  coords="3811,3163,3932,3121,3974,3237,3843,3273" shape="poly">
                                    <area target="_blank" alt="132" title="132"  coords="3848,3279,3964,3247,4001,3337,3880,3384" shape="poly">
                                    <area target="_blank" alt="131" title="131"  coords="3885,3395,4006,3342,4048,3447,3927,3489" shape="poly">
                                    <area target="_blank" alt="130" title="130"  coords="3927,3500,4038,3452,4080,3558,3953,3605" shape="poly">
                                    <area target="_blank" alt="129" title="129"  coords="3959,3605,4080,3558,4111,3663,3990,3705" shape="poly">
                                    <area target="_blank" alt="128" title="128"  coords="4001,3710,4111,3668,4153,3779,4032,3810" shape="poly">
                                    <area target="_blank" alt="127" title="127"  coords="4032,3805,4153,3784,4174,3847,4211,3852,4232,3905,4064,3916" shape="poly">
                                    <area target="_blank" alt="126" title="126"  coords="4206,3842,4290,3821,4264,3784,4364,3752,4400,3905,4232,3905" shape="poly">
                                    <area target="_blank" alt="125" title="125"  coords="4185,3684,4327,3631,4364,3742,4222,3789" shape="poly">
                                    <area target="_blank" alt="124" title="124"  coords="4143,3558,4306,3516,4343,3631,4185,3679" shape="poly">
                                    <area target="_blank" alt="123" title="123"  coords="4106,3458,4274,3400,4311,3516,4143,3563" shape="poly">
                                    <area target="_blank" alt="122" title="122"  coords="4064,3337,4222,3284,4274,3395,4101,3452" shape="poly">
                                    <area target="_blank" alt="121" title="121"  coords="4022,3210,4180,3158,4227,3295,4064,3331" shape="poly">
                                    <area target="_blank" alt="120" title="120"  coords="3980,3079,4116,3031,4174,3158,4016,3205" shape="poly">
                                    <area target="_blank" alt="119" title="119"  coords="3932,2942,4059,2895,4127,3026,3974,3073" shape="poly">
                                    <area target="_blank" alt="118" title="118"  coords="3880,2821,3985,2752,4059,2895,3922,2931" shape="poly">
                                    <area target="_blank" alt="117" title="117"  coords="3801,2674,3785,2595,3864,2621,3932,2679,3974,2752,3880,2816,3817,2742,3848,2710" shape="poly">
                                    <area target="_blank" alt="116" title="116"  coords="3611,2626,3701,2595,3796,2600,3801,2679,3748,2763,3675,2789,3643,2705" shape="poly">
                                    <area target="_blank" alt="115" title="115"  coords="3664,2779,3743,2768,3785,2810,3838,2868,3869,2926,3738,2979" shape="poly">
                                    <area target="_blank" alt="114" title="114"  coords="3748,2979,3859,2931,3901,3063,3780,3110" shape="poly">
                                    <area target="_blank" alt="113" title="113"  coords="3611,3031,3727,2984,3790,3100,3653,3147" shape="poly">
                                    <area target="_blank" alt="112" title="112"  coords="3575,2910,3696,2868,3738,2973,3606,3031" shape="poly">
                                    <area target="_blank" alt="111" title="111"  coords="3538,2805,3653,2763,3701,2874,3569,2910" shape="poly">
                                    <area target="_blank" alt="110" title="110"  coords="3480,2663,3601,2621,3659,2758,3527,2800" shape="poly">

                                    <area target="_blank" alt="109" title="109"  coords="3406,2416,3590,2358,3638,2421,3638,2468,3601,2495,3454,2552"  shape="poly">
                                    <area target="_blank" alt="108" title="108"  coords="3364,2300,3496,2252,3585,2358,3406,2410" shape="poly">
                                    <area target="_blank" alt="107" title="107"  coords="3327,2195,3448,2152,3496,2252,3359,2295" shape="poly">
                                    <area target="_blank" alt="106" title="106"  coords="3264,2100,3401,2047,3443,2152,3338,2184,3322,2152,3291,2163" shape="poly">
                                    <area target="_blank" alt="105" title="105"  coords="3106,2158,3259,2105,3291,2147,3254,2168,3269,2210,3143,2258" shape="poly">
                                    <area target="_blank" alt="104" title="104"  coords="3148,2258,3269,2210,3312,2326,3191,2374" shape="poly">
                                    <area target="_blank" alt="103" title="103"  coords="3191,2374,3301,2342,3354,2437,3227,2479" shape="poly">
                                    <area target="_blank" alt="102" title="102"  coords="3227,2484,3348,2452,3390,2574,3275,2610" shape="poly">
                                    <area target="_blank" alt="101" title="101"  coords="3101,2531,3217,2489,3269,2616,3138,2658" shape="poly">
                                    <area target="_blank" alt="100" title="100"  coords="3054,2416,3180,2379,3227,2484,3106,2521" shape="poly">
                                    <area target="_blank" alt="99" title="99"  coords="3022,2295,3133,2263,3185,2379,3054,2410" shape="poly">
                                    <area target="_blank" alt="98" title="98"  coords="2959,2205,3096,2163,3138,2258,3033,2289,3006,2247,2975,2252" shape="poly">
                                    <area target="_blank" alt="97" title="97"  coords="2822,2252,2954,2195,2970,2242,2943,2268,2959,2310,2849,2347" shape="poly">
                                    <area target="_blank" alt="96" title="96"  coords="2859,2347,2959,2316,3001,2421,2885,2458" shape="poly">
                                    <area target="_blank" alt="95" title="95"  coords="2891,2458,3001,2426,3038,2537,2917,2579" shape="poly">
                                    <area target="_blank" alt="94" title="94"  coords="2938,2579,3043,2537,3075,2674,2975,2716" shape="poly">
                                    <area target="_blank" alt="93" title="93"  coords="2817,2610,2922,2574,2970,2710,2854,2758" shape="poly">
                                    <area target="_blank" alt="92" title="92"  coords="2770,2500,2880,2474,2928,2574,2812,2605" shape="poly">
                                    <area target="_blank" alt="91" title="91"  coords="2738,2395,2854,2352,2885,2463,2780,2500" shape="poly">
                                    <area target="_blank" alt="90" title="90"  coords="2680,2310,2812,2247,2843,2347,2738,2389,2733,2347,2696,2347" shape="poly">
                                    <area target="_blank" alt="89" title="89"  coords="2549,2352,2670,2300,2691,2342,2664,2363,2686,2421,2565,2458" shape="poly">
                                    <area target="_blank" alt="88" title="88"  coords="2580,2463,2680,2426,2722,2531,2607,2568" shape="poly">
                                    <area target="_blank" alt="87" title="87"  coords="2612,2574,2728,2526,2759,2642,2638,2684" shape="poly">
                                    <area target="_blank" alt="86" title="86"  coords="2638,2684,2754,2647,2812,2779,2675,2816" shape="poly">

                                    <area target="_blank" alt="85" title="85"  coords="2517,2721,2633,2689,2680,2816,2559,2858" shape="poly">
                                    <area target="_blank" alt="84" title="84"  coords="2486,2605,2601,2579,2643,2684,2517,2721" shape="poly">
                                    <area target="_blank" alt="83" title="83"  coords="2454,2500,2580,2463,2607,2574,2486,2610" shape="poly">
                                    <area target="_blank" alt="82" title="82"  coords="2423,2379,2549,2352,2580,2474,2459,2495" shape="poly">
                                    <area target="_blank" alt="81" title="81"  coords="2412,2274,2538,2242,2559,2352,2423,2379" shape="poly">
                                    <area target="_blank" alt="80" title="80"  coords="2386,2158,2512,2137,2538,2242,2407,2268" shape="poly">
                                    <area target="_blank" alt="79" title="79"  coords="2365,2053,2486,2031,2512,2137,2380,2158" shape="poly">
                                    <area target="_blank" alt="78" title="78"  coords="2349,1958,2470,1926,2491,2021,2359,2053" shape="poly">
                                    <area target="_blank" alt="77" title="77"  coords="2323,1847,2444,1816,2465,1926,2338,1958" shape="poly">
                                    <area target="_blank" alt="76" title="76"  coords="2296,1721,2417,1705,2454,1816,2323,1842" shape="poly">
                                    <area target="_blank" alt="75" title="75"  coords="2275,1595,2391,1574,2428,1695,2296,1726" shape="poly">
                                    <area target="_blank" alt="74" title="74"  coords="2238,1463,2365,1437,2401,1568,2270,1589" shape="poly">
                                    <area target="_blank" alt="73" title="73"  coords="2217,1326,2333,1305,2365,1431,2238,1458" shape="poly">
                                    <area target="_blank" alt="72" title="72"  coords="2186,1200,2317,1174,2333,1310,2212,1326" shape="poly">
                                    <area target="_blank" alt="71" title="71"  coords="2159,1095,2280,1074,2307,1174,2191,1205" shape="poly">
                                    <area target="_blank" alt="70" title="70"  coords="2144,995,2265,974,2280,1063,2165,1095" shape="poly">
                                    <area target="_blank" alt="69" title="69"  coords="2112,895,2249,874,2265,974,2144,1000" shape="poly">
                                    <area target="_blank" alt="68" title="68"  coords="2091,800,2233,779,2244,868,2117,889" shape="poly">
                                    <area target="_blank" alt="67" title="67"  coords="2065,695,2207,674,2228,774,2096,795" shape="poly">
                                    <area target="_blank" alt="66" title="66"  coords="2044,584,2159,553,2191,663,2075,695" shape="poly">
                                    <area target="_blank" alt="65" title="65"  coords="2017,463,2123,442,2159,553,2044,574" shape="poly">
                                    <area target="_blank" alt="64" title="64"  coords="1981,353,2096,332,2123,432,2017,463" shape="poly">
                                    <area target="_blank" alt="63" title="63"  coords="1928,221,2065,195,2086,316,1991,342,1991,300,1954,295"  shape="poly">

                                    <area target="_blank" alt="62" title="62"  coords="1796,253,1933,226,1949,284,1917,300,1923,342,1818,358" shape="poly">
                                    <area target="_blank" alt="61" title="61"  coords="1823,368,1923,347,1954,458,1849,474" shape="poly">
                                    <area target="_blank" alt="60" title="60"  coords="1844,474,1954,458,1991,568,1875,600" shape="poly">
                                    <area target="_blank" alt="59" title="59"  coords="1891,658,1991,637,2023,753,1917,774" shape="poly">
                                    <area target="_blank" alt="58" title="58"  coords="1917,779,2023,763,2054,853,1939,874" shape="poly">
                                    <area target="_blank" alt="57" title="57"  coords="1933,884,2054,853,2070,947,1960,974" shape="poly">
                                    <area target="_blank" alt="56" title="56"  coords="1965,974,2070,958,2091,1042,1981,1068" shape="poly">
                                    <area target="_blank" alt="55" title="55"  coords="1986,1063,2086,1053,2112,1147,2002,1174" shape="poly">
                                    <area target="_blank" alt="54" title="54"  coords="2012,1179,2112,1153,2144,1295,2038,1316" shape="poly">
                                    <area target="_blank" alt="53" title="53"  coords="2133,1789,2238,1763,2280,1910,2159,1931" shape="poly">
                                    <area target="_blank" alt="52" title="52"  coords="2159,1931,2270,1900,2291,2010,2175,2037" shape="poly">
                                    <area target="_blank" alt="51" title="51"  coords="2181,2037,2296,2021,2312,2110,2196,2137" shape="poly">
                                    <area target="_blank" alt="50" title="50"  coords="2196,2137,2317,2121,2328,2210,2217,2237" shape="poly">
                                    <area target="_blank" alt="49" title="49"  coords="2217,2231,2328,2216,2349,2305,2238,2326" shape="poly">
                                    <area target="_blank" alt="48" title="48"  coords="2244,2331,2349,2316,2375,2410,2254,2431" shape="poly">
                                    <area target="_blank" alt="47" title="47"  coords="2265,2431,2370,2410,2391,2510,2280,2542" shape="poly">
                                    <area target="_blank" alt="46" title="46"  coords="2291,2542,2401,2516,2423,2631,2312,2652" shape="poly">
                                    <area target="_blank" alt="45" title="45"  coords="2312,2658,2428,2642,2459,2747,2338,2768" shape="poly">
                                    <area target="_blank" alt="44" title="44"  coords="2344,2774,2454,2752,2491,2884,2380,2916" shape="poly">

                                    <area target="_blank" alt="43" title="43" coords="2217,2805,2338,2779,2375,2921,2259,2963" shape="poly">
                                    <area target="_blank" alt="42" title="42" coords="2186,2684,2307,2668,2333,2774,2217,2800" shape="poly">
                                    <area target="_blank" alt="41" title="41" coords="2170,2568,2280,2552,2312,2668,2191,2679" shape="poly">
                                    <area target="_blank" alt="40" title="40" coords="2144,2452,2254,2426,2286,2542,2170,2563" shape="poly">
                                    <area target="_blank" alt="39" title="39" coords="2128,2352,2228,2331,2244,2421,2154,2452" shape="poly">
                                    <area target="_blank" alt="38" title="38" coords="2112,2258,2212,2237,2228,2326,2128,2347" shape="poly">
                                    <area target="_blank" alt="37" title="37" coords="2091,2163,2191,2137,2217,2231,2112,2252" shape="poly">
                                    <area target="_blank" alt="36" title="36" coords="2070,2058,2170,2037,2196,2137,2091,2163" shape="poly">
                                    <area target="_blank" alt="35" title="35" coords="2044,1953,2159,1937,2181,2031,2065,2053" shape="poly">
                                    <area target="_blank" alt="34" title="34" coords="2017,1810,2123,1795,2159,1931,2044,1958" shape="poly">
                                    <area target="_blank" alt="33" title="33" coords="1896,1200,2002,1184,2028,1316,1923,1337" shape="poly">
                                    <area target="_blank" alt="32" title="32" coords="1875,1089,1975,1068,2012,1174,1896,1195" shape="poly">
                                    <area target="_blank" alt="31" title="31" coords="1854,989,1954,979,1981,1068,1875,1084" shape="poly">
                                    <area target="_blank" alt="30" title="30" coords="1833,900,1933,874,1965,974,1844,995" shape="poly">
                                    <area target="_blank" alt="29" title="29" coords="1812,805,1912,784,1939,868,1833,900" shape="poly">
                                    <area target="_blank" alt="28" title="28" coords="1781,674,1886,658,1912,774,1802,800" shape="poly">
                                    <area target="_blank" alt="27" title="27" coords="1733,500,1844,479,1875,600,1760,621" shape="poly">
                                    <area target="_blank" alt="26" title="26" coords="1712,384,1818,368,1844,468,1739,500" shape="poly">
                                    <area target="_blank" alt="25" title="25" coords="1691,268,1791,258,1823,363,1712,384" shape="poly">

                                    <area target="_blank" id="a_24" title="24" coords="1539,326,1639,305,1670,447,1565,474"  shape="poly">
                                    <area target="_blank" id="a_23" title="23" coords="1560,480,1665,453,1686,553,1576,574"   shape="poly">
                                    <area target="_blank" id="a_22" title="22" coords="1570,574,1686,553,1712,663,1591,689" shape="poly">
                                    <area target="_blank" id="a_21" title="21" coords="1591,689,1712,663,1739,768,1612,800"   shape="poly">
                                    <area target="_blank" id="a_20" title="20" coords="1614,800,1736,768,1760,874,1633,900" shape="poly">
                                    <area target="_blank" id="a_19" title="19" coords="1640,900,1760,879,1786,974,1665,1005"  shape="poly">
                                    <area target="_blank" alt="18" title="18" coords="1664,1003,1691,1103,1805,1070,1785,977" shape="poly">
                                    <area target="_blank" alt="17" title="17" coords="1697,1100,1807,1074,1833,1168,1712,1200" shape="poly">
                                    <area target="_blank" alt="16" title="16" coords="1712,1200,1833,1174,1849,1274,1728,1295"  shape="poly">
                                    <area target="_blank" alt="15" title="15" coords="1728,1300,1850,1273,1875,1400,1744,1421" shape="poly">
                                    <area target="_blank" alt="14" title="14" coords="1744,1426,1875,1400,1898,1535,1770,1558" shape="poly">
                                    <area target="_blank" alt="13" title="13" coords="1758,1560,1896,1531,1923,1658,1796,1690" shape="poly">
                                    <area target="_blank" alt="12" title="12" coords="1796,1689,1923,1664,1952,1795,1823,1821"  shape="poly">
                                    <area target="_blank" alt="11" title="11" coords="1828,1821,1952,1800,1981,1916,1854,1947" shape="poly">
                                    <area target="_blank" alt="10" title="10" coords="1854,1947,1975,1921,1996,2021,1875,2047" shape="poly">
                                    <area target="_blank" alt="9" title="9" coords="1875,2047,1996,2026,2017,2121,1896,2142" shape="poly">
                                    <area target="_blank" alt="8" title="8" coords="1896,2147,2017,2126,2038,2221,1917,2247" shape="poly">
                                    <area target="_blank" alt="7" title="7" coords="1917,2252,2033,2224,2060,2321,1933,2352" shape="poly">
                                    <area target="_blank" alt="6" title="6" coords="1939,2352,2054,2331,2081,2426,1960,2452" shape="poly">
                                    <area target="_blank" alt="5" title="5" coords="1960,2458,2075,2431,2100,2526,1981,2558" shape="poly">
                                    <area target="_blank" alt="4" title="4" coords="1981,2560,2102,2540,2117,2652,2007,2674" shape="poly">
                                    <area target="_blank" alt="3" title="3" coords="2012,2675,2123,2658,2149,2784,2033,2800" shape="poly">
                                    <area target="_blank" alt="2" title="2" coords="2033,2800,2144,2784,2170,2900,2049,2926" shape="poly">
                                    <area target="_blank" id="ap2_1" title="1" coords="2044,2927,2175,2905,2223,3089,2181,3105,2202,3326,2154,3347" shape="poly">

                                </map>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="width: 100%">
                    <div id="mapContainer_p3" style="display: none;" >
                        <div style="width: 100%; height: 657px; overflow: hidden; background-color: white;" class="boder-plan rounded">
                            <div id="mapObject3">
                                <img  id="m_main3" src="<?= base_url('assets/images/plan.jpg') ?>" usemap="#image-map3" width="930" height="657">
                                <map name="image-map3" >
                                    <area target="_blank" alt="142" title="142"  coords="5011,321,5105,289,5158,395,5063,453" shape="poly">
                                    <area target="_blank" alt="141" title="141"  coords="4895,358,5016,310,5053,453,4953,484" shape="poly">
                                    <area target="_blank" alt="140" title="140"  coords="4800,389,4906,353,4948,479,4848,526" shape="poly">
                                    <area target="_blank" alt="139" title="139"  coords="4695,432,4795,400,4848,516,4742,563" shape="poly">
                                    <area target="_blank" alt="138" title="138"  coords="4595,458,4700,421,4737,558,4679,579,4690,626,4658,647" shape="poly">
                                    <area target="_blank" alt="137" title="137"  coords="4658,653,4753,621,4806,763,4706,800" shape="poly">
                                    <area target="_blank" alt="136" title="136"  coords="4516,668,4642,632,4716,789,4579,842" shape="poly">
                                    <area target="_blank" alt="135" title="135"  coords="4437,516,4590,463,4653,626,4521,663,4495,595,4453,600" shape="poly">
                                    <area target="_blank" alt="134" title="134"  coords="4279,558,4427,510,4458,605,4432,616,4458,684,4332,737" shape="poly">
                                    <area target="_blank" alt="133" title="133"  coords="4337,731,4458,695,4521,858,4395,895" shape="poly">
                                    <area target="_blank" alt="132" title="132"  coords="4653,895,4827,847,4816,905,4806,963,4811,1010,4690,1053" shape="poly">
                                    <area target="_blank" alt="131" title="131"  coords="4706,1058,4806,1016,4821,1068,4879,1158,4748,1195" shape="poly">
                                    <area target="_blank" alt="130" title="130"  coords="4748,1205,4879,1158,4916,1221,4948,1295,4958,1352,4795,1358" shape="poly">
                                    <area target="_blank" alt="129" title="129"  coords="4800,1358,4948,1363,4911,1521,4795,1510" shape="poly">
                                    <area target="_blank" alt="128" title="128"  coords="4785,1521,4906,1526,4890,1663,4806,1716,4774,1647" shape="poly">
                                    <area target="_blank" alt="127" title="127"  coords="4806,1716,4884,1679,4895,1763,5005,1795,4963,1921,4884,1889" shape="poly">
                                    <area target="_blank" alt="126" title="126"  coords="4969,1921,5005,1795,5116,1831,5074,1979,5027,1968,5037,1937" shape="poly">
                                    <area target="_blank" alt="125" title="125"  coords="4948,1973,5011,2005,5042,1952,5079,1984,5032,2110,4906,2100" shape="poly">
                                    <area target="_blank" alt="124" title="124"  coords="4848,1947,4958,1989,4911,2100,4763,2079" shape="poly">
                                    <area target="_blank" alt="123" title="123"  coords="4685,1900,4800,1847,4848,1937,4753,2089" shape="poly">
                                    <area target="_blank" alt="122" title="122"  coords="4648,1795,4748,1752,4806,1847,4685,1895" shape="poly">
                                    <area target="_blank" alt="121" title="121"  coords="4595,1695,4706,1637,4753,1747,4642,1789" shape="poly">
                                    <area target="_blank" alt="120" title="120"  coords="4553,1584,4653,1537,4706,1637,4595,1684" shape="poly">
                                    <area target="_blank" alt="119" title="119"  coords="4495,1468,4606,1416,4664,1526,4543,1589" shape="poly">
                                    <area target="_blank" alt="118" title="118"  coords="4400,1316,4553,1274,4600,1410,4479,1474" shape="poly">
                                    <area target="_blank" alt="117" title="117"  coords="4316,1184,4479,1131,4543,1263,4364,1326" shape="poly">
                                    <area target="_blank" alt="116" title="116"  coords="4264,1031,4411,989,4474,1121,4322,1179" shape="poly">
                                    <area target="_blank" alt="115" title="115"  coords="4201,774,4327,742,4395,900,4258,937" shape="poly">
                                    <area target="_blank" alt="114" title="114"  coords="4117,616,4269,574,4322,721,4196,768,4169,695,4138,700" shape="poly">
                                    <area target="_blank" alt="113" title="113"  coords="4169,1231,4316,1177,4358,1331,4190,1384,4174,1337,4201,1326" shape="poly">
                                    <area target="_blank" alt="112" title="112"  coords="4122,1074,4269,1026,4322,1184,4169,1237" shape="poly">
                                    <area target="_blank" alt="111" title="111"  coords="4016,837,4153,795,4195,968,4074,1000" shape="poly">
                                    <area target="_blank" alt="110" title="110"  coords="3964,674,4111,621,4148,710,4122,721,4143,789,4016,831" shape="poly">

                                    <area target="_blank" alt="109" title="109"  coords="3811,726,3959,679,4016,826,3880,889,3864,805,3827,805" shape="poly">
                                    <area target="_blank" alt="108" title="108"  coords="3885,879,4011,842,4069,995,3932,1042" shape="poly">
                                    <area target="_blank" alt="107" title="107"  coords="3932,1147,4069,1105,4106,1242,3990,1284" shape="poly">
                                    <area target="_blank" alt="106" title="106"  coords="3990,1289,4106,1252,4137,1316,4169,1321,4190,1379,4022,1426" shape="poly">
                                    <area target="_blank" alt="105" title="105"  coords="3848,1342,3980,1295,4032,1437,3890,1474,3864,1432,3874,1410" shape="poly">
                                    <area target="_blank" alt="104" title="104"  coords="3796,1195,3938,1152,3990,1289,3853,1331" shape="poly">
                                    <area target="_blank" alt="103" title="103"  coords="3701,942,3827,889,3880,1058,3753,1100" shape="poly">
                                    <area target="_blank" alt="102" title="102"  coords="3638,768,3801,726,3838,805,3806,821,3817,889,3701,931" shape="poly">
                                    <area target="_blank" alt="101" title="101"  coords="3475,831,3648,779,3696,937,3554,984,3538,926,3496,921" shape="poly">
                                    <area target="_blank" alt="100" title="100"  coords="3554,995,3701,926,3753,1100,3611,1147" shape="poly">
                                    <area target="_blank" alt="99" title="99"  coords="3611,1258,3759,1210,3796,1352,3659,1400" shape="poly">
                                    <area target="_blank" alt="98" title="98"  coords="3659,1395,3790,1358,3817,1437,3869,1421,3880,1474,3696,1531" shape="poly">
                                    <area target="_blank" alt="97" title="97"  coords="3506,1447,3653,1395,3701,1531,3543,1595,3527,1542,3543,1537" shape="poly">
                                    <area target="_blank" alt="96" title="96"  coords="3469,1300,3596,1258,3664,1389,3511,1452" shape="poly">
                                    <area target="_blank" alt="95" title="95"  coords="3359,1053,3496,1010,3548,1168,3401,1221" shape="poly">
                                    <area target="_blank" alt="94" title="94"  coords="3306,879,3469,842,3511,916,3475,926,3496,1000,3354,1047" shape="poly">
                                    <area target="_blank" alt="93" title="93"  coords="3133,947,3301,889,3354,1053,3206,1100,3191,1031,3159,1031" shape="poly">
                                    <area target="_blank" alt="92" title="92"  coords="3212,1100,3359,1047,3406,1216,3254,1263" shape="poly">
                                    <area target="_blank" alt="91" title="91"  coords="3264,1368,3401,1326,3459,1463,3312,1510" shape="poly">
                                    <area target="_blank" alt="90" title="90"  coords="3312,1505,3448,1469,3475,1547,3527,1532,3543,1584,3364,1637" shape="poly">
                                    <area target="_blank" alt="89" title="89"  coords="3170,1563,3312,1505,3359,1647,3191,1695,3170,1652,3201,1631" shape="poly">
                                    <area target="_blank" alt="88" title="88"  coords="3127,1416,3254,1374,3312,1500,3164,1563" shape="poly">
                                    <area target="_blank" alt="87" title="87"  coords="3017,1163,3154,1110,3206,1279,3064,1331" shape="poly">
                                    <area target="_blank" alt="86" title="86"  coords="2964,1000,3133,947,3164,1026,3133,1053,3154,1116,3017,1158" shape="poly">
                                    <area target="_blank" alt="85" title="85"  coords="2791,1058,2959,1010,3017,1158,2870,1205,2843,1131,2817,1142" shape="poly">
                                    <area target="_blank" alt="84" title="84"  coords="2875,1210,3017,1158,3059,1326,2917,1374" shape="poly">
                                    <area target="_blank" alt="83" title="83"  coords="2917,1484,3070,1431,3106,1568,2980,1621" shape="poly">
                                    <area target="_blank" alt="82" title="82"  coords="2964,1631,3101,1579,3127,1652,3180,1637,3191,1705,3012,1758" shape="poly">
                                    <area target="_blank" alt="81" title="81"  coords="2817,1668,2954,1621,3017,1763,2843,1816,2828,1747,2859,1737" shape="poly">
                                    <area target="_blank" alt="80" title="80"  coords="2785,1526,2906,1479,2964,1616,2822,1668" shape="poly">
                                    <area target="_blank" alt="79" title="79"  coords="2680,1279,2812,1237,2864,1389,2712,1442" shape="poly">
                                    <area target="_blank" alt="78" title="78"  coords="2622,1110,2785,1063,2812,1131,2796,1152,2817,1231,2670,1274" shape="poly">
                                    <area target="_blank" alt="77" title="77"  coords="2433,1179,2622,1116,2664,1268,2507,1326,2491,1268,2454,1263" shape="poly">
                                    <area target="_blank" alt="76" title="76"  coords="2517,1331,2670,1274,2722,1437,2565,1489" shape="poly">
                                    <area target="_blank" alt="75" title="75"  coords="2554,1605,2717,1552,2764,1684,2586,1742" shape="poly">
                                    <area target="_blank" alt="74" title="74"  coords="2591,1742,2764,1695,2791,1752,2828,1758,2843,1810,2638,1884" shape="poly">
                                    <area target="_blank" alt="73" title="73"  coords="2302,1389,2454,1342,2507,1505,2359,1558" shape="poly">
                                    <area target="_blank" alt="72" title="72"  coords="2259,1247,2423,1189,2459,1252,2423,1279,2454,1342,2307,1389" shape="poly">
                                    <area target="_blank" alt="71" title="71"  coords="2070,1300,2249,1247,2302,1389,2144,1447,2117,1379,2091,1384" shape="poly">
                                    <area target="_blank" alt="70" title="70"  coords="2307,1400,2359,1558,2249,1595,2186,1573,2144,1447" shape="poly">
                                    <area target="_blank" alt="69" title="69"  coords="1928,1521,2086,1474,2123,1589,2096,1631,1986,1679" shape="poly">
                                    <area target="_blank" alt="68" title="68"  coords="1886,1342,2060,1305,2096,1384,2060,1400,2081,1463,1933,1516" shape="poly">

                                    <area target="_blank" alt="67" title="67"  coords="2081,2610,2196,2573,2244,2705,2133,2747" shape="poly">
                                    <area target="_blank" alt="66" title="66"  coords="2049,2489,2154,2452,2202,2579,2081,2605" shape="poly">
                                    <area target="_blank" alt="65" title="65"  coords="2007,2379,2123,2331,2159,2452,2038,2489" shape="poly">
                                    <area target="_blank" alt="64" title="64"  coords="1960,2258,2086,2210,2123,2321,2002,2379" shape="poly">
                                    <area target="_blank" alt="63" title="63"  coords="1923,2147,2054,2099,2081,2215,1960,2252" shape="poly">
                                    <area target="_blank" alt="62" title="62"  coords="1886,2021,2002,1968,2049,2105,1917,2142" shape="poly">
                                    <area target="_blank" alt="61" title="61"  coords="1823,1858,1954,1805,2007,1979,1881,2021" shape="poly">
                                    <area target="_blank" alt="60" title="60"  coords="1760,1563,1928,1510,1975,1673,1818,1742" shape="poly">
                                    <area target="_blank" alt="59" title="59"  coords="1686,1416,1875,1358,1923,1505,1754,1563,1728,1489,1702,1500" shape="poly">
                                    <area target="_blank" alt="58" title="58"  coords="1533,1479,1670,1426,1697,1495,1670,1510,1707,1573,1586,1621" shape="poly">
                                    <area target="_blank" alt="57" title="57"  coords="1591,1626,1702,1584,1760,1752,1649,1789" shape="poly">
                                    <area target="_blank" alt="56" title="56"  coords="1702,1895,1823,1847,1881,2016,1765,2063" shape="poly">
                                    <area target="_blank" alt="55" title="55"  coords="1765,2063,1881,2026,1928,2131,1796,2173" shape="poly">
                                    <area target="_blank" alt="54" title="54"  coords="1807,2179,1912,2142,1960,2258,1849,2300" shape="poly">
                                    <area target="_blank" alt="53" title="53"  coords="1839,2294,1954,2263,2002,2368,1875,2410" shape="poly">
                                    <area target="_blank" alt="52" title="52"  coords="1886,2415,1996,2379,2038,2494,1917,2526" shape="poly">
                                    <area target="_blank" alt="51" title="51"  coords="1928,2531,2033,2489,2081,2605,1965,2647" shape="poly">
                                    <area target="_blank" alt="50" title="50"  coords="1960,2652,2075,2615,2133,2742,2007,2779" shape="poly">

                                    <area target="_blank" alt="49" title="49"  coords="1775,2663,1896,2615,1954,2805,1833,2847" shape="poly">
                                    <area target="_blank" alt="48" title="48"  coords="1707,2494,1833,2447,1886,2621,1765,2652" shape="poly">
                                    <area target="_blank" alt="47" title="47"  coords="1654,2321,1770,2279,1833,2442,1712,2489" shape="poly">
                                    <area target="_blank" alt="46" title="46"  coords="1597,2152,1712,2100,1775,2284,1649,2316" shape="poly">
                                    <area target="_blank" alt="45" title="45"  coords="1533,1953,1649,1911,1717,2095,1596,2142" shape="poly">
                                    <area target="_blank" alt="44" title="44"  coords="1465,1668,1576,1631,1639,1789,1518,1837" shape="poly">
                                    <area target="_blank" alt="43" title="43"  coords="1376,1516,1518,1474,1581,1616,1460,1668,1439,1605,1412,1605" shape="poly">
                                    <area target="_blank" alt="42" title="42"  coords="1234,1579,1376,1526,1418,1605,1376,1616,1397,1679,1291,1731" shape="poly">
                                    <area target="_blank" alt="41" title="41"  coords="1276,1731,1397,1679,1460,1847,1344,1895" shape="poly">
                                    <area target="_blank" alt="40" title="40"  coords="1397,2000,1518,1963,1591,2147,1465,2189" shape="poly">
                                    <area target="_blank" alt="39" title="39"  coords="1465,2194,1591,2147,1644,2321,1528,2363" shape="poly">
                                    <area target="_blank" alt="38" title="38"  coords="1533,2358,1644,2326,1707,2489,1586,2531" shape="poly">
                                    <area target="_blank" alt="37" title="37"  coords="1586,2531,1697,2489,1760,2652,1644,2705" shape="poly">
                                    <area target="_blank" alt="36" title="36"  coords="1649,2710,1760,2663,1823,2842,1702,2879" shape="poly">
                                    <area target="_blank" alt="35" title="35"  coords="1470,2810,1597,2768,1665,2963,1528,3005" shape="poly">
                                    <area target="_blank" alt="34" title="34"  coords="1391,2621,1539,2573,1597,2763,1470,2810" shape="poly">
                                    <area target="_blank" alt="33" title="33"  coords="1334,2437,1470,2394,1539,2568,1391,2615" shape="poly">
                                    <area target="_blank" alt="32" title="32"  coords="1276,2258,1418,2210,1476,2394,1328,2437" shape="poly">
                                    <area target="_blank" alt="31" title="31"  coords="1207,2068,1339,2021,1418,2205,1276,2263" shape="poly">
                                    <area target="_blank" alt="30" title="30"  coords="1134,1773,1276,1726,1334,1895,1192,1947" shape="poly">
                                    <area target="_blank" alt="29" title="29"  coords="1065,1637,1223,1579,1286,1731,1128,1768,1107,1689,1092,1700" shape="poly">
                                    <area target="_blank" alt="28" title="28"  coords="886,1700,1072,1630,1104,1693,1061,1719,1086,1789,934,1847" shape="poly">
                                    <area target="_blank" alt="27" title="27"  coords="939,1842,1071,1805,1139,1958,1007,2010" shape="poly">
                                    <area target="_blank" alt="26" title="26"  coords="1060,2110,1197,2063,1270,2263,1128,2305" shape="poly">
                                    <area target="_blank" alt="25" title="25"  coords="1128,2310,1260,2252,1334,2442,1186,2484" shape="poly">
                                    <area target="_blank" alt="24" title="24"  coords="1192,2489,1339,2437,1402,2615,1249,2679" shape="poly">
                                    <area target="_blank" alt="23" title="23"  coords="1249,2673,1391,2626,1460,2805,1318,2858" shape="poly">
                                    <area target="_blank" alt="22" title="22"  coords="1323,2863,1460,2805,1528,3000,1386,3052" shape="poly">
                                    <area target="_blank" alt="21" title="21"  coords="1086,2884,1239,2831,1313,3015,1160,3073" shape="poly">
                                    <area target="_blank" alt="20" title="20"  coords="1034,2710,1186,2663,1244,2826,1092,2894" shape="poly">
                                    <area target="_blank" alt="19" title="19"  coords="981,2542,1128,2494,1186,2663,1028,2705" shape="poly">
                                    <area target="_blank" alt="18" title="18"  coords="923,2373,1076,2326,1134,2489,971,2537" shape="poly">
                                    <area target="_blank" alt="17" title="17"  coords="855,2184,1002,2131,1071,2321,918,2373" shape="poly">
                                    <area target="_blank" alt="16" title="16"  coords="776,1905,934,1842,1002,2005,839,2068" shape="poly">
                                    <area target="_blank" alt="15" title="15"  coords="708,1763,881,1700,934,1842,786,1900,760,1821,734,1842" shape="poly">
                                    <area target="_blank" alt="14" title="14"  coords="523,1831,697,1768,734,1842,692,1847,729,1916,566,1968" shape="poly">
                                    <area target="_blank" alt="13" title="13"  coords="576,1968,723,1910,786,2079,629,2137" shape="poly">
                                    <area target="_blank" alt="12" title="12"  coords="697,2242,855,2184,913,2379,765,2431" shape="poly">
                                    <area target="_blank" alt="11" title="11"  coords="760,2442,918,2384,971,2547,818,2605" shape="poly">
                                    <area target="_blank" alt="10" title="10"  coords="813,2610,971,2552,1028,2710,865,2763" shape="poly">
                                    <area target="_blank" alt="9" title="9"  coords="876,2763,1034,2710,1086,2879,939,2931" shape="poly">
                                    <area target="_blank" alt="8" title="8"  coords="928,2936,1081,2884,1155,3073,997,3126" shape="poly">
                                    <area target="_blank" alt="7" title="7"  coords="581,2668,760,2600,808,2768,667,2810,661,2777,623,2784" shape="poly">
                                    <area target="_blank" alt="6" title="6"  coords="529,2494,702,2442,755,2594,587,2663" shape="poly">
                                    <area target="_blank" alt="5" title="5"  coords="471,2321,644,2258,697,2447,523,2494" shape="poly">
                                    <area target="_blank" alt="4" title="4"  coords="397,2031,571,1973,629,2137,455,2200" shape="poly">
                                    <area target="_blank" id="ap3_3" title="3"  coords="318,1895,513,1826,571,1968,402,2037,375,1952,352,1965" shape="poly">
                                    <area target="_blank" id="ap3_2" title="2"  coords="145,1947,313,1900,345,1968,313,1979,345,2052,181,2100" shape="poly">
                                    <area target="_blank" id="ap3_1" title="1"  coords="187,2100,345,2058,397,2216,197,2289" shape="poly">

                                </map>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 100%">
                    <div id="mapContainer_p4" style="display: none;"  >
                        <div style="width: 100%; height: 657px; overflow: hidden; background-color: white;" class="boder-plan rounded">
                            <div id="mapObject4">
                                <img  id="m_main4" src="<?= base_url('assets/images/plansale.jpg') ?>" usemap="#image-map4" width="930" height="657">
                                <map name="image-map4" >
                                    <area target="_blank" alt="142" title="142"  coords="5011,321,5105,289,5158,395,5063,453" shape="poly">
                                    <area target="_blank" alt="141" title="141"  coords="4895,358,5016,310,5053,453,4953,484" shape="poly">
                                    <area target="_blank" alt="140" title="140"  coords="4800,389,4906,353,4948,479,4848,526" shape="poly">
                                    <area target="_blank" alt="139" title="139"  coords="4695,432,4795,400,4848,516,4742,563" shape="poly">
                                    <area target="_blank" alt="138" title="138"  coords="4595,458,4700,421,4737,558,4679,579,4690,626,4658,647" shape="poly">
                                    <area target="_blank" alt="137" title="137"  coords="4658,653,4753,621,4806,763,4706,800" shape="poly">
                                    <area target="_blank" alt="136" title="136"  coords="4516,668,4642,632,4716,789,4579,842" shape="poly">
                                    <area target="_blank" alt="135" title="135"  coords="4437,516,4590,463,4653,626,4521,663,4495,595,4453,600" shape="poly">
                                    <area target="_blank" alt="134" title="134"  coords="4279,558,4427,510,4458,605,4432,616,4458,684,4332,737" shape="poly">
                                    <area target="_blank" alt="133" title="133"  coords="4337,731,4458,695,4521,858,4395,895" shape="poly">
                                    <area target="_blank" alt="132" title="132"  coords="4653,895,4827,847,4816,905,4806,963,4811,1010,4690,1053" shape="poly">
                                    <area target="_blank" alt="131" title="131"  coords="4706,1058,4806,1016,4821,1068,4879,1158,4748,1195" shape="poly">
                                    <area target="_blank" alt="130" title="130"  coords="4748,1205,4879,1158,4916,1221,4948,1295,4958,1352,4795,1358" shape="poly">
                                    <area target="_blank" alt="129" title="129"  coords="4800,1358,4948,1363,4911,1521,4795,1510" shape="poly">
                                    <area target="_blank" alt="128" title="128"  coords="4785,1521,4906,1526,4890,1663,4806,1716,4774,1647" shape="poly">
                                    <area target="_blank" alt="127" title="127"  coords="4806,1716,4884,1679,4895,1763,5005,1795,4963,1921,4884,1889" shape="poly">
                                    <area target="_blank" alt="126" title="126"  coords="4969,1921,5005,1795,5116,1831,5074,1979,5027,1968,5037,1937" shape="poly">
                                    <area target="_blank" alt="125" title="125"  coords="4948,1973,5011,2005,5042,1952,5079,1984,5032,2110,4906,2100" shape="poly">
                                    <area target="_blank" alt="124" title="124"  coords="4848,1947,4958,1989,4911,2100,4763,2079" shape="poly">
                                    <area target="_blank" alt="123" title="123"  coords="4685,1900,4800,1847,4848,1937,4753,2089" shape="poly">
                                    <area target="_blank" alt="122" title="122"  coords="4648,1795,4748,1752,4806,1847,4685,1895" shape="poly">
                                    <area target="_blank" alt="121" title="121"  coords="4595,1695,4706,1637,4753,1747,4642,1789" shape="poly">
                                    <area target="_blank" alt="120" title="120"  coords="4553,1584,4653,1537,4706,1637,4595,1684" shape="poly">
                                    <area target="_blank" alt="119" title="119"  coords="4495,1468,4606,1416,4664,1526,4543,1589" shape="poly">
                                    <area target="_blank" alt="118" title="118"  coords="4400,1316,4553,1274,4600,1410,4479,1474" shape="poly">
                                    <area target="_blank" alt="117" title="117"  coords="4316,1184,4479,1131,4543,1263,4364,1326" shape="poly">
                                    <area target="_blank" alt="116" title="116"  coords="4264,1031,4411,989,4474,1121,4322,1179" shape="poly">
                                    <area target="_blank" alt="115" title="115"  coords="4201,774,4327,742,4395,900,4258,937" shape="poly">
                                    <area target="_blank" alt="114" title="114"  coords="4117,616,4269,574,4322,721,4196,768,4169,695,4138,700" shape="poly">
                                    <area target="_blank" alt="113" title="113"  coords="4169,1231,4316,1177,4358,1331,4190,1384,4174,1337,4201,1326" shape="poly">
                                    <area target="_blank" alt="112" title="112"  coords="4122,1074,4269,1026,4322,1184,4169,1237" shape="poly">
                                    <area target="_blank" alt="111" title="111"  coords="4016,837,4153,795,4195,968,4074,1000" shape="poly">
                                    <area target="_blank" alt="110" title="110"  coords="3964,674,4111,621,4148,710,4122,721,4143,789,4016,831" shape="poly">

                                    <area target="_blank" alt="109" title="109"  coords="3811,726,3959,679,4016,826,3880,889,3864,805,3827,805" shape="poly">
                                    <area target="_blank" alt="108" title="108"  coords="3885,879,4011,842,4069,995,3932,1042" shape="poly">
                                    <area target="_blank" alt="107" title="107"  coords="3932,1147,4069,1105,4106,1242,3990,1284" shape="poly">
                                    <area target="_blank" alt="106" title="106"  coords="3990,1289,4106,1252,4137,1316,4169,1321,4190,1379,4022,1426" shape="poly">
                                    <area target="_blank" alt="105" title="105"  coords="3848,1342,3980,1295,4032,1437,3890,1474,3864,1432,3874,1410" shape="poly">
                                    <area target="_blank" alt="104" title="104"  coords="3796,1195,3938,1152,3990,1289,3853,1331" shape="poly">
                                    <area target="_blank" alt="103" title="103"  coords="3701,942,3827,889,3880,1058,3753,1100" shape="poly">
                                    <area target="_blank" alt="102" title="102"  coords="3638,768,3801,726,3838,805,3806,821,3817,889,3701,931" shape="poly">
                                    <area target="_blank" alt="101" title="101"  coords="3475,831,3648,779,3696,937,3554,984,3538,926,3496,921" shape="poly">
                                    <area target="_blank" alt="100" title="100"  coords="3554,995,3701,926,3753,1100,3611,1147" shape="poly">
                                    <area target="_blank" alt="99" title="99"  coords="3611,1258,3759,1210,3796,1352,3659,1400" shape="poly">
                                    <area target="_blank" alt="98" title="98"  coords="3659,1395,3790,1358,3817,1437,3869,1421,3880,1474,3696,1531" shape="poly">
                                    <area target="_blank" alt="97" title="97"  coords="3506,1447,3653,1395,3701,1531,3543,1595,3527,1542,3543,1537" shape="poly">
                                    <area target="_blank" alt="96" title="96"  coords="3469,1300,3596,1258,3664,1389,3511,1452" shape="poly">
                                    <area target="_blank" alt="95" title="95"  coords="3359,1053,3496,1010,3548,1168,3401,1221" shape="poly">
                                    <area target="_blank" alt="94" title="94"  coords="3306,879,3469,842,3511,916,3475,926,3496,1000,3354,1047" shape="poly">
                                    <area target="_blank" alt="93" title="93"  coords="3133,947,3301,889,3354,1053,3206,1100,3191,1031,3159,1031" shape="poly">
                                    <area target="_blank" alt="92" title="92"  coords="3212,1100,3359,1047,3406,1216,3254,1263" shape="poly">
                                    <area target="_blank" alt="91" title="91"  coords="3264,1368,3401,1326,3459,1463,3312,1510" shape="poly">
                                    <area target="_blank" alt="90" title="90"  coords="3312,1505,3448,1469,3475,1547,3527,1532,3543,1584,3364,1637" shape="poly">
                                    <area target="_blank" alt="89" title="89"  coords="3170,1563,3312,1505,3359,1647,3191,1695,3170,1652,3201,1631" shape="poly">
                                    <area target="_blank" alt="88" title="88"  coords="3127,1416,3254,1374,3312,1500,3164,1563" shape="poly">
                                    <area target="_blank" alt="87" title="87"  coords="3017,1163,3154,1110,3206,1279,3064,1331" shape="poly">
                                    <area target="_blank" alt="86" title="86"  coords="2964,1000,3133,947,3164,1026,3133,1053,3154,1116,3017,1158" shape="poly">
                                    <area target="_blank" alt="85" title="85"  coords="2791,1058,2959,1010,3017,1158,2870,1205,2843,1131,2817,1142" shape="poly">
                                    <area target="_blank" alt="84" title="84"  coords="2875,1210,3017,1158,3059,1326,2917,1374" shape="poly">
                                    <area target="_blank" alt="83" title="83"  coords="2917,1484,3070,1431,3106,1568,2980,1621" shape="poly">
                                    <area target="_blank" alt="82" title="82"  coords="2964,1631,3101,1579,3127,1652,3180,1637,3191,1705,3012,1758" shape="poly">
                                    <area target="_blank" alt="81" title="81"  coords="2817,1668,2954,1621,3017,1763,2843,1816,2828,1747,2859,1737" shape="poly">
                                    <area target="_blank" alt="80" title="80"  coords="2785,1526,2906,1479,2964,1616,2822,1668" shape="poly">
                                    <area target="_blank" alt="79" title="79"  coords="2680,1279,2812,1237,2864,1389,2712,1442" shape="poly">
                                    <area target="_blank" alt="78" title="78"  coords="2622,1110,2785,1063,2812,1131,2796,1152,2817,1231,2670,1274" shape="poly">
                                    <area target="_blank" alt="77" title="77"  coords="2433,1179,2622,1116,2664,1268,2507,1326,2491,1268,2454,1263" shape="poly">
                                    <area target="_blank" alt="76" title="76"  coords="2517,1331,2670,1274,2722,1437,2565,1489" shape="poly">
                                    <area target="_blank" alt="75" title="75"  coords="2554,1605,2717,1552,2764,1684,2586,1742" shape="poly">
                                    <area target="_blank" alt="74" title="74"  coords="2591,1742,2764,1695,2791,1752,2828,1758,2843,1810,2638,1884" shape="poly">
                                    <area target="_blank" alt="73" title="73"  coords="2302,1389,2454,1342,2507,1505,2359,1558" shape="poly">
                                    <area target="_blank" alt="72" title="72"  coords="2259,1247,2423,1189,2459,1252,2423,1279,2454,1342,2307,1389" shape="poly">
                                    <area target="_blank" alt="71" title="71"  coords="2070,1300,2249,1247,2302,1389,2144,1447,2117,1379,2091,1384" shape="poly">
                                    <area target="_blank" alt="70" title="70"  coords="2307,1400,2359,1558,2249,1595,2186,1573,2144,1447" shape="poly">
                                    <area target="_blank" alt="69" title="69"  coords="1928,1521,2086,1474,2123,1589,2096,1631,1986,1679" shape="poly">
                                    <area target="_blank" alt="68" title="68"  coords="1886,1342,2060,1305,2096,1384,2060,1400,2081,1463,1933,1516" shape="poly">

                                    <area target="_blank" alt="67" title="67"  coords="2081,2610,2196,2573,2244,2705,2133,2747" shape="poly">
                                    <area target="_blank" alt="66" title="66"  coords="2049,2489,2154,2452,2202,2579,2081,2605" shape="poly">
                                    <area target="_blank" alt="65" title="65"  coords="2007,2379,2123,2331,2159,2452,2038,2489" shape="poly">
                                    <area target="_blank" alt="64" title="64"  coords="1960,2258,2086,2210,2123,2321,2002,2379" shape="poly">
                                    <area target="_blank" alt="63" title="63"  coords="1923,2147,2054,2099,2081,2215,1960,2252" shape="poly">
                                    <area target="_blank" alt="62" title="62"  coords="1886,2021,2002,1968,2049,2105,1917,2142" shape="poly">
                                    <area target="_blank" alt="61" title="61"  coords="1823,1858,1954,1805,2007,1979,1881,2021" shape="poly">
                                    <area target="_blank" alt="60" title="60"  coords="1760,1563,1928,1510,1975,1673,1818,1742" shape="poly">
                                    <area target="_blank" alt="59" title="59"  coords="1686,1416,1875,1358,1923,1505,1754,1563,1728,1489,1702,1500" shape="poly">
                                    <area target="_blank" alt="58" title="58"  coords="1533,1479,1670,1426,1697,1495,1670,1510,1707,1573,1586,1621" shape="poly">
                                    <area target="_blank" alt="57" title="57"  coords="1591,1626,1702,1584,1760,1752,1649,1789" shape="poly">
                                    <area target="_blank" alt="56" title="56"  coords="1702,1895,1823,1847,1881,2016,1765,2063" shape="poly">
                                    <area target="_blank" alt="55" title="55"  coords="1765,2063,1881,2026,1928,2131,1796,2173" shape="poly">
                                    <area target="_blank" alt="54" title="54"  coords="1807,2179,1912,2142,1960,2258,1849,2300" shape="poly">
                                    <area target="_blank" alt="53" title="53"  coords="1839,2294,1954,2263,2002,2368,1875,2410" shape="poly">
                                    <area target="_blank" alt="52" title="52"  coords="1886,2415,1996,2379,2038,2494,1917,2526" shape="poly">
                                    <area target="_blank" alt="51" title="51"  coords="1928,2531,2033,2489,2081,2605,1965,2647" shape="poly">
                                    <area target="_blank" alt="50" title="50"  coords="1960,2652,2075,2615,2133,2742,2007,2779" shape="poly">

                                    <area target="_blank" alt="49" title="49"  coords="1775,2663,1896,2615,1954,2805,1833,2847" shape="poly">
                                    <area target="_blank" alt="48" title="48"  coords="1707,2494,1833,2447,1886,2621,1765,2652" shape="poly">
                                    <area target="_blank" alt="47" title="47"  coords="1654,2321,1770,2279,1833,2442,1712,2489" shape="poly">
                                    <area target="_blank" alt="46" title="46"  coords="1597,2152,1712,2100,1775,2284,1649,2316" shape="poly">
                                    <area target="_blank" alt="45" title="45"  coords="1533,1953,1649,1911,1717,2095,1596,2142" shape="poly">
                                    <area target="_blank" alt="44" title="44"  coords="1465,1668,1576,1631,1639,1789,1518,1837" shape="poly">
                                    <area target="_blank" alt="43" title="43"  coords="1376,1516,1518,1474,1581,1616,1460,1668,1439,1605,1412,1605" shape="poly">
                                    <area target="_blank" alt="42" title="42"  coords="1234,1579,1376,1526,1418,1605,1376,1616,1397,1679,1291,1731" shape="poly">
                                    <area target="_blank" alt="41" title="41"  coords="1276,1731,1397,1679,1460,1847,1344,1895" shape="poly">
                                    <area target="_blank" alt="40" title="40"  coords="1397,2000,1518,1963,1591,2147,1465,2189" shape="poly">
                                    <area target="_blank" alt="39" title="39"  coords="1465,2194,1591,2147,1644,2321,1528,2363" shape="poly">
                                    <area target="_blank" alt="38" title="38"  coords="1533,2358,1644,2326,1707,2489,1586,2531" shape="poly">
                                    <area target="_blank" alt="37" title="37"  coords="1586,2531,1697,2489,1760,2652,1644,2705" shape="poly">
                                    <area target="_blank" alt="36" title="36"  coords="1649,2710,1760,2663,1823,2842,1702,2879" shape="poly">
                                    <area target="_blank" alt="35" title="35"  coords="1470,2810,1597,2768,1665,2963,1528,3005" shape="poly">
                                    <area target="_blank" alt="34" title="34"  coords="1391,2621,1539,2573,1597,2763,1470,2810" shape="poly">
                                    <area target="_blank" alt="33" title="33"  coords="1334,2437,1470,2394,1539,2568,1391,2615" shape="poly">
                                    <area target="_blank" alt="32" title="32"  coords="1276,2258,1418,2210,1476,2394,1328,2437" shape="poly">
                                    <area target="_blank" alt="31" title="31"  coords="1207,2068,1339,2021,1418,2205,1276,2263" shape="poly">
                                    <area target="_blank" alt="30" title="30"  coords="1134,1773,1276,1726,1334,1895,1192,1947" shape="poly">
                                    <area target="_blank" alt="29" title="29"  coords="1065,1637,1223,1579,1286,1731,1128,1768,1107,1689,1092,1700" shape="poly">
                                    <area target="_blank" alt="28" title="28"  coords="886,1700,1072,1630,1104,1693,1061,1719,1086,1789,934,1847" shape="poly">
                                    <area target="_blank" alt="27" title="27"  coords="939,1842,1071,1805,1139,1958,1007,2010" shape="poly">
                                    <area target="_blank" alt="26" title="26"  coords="1060,2110,1197,2063,1270,2263,1128,2305" shape="poly">
                                    <area target="_blank" alt="25" title="25"  coords="1128,2310,1260,2252,1334,2442,1186,2484" shape="poly">
                                    <area target="_blank" alt="24" title="24"  coords="1192,2489,1339,2437,1402,2615,1249,2679" shape="poly">
                                    <area target="_blank" alt="23" title="23"  coords="1249,2673,1391,2626,1460,2805,1318,2858" shape="poly">
                                    <area target="_blank" alt="22" title="22"  coords="1323,2863,1460,2805,1528,3000,1386,3052" shape="poly">
                                    <area target="_blank" alt="21" title="21"  coords="1086,2884,1239,2831,1313,3015,1160,3073" shape="poly">
                                    <area target="_blank" alt="20" title="20"  coords="1034,2710,1186,2663,1244,2826,1092,2894" shape="poly">
                                    <area target="_blank" alt="19" title="19"  coords="981,2542,1128,2494,1186,2663,1028,2705" shape="poly">
                                    <area target="_blank" alt="18" title="18"  coords="923,2373,1076,2326,1134,2489,971,2537" shape="poly">
                                    <area target="_blank" alt="17" title="17"  coords="855,2184,1002,2131,1071,2321,918,2373" shape="poly">
                                    <area target="_blank" alt="16" title="16"  coords="776,1905,934,1842,1002,2005,839,2068" shape="poly">
                                    <area target="_blank" alt="15" title="15"  coords="708,1763,881,1700,934,1842,786,1900,760,1821,734,1842" shape="poly">
                                    <area target="_blank" alt="14" title="14"  coords="523,1831,697,1768,734,1842,692,1847,729,1916,566,1968" shape="poly">
                                    <area target="_blank" alt="13" title="13"  coords="576,1968,723,1910,786,2079,629,2137" shape="poly">
                                    <area target="_blank" alt="12" title="12"  coords="697,2242,855,2184,913,2379,765,2431" shape="poly">
                                    <area target="_blank" alt="11" title="11"  coords="760,2442,918,2384,971,2547,818,2605" shape="poly">
                                    <area target="_blank" alt="10" title="10"  coords="813,2610,971,2552,1028,2710,865,2763" shape="poly">
                                    <area target="_blank" alt="9" title="9"  coords="876,2763,1034,2710,1086,2879,939,2931" shape="poly">
                                    <area target="_blank" alt="8" title="8"  coords="928,2936,1081,2884,1155,3073,997,3126" shape="poly">
                                    <area target="_blank" alt="7" title="7"  coords="581,2668,760,2600,808,2768,667,2810,661,2777,623,2784" shape="poly">
                                    <area target="_blank" alt="6" title="6"  coords="529,2494,702,2442,755,2594,587,2663" shape="poly">
                                    <area target="_blank" alt="5" title="5"  coords="471,2321,644,2258,697,2447,523,2494" shape="poly">
                                    <area target="_blank" alt="4" title="4"  coords="397,2031,571,1973,629,2137,455,2200" shape="poly">
                                    <area target="_blank" id="ap4_3" title="3"  coords="318,1895,513,1826,571,1968,402,2037,375,1952,352,1965" shape="poly">
                                    <area target="_blank" id="ap4_2" title="2"  coords="145,1947,313,1900,345,1968,313,1979,345,2052,181,2100" shape="poly">
                                    <area target="_blank" id="ap4_1" title="1"  coords="187,2100,345,2058,397,2216,197,2289" shape="poly">

                                </map>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="width: 100%">
                    <div id="mapContainer_p5" style="display: none;"  >
                        <div style="width: 100%; height: 657px; overflow: hidden; background-color: white;" class="boder-plan rounded">
                            <div id="mapObject5">
                                <img  id="m_main5" src="<?= base_url('assets/images/Plan2Cus.jpg') ?>" usemap="#image-map5" width="930" height="657">
                                <map name="image-map5">
                                    <area target="_blank" alt="68" title="68"  coords="187,2089,260,2347,502,2263,476,2142,518,2110,471,1984" shape="poly">
                                    <area target="_blank" alt="71" title="71"  coords="487,1989,529,2116,576,2116,602,2231,844,2147,776,1921" shape="poly">
                                    <area target="_blank" alt="72" title="72"  coords="776,1905,860,2147,1092,2073,1060,1947,1102,1931,1060,1816" shape="poly">
                                    <area target="_blank" alt="69" title="69"  coords="281,2358,355,2589,518,2526,576,2452,513,2273" shape="poly">
                                    <area target="_blank" alt="70" title="70"  coords="602,2237,665,2426,765,2463,928,2405,844,2147" shape="poly">
                                    <area target="_blank" alt="73" title="73"  coords="865,2152,1102,2068,1181,2326,950,2415" shape="poly">
                                    <area target="_blank" alt="76" title="76"  coords="1202,2042,1439,1958,1523,2210,1276,2300" shape="poly">
                                    <area target="_blank" alt="77" title="77"  coords="1081,1805,1107,1916,1155,1910,1202,2031,1428,1947,1344,1689" shape="poly">
                                    <area target="_blank" alt="78" title="78"  coords="1376,1710,1628,1616,1670,1737,1623,1763,1665,1873,1439,1947" shape="poly">
                                    <area target="_blank" alt="79" title="79"  coords="1455,1958,1665,1895,1749,2142,1539,2210" shape="poly">
                                    <area target="_blank" alt="75" title="75"  coords="1255,2489,1313,2705,1586,2605,1523,2394" shape="poly">
                                    <area target="_blank" alt="74" title="74"  coords="1313,2710,1591,2615,1628,2742,1686,2742,1702,2784,1365,2894" shape="poly">
                                    <area target="_blank" alt="80" title="80"  coords="1623,2363,1833,2279,1902,2505,1697,2573" shape="poly">
                                    <area target="_blank" alt="81" title="81"  coords="1702,2594,1912,2526,1965,2689,1702,2773,1686,2731,1739,2694" shape="poly">
                                    <area target="_blank" alt="82" title="82"  coords="1928,2521,2138,2447,2181,2542,2223,2563,2238,2605,1991,2689" shape="poly">
                                    <area target="_blank" alt="84" title="84"  coords="1854,2273,2060,2221,2128,2431,1923,2494" shape="poly">
                                    <area target="_blank" alt="84" title="84"  coords="1775,1868,1981,1795,2054,2031,1844,2110" shape="poly">
                                    <area target="_blank" alt="85" title="85"  coords="1639,1621,1891,1526,1975,1784,1770,1842,1718,1731,1676,1737" shape="poly">
                                    <area target="_blank" alt="86" title="86"  coords="2165,1431,1895,1532,1991,1779,2202,1705,2165,1595,2212,1568" shape="poly">
                                    <area target="_blank" alt="87" title="87"  coords="1996,1795,2207,1716,2286,1968,2070,2031" shape="poly">
                                    <area target="_blank" alt="88" title="88"  coords="2175,2189,2380,2116,2449,2321,2244,2394" shape="poly">
                                    <area target="_blank" alt="89" title="89"  coords="2233,2421,2454,2342,2517,2515,2254,2594,2233,2547,2275,2521" shape="poly">
                                    <area target="_blank" alt="90" title="90"  coords="2480,2331,2680,2273,2707,2358,2759,2373,2780,2431,2528,2505" shape="poly">
                                    <area target="_blank" alt="91" title="91"  coords="2396,2110,2607,2047,2675,2252,2480,2321" shape="poly">
                                    <area target="_blank" alt="92" title="92"  coords="2317,1679,2528,1616,2601,1858,2391,1921" shape="poly">
                                    <area target="_blank" alt="93" title="93"  coords="2186,1421,2428,1347,2517,1600,2302,1658,2270,1542,2223,1558" shape="poly">
                                    <area target="_blank" alt="94" title="94"  coords="2459,1331,2712,1263,2764,1368,2722,1395,2749,1521,2544,1595" shape="poly">
                                    <area target="_blank" alt="95" title="95"  coords="2544,1610,2759,1542,2828,1784,2622,1847" shape="poly">
                                    <area target="_blank" alt="96" title="96"  coords="2728,2005,2922,1937,2991,2147,2791,2216" shape="poly">
                                    <area target="_blank" alt="97" title="97"  coords="2796,2237,3006,2168,3064,2342,2812,2421,2780,2373,2828,2352" shape="poly">
                                    <area target="_blank" alt="98" title="98"  coords="3006,2158,3227,2089,3269,2205,3322,2200,3333,2258,3075,2342" shape="poly">
                                    <area target="_blank" alt="99" title="99"  coords="2933,1942,3159,1863,3227,2068,3006,2142" shape="poly">
                                    <area target="_blank" alt="100" title="100"  coords="2864,1516,3075,1437,3154,1684,2938,1758" shape="poly">
                                    <area target="_blank" alt="101" title="101"  coords="2733,1263,2764,1374,2822,1374,2854,1484,3064,1410,2985,1174" shape="poly">
                                    <area target="_blank" alt="102" title="102"  coords="3001,1158,3243,1089,3275,1200,3238,1221,3275,1342,3091,1410" shape="poly">
                                    <area target="_blank" alt="103" title="103"  coords="3091,1431,3285,1363,3364,1621,3180,1679" shape="poly">
                                    <area target="_blank" alt="104" title="104"  coords="3259,1826,3464,1758,3532,1973,3322,2047" shape="poly">
                                    <area target="_blank" alt="105" title="105"  coords="3327,2058,3369,2163,3327,2210,3359,2242,3585,2168,3522,1994" shape="poly">
                                    <area target="_blank" alt="106" title="106"  coords="3548,1979,3738,1921,3780,2042,3832,2031,3853,2094,3611,2163" shape="poly">
                                    <area target="_blank" alt="107" title="107"  coords="3475,1752,3643,1695,3732,1910,3538,1973" shape="poly">
                                    <area target="_parent" alt="108" title="108"  coords="3259,1084,3291,1200,3333,1189,3375,1326,3575,1247,3490,1000" shape="poly">
                                    <area target="_blank" alt="109" title="109"  coords="3380,1331,3569,1258,3653,1516,3454,1584" shape="poly">
                                    <area target="_blank" alt="110" title="110"  coords="3511,1005,3748,910,3785,1031,3748,1063,3790,1179,3590,1247" shape="poly">
                                    <area target="_blank" alt="111" title="111"  coords="3590,1263,3785,1200,3859,1447,3664,1505" shape="poly">
                                    <area target="_blank" alt="112" title="112"  coords="3759,1658,3974,1589,4053,1805,3822,1884" shape="poly">
                                    <area target="_blank" alt="113" title="113"  coords="3827,1895,4043,1821,4116,2005,3874,2084,3838,2042,3874,2010" shape="poly">
                                    <area target="_blank" alt="114" title="114"  coords="3759,916,3801,1042,3843,1047,3885,1147,4080,1089,3985,826" shape="poly">
                                    <area target="_blank" alt="115" title="115"  coords="3890,1163,4080,1089,4169,1347,3969,1410" shape="poly">
                                    <area target="_blank" alt="116" title="116"  coords="3990,1568,4216,1510,4322,1726,4059,1810" shape="poly">
                                    <area target="_blank" alt="117" title="117"  coords="4064,1821,4316,1742,4422,1947,4132,2042" shape="poly">
                                    <area target="_blank" alt="118" title="118"  coords="4195,2037,4416,1958,4516,2168,4337,2252" shape="poly">
                                    <area target="_blank" alt="119" title="119"  coords="4348,2263,4511,2179,4611,2358,4427,2437" shape="poly">
                                    <area target="_blank" alt="120" title="120"  coords="4448,2437,4611,2373,4690,2537,4511,2600" shape="poly">
                                    <area target="_blank" alt="121" title="121"  coords="4521,2610,4690,2542,4753,2705,4585,2773" shape="poly">
                                    <area target="_blank" alt="122" title="122"  coords="4595,2784,4748,2715,4827,2868,4658,2942" shape="poly">
                                    <area target="_blank" alt="123" title="123"  coords="4664,2947,4821,2879,4900,3026,4774,3226" shape="poly">
                                    <area target="_blank" alt="124" title="124"  coords="4906,3031,5069,3079,5011,3263,4800,3215" shape="poly">
                                    <area target="_blank" alt="125" title="125"  coords="5079,3089,5169,3105,5190,3058,5247,3094,5195,3289,5021,3263" shape="poly">
                                    <area target="_blank" alt="126" title="126"  coords="5100,2989,5200,3015,5205,3052,5268,3058,5337,2842,5163,2784" shape="poly">
                                    <area target="_blank" alt="127" title="127"  coords="4837,2663,4974,2610,4979,2726,5153,2794,5090,2994,4969,2963" shape="poly">
                                    <area target="_blank" alt="128" title="128"  coords="4816,2342,5005,2363,4974,2594,4842,2663,4790,2563" shape="poly">
                                    <area target="_blank" alt="129" title="129"  coords="4842,2084,5063,2089,5000,2363,4811,2342" shape="poly">
                                    <area target="_blank" alt="130" title="130"  coords="4748,1847,4963,1779,5069,1973,5079,2084,4837,2079" shape="poly">
                                    <area target="_blank" alt="131" title="131"  coords="4674,1605,4848,1552,4863,1605,4953,1773,4753,1847" shape="poly">
                                    <area target="_blank" alt="132" title="132"  coords="4595,1374,4879,1274,4848,1447,4842,1542,4669,1605" shape="poly">
                                    <area target="_blank" alt="133" title="133"  coords="4016,821,4253,742,4290,879,4253,905,4290,1016,4090,1079" shape="poly">
                                    <area target="_blank" alt="134" title="134"  coords="4090,1089,4290,1037,4364,1284,4174,1342" shape="poly">
                                    <area target="_blank" alt="135" title="135"  coords="4258,742,4490,668,4579,921,4385,979,4343,863,4295,868" shape="poly">
                                    <area target="_blank" alt="136" title="136"  coords="4395,1005,4590,937,4679,1174,4469,1258" shape="poly">
                                    <area target="_blank" alt="137" title="137"  coords="4606,963,4748,916,4827,1137,4695,1184" shape="poly">
                                    <area target="_blank" alt="138" title="138"  coords="4521,663,4606,937,4653,926,4621,858,4732,810,4653,605" shape="poly">
                                    <area target="_blank" alt="139" title="139"  coords="4679,595,4832,553,4890,758,4742,816" shape="poly">
                                    <area target="_blank" alt="140" title="140"  coords="4842,553,4990,505,5069,700,4906,758" shape="poly">
                                    <area target="_blank" alt="141" title="141"  coords="5000,495,5163,442,5242,647,5079,689" shape="poly">
                                    <area target="_blank" alt="142" title="142"  coords="5169,442,5332,379,5395,568,5247,637" shape="poly">
                                </map>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="width: 100%">
                    <div id="mapContainer_p6" style="display: none;"  >
                        <div style="width: 100%; height: 657px; overflow: hidden; background-color: white;" class="boder-plan rounded">
                            <div id="mapObject6">
                                <img  id="m_main6" src="<?= base_url('assets/images/Plan2Sale.jpg') ?>" usemap="#image-map6" width="930" height="657">
                                <map name="image-map6">
                                    <area target="_blank" alt="68" title="68"  coords="187,2089,260,2347,502,2263,476,2142,518,2110,471,1984" shape="poly">
                                    <area target="_blank" alt="71" title="71"  coords="487,1989,529,2116,576,2116,602,2231,844,2147,776,1921" shape="poly">
                                    <area target="_blank" alt="72" title="72"  coords="776,1905,860,2147,1092,2073,1060,1947,1102,1931,1060,1816" shape="poly">
                                    <area target="_blank" alt="69" title="69"  coords="281,2358,355,2589,518,2526,576,2452,513,2273" shape="poly">
                                    <area target="_blank" alt="70" title="70"  coords="602,2237,665,2426,765,2463,928,2405,844,2147" shape="poly">
                                    <area target="_blank" alt="73" title="73"  coords="865,2152,1102,2068,1181,2326,950,2415" shape="poly">
                                    <area target="_blank" alt="76" title="76"  coords="1202,2042,1439,1958,1523,2210,1276,2300" shape="poly">
                                    <area target="_blank" alt="77" title="77"  coords="1081,1805,1107,1916,1155,1910,1202,2031,1428,1947,1344,1689" shape="poly">
                                    <area target="_blank" alt="78" title="78"  coords="1376,1710,1628,1616,1670,1737,1623,1763,1665,1873,1439,1947" shape="poly">
                                    <area target="_blank" alt="79" title="79"  coords="1455,1958,1665,1895,1749,2142,1539,2210" shape="poly">
                                    <area target="_blank" alt="75" title="75"  coords="1255,2489,1313,2705,1586,2605,1523,2394" shape="poly">
                                    <area target="_blank" alt="74" title="74"  coords="1313,2710,1591,2615,1628,2742,1686,2742,1702,2784,1365,2894" shape="poly">
                                    <area target="_blank" alt="80" title="80"  coords="1623,2363,1833,2279,1902,2505,1697,2573" shape="poly">
                                    <area target="_blank" alt="81" title="81"  coords="1702,2594,1912,2526,1965,2689,1702,2773,1686,2731,1739,2694" shape="poly">
                                    <area target="_blank" alt="82" title="82"  coords="1928,2521,2138,2447,2181,2542,2223,2563,2238,2605,1991,2689" shape="poly">
                                    <area target="_blank" alt="84" title="84"  coords="1854,2273,2060,2221,2128,2431,1923,2494" shape="poly">
                                    <area target="_blank" alt="84" title="84"  coords="1775,1868,1981,1795,2054,2031,1844,2110" shape="poly">
                                    <area target="_blank" alt="85" title="85"  coords="1639,1621,1891,1526,1975,1784,1770,1842,1718,1731,1676,1737" shape="poly">
                                    <area target="_blank" alt="86" title="86"  coords="2165,1431,1895,1532,1991,1779,2202,1705,2165,1595,2212,1568" shape="poly">
                                    <area target="_blank" alt="87" title="87"  coords="1996,1795,2207,1716,2286,1968,2070,2031" shape="poly">
                                    <area target="_blank" alt="88" title="88"  coords="2175,2189,2380,2116,2449,2321,2244,2394" shape="poly">
                                    <area target="_blank" alt="89" title="89"  coords="2233,2421,2454,2342,2517,2515,2254,2594,2233,2547,2275,2521" shape="poly">
                                    <area target="_blank" alt="90" title="90"  coords="2480,2331,2680,2273,2707,2358,2759,2373,2780,2431,2528,2505" shape="poly">
                                    <area target="_blank" alt="91" title="91"  coords="2396,2110,2607,2047,2675,2252,2480,2321" shape="poly">
                                    <area target="_blank" alt="92" title="92"  coords="2317,1679,2528,1616,2601,1858,2391,1921" shape="poly">
                                    <area target="_blank" alt="93" title="93"  coords="2186,1421,2428,1347,2517,1600,2302,1658,2270,1542,2223,1558" shape="poly">
                                    <area target="_blank" alt="94" title="94"  coords="2459,1331,2712,1263,2764,1368,2722,1395,2749,1521,2544,1595" shape="poly">
                                    <area target="_blank" alt="95" title="95"  coords="2544,1610,2759,1542,2828,1784,2622,1847" shape="poly">
                                    <area target="_blank" alt="96" title="96"  coords="2728,2005,2922,1937,2991,2147,2791,2216" shape="poly">
                                    <area target="_blank" alt="97" title="97"  coords="2796,2237,3006,2168,3064,2342,2812,2421,2780,2373,2828,2352" shape="poly">
                                    <area target="_blank" alt="98" title="98"  coords="3006,2158,3227,2089,3269,2205,3322,2200,3333,2258,3075,2342" shape="poly">
                                    <area target="_blank" alt="99" title="99"  coords="2933,1942,3159,1863,3227,2068,3006,2142" shape="poly">
                                    <area target="_blank" alt="100" title="100"  coords="2864,1516,3075,1437,3154,1684,2938,1758" shape="poly">
                                    <area target="_blank" alt="101" title="101"  coords="2733,1263,2764,1374,2822,1374,2854,1484,3064,1410,2985,1174" shape="poly">
                                    <area target="_blank" alt="102" title="102"  coords="3001,1158,3243,1089,3275,1200,3238,1221,3275,1342,3091,1410" shape="poly">
                                    <area target="_blank" alt="103" title="103"  coords="3091,1431,3285,1363,3364,1621,3180,1679" shape="poly">
                                    <area target="_blank" alt="104" title="104"  coords="3259,1826,3464,1758,3532,1973,3322,2047" shape="poly">
                                    <area target="_blank" alt="105" title="105"  coords="3327,2058,3369,2163,3327,2210,3359,2242,3585,2168,3522,1994" shape="poly">
                                    <area target="_blank" alt="106" title="106"  coords="3548,1979,3738,1921,3780,2042,3832,2031,3853,2094,3611,2163" shape="poly">
                                    <area target="_blank" alt="107" title="107"  coords="3475,1752,3643,1695,3732,1910,3538,1973" shape="poly">
                                    <area target="_parent" alt="108" title="108"  coords="3259,1084,3291,1200,3333,1189,3375,1326,3575,1247,3490,1000" shape="poly">
                                    <area target="_blank" alt="109" title="109"  coords="3380,1331,3569,1258,3653,1516,3454,1584" shape="poly">
                                    <area target="_blank" alt="110" title="110"  coords="3511,1005,3748,910,3785,1031,3748,1063,3790,1179,3590,1247" shape="poly">
                                    <area target="_blank" alt="111" title="111"  coords="3590,1263,3785,1200,3859,1447,3664,1505" shape="poly">
                                    <area target="_blank" alt="112" title="112"  coords="3759,1658,3974,1589,4053,1805,3822,1884" shape="poly">
                                    <area target="_blank" alt="113" title="113"  coords="3827,1895,4043,1821,4116,2005,3874,2084,3838,2042,3874,2010" shape="poly">
                                    <area target="_blank" alt="114" title="114"  coords="3759,916,3801,1042,3843,1047,3885,1147,4080,1089,3985,826" shape="poly">
                                    <area target="_blank" alt="115" title="115"  coords="3890,1163,4080,1089,4169,1347,3969,1410" shape="poly">
                                    <area target="_blank" alt="116" title="116"  coords="3990,1568,4216,1510,4322,1726,4059,1810" shape="poly">
                                    <area target="_blank" alt="117" title="117"  coords="4064,1821,4316,1742,4422,1947,4132,2042" shape="poly">
                                    <area target="_blank" alt="118" title="118"  coords="4195,2037,4416,1958,4516,2168,4337,2252" shape="poly">
                                    <area target="_blank" alt="119" title="119"  coords="4348,2263,4511,2179,4611,2358,4427,2437" shape="poly">
                                    <area target="_blank" alt="120" title="120"  coords="4448,2437,4611,2373,4690,2537,4511,2600" shape="poly">
                                    <area target="_blank" alt="121" title="121"  coords="4521,2610,4690,2542,4753,2705,4585,2773" shape="poly">
                                    <area target="_blank" alt="122" title="122"  coords="4595,2784,4748,2715,4827,2868,4658,2942" shape="poly">
                                    <area target="_blank" alt="123" title="123"  coords="4664,2947,4821,2879,4900,3026,4774,3226" shape="poly">
                                    <area target="_blank" alt="124" title="124"  coords="4906,3031,5069,3079,5011,3263,4800,3215" shape="poly">
                                    <area target="_blank" alt="125" title="125"  coords="5079,3089,5169,3105,5190,3058,5247,3094,5195,3289,5021,3263" shape="poly">
                                    <area target="_blank" alt="126" title="126"  coords="5100,2989,5200,3015,5205,3052,5268,3058,5337,2842,5163,2784" shape="poly">
                                    <area target="_blank" alt="127" title="127"  coords="4837,2663,4974,2610,4979,2726,5153,2794,5090,2994,4969,2963" shape="poly">
                                    <area target="_blank" alt="128" title="128"  coords="4816,2342,5005,2363,4974,2594,4842,2663,4790,2563" shape="poly">
                                    <area target="_blank" alt="129" title="129"  coords="4842,2084,5063,2089,5000,2363,4811,2342" shape="poly">
                                    <area target="_blank" alt="130" title="130"  coords="4748,1847,4963,1779,5069,1973,5079,2084,4837,2079" shape="poly">
                                    <area target="_blank" alt="131" title="131"  coords="4674,1605,4848,1552,4863,1605,4953,1773,4753,1847" shape="poly">
                                    <area target="_blank" alt="132" title="132"  coords="4595,1374,4879,1274,4848,1447,4842,1542,4669,1605" shape="poly">
                                    <area target="_blank" alt="133" title="133"  coords="4016,821,4253,742,4290,879,4253,905,4290,1016,4090,1079" shape="poly">
                                    <area target="_blank" alt="134" title="134"  coords="4090,1089,4290,1037,4364,1284,4174,1342" shape="poly">
                                    <area target="_blank" alt="135" title="135"  coords="4258,742,4490,668,4579,921,4385,979,4343,863,4295,868" shape="poly">
                                    <area target="_blank" alt="136" title="136"  coords="4395,1005,4590,937,4679,1174,4469,1258" shape="poly">
                                    <area target="_blank" alt="137" title="137"  coords="4606,963,4748,916,4827,1137,4695,1184" shape="poly">
                                    <area target="_blank" alt="138" title="138"  coords="4521,663,4606,937,4653,926,4621,858,4732,810,4653,605" shape="poly">
                                    <area target="_blank" alt="139" title="139"  coords="4679,595,4832,553,4890,758,4742,816" shape="poly">
                                    <area target="_blank" alt="140" title="140"  coords="4842,553,4990,505,5069,700,4906,758" shape="poly">
                                    <area target="_blank" alt="141" title="141"  coords="5000,495,5163,442,5242,647,5079,689" shape="poly">
                                    <area target="_blank" alt="142" title="142"  coords="5169,442,5332,379,5395,568,5247,637" shape="poly">
                                </map>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/plugins/aos/js/aos.js'); ?>"></script>
<script>
    AOS.init();
</script>


<!--Modal-->
<!--<div id="myModalHome" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 1000px;">
        <div class="modal-content">
            <div class="modal-content p-3 " >
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel"><i class="icon-star"></i> เลือกแบบบบ้าน </a></h5>
                    <button  onclick="hideHomeSelect()" type="button" class="close">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-1">
                            <label>แปลง</label>
                        </div>
                        <div class="col-3">
                            <label>12, 13</label>
                        </div>
                        <div class="col-3">
                            <label>เนื้อที่: กว้าง 16 * 32</label>
                        </div>
                        <div class="col-3 text-right">
                            <label>ราคาที่ดิน 3,000,000</label>
                        </div>
                    </div> 
                    <div class="row pt-1">
                        <div class="col-2">
                            <label>แบบบ้าน</label>
                        </div>
                        <div class="col-2">
                            <div class="custom-control custom-radio form-check">
                                <input type="radio" id="standard" name="exampleRadios" value="1" class="custom-control-input" checked>
                                <label class="custom-control-label check-radio" for="standard">Standard</label>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="custom-control custom-radio form-check">
                                <input type="radio" id="modem" name="exampleRadios" value="1" class="custom-control-input" >
                                <label class="custom-control-label check-radio" for="modem">Modem</label>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="custom-control custom-radio form-check">
                                <input type="radio" id="vintage" name="exampleRadios" value="1" class="custom-control-input" >
                                <label class="custom-control-label check-radio" for="vintage">Vintage</label>
                            </div>
                        </div>
                        <div class="col-1">
                            <label>ราคา</label>
                        </div>
                        <div class="col-3">
                            <div class="form-group pl-3" style="width: 150px;">
                                <select id="inputPrice" class="form-control border-success">
                                    <option id="v1" selected value="1">1,000,000</option>
                                    <option id="v2" value="2" >2,000,000</option>
                                    <option id="v3" value="3">3,000,000</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row pt-1">
                        <div class="col-12 text-center">
                            <div class="text-center" id="img_homestatndard" style="border: 1px solid green; border-radius: 12px; width: 800px; height: 320px; margin-left: 50px;">
                                <img src="<?//= base_url('assets/images/homeselect1.jpg') ?>" width="700" height="300" style="padding-top: 15px;"/>
                            </div>

                        </div>
                    </div>

                    <div class="row pt-1">
                        <div class="col-1 pt-4">
                            <label>หมายเหตุ</label>
                        </div>
                        <div class="col-10 text-left">
                            <div class="form-group">
                                <label for="exampleTextarea"></label>
                                <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row pt-1">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-success">ยืนยันการจอง</button>   
                        </div>
                    </div>

                    <div class="row pt-1">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-warning">แก้ไขการจอง</button> 
                            <button type="button" class="btn btn-danger">ยกเลิกการจอง</button> 
                            <button type="button" class="btn btn-primary">ทำสัญญาการจอง</button> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->
</div>





<!--                <map name="image-map" >
                   <area state="CA" target="" alt="test" title="test" coords="411,319,18" shape="circle"> 
                   <area state="CA2" target="" alt="test2" title="test2"  coords="374,328,16" shape="circle">
                   <area state="CA3" target="" alt="test3" title="test3"  coords="400,282,16" shape="circle">
               </map>-->   

<!--    <div id="image-div">
        <map name="image-map" id="map">
            <area state="CA" id="xx" target="" alt="test" title="test" coords="411,319,18" shape="circle" style="border:70;background-color:#000000"> 
            <area state="CA2" target="" alt="test2" title="test2"  coords="374,328,16" shape="circle">
            <area state="CA3" target="" alt="test3" title="test3"  coords="400,282,16" shape="circle">
        </map>
        <img style="max-width: 100%; max-height: 100%;" src="<?//= base_url('assets/images/plan.jpg') ?>" usemap="#image-map" >
        
    </div>-->
<!--                <img style="max-width: 100%; max-height: 100%;" src="<//?= base_url('assets/images/plan.jpg') ?>" usemap="#image-map" >
            <map name="image-map" id="map">
                <area state="CA" id="xx" target="" alt="test" title="test" coords="411,319,18" shape="circle" style="border:70;background-color:#000000"> 
                <area state="CA2" target="" alt="test2" title="test2"  coords="374,328,16" shape="circle">
                <area state="CA3" target="" alt="test3" title="test3"  coords="400,282,16" shape="circle">
            </map>-->

<!--                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; min-width: 320px;">
                    <tr>
                        <td style="padding: 10px">
                            <div align="center">-->
<!--                            </div>
                        </td>
                    </tr>
                </table>-->
<!--<div class="container">-->
<!--    <div class="row">
        <div class="col-md-3">
            <div class="form-group pl-3">
                <select id="inputState" class="form-control border-success">
                    <option selected>ผังที่ 1</option>
                    <option>ผังที่ 2</option>
                    <option>ผังที่ 3</option>
                </select>
            </div>
        </div>    
        <div class="col-md-2 p-2 pl-4">    
            <div class="custom-control custom-radio form-check">
                <input type="radio" id="cusRadios" name="exampleRadios" value="1" class="custom-control-input" checked>
                <label class="custom-control-label check-radio" for="cusRadios">รูปผังสำหรับลูกค้า</label>
            </div>

        </div>
        <div class="col-md-6 text-left p-2 pl-4">
            <div class="custom-control custom-radio form-check">
                <input type="radio" id="saleplan" name="exampleRadios" value="2" class="custom-control-input" >
                <label class="custom-control-label check-radio" for="saleplan">รูปผังจริง</label>
            </div>

        </div>
    </div>
<div class="row">
    <div class="col-md-12 w-100" id="image-map-wrapper">
        <div id="image-map-container">
            <div class="image-mapper">
                <img style="max-width: 100%; max-height: 100%;" src="<?//= base_url('assets/images/plan.jpg') ?>" usemap="#image-map" >
                <map name="image-map">
                    <area target="" alt="test" title="test"  coords="467,369,38" shape="circle" style="background-color: red;">
                </map>
            </div>
        </div>
    </div>
</div>
</div>-->

