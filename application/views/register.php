<style type='text/css'>
    .boder-plan{
        border-style: solid; 
        color: white;
        border-width: 8px;
    }
</style>
<div class="container-fluid" style="min-height: 1400px;">
    <div class="pt-3"></div>
    <div class="card text-dark" style="background-color: #2ca535;">
        <div class="card-header">
            <label class="font-weight-bold text-white">ข้อมูลผู้ใช้งานระบบ</label>
        </div>
        <div class="card-body" style="background-color: #5ebb6d;">
            <div class="row">
                <div class="col-12 text-right ">
                    <div class="m-auto">
                        <button type = "button"  onclick="showOptionModalAdd()" class="btn btn-success fa fa-user-plus  waves-effect p-1">เพิ่มผู้ใช้งาน</button>
                    </div>
                </div>
            </div>
            <div class="pt-2"></div>


            <table id="userlist" class="display responsive nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead class="thead-light bg-success">
                    <tr>
                        <th scope="col" width="8%">ลำดับที่</th>
                        <th scope="col">เลขประจำตัวประชาชน</th>
                        <th scope="col">ชื่อ-นามสกุล</th>
                        <th scope="col">เบอร์โทรศัพท์</th>
                        <th scope="col">วันที่เพิ่ม</th>
                        <th scope="col">วันทีแก้ไข</th>
                        <th scope="col">ประเภทผู้ใช้งาน</th>
                        <th scope="col" width="8%">แก้ไข</th>
                        <th scope="col" width="8%">ยกเลิก</th>
                    </tr>
                </thead>
            </table>


        </div>
    </div>


    <!--</div>-->


    <div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="max-width: 700px;">
            <div class="modal-content">
                <div class="modal-content p-3 " >
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel"><i class="icon-star"></i> เพิ่มผู้ใช้งานระบบ </a></h5>
                        <button onclick="closeOptionModalAdd()" type="button" class="close">×</button>
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url('register/create_user') ?>" method="post" id="identicalForm">

                            <div class="row">
                                <div class="col-6" >

                                    <div class="input2-group">
                                        <label class="label">ผู้ใช้งานระบบ&nbsp;&nbsp; </label>
                                        <div class="p-t-10">
                                            <label class="radio-container m-r-45">จัดการระบบ
                                                <input type="radio" checked="checked" name="usertype" id="radio1" value="2">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="radio-container">ทั่วไป
                                                <input type="radio" name="usertype" value="3" id="radio2">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>


                            <div class="row">
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="หมายเลขประจำตัวประชาชน" name="pid" id="pid" oninput="numberOnly(this.id);" required/>
                                        <div class="input-icon"><i class="fa fa-drivers-license"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="username" name="user_name" id="user_name" required/>
                                        <div class="input-icon"><i class="fa fa-drivers-license"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-6">    
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="ชื่อ" name="first_name" id="first_name" required/>
                                        <div class="input-icon"><i class="fa fa-user"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>    
                                <div class="col-6">    
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="นามสกุล" name="last_name" id="last_name" required/>
                                        <div class="input-icon"><i class="fa fa-user"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="email" placeholder="Email" name="email" id="email"/>
                                        <div class="input-icon"><i class="fa fa-envelope" ></i></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="เบอร์โทรศัพท์" name="phone" id="phone" required/>
                                        <div class="input-icon"><i class="fa fa-phone"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="password" placeholder="Password" name="password" id="password"  required/>
                                        <div class="input-icon"><i class="fa fa-key"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="password" placeholder="Confirm Password" name="password_confirm" id="password_confirm" data-minlength="8" data-match="#password" data-match-error="password don't match" required/>
                                        <div class="input-icon"><i class="fa fa-key"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-12 text-center">
                                    <div class="m-auto">
                                        <button id="btn_insert_user" type = "button" class="btn btn-success fa fa-user-plus p-1 ">บันทึกข้อมูล</button>
                                        <button onclick="closeOptionModalAdd()" type="button" class="btn btn-success fa fa-close p-1">ยกเลิก</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModalUpdate" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="max-width: 700px;">
            <div class="modal-content w-100">
                <div class="modal-content p-3 " >
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel"><i class="icon-star"></i> แก้ไขผู้ใช้งาน </a></h5>
                        <button onclick="closeOptionModalEdit()" type="button" class="close">×</button>
                    </div>
                    <div class="modal-body">

                        <form action="<?= base_url('register/update_user') ?>" method="post" id="identicalForm2">
                            <div id="hiddenuserid"></div>
                            
                            <div class="row" id="group_type">
                                <div class="col-6" >

                                    <div class="input2-group">
                                        <label class="label">ผู้ใช้งานระบบ&nbsp;&nbsp; </label>
                                        <div class="p-t-10">
                                            <label class="radio-container m-r-45">จัดการระบบ
                                                <input type="radio" name="usertype2" id="radio3" value="2">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="radio-container">ทั่วไป
                                                <input type="radio" name="usertype2" value="3" id="radio4">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                            
                            <div class="row">
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="หมายเลขประจำตัวประชาชน" name="pid" id="pid2" oninput="numberOnly(this.id);" required/>
                                        <div class="input-icon"><i class="fa fa-drivers-license"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="username" name="user_name" id="user_name2" required/>
                                        <div class="input-icon"><i class="fa fa-drivers-license"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-6">    
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="ชื่อ" name="first_name" id="first_name2" required/>
                                        <div class="input-icon"><i class="fa fa-user"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>    
                                <div class="col-6">    
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="นามสกุล" name="last_name" id="last_name2" required/>
                                        <div class="input-icon"><i class="fa fa-user"><font class="text-danger">*</font>  </i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="email" placeholder="Email" name="email" id="email2"/>
                                        <div class="input-icon"><i class="fa fa-envelope" ></i></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="text" placeholder="เบอร์โทรศัพท์" name="phone" id="phone2" required/>
                                        <div class="input-icon"><i class="fa fa-phone"><font class="text-danger">*</font></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="password" placeholder="Password" name="password" id="password2" />
                                        <div class="input-icon"><i class="fa fa-key"></i></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group input-group-icon">
                                        <input type="password" placeholder="Confirm Password" name="password_confirm" data-match="#password2" data-match-error="รหัสไม่ตรงกัน" id="password_confirm2"/>
                                        <div class="input-icon"><i class="fa fa-key"></i></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 text-center">
                                    <div class="m-auto">
                                        <button id="btn_update_user" type = "button" class="btn btn-success fa fa-edit p-1">แก้ไขข้อมูล</button>
                                        <button onclick="closeOptionModalEdit()" type="button" class="btn btn-success fa fa-close p-1">ยกเลิก</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--modal-->
    <div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
        <div class="modal-dialog modal-sm" style="max-width: 350px;">
            <div class="modal-content">
                <div style="border-bottom: 1px solid #e9ecef;">
                    <div class="text-center">
                        <div class="row p-2">
                            <div class="col-11 text-center pt-2"><pre class="text-center " id="lablepopup"></pre></div>
                            <div class="col-1 text-left pt-1"><button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button></div>
                        </div>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <input id="hidden_user" type="hidden"/>
                        <button type="button" class="btn btn-primary" id="modal-btn-y-cancel" style="width: 30%;">Yes</button>
                        <button type="button" class="btn btn-danger" id="modal-btn-n-cancel" style="width: 30%;">No</button>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--modal information-->
    <div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message">
        <div class="modal-dialog modal-sm" style="max-width: 400px;">
            <div class="modal-content">
                <div style="border-bottom: 1px solid #e9ecef;">
                    <div class="text-center">
                        <dd id="idmessage" class="pt-3"></dd>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <button type="button" class="btn btn-warning" id="modal-btn-n-message" style="width: 30%;" >ตกลง</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
