<link href="<?php echo base_url('assets/plugins/aos/css/aos.css'); ?>" rel="stylesheet">
<style type='text/css'>
    .boder-image{
        border-style: solid; 
        color: #eff3ef;
        border-width: 10px;
        border-radius: 25px;
        width: 82%;

    }


</style>
<div class="container">
    <div class="pt-3"></div>
    <form id="formselect" method="post" action='<?= base_url('promise/promiseselet?menu=projectplan') ?>'>
        <input type="hidden" id="hidden_user" name="hidden_user"  value="<?= $userrole ?>"/>
        <!--send to select person-->
        <input type="hidden" id="person_plan_save" name="person_plan_save"  value="<?= $data_plan_save ?>"/>

        <div class="card text-white" style="background-color: #2ca535; box-shadow: 5px 2px 5px 2px rgba(255, 255, 255, 255);">
            <div class="card-header">
                <label class="font-weight-bold">รายละเอียดแปลงที่ดิน</label> 
            </div>

            <div class="card-body" style="background-color: #5ebb6d;">
                <div class="row md-12">
                    <div class="col-12 text-right " style="padding-right: 40px;">
                        <label class="font-weight-bold ">วันที่จอง</label> 
                        <span class="font-weight-bold " id="date_order"></span>
                        <input type="hidden" id="hidden_date_order" name="hidden_date_order">    
                    </div>
                    
                </div>
                
                <div class="row md-12 text-right">
                    <div class="col-2 ">
                        <label class="font-weight-bold text-right">หมายเลขแปลง: </label> 
                    </div>
                    <div class="col-10 text-left">
                        <label class="font-weight-bold text-left"><?= $plans ?></label> 
                        <input type="hidden" id="hidden_plans" value="<?= $plans ?>" />
                    </div>
                </div>
                <div class="row md-12 text-right">
                    
                    <div class="col-2 text-right">
                        <label class="font-weight-bold text-left">จำนวน:</label>
                    </div>
                    
                    <div class="col-3 text-left">
                        <label class="font-weight-bold text-left"> <span id="total_plang"></span> แปลง</label>
                    </div>
                    
                    <div class="col-2 ">
                        <label class="font-weight-bold text-right">เนื้อที่:</label>
                    </div>
                    <div class="col-3 text-left">
                        <label class="font-weight-bold text-left"><?= $ris ?>  ไร่ &nbsp;&nbsp;  <?= $ngan ?>  งาน  &nbsp;&nbsp; <?= $wa ?> วา </label> 
                        <input type="hidden" name="ri" id="ri" value="<?= $ris ?>">
                        <input type="hidden" name="ngan" id="ngan" value="<?= $ngan ?>">
                        <input type="hidden" name="wa" id="wa" value="<?= $wa ?>">
                        <input type="hidden" id="hidden_price_tarangwa" name="hidden_price_tarangwa"  value="<?= $input_form_price_tarangwa ?>"/>
                    </div>
                </div>
                
                <div class="row md-12 text-right">
                    <div class="col-2 text-right">
                        <label class="font-weight-bold text-left">กว้าง:</label>
                    </div>
                    
                    <div class="col-3 text-left">
                        <label class="font-weight-bold text-left"><?= $p_width ?>(เมตร)</label>
                    </div>
                    
                    <div class="col-2 text-right">
                        <label class="font-weight-bold text-left">ยาว:</label>
                    </div>
                    
                    <div class="col-5 text-left">
                        <label class="font-weight-bold text-left"><?= $p_height ?> (เมตร)</label>
                    </div>
                    
                </div>
                <div class="row md-12 text-right">
                    <div class="col-2 ">
                        <label class="font-weight-bold text-right">ราคาแปลง:</label>
                    </div>
                    <div class="col-10 text-left">
                        <label class="font-weight-bold text-left"><?= $prices ?> (บาท)</label> 
                    </div>

                    <input type="hidden" id="input_hidden_plan" name="plans"  value="<?= $data_plan ?>"/>
                    <input type="hidden" id="input_hidden_price" name="prices"  value="<?= $prices ?>"/>
                    <input type="hidden" id="input_hidden_price2" name="prices2"  value="<?= $prices ?>"/>

                    <input type="hidden" id="planlist" name="planlist"  />
                    <input type="hidden" id="input_hidden_plan_master" name="plan_master" value="<?= $plan_master ?>"  />


                </div>

                <div class="row md-12 text-right">
                    <div class="col-2 ">
                        <label class="font-weight-bold text-right">ที่ดินเปล่า:</label>
                    </div>
                    <div class="col-10 text-left" style="padding-left: 1.1rem !important;">
                        <div class="font-weight-bold text-left" id="check_land">
                            
                        </div>
                        <input name="checkdata" id="checkdata" type="hidden">

                    </div>
                </div>
                
                <div class="row md-12 text-right">
                    <div class="col-2 ">
                        <label class="font-weight-bold text-right">ราคาที่ดินเปล่า:</label>
                    </div>
                    <div class="col-10 text-left" style="padding-left: 1.1rem !important;">
                        <label class="font-weight-bold text-left"><span id="price_plan_land_emty"></span> บาท</label>
                        <input type="hidden" id="hidden_price_plan_land_emty" name="hidden_price_plan_land_emty"  />
                    </div>
                </div>

                <div class="row md-12 text-right">
                    <div class="col-2 ">
                        <label class="font-weight-bold text-right">ราคารวม:</label>
                    </div>
                    <div class="col-10 text-left" style="padding-left: 1.1rem !important;">
                        <label class="font-weight-bold text-left"><span id="price_plan_total_select"></span> บาท</label>
                        <input type="hidden" id="hidden_price_plan_total_select" name="hidden_price_plan_total_select"  />
                    </div>
                </div>
                
            </div>
        </div>

        <div class="pt-1"></div>
        <div class="card text-white" style="background-color: #2ca535; box-shadow: 5px 2px 5px 2px rgba(255, 255, 255, 255);" >
            <div class="card-header">
                <label class="font-weight-bold">รายละเอียดแปลงบ้าน</label>
            </div>
            <div class="card-body" style="background-color: #5ebb6d;">
                <div class="row md-12 text-left">
                    <div class="col-2 text-right">
                        <label class="font-weight-bold">แบบบ้าน:</label>
                    </div>
                    <div class="col-2 text-left">
                        <div class="custom-control custom-radio form-check">
                            <input type="radio" id="standard" name="exampleRadios" value="1" <?= ('1' == $home_type) ? 'checked' : ''; ?> class="custom-control-input" <?= $disableradio ?>>
                            <input type="hidden" id="hidden_home_name1" value="<?= ($h_name1) ?>">
                            <input type="hidden" id="hidden_home_price1" value="<?= ($home_price) ?>">
                            <input type="hidden" id="hidden_home_type1" value="<?= ($home_type) ?>">
                            <label class="custom-control-label check-radio font-weight-bold text-white" for="standard"><?= $h_name1 ?></label>
                        </div>
                    </div>

                    <div class="col-2 text-left">
                        <div class="custom-control custom-radio form-check">
                            <input type="radio" id="modem" name="exampleRadios" value="2" <?= ('2' == $home_type) ? 'checked' : ''; ?> class="custom-control-input" <?= $disableradio ?>>
                            <input type="hidden" id="hidden_home_name2" value="<?= ($h_name2) ?>">
                            <input type="hidden" id="hidden_home_price2" value="<?= ($home_price) ?>">
                            <input type="hidden" id="hidden_home_type2" value="<?= ($home_type) ?>">
                            <label class="custom-control-label check-radio font-weight-bold text-white" for="modem"><?= $h_name2 ?></label>
                        </div>
                    </div>

                    <div class="col-2 text-left">
                        <div class="custom-control custom-radio form-check">
                            <input type="radio" id="vintage" name="exampleRadios" value="3" <?= ('3' == $home_type) ? 'checked' : ''; ?> class="custom-control-input" <?= $disableradio ?>>
                            <input type="hidden" id="hidden_home_name3" value="<?= ($h_name3) ?>">
                            <input type="hidden" id="hidden_home_price3" value="<?= ($home_price) ?>">
                            <input type="hidden" id="hidden_home_type3" value="<?= ($home_type) ?>">
                            <label class="custom-control-label check-radio font-weight-bold text-white" for="vintage"><?= $h_name3 ?></label>
                        </div>
                    </div>

                    <div class="col-2 text-left">
                        <div class="custom-control custom-radio form-check">
                            <input type="radio" id="div_x" name="exampleRadios" value="4" <?= ('4' == $home_type) ? 'checked' : ''; ?> class="custom-control-input" <?= $disableradio ?> >
                            <input type="hidden" id="hidden_home_name4" value="<?= ($h_name4) ?>">
                            <input type="hidden" id="hidden_home_price4" value="<?= ($home_price) ?>">
                            <input type="hidden" id="hidden_home_type4" value="<?= ($home_type) ?>">
                            <label class="custom-control-label check-radio font-weight-bold text-white" for="div_x"><?= $h_name4 ?></label>
                        </div>
                    </div>
                    
                    
                </div>
                
                <div class="row md-12 text-left">
                    <div class="col-2 text-right">
                        <label class="font-weight-bold"></label>
                    </div>
                    <div class="col-2 text-left">
                        <div class="custom-control custom-radio form-check">
                            <input type="radio" id="h_name5" name="exampleRadios" value="5" <?= ('5' == $home_type) ? 'checked' : ''; ?> class="custom-control-input" <?= $disableradio ?>>
                            <input type="hidden" id="hidden_home_name5" value="<?= ($h_name5) ?>">
                            <input type="hidden" id="hidden_home_price5" value="<?= ($home_price) ?>">
                            <input type="hidden" id="hidden_home_type5" value="<?= ($home_type) ?>">
                            <label class="custom-control-label check-radio font-weight-bold text-white" for="h_name5"><?= $h_name5 ?></label>
                        </div>
                    </div>

                    <div class="col-2 text-left">
                        <div class="custom-control custom-radio form-check">
                            <input type="radio" id="h_name6" name="exampleRadios" value="6" <?= ('6' == $home_type) ? 'checked' : ''; ?> class="custom-control-input" <?= $disableradio ?>>
                            <input type="hidden" id="hidden_home_name6" value="<?= ($h_name6) ?>">
                            <input type="hidden" id="hidden_home_price6" value="<?= ($home_price) ?>">
                            <input type="hidden" id="hidden_home_type6" value="<?= ($home_type) ?>">
                            <label class="custom-control-label check-radio font-weight-bold text-white" for="h_name6"><?= $h_name6 ?></label>
                        </div>
                    </div>

                    <div class="col-2 text-left">
                        <div class="custom-control custom-radio form-check">
                            <input type="radio" id="h_name7" name="exampleRadios" value="7" <?= ('7' == $home_type) ? 'checked' : ''; ?> class="custom-control-input" <?= $disableradio ?>>
                            <input type="hidden" id="hidden_home_name7" value="<?= ($h_name7) ?>">
                            <input type="hidden" id="hidden_home_price7" value="<?= ($home_price) ?>">
                            <input type="hidden" id="hidden_home_type7" value="<?= ($home_type) ?>">
                            <label class="custom-control-label check-radio font-weight-bold text-white" for="h_name7"><?= $h_name7 ?></label>
                        </div>
                    </div>

                    <div class="col-2 text-left">
                        <div class="custom-control custom-radio form-check">
                            <input type="radio" id="h_name8" name="exampleRadios" value="8" <?= ('8' == $home_type) ? 'checked' : ''; ?> class="custom-control-input" <?= $disableradio ?> >
                            <input type="hidden" id="hidden_home_name8" value="<?= ($h_name8) ?>">
                            <input type="hidden" id="hidden_home_price8" value="<?= ($home_price) ?>">
                            <input type="hidden" id="hidden_home_type8" value="<?= ($home_type) ?>">
                            <label class="custom-control-label check-radio font-weight-bold text-white" for="h_name8"><?= $h_name8 ?></label>
                        </div>
                    </div>
                    
                    
                </div>
                
                <div class="row md-12 text-left pt-2">
                    <div class="col-2 text-right">
                        <label class="font-weight-bold text-right">ราคา:</label>
                    </div>
                    <div class="col-10 text-left">
                        <label class="font-weight-bold " id="h_price"></label> 
                    </div>
                </div>
                
                <div class="row md-12 text-left pt-2">
                    <div class="col-2 text-right">
                        <label class="font-weight-bold text-right">กว้าง:</label>
                    </div>
                    <div class="col-3 text-left">
                        <label class="font-weight-bold " id="h_width"></label> <label class="font-weight-bold ">(เมตร)</label>  
                    </div>
                    <div class="col-2 text-right">
                        <label class="font-weight-bold text-right">ยาว:</label>
                    </div>
                    <div class="col-4 text-left">
                        <label class="font-weight-bold " id="h_height"></label> <label class="font-weight-bold ">(เมตร)</label> 
                    </div>
                </div>

                <div class="row md-12 text-left pt-2">
                    <div class="col-2 text-right">
                        <label class="font-weight-bold text-right">พื้นที่ใช้สอย:</label>
                    </div>
                    <div class="col-4 text-left">
                        <label class="font-weight-bold " id="h_area"></label> 
                    </div>
                </div>
                
                <div class="row p-2 pt-5 text-center"> 
                    <div class="col-12 text-center" style="width: 100%;">

                        <div id="div_standard" class="col-12 text-center" style="display: none; width: 100%;">
                            <div id="demo" class="carousel border-0 slide text-center" style="width: 100%;">


                                <!-- The slideshow -->
                                <div class="carousel-inner slide text-center" id="slideshow1" ></div>

                                <!-- Left and right controls -->
                                <a class="carousel-control-prev " href="#demo" data-slide="prev" >
                                    <span class="carousel-control-prev-icon bg-success"></span>
                                </a>
                                <a class="carousel-control-next " href="#demo" data-slide="next" >
                                    <span class="carousel-control-next-icon bg-success"></span>
                                </a>
                            </div>
                        </div>

                        <div id="div_modem" class="col-12 text-center" style="display: none;">
                            <div id="demo2" class="carousel slide" >

                                <div class="carousel-inner" id="slideshow2"></div>


                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#demo2" data-slide="prev" >
                                    <span class="carousel-control-prev-icon bg-success"></span>
                                </a>
                                <a class="carousel-control-next" href="#demo2" data-slide="next">
                                    <span class="carousel-control-next-icon bg-success"></span>
                                </a>
                            </div>
                        </div>

                        <div id="div_vintage" class="col-12 text-center" style="display: none;">
                            <div id="demo3" class="carousel slide" >

                                <div class="carousel-inner" id="slideshow3"></div>

                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#demo3" data-slide="prev">
                                    <span class="carousel-control-prev-icon bg-success"></span>
                                </a>
                                <a class="carousel-control-next" href="#demo3" data-slide="next">
                                    <span class="carousel-control-next-icon bg-success"></span>
                                </a>
                            </div>
                        </div>

                        <div id="div_x4" class="col-12 text-center" style="display: none;">
                            <div id="demo4" class="carousel slide" >

                                <div class="carousel-inner" id="slideshow4"></div>



                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#demo4" data-slide="prev">
                                    <span class="carousel-control-prev-icon bg-success"></span>
                                </a>
                                <a class="carousel-control-next" href="#demo4" data-slide="next">
                                    <span class="carousel-control-next-icon bg-success"></span>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row pt-2">
                    <div class="col-1"></div>
                    <div class="col-2 pt-4" align="right">
                        <label class="font-weight-bold" style="padding-top: 22px;">หมายเหตุ</label>
                    </div>
                    <div class="col-6 text-left">
                        <div class="form-group">
                            <label for="comment"></label>
                            <textarea class="form-control" id="comment" name="comment" rows="3" <?= $disableradio ?> ><?= $comment ?></textarea>
                        </div>
                    </div>
                </div>

            </div>

            <input type="hidden" id="home_price" name="home_price"  />
            <input type="hidden" id="hiden_person_order_no" name="hiden_person_order_no"  value="<?= $order_no ?>"/>
            <input type="hidden" id="hiden_person_master_plan" name="hiden_person_master_plan"  value="<?= $plan_master ?>"/>
    </form>
</div>

<div class="row pt-2">

    <div class="col-12 text-center">
        <form id='redirectForm' method='POST' action='<?= base_url('projectplan/bookhome?menu=projectplan') ?>'>
            <input type="hidden" id="input_form_plan"  name="input_form_plan"  value="<?= $data_plan ?>"/>
            <input type="hidden" id="input_form_price" name="input_form_price"  value="<?= $prices ?>"/>
            <input type="hidden" id="input_form_price_tarangwa" name="input_form_price_tarangwa"  value="<?= $input_form_price_tarangwa ?>"/>
            <input type="hidden" id="plan_id" name="plan_id"  value="<?= $plan_id ?>"/>
            <input type="hidden" id="input_hidden_plan_save" name="planssave"  value="<?= $data_plan_save ?>"/>
            <input type="hidden" id="hiden_plan_master" name="hiden_plan_master"  value="<?= $plan_master ?>"/>
            <input type="hidden" id="hiden_order_no" name="hiden_order_no"  value="<?= $order_no ?>"/>
            <input type="hidden" id="home_type2" name="home_type2"  />
            <input type="hidden" id="home_price2" name="home_price2"  />
            <input type="hidden" id="home_name2" name="home_name2"  />
            <button type="button" class="btn btn-success" id="confirm_bookhome" <?= $disableadd ?> >ยืนยันการจอง</button>
            <button type="button" class="btn btn-danger" id="cancel_bookhome" <?= $disablecancel ?>>ยกเลิกการจอง</button>
            <button type="button" class="btn btn-warning " id="edit_bookhome" <?= $disableedit ?>>บันทึกการแก้ไข</button>
            <button type="submit" class="btn btn-primary" id="promit_bookhome" <?= $disableagreement ?>>ทำสัญญาการจอง</button>
            <button type="button" class="btn btn-primary" id="promit_promise" <?= $disablepromise ?>>ทำสัญญาการซื้อขาย</button>

        </form>

    </div>
</div>

<!--modal information-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message">
    <div class="modal-dialog modal-sm" style="max-width: 400px;">
        <div class="modal-content">
            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <dd id="idmessage" class="pt-3"></dd>
                </div>

            </div>
            <div class="modal-body">
                <div class="text-center">
                    <button type="button" class="btn btn-warning" id="modal-btn-n-message" style="width: 30%;" >ตกลง</button>
                </div>

            </div>
        </div>
    </div>
</div>

<!--error-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message-error">
    <div class="modal-dialog modal-sm" style="max-width: 400px;">
        <div class="modal-content">
            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <dd id="idmessage-error" class="pt-3"></dd>
                </div>

            </div>
            <div class="modal-body">
                <div class="text-center">
                    <button type="button" class="btn btn-warning" id="modal-btn-n-message-error" style="width: 30%;" >ตกลง</button>
                </div>

            </div>
        </div>
    </div>
</div>

<!--modal-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
    <div class="modal-dialog modal-sm" style="max-width: 350px;">
        <div class="modal-content">
            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <div class="row p-2">
                        <div class="col-11 text-center pt-2"><pre class="text-center " id="lablepopup"></pre></div>
                        <div class="col-1 text-left pt-1"><button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button></div>
                    </div>
                </div>

            </div>
            <div class="modal-body">
                <div class="text-center">
                    <button type="button" class="btn btn-primary" id="modal-btn-y-edit" style="display: none; width: 25%; height: 5%">Yes</button>
                    <button type="button" class="btn btn-primary" id="modal-btn-y-cancel" style="display: none; width: 20%;">Yes</button>
                    <button type="button" class="btn btn-danger" id="modal-btn-n-edit" style="display: none; width: 25%; height: 5%">No</button>
                    <button type="button" class="btn btn-danger" id="modal-btn-n-cancel" style="display: none; width: 20%;">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script src="<?= base_url('assets/plugins/aos/js/aos.js'); ?>"></script>
<script>
    AOS.init();
</script>