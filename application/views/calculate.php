<div class="container-fluid" style="height: 1400px;">
    <div class="pt-3"></div>
    <div class="card" style="background-color: #2ca535;">
        <div class="card-header text-white">
            <label class="font-weight-bold">คำนวนการผ่อนชำระ</label>
        </div>
        <div class="card-body text-dark text-center" style="background-color: #5ebb6d;">

            <div class="text-center ">
                
                <div class="row pl-5">
                    <div class="col-12 ">
                        <div class=" row pt-1">
                            <div class="col-2"></div>
                            <label class="control-label text-right col-2 pt-2 font-weight-bold text-dark">ราคา:<label class="text-danger">*</label></label>
                            <div class="col-3 p-0" >
                                <input style="width: 100%;"  type="text" name="input_price" placeholder="price" id="input_price" class="form-control" oninput="floatOnly(this.id);">
                            </div>
                            <label class="control-label text-left col-1 pt-2 text-dark" style="padding: 0px;">(บาท)</label>
                        </div>
                    </div>
                </div>
                <div class="row pl-5">
                    <div class="col-12">
                        <div class=" row pt-0">
                            <div class="col-2"></div>
                            <label class="control-label text-right col-2 pt-2 font-weight-bold text-dark">จำนวนปี:<label class="text-danger">*</label></label>
                            <div class="col-3 p-0" style="width: 90%;">
                                <input style="width: 100%;" type="text" name="input_year" placeholder="year" id="input_year" class="form-control" oninput="numberOnly(this.id);">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pl-5">
                    <div class="col-12">
                        <div class=" row pt-0">
                            <div class="col-2"></div>
                            <label class="control-label text-right col-2 pt-2 font-weight-bold text-dark">อัตราดอกเบี้ย:<label class="text-danger">*</label></label>
                            <div class="col-3 p-0">
                                <input style="width: 100%;" type="text" name="input_rate" placeholder="interest" id="input_rate" class="form-control" oninput="floatOnly(this.id);">
                            </div>
                            <label class="control-label text-left col-1 pt-2 text-dark" style="padding: 0px;">%</label>
                        </div>
                    </div>
                </div>

                <div class="row pr-4">
                    <div class="col-12">
                        <div class="row pt-2">
                            <label class="control-label text-right col-3 pt-1"></label>
                            <div class="col-6 text-center" >
                                <div class="form-group col-11">
                                    <button type="button" class="btn btn-warning " id="btn_cal" style="width: 20%;" >คำนวณ</button> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row text-center pt-1 " id="display" style="display: none;">
                    <div class="col-12 pt-2">
                        <div class="form-group row p-0">
                            <div class="col-2"></div>
                            <label class="control-label text-right col-2 pt-1 font-weight-bold">ผลการคำนวณ:</label>
                            <div class="col-4 rounded" style="width: 50%; 
                                 border-style: solid; 
                                 color: #000;
                                 border-width: 2px;
                                 border-radius: 1px;
                                 border-color: #FFFFFF;
                                 ">

                                <div class="form-group row pt-2" style="width: 100%">
                                    <label class="control-label text-right pt-1 col-5">ราคา:</label>
                                    <div class="pt-1 pl-3">
                                        <label id="result_price"></label>
                                    </div>
                                </div>

                                <div class="form-group row" style="width: 100%">
                                    <label class="control-label text-right  pt-1 col-5">จำนวนปี:</label>
                                    <div class="pt-1 pl-3">
                                        <label id="result_year"></label>
                                    </div>
                                </div>

                                <div class="form-group row" style="width: 100%">
                                    <label class="control-label text-right  pt-1 col-5">ดอกเบี้ย:</label>
                                    <div class="pt-1 pl-3">
                                        <label id="result_rate"></label>
                                    </div>
                                </div>

                                <div class="form-group row" style="width: 100%">
                                    <label class="control-label text-right  pt-1 col-5">จำนวนเงิน/เดือน:</label>
                                    <div class="pt-1 pl-3">
                                        <label id="result_total"></label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div><label style="color: red;">ผลการคำนวนดังกล่าวเป็นเพียงตัวเลขโดยประมาณการเท่านั้น</label></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>