<div class="container" style="height: 500px;">

    <div class="card">
        <div class="card-header">
            <label class="font-weight-bold">ข้อมูลสแกนสัญญาเลขที่ </label> 


        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="m-auto">
                        <button type = "button" id="btn_scan2" class="btn btn-success fa fa-print  waves-effect p-1">Scan เอกสาร</button>
                    </div>
                </div>
            </div>
            <div class="pt-2"></div>
            <div class="table table-bordered">
                <table id="scan_list2" class="display nowrap table table-hover table-striped table-bordered " cellspacing="0" width="100%" >
                    <thead class="thead-light bg-success">
                        <tr>
                            <th scope="col" width="15%">ลำดับที่</th>
                            <th scope="col" class="text-center">เอกสาร</th>
                            <th scope="col" width="25%">วันที่สแกน</th>
                            <th scope="col" width="8%">View</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
        <div class="modal-footer">
            <!--<button class="btn btn-default" data-dismiss="modal">Close</button>-->
            <button onclick="closeOptionPop1('2')" type="button" class="btn btn-default" >Close</button>
        </div>
    </div>
</div>
<div class="container" style="display: none;">
    <div class="printableAreaImage">

    </div>
</div>

