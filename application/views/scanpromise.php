<div class="container" style="height: 650px;">

    <div class="card">
        <div class="card-header">
            <label class="font-weight-bold">ข้อมูลสแกนสัญญาเลขที่ <?= $promiseid ?> </label> 
            <input name="hidden_promiseid" type="hidden" id="hidden_promiseid" value="<?= $promiseid ?> "/>
            <input type="hidden" id="plan_master_id" name="plan_master_id"  value="<?= $plan_master ?>"/>
            <input type="hidden" id="hidden_order" name="hidden_order"  value="<?= $order_no ?>"/>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="m-auto">
                        <button type = "button" id="scan" class="btn btn-success fa fa-print  waves-effect p-1">Scan เอกสาร</button>
                    </div>
                </div>
            </div>
            <div class="pt-2"></div>
            <div class="table table-bordered">
                <table id="scanlist" class="display nowrap table table-hover table-striped table-bordered " cellspacing="0" width="100%" >
                    <thead class="thead-light bg-success">
                        <tr>
                            <th scope="col" width="8%">ลำดับที่</th>
                            <th scope="col" class="text-center">เอกสาร</th>
                            <th scope="col" width="25%">วันที่สแกน</th>
                            <th scope="col" width="8%">View</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <form id='redirectForm' method='POST' action='<?= base_url('promise/promiseselet?menu=projectplan') ?>'>

                        <input type="hidden" id="hiden_person_order_no" name="hiden_person_order_no"  value="<?= $order_no ?>"/>
                        <input type="hidden" id="hiden_person_master_plan" name="hiden_person_master_plan"  value="<?= $master_plan ?>"/>
                        <input type="hidden" id="person_plan_save" name="person_plan_save"  value="<?= $planssave ?>"/>

                        <button type = "submit"  class="btn btn-success fa fa-arrow-left waves-effect p-1">ย้อนกลับ</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

</div>

<div id="myModalFileScan" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 700px;">
        <div class="modal-content w-100">
            <div class="modal-content p-3 " >
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel"><i class="icon-star"></i> ไฟล์สแกน </a></h5>
                    <button onclick="closeFileScan()" type="button" class="close">×</button>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="identicalForm">
                        <div id="hiddenuserid"></div>
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group input-group-icon">
                                    <input type="text" placeholder="ชื่อไฟล์สแกน" name="filename_scan" id="filename_scan"  required/>
                                    <div class="input-icon"><i class="fa fa-file"><font class="text-danger">*</font>  </i></div>
                                </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-12 text-center">
                                <div class="m-auto">
                                    <button id="btn_save_file_scan" type = "button" class="btn btn-success fa fa-edit p-1">ตกลง</button>
                                    <button onclick="closeFileScan()" type="button" class="btn btn-success fa fa-close p-1">ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container" style="display: none;">
    <div class="printableAreaImage">

    </div>
</div>
