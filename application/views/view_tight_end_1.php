

<div class="container" style="display: none;">
    <div class="pt-1"></div>
    <div class="printableArea1">
        <style>
            .report-font{
                font-size:27px; 
                font-family:'Angsana New'; 
                font-style: normal;
            }
        </style>

        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_pph"></span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/deposit.png') ?>" width="250" height="150" />
                        </div>
                    </td>
                </tr>


                <tr>
                    <td height="47" colspan="5">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td style="font-size:18px; font-family:'Angsana New';" class="report-font">
                                    <div align="center"  style="width: 100%; display: inline-block;">
                                        เลขที่ 87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน  จังหวัดพระนครศรีอยุธยา 13160 โทร 035-881888-9 โทรสาร 035-881881
                                    </div>
                                    <div align="center" style="width: 100%; display: inline-block;">
                                        87 MOO 2 BANKOT BANG-PA-IN AYUTTHAYA THAILAND 13160 TEL : 035-881888-9   FAX : 035-881881
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 90%">
                            <tr>
                                <td class="report-font">
                                    <div align="center" class="font-weight-bold">สัญญาจ้างก่อสร้างบ้าน</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date_home"></span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">
                                    <div style="display: inline-block; text-indent: 2.0em; width: 100%">
                                        สัญญานี้ทำขึ้นระหว่าง
                                        <span id="view_fullname_home"></span>                              
                                        ถือบัตรประชาชนหรือบัตรข้าราชการ เลขที่<span id="view_pid_home"></span>
                                        ออกให้ ณ กรมการปกครอง   หมดอายุ   วันที่ <span id="view_expridate_home"></span>  
                                        <span id="view_address_home"></span> 
                                        <span id="view_phone_home"></span> ต่อไปในสัญญานี้เรียกว่า “ผู้ว่าจ้าง” ฝ่ายหนึ่ง  กับบริษัท พรพิศโฮม คอนสตรัคชั่น จำกัด     
                                        สำนักงานตั้งอยู่ที่เลขที่     87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160 
                                        ซึ่งต่อไปในสัญญานี้เรียกว่า “ผู้รับจ้าง” อีกฝ่ายหนึ่ง กับบริษัท พรพิศโฮม คอนสตรัคชั่น จำกัด     สำนักงานตั้งอยู่ที่เลขที่     87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน จังหวัดพระนครศรีอยุธยา 13160 ซึ่งต่อไปในสัญญานี้เรียกว่า “ผู้รับจ้าง” อีกฝ่ายหนึ่ง
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        โดยที่ผู้ว่าจ้างตกลงซื้อที่ดินในโครงการ “บ้านพรพิศ 2” แปลงหมายเลขที่ <span id="view_home_plan_number"></span>     
                                        บนโฉนดเลขที่    <span id="view_home_land_no"></span> เลขที่ดิน  <span id="view_home_land_no2"></span>   ตั้งอยู่ตำบลบ้านกรด อำเภอบางปะอิน 
                                        จังหวัดพระนครศรีอยุธยา เนื้อที่ดิน  <span id="view_home_tarangwa"></span>  ตารางวา และประสงค์จะปลูกสร้างบ้านบนที่ดินดังกล่าวตามแบบและรายการประกอบแบบ
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        คู่สัญญาทั้งสองฝ่ายตกลงทำสัญญากันดังมีข้อความดังต่อไปนี้
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 1. ผู้ว่าจ้างตกลงจ้างและผู้รับจ้างตกลงรับจ้างก่อสร้างบ้านแบบ      พรพิศมณี   
                                        บนที่ดินแปลงหมายเลขที่  <span id="view_home_plan_number2"></span> ในโครงการ “บ้านพรพิศ 2”  หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน 
                                        จังหวัดพระนครศรีอยุธยา  ซึ่งต่อไปในสัญญานี้เรียกว่า “งาน”ในราคาทั้งสิ้นเป็นเงิน  <span id="view_home_price"></span>  บาท (<span id="view_home_price_char"></span>) 
                                        อันเป็นราคาเหมารวม ค่าแรง ค่าวัสดุ อุปกรณ์ ค่าธรรมเนียมการขออนุญาตก่อสร้าง ค่าธรรมเนียมการขอทะเบียนบ้าน 
                                        ค่าธรรมเนียมการติดตั้งมาตรวัดน้ำประปาและไฟฟ้าแล้ว
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        งานตามสัญญาฉบับนี้ไม่รวมถึง ค่าประกันสำหรับมาตรวัดน้ำประปาและไฟฟ้า 
                                        งานต่อเติมเปลี่ยนแปลงอื่น ๆ และค่าใช้จ่ายอื่นที่มิได้ระบุไว้ในสัญญานี้
                                    </div>

                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                
                
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/ข้อ 2. ผู้รับจ้างตกลง................</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้ว่าจ้าง</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้รับจ้าง</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    
                    <td colspan="5" height="90">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    
                </tr>
                
            </table>
        </div>
    </div>

    <!--page2-->
    <div class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/deposit.png') ?>" width="250" height="150" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td style="font-size:18px; font-family:'Angsana New';" class="report-font">
                                    <div align="center"  style="width: 100%; display: inline-block;">
                                        เลขที่ 87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน  จังหวัดพระนครศรีอยุธยา 13160 โทร 035-881888-9 โทรสาร 035-881881
                                    </div>
                                    <div align="center" style="width: 100%; display: inline-block;">
                                        87 MOO 2 BANKOT BANG-PA-IN AYUTTHAYA THAILAND 13160 TEL : 035-881888-9   FAX : 035-881881
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td  class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-2-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td  class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 2. ผู้รับจ้างตกลงก่อสร้างงานภายในกำหนด 15 (สิบห้า) เดือน นับจากวันที่ผู้ว่าจ้างรับโอนกรรมสิทธิ์ที่ดินแล้วเสร็จ 
                                        ส่งมอบพื้นที่ให้แก่ผู้รับจ้าง และผู้รับจ้างทำการตอกเสาเข็มแล้วเสร็จแล้ว
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ในกรณีที่ผู้ว่าจ้างยังไม่ได้เป็นเจ้าของกรรมสิทธิ์ในที่ดินแปลงหมายเลขที่จะใช้ก่อสร้างดังกล่าว 
                                        ผู้ว่าจ้างและผู้รับจ้างตกลงให้ผู้รับจ้างเป็นตัวแทนในการยื่นขออนุญาตต่าง ๆ ที่เกี่ยวข้องกับงานในสัญญานี้ในนามผู้ว่าจ้าง 
                                        และให้ผู้รับจ้างสามารถมอบอำนาจให้บุคคลอื่นดำเนินการก็ได้

                                    </div> 
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ทั้งนี้หากผู้ว่าจ้างไม่สามารถรับโอนกรรมสิทธิ์ในที่ดินแปลงหมายเลขที่จะใช้ก่อสร้างนี้ก่อนที่การก่อสร้างจะเริ่ม 
                                        ผู้ว่าจ้างยินยอมให้ผู้รับจ้างดำเนินการด้านเอกสารไปก่อน หรือหากผู้ว่าจ้างไม่สามารถรับโอนกรรมสิทธิ์ในที่ดินแปลงหมายเลขที่จะใช้ก่อสร้างนี้ไม่ว่ากรณีใด ๆ 
                                        ผู้ว่าจ้างยินยอมให้ผู้รับจ้างทำการเปลี่ยนชื่อผู้ขออนุญาตตามที่ผู้รับจ้างประสงค์โดยผู้รับจ้างไม่ต้องแจ้งผู้ว่าจ้างให้ทราบก่อนล่วงหน้า
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 3. คู่สัญญาเป็นอันตกลงว่าในวันทำสัญญาฉบับนี้ ผู้ว่าจ้างได้ชำระค่ามัดจำในการก่อสร้างเป็นเงิน     <span id="view_pay_price_home"></span>     บาท 
                                        (<span id="view_pay_price_home_char"></span>) และชำระเงินทำสัญญา ณ วันทำสัญญางวดที่ 1 เป็นเงิน <span id="view_pay_price_pirod1"></span>  บาท  
                                        (<span id="view_pay_price_pirod1_char"></span>)  หลังจากนั้นจะชำระค่าจ้างก่อสร้างงานตามสัญญาออกเป็นงวดงาน ดังนี้

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        งวดที่ 2	ชำระเงิน   <span id="view_pay_price_pirod2"></span>    บาท  (<span id="view_pay_price_pirod2_char"></span>)
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 0.0em;">
                                        เมื่อ งานวางหมุด ตอกเข็ม ขุดดิน เทลีน งานฐานราก งานหล่อคานคอดิน วางแผ่นพื้นสำเร็จ เทพื้นสำเร็จ เทพื้น ตั้งเสาชั้นหนึ่ง
                                        งานหล่อคานชั้นสอง วางแผ่นพื้นสำเร็จ เทพื้น ตั้งเสาชั้นสอง งานหล่อคานหลังคา ทำโครงเหล็กหลังคา ติดเชิงชาย มุงกระเบื้องหลังคา 
                                        ก่ออิฐ ตั้งวงกบชั้นล่าง-ชั้นบน เดินท่อไฟฟ้า ระบบประปา ติดลวดกันร้าว

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        งวดที่ 3   ชำระเงิน   <span id="view_pay_price_pirod3"></span>    บาท  (<span id="view_pay_price_pirod3_char"></span>) 
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 0.0em;">
                                        เมื่อ ฉาบปูนภายใน-ภายนอก งานบัวปูนภายนอก งานร้อยสายไฟในท่อ ติดตั้งถังบำบัด งานฝ้าชายคา ฝ้าภายใน งานท่อระบายรอบนอก 
                                        งานกระเบื้องห้องน้ำ งานประตู-หน้าต่าง งานบันได งานปูพื้นชั้นบน-ชั้นล่าง เก็บงานปูนทั้งหมด งานสีรองพื้นงานบัวพื้น งานบัวฝ้า 
                                        งานสี งานติดอุปกรณ์สุขภัณฑ์ งานไฟฟ้า ตรวจสอบการใช้งาน ระบบไฟฟ้าประปา เก็บงานตัวบ้าน และบริเวณพื้นที่โดยรอบ 
                                        ทำความสะอาด แล้วเสร็จ

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td  class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ทั้งนี้ผู้รับจ้างอาจเรียกเก็บค่างวดงานได้มากกว่าหนึ่งงวดงานต่อการเรียกเก็บในแต่ละครั้งก็ได้
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/ข้อ 4. ผู้ว่าจ้างตกลงว่า................</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้ว่าจ้าง</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้รับจ้าง</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--page3-->
    <div class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/deposit.png') ?>" width="250" height="150" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td style="font-size:18px; font-family:'Angsana New';" class="report-font">
                                    <div align="center"  style="width: 100%; display: inline-block;">
                                        เลขที่ 87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน  จังหวัดพระนครศรีอยุธยา 13160 โทร 035-881888-9 โทรสาร 035-881881
                                    </div>
                                    <div align="center" style="width: 100%; display: inline-block;">
                                        87 MOO 2 BANKOT BANG-PA-IN AYUTTHAYA THAILAND 13160 TEL : 035-881888-9   FAX : 035-881881
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td  class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-3-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 4. ผู้ว่าจ้างตกลงว่า เมื่อก่อสร้างงานในแต่ละงวดเสร็จแล้ว และผู้รับจ้างส่งใบแจ้งหนี้งวดงานก่อสร้างนั้นแล้ว 
                                        ผู้ว่าจ้างตกลงตรวจงวดงาน และชำระค่างวดงานนั้น ๆ ให้เสร็จสิ้นภายใน 7 (เจ็ด)วัน 
                                        นับแต่ผู้รับจ้างส่งใบแจ้งหนี้งวดงานหรือนับแต่ได้รับโทรศัพท์แจ้งจากทางผู้รับจ้าง
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ผู้ว่าจ้างรับรองว่า การชำระค่าจ้างงวดงานตามสัญญานี้ หากผู้ว่าจ้างจะขอสินเชื่อเพื่อการปลูกสร้างบ้านต่อสถาบันการเงิน 
                                        และเมื่อสถาบันการเงินตกลงให้กู้ยืมเงินเพื่อชำระค่าจ้างตามสัญญา โดยแบ่งเป็นงวด ๆ ตามความสำเร็จของงวดงานแล้ว 
                                        ผู้ว่าจ้างตกลงให้สถาบันการเงินหรือบุคคลอันสถาบันการเงิน มอบหมายช่วงเป็นผู้มีอำนาจในการตรวจงวดงาน และชำระค่าจ้างงวดงานแทนผู้ว่าจ้าง 
                                        การใดอันสถาบันการเงินทำเพื่อการตรวจงวดงาน และชำระราคาแล้ว ผู้ว่าจ้างตกลงรับเป็นการตรวจงวดงานของผู้ว่าจ้างเอง 
                                        โดยตนเองและยินยอมให้สถาบันการเงินชำระค่าจ้างในงวดงานนั้นได้โดยไม่คัดค้านแต่อย่างใด สำหรับใบแจ้งหนี้งวดงานนั้น 
                                        ผู้ว่าจ้างจะแจ้งต่อสถาบันการเงินโดยทันทีเมื่อได้รับ และยินยอมให้ผู้รับจ้างแจ้งต่อสถาบันการเงินโดยตรงด้วยเช่นกัน 
                                        เพื่อให้การชำระหนี้เป็นไปโดยไม่ล่าช้า พร้อมกันนั้น ผู้ว่าจ้างยินยอมให้สถาบันการเงินเข้าตรวจงวดงาน ชำระราคางวดงานโดยทันทีด้วยเช่นกัน 
                                        และหากสถาบันการเงินเรียกเก็บเงินค่าธรรมเนียมเพื่อการดังกล่าวแล้ว ผู้ว่าจ้างตกลงเป็นผู้ชำระค่าธรรมเนียมให้แก่สถาบันการเงินโดยตนเอง
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        การตรวจงวดงานนั้น หากพบข้อบกพร่องหรือไม่สมบูรณ์อันเกิดจากผู้รับจ้างและต้องทำการแก้ไขแล้ว 
                                        ผู้ว่าจ้างหรือตัวแทนตกลงจะทำบันทึกรายการแก้ไขเป็นลายลักษณ์อักษร และส่งมอบให้แก่ผู้รับจ้างในวันทำการตรวจงวดงานนั้นทันที 
                                        หากทราบความบกพร่องดังกล่าวแล้ว ผู้รับจ้างจะต้องดำเนินการแก้ไขส่วนบกพร่องนั้น ๆ 
                                        ทั้งนี้การแก้ไขดังกล่าวให้กระทำภายในเวลาอันเหมาะสมแห่งงาน และให้แล้วเสร็จก่อนส่งมอบงานตามสัญญา 
                                        ทั้งนี้การแก้ไขข้อบกพร่องดังกล่าว ผู้ว่าจ้างไม่ถือเป็นเหตุให้หยุดหรือระงับการชำระราคาค่างวดงานนั้น 
                                        และให้ผู้รับจ้างดำเนินการก่อสร้างงานก่อสร้างบ้านในงวดถัด ๆ ไปพร้อมกันด้วย
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        หากผู้ว่าจ้างผิดนัดชำระค่างวดงานใด ๆ แล้ว ผู้ว่าจ้างยินยอมให้ผู้รับจ้างคิดดอกเบี้ยระหว่างผิดนัดในอัตราร้อยละ 1.5 (หนึ่งจุดห้า) 
                                        ต่อเดือนของต้นเงินผิดนัดจนกว่าจะชำระเสร็จสิ้น และผู้รับจ้างมีสิทธิหยุดงานหรือระงับการก่อสร้างงานตามสัญญาไว้ได้จนกว่าจะได้รับชำระค่าจ้างและดอกเบี้ยแล้ว 
                                        พร้อมให้ขยายระยะเวลางานแล้วเสร็จออกไปเท่าจำนวนหยุดงานนั้น และให้ถือเป็นการกำหนดวันแล้วเสร็จของงานตามสัญญาขึ้นมาใหม่
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 5.  ณ วันทำสัญญาฉบับนี้ ผู้ว่าจ้างตรวจสอบแบบและรายการประกอบแบบเห็นว่าถูกต้องตรงความประสงค์
                                        แล้ว และจะไม่ทำการแก้ไขหรือเปลี่ยนแปลงแต่อย่างใด รวมทั้งผู้ว่าจ้างตกลงไม่ทำการแก้ไข เปลี่ยนแปลง เพิ่ม ลด หรือ ตัด 
                                        รายการวัสดุ หรือจัดหาวัสดุอุปกรณ์ต่าง ๆ
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/หากว่าผู้ว่าจ้าง................</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้ว่าจ้าง</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้รับจ้าง</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

            </table>
        </div>
    </div>

    <!--page4-->
    <div class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/deposit.png') ?>" width="250" height="150" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td style="font-size:18px; font-family:'Angsana New';" >
                                    <div align="center"  style="width: 100%; display: inline-block;">
                                        เลขที่ 87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน  จังหวัดพระนครศรีอยุธยา 13160 โทร 035-881888-9 โทรสาร 035-881881
                                    </div>
                                    <div align="center" style="width: 100%; display: inline-block;">
                                        87 MOO 2 BANKOT BANG-PA-IN AYUTTHAYA THAILAND 13160 TEL : 035-881888-9   FAX : 035-881881
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" >
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-4-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        หากผู้ว่าจ้างประสงค์จะทำการแก้ไขหรือเปลี่ยนแปลงแบบและรายการประกอบแบบ หรือรายการประกอบแบบ 
                                        หรือรายการวัสดุอุปกรณ์แล้ว ผู้ว่าจ้างและผู้รับจ้างตกลงว่าจะทำข้อตกลงเป็นลายลักษณ์อักษรก่อน 
                                        โดยกำหนดรายการแก้ไขเปลี่ยนแปลง กำหนดขยายระยะเวลาแล้วเสร็จของงานออกไป 
                                        และมีการชำระเงินให้ครบก่อนจึงจะดำเนินการแก้ไขเปลี่ยนแปลงได้ 
                                        หรือนำหลักฐานอันสถาบันการเงินรับรองยอดเงินเพื่องานแก้ไขเปลี่ยนแปลงมาแสดงต่อผู้รับจ้าง ทั้งนี้การแก้ไขเพิ่มเติมงานนั้น 
                                        หากกระทำเมื่อล่วงเลยขั้นตอนอันเหมาะสมแล้ว การก่อสร้างงานเพิ่มลดดังกล่าวไปแล้ว การแก้ไข ต่อเติม หรือเปลี่ยนแปลง 
                                        อาจไม่เหมาะสม ไม่สวยงาม ล่าช้า เช่นการเพิ่มหรือเปลี่ยนตำแหน่งจุดไฟฟ้า ประปา ผู้ว่าจ้างเป็นอันยอมรับผลงานดังกล่าว 
                                        โดยไม่คัดค้านแต่อย่างใด ทั้งนี้ หากผู้ว่าจ้างต้องการจัดหาวัสดุบางส่วนเข้ามาเองแล้ว 
                                        ผู้ว่าจ้างตกลงจัดหาวัสดุดังกล่าวส่งมอบให้ผู้รับจ้างก่อนกำหนดใช้วัสดุดังกล่าวเป็นการล่วงหน้า 20 (ยี่สิบ) วันก่อนเริ่มงานในส่วนนั้น 
                                        และหากผู้ว่าจ้างไม่สามารถจัดหาวัสดุเข้ามาได้ภายในกำหนดแล้ว ผู้ว่าจ้างยินยอมที่จะขยายกำหนดระยะเวลาการก่อสร้างออกไปตามความเหมาะสม
                                    </div> 
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        หากผู้รับจ้างตกลงรับทำงานแก้ไขหรือเปลี่ยนแปลงแล้ว ผู้ว่าจ้างตกลงดังนี้
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        5.1. การแจ้งเปลี่ยนแปลงใด ๆ จะต้องเป็นการแจ้งต่อผู้รับจ้างเป็นลายลักษณ์อักษรในเอกสารแบบแก้ไขเปลี่ยน
                                        แปลงของทางผู้รับจ้างเท่านั้น  โดยผู้ว่าจ้างจะลงลายมือชื่อกำกับเอกสารแก้ไขหรือต่อเติมหรือเปลี่ยนแปลงภายในกำหนด  15      (สิบห้า) 
                                        วันนับจากวันทำสัญญาฉบับนี้ โดยทางผู้รับจ้างจะพิจารณาข้อจำกัด และเงื่อนเวลาที่จำเป็นจะต้องใช้ในการทำงาน 
                                        และเนื่องจากเป็นเพียงการแจ้งความประสงค์ของผู้ว่าจ้างทางผู้รับจ้างขอสงวนสิทธิรับทำรายการที่รับแจ้งทำการเปลี่ยนแปลงนั้นตามสมควร 
                                        โดยราคาค่าใช้จ่ายเป็นดังนี้

                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 6.0em;">
                                        5.1.1. หากเป็นรายการที่มีราคามาตรฐานทางโครงการจะจัดทำเอกสารงานต่อเติม โดยผู้ว่าจ้างจะต้อง
                                        ชำระมัดจำร้อยละ 40 (สี่สิบ) จากยอดทั้งหมดก่อนที่จะเริ่มงาน
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 6.0em;">
                                        5.1.2. หากเป็นรายการที่ยังไม่มีราคามาตรฐาน ทางโครงการจะทำการประเมินให้ผู้ว่าจ้าง ทราบก่อน
                                        เป็นลายลักษณ์อักษร หากผู้ว่าจ้างประสงค์ที่จะดำเนินการจะต้องมาลงลายมืออนุมัติการก่อสร้างภายในระยะเวลาที่ผู้รับจ้างได้แจ้ง
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 6.0em;">
                                        5.1.3. หากผู้ว่าจ้างจะแจ้งยกเลิกการแก้ไขต่อเติมใด ๆ ที่ได้เริ่มงานไปแล้ว ทางผู้ว่าจ้างจะต้อง
                                        รับผิดชอบค่าใช้จ่ายต่าง ๆ ที่เกิดขึ้นทั้งหมดและให้ใช้ความตามข้อ 4. ของสัญญานี้มาบังคับใช้โดยอนุโลม
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 6.0em;">
                                        5.1.4. หากผู้ว่าจ้างทำการจัดซื้อของ/วัสดุ และอุปกรณ์ใด ๆ และนำมาฝากให้ทางผู้รับจ้างเพื่อทำการ
                                        ติดตั้งนั้น ทางผู้รับจ้างสงวนสิทธิในการรับฝากตามสมควร หากมีความสูญหายทางผู้รับจ้างจะรับผิดชอบความเสียหายไม่เกินราคาที่ผู้ว่าจ้างจัดซื้อมา
                                        และหากมีความเสียหายที่เกิดขึ้นจากความผิดพลาดที่ไม่ใช่ความประมาทเลินเล่ออย่างร้ายแรง ทางผู้รับจ้างจะทำการแก้ไขให้ใช้งานได้ 
                                        และวัสดุบางประเภทเช่น กระเบื้อง เป็นต้น ผู้ว่าจ้างต้องเผื่อการสูญเสียจากการก่อสร้างอีกไม่ต่ำกว่าร้อยละ 5 (ห้า) ของวัสดุแต่ละแบบ 
                                        โดยไม่ถือว่าส่วนที่เผื่อเป็นความเสียหายที่ผู้รับจ้างต้องรับผิดชอบแต่อย่างใด

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>


                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/ทั้งนี้ หากมีการลด................</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 95%">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3" style="report-font">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้ว่าจ้าง</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้รับจ้าง</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--page5 Tomorrow-->
    <div class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/deposit.png') ?>" width="250" height="150" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td style="font-size:18px; font-family:'Angsana New';" class="report-font">
                                    <div align="center"  style="width: 100%; display: inline-block;">
                                        เลขที่ 87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน  จังหวัดพระนครศรีอยุธยา 13160 โทร 035-881888-9 โทรสาร 035-881881
                                    </div>
                                    <div align="center" style="width: 100%; display: inline-block;">
                                        87 MOO 2 BANKOT BANG-PA-IN AYUTTHAYA THAILAND 13160 TEL : 035-881888-9   FAX : 035-881881
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-5-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ทั้งนี้ หากมีการลดงานตามสัญญาแล้ว การลดงานดังกล่าวนั้นมิให้ถือเป็นการปรับลดราคาค่าจ้างให้ต่ำกว่าราคาค่าจ้างที่กำหนดในสัญญานี้ 
                                        ราคาการลดงานนั้น ๆ จะให้คิดลดเฉพาะกรณีหักกลบกับงานเพิ่มเติมเท่านั้น
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 6. เพื่อประโยชน์ในการทำงานตามสัญญา ระหว่างผู้รับจ้างทำงานตามสัญญาฉบับนี้ 
                                        ผู้ว่าจ้างจะไม่จัดส่งผู้รับเหมางานอื่น ๆ เข้ามาทำงานในสถานที่ก่อสร้าง เว้นแต่ผู้รับจ้างพิจารณาอนุมัติแล้ว 
                                        ทั้งนี้ผู้รับเหมาอื่น ๆ จะต้องประสานงานกับผู้รับจ้าง และจะต้องปฎิบัติตามระเบียบที่ผู้รับจ้างกำหนด 
                                        ทั้งนี้ผู้รับจ้างจะคิดค่าใช้จ่ายค่าประสานงาน และเงินวางประกันก็ได้ 
                                        และหากผู้ว่าจ้างดำเนินการโดยปรากฏจากความยินยอมของผู้รับจ้างแล้วให้ถือว่า ผู้ว่าจ้างไม่ประสงค์การประกันการก่อสร้าง 
                                        และผู้รับจ้างมีสิทธิบอกเลิกสัญญา พร้อมผู้ว่าจ้างตกลงชดใช้ค่าเสียหายให้แก่ผู้รับจ้างเท่าจำนวนค่าจ้างส่วนที่เหลือทั้งหมดตามสัญญา
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 7. ผู้ว่าจ้างสามารถเข้าตรวจตราการก่อสร้างงานตามสัญญาได้ตลอดเวลาตามความสมควรและ
                                        เหมาะสมโดยต้องไม่เป็นการรบกวนหรือขัดขวางการทำงานของผู้รับจ้าง
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 8. ผู้รับจ้างตกลงว่า
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        8.1. จะทำการก่อสร้างงานตามสัญญาตามแบบและรายการประกอบแบบในสัญญาฉบับนี้
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        8.2. จะจัดหาวัสดุที่มีคุณภาพดี ตามที่กำหนดในแบบแปลนการก่อสร้าง และรายการวัสดุประกอบแบบในสัญญา
                                        ฉบับนี้ หรือจัดหาวัสดุที่มีคุณภาพเทียบเท่าเพื่อทำงานตามสัญญา

                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        8.3. จะเป็นผู้ทำการจัดหาเครื่องมือเครื่องใช้ต่าง ๆ ที่ต้องการในการก่อสร้างงาน
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 4.0em;">
                                        8.4. จะต้องจัดหาช่างฝีมือที่มีมาตรฐานและวัสดุอุปกรณ์เครื่องมือเครื่องใช้ที่ได้มาตรฐานสำหรับงานก่อสร้าง 
                                        และต้องทำการก่อสร้างตามหลักวิชาช่างที่ดี เพื่อให้ได้ผลงานที่มีความมั่นคงปลอดภัยต่อการใช้งาน

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 9. ผู้รับจ้างตกลงเป็นผู้ดำเนินการประสานงานเพื่อขออนุญาตติดตั้งมาตรวัดน้ำประปาและไฟฟ้า 
                                        โดยผู้รับจ้างจะเป็นผู้รับผิดชอบค่าธรรมเนียมในการติดตั้งมิเตอร์ไฟฟ้าขนาด 15 แอมป์และมิเตอร์ประปา 
                                        แต่ผู้ว่าจ้างต้องเป็นผู้รับผิดชอบเงินประกันและค่าใช้จ่ายอื่น ๆ ที่เกี่ยวข้องของมาตรวัดน้ำประปา และไฟฟ้า 
                                        ทั้งนี้ระยะเวลาการขออนุญาตการติดตั้งจะไม่รวมอยู่ในกำหนดการทำงานตามสัญญา
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/ข้อ 10. บรรดางานก่อสร้าง................</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้ว่าจ้าง</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้รับจ้าง</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--page6 This-->
    <div class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/deposit.png') ?>" width="250" height="150" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td style="font-size:18px; font-family:'Angsana New';" class="report-font">
                                    <div align="center"  style="width: 100%; display: inline-block;">
                                        เลขที่ 87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน  จังหวัดพระนครศรีอยุธยา 13160 โทร 035-881888-9 โทรสาร 035-881881
                                    </div>
                                    <div align="center" style="width: 100%; display: inline-block;">
                                        87 MOO 2 BANKOT BANG-PA-IN AYUTTHAYA THAILAND 13160 TEL : 035-881888-9   FAX : 035-881881
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-6-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 10. บรรดางานก่อสร้างตามสัญญานี้ คู่สัญญาเป็นอันตกลงว่า ผู้รับจ้างมีสิทธินำงานก่อสร้างทั้งหมดหรือบางส่วนให้
                                        ผู้อื่นดำเนินการหรือผู้รับจ้างช่วงก็ได้ แต่ผู้รับจ้างต้องเป็นผู้รับผิดชอบต่อผู้ว่าจ้างโดยตรงเสมือนหนึ่งว่าผู้รับจ้างเป็นผู้ก่อสร้างงานบ้านโดยตนเอง
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 11. การตรวจรับมอบงานตามสัญญาทั้งหมดนั้น ผู้ว่าจ้างตกลงว่านับแต่ได้รับแจ้งจากผู้รับจ้างเพื่อตรวจรับมอบงานทั้งหมดตามสัญญาแล้ว 
                                        ผู้ว่าจ้างจะตรวจรับมอบงานให้แล้วเสร็จภายใน 7 (เจ็ด) วันนับแต่ได้รับแจ้งให้ตรวจรับมอบงาน หรือได้รับแจ้งตรวจรับมอบงานแก้ไขบ้าน 
                                        ทั้งนี้ระหว่างการตรวจรับมอบงานนั้น หากมีความบกพร่องที่ต้องแก้ไขแล้ว ผู้ว่าจ้างจะต้องมีคำสั่งให้แก้ไขเป็นลายลักษณ์อักษรมอบให้แก่ผู้รับจ้าง 
                                        ณ วันตรวจรับมอบงานทั้งหมดตามสัญญาทันที หากผู้ว่าจ้างไม่มีคำสั่งเป็นลายลักษณ์อักษรให้แก้ไขงานดังกล่าวแล้ว 
                                        หรือหากผู้ว่าจ้างไม่ตรวจรับงานตามสัญญาทั้งหมดหรืองานแก้ไขงานบ้านภายในเวลาที่กำหนดในข้อนี้แล้ว 
                                        ผู้ว่าจ้างถือว่างานตามสัญญาทั้งหมดเรียบร้อยครบถ้วนตามสัญญาแล้ว 
                                        ผู้ว่าจ้างยอมรับมอบงานดังกล่าวโดยให้ถือว่ารับมอบนับแต่พ้นกำหนดตรวจรับแล้วโดยปริยายทันทีโดยไม่ได้โต้แย้งใด ๆ
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 12. คู่สัญญาตกลงว่า ในกรณีหากงานตามสัญญาต้องหยุดชะงักลงเพราะเหตุสุดวิสัย หรืออัคคีภัย น้ำท่วม การก่อการ
                                        จลาจล ภาวะสงคราม หรือเกิดจากวัสดุ หรืออุปกรณ์การก่อสร้างตามแบบแปลนและรายการประกอบไม่สามารถจัดหาได้ในท้องตลาด 
                                        หรือการแก้ไขเปลี่ยนแปลงแบบและรายการประกอบแบบ หรือเหตุอันมิอาจโทษฝ่ายหนึ่งฝ่ายใดได้ 
                                        ผู้ว่าจ้างยินยอมให้มีการขยายระยะเวลาแล้วเสร็จของงานตามสัญญาออกไปเท่ากับวันเวลาที่ผู้รับจ้างต้องเสียไปเพราะเหตุอัน
                                        ทำให้มีการหยุดชะงักแห่งการงานนั้น โดยผู้รับจ้างไม่ต้องรับผิดในความล่าช้าดังกล่าวและให้ถือเป็นการกำหนดแล้วเสร็จของงานขึ้นมาใหม่
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 13. ผู้ว่าจ้างตกลงว่า หากผู้ว่าจ้างโอนกรรมสิทธิ์ที่ดินที่ปลูกสร้างงานตามสัญญาให้แก่บุคคลอื่นแล้ว 
                                        ก่อนการปลูกสร้างงานตามสัญญาเสร็จสิ้น ผู้ว่าจ้างตกลงว่า สัญญาจ้างนี้ไม่ระงับแต่อย่างใด 
                                        ผู้ว่าจ้างจะแจ้งต่อผู้รับโอนถึงสิทธิและหน้าที่ตามสัญญานี้ 
                                        โดยผู้ว่าจ้างตกลงดำเนินการให้ผู้รับโอนเข้าสวมสิทธิเป็นผู้ว่าจ้างตามสัญญานี้ก่อนหรือขณะวันโอนกรรมสิทธิ์ที่ดิน 
                                        และให้ผู้เข้าสวมสิทธิหาหลักประกันการชำระค่าจ้างต่อผู้รับจ้าง พร้อมผู้ว่าจ้างตกลงเป็นผู้ค้ำประกันการชำระเงินตามสัญญานี้ 
                                        ทั้งนี้หากผู้รับโอนไม่เข้าสวมสิทธิหรือไม่สามารถจัดหาหลักประกันดังกล่าวได้แล้ว 
                                        ผู้ว่าจ้างตกลงชำระค่าจ้างในงวดงานส่วนที่เหลือทั้งหมดเป็นการล่วงหน้าทันที
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/ข้อ 14. เพื่อประโยชน์แห่ง................</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้ว่าจ้าง</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้รับจ้าง</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--page7-->
    <div class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/deposit.png') ?>" width="250" height="150" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td style="font-size:18px; font-family:'Angsana New';" class="report-font">
                                    <div align="center"  style="width: 100%; display: inline-block;">
                                        เลขที่ 87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน  จังหวัดพระนครศรีอยุธยา 13160 โทร 035-881888-9 โทรสาร 035-881881
                                    </div>
                                    <div align="center" style="width: 100%; display: inline-block;">
                                        87 MOO 2 BANKOT BANG-PA-IN AYUTTHAYA THAILAND 13160 TEL : 035-881888-9   FAX : 035-881881
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; font-size:30px; font-family:'Angsana New';" class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-7-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 14. เพื่อประโยชน์แห่งสัญญา ณ วันทำสัญญาฉบับนี้ ผู้ว่าจ้างตกลงให้ผู้รับจ้างเป็นผู้ดูแลรับผิดชอบที่ดินและสิ่งปลูกสร้างตามสัญญาแต่ผู้เดียว 
                                        ผู้รับจ้างต้องดูแลรักษาที่ดินและงานตามสัญญาดังวิญญูชนโดยค่าใช้จ่ายตนเอง	และให้สิทธิดูแลรับผิดชอบสิ้นสุดลงทันที 
                                        เมื่อผู้ว่าจ้างชำระราคาค่าจ้างตามสัญญาครบถ้วนแล้ว และผู้รับจ้างส่งมอบงานและผู้ว่าจ้างรับมอบงานตามสัญญา
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 11. การตรวจรับมอบงานตามสัญญาทั้งหมดนั้น ผู้ว่าจ้างตกลงว่านับแต่ได้รับแจ้งจากผู้รับจ้างเพื่อตรวจรับมอบงานทั้งหมดตามสัญญาแล้ว 
                                        ผู้ว่าจ้างจะตรวจรับมอบงานให้แล้วเสร็จภายใน 7 (เจ็ด) วันนับแต่ได้รับแจ้งให้ตรวจรับมอบงาน หรือได้รับแจ้งตรวจรับมอบงานแก้ไขบ้าน 
                                        ทั้งนี้ระหว่างการตรวจรับมอบงานนั้น หากมีความบกพร่องที่ต้องแก้ไขแล้ว ผู้ว่าจ้างจะต้องมีคำสั่งให้แก้ไขเป็นลายลักษณ์อักษรมอบให้แก่ผู้รับจ้าง 
                                        ณ วันตรวจรับมอบงานทั้งหมดตามสัญญาทันที หากผู้ว่าจ้างไม่มีคำสั่งเป็นลายลักษณ์อักษรให้แก้ไขงานดังกล่าวแล้ว 
                                        หรือหากผู้ว่าจ้างไม่ตรวจรับงานตามสัญญาทั้งหมดหรืองานแก้ไขงานบ้านภายในเวลาที่กำหนดในข้อนี้แล้ว 
                                        ผู้ว่าจ้างถือว่างานตามสัญญาทั้งหมดเรียบร้อยครบถ้วนตามสัญญาแล้ว 
                                        ผู้ว่าจ้างยอมรับมอบงานดังกล่าวโดยให้ถือว่ารับมอบนับแต่พ้นกำหนดตรวจรับแล้วโดยปริยายทันทีโดยไม่ได้โต้แย้งใด ๆ
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 15. การประกันผลงานตามสัญญา มีกำหนด 1 (หนึ่ง) ปี นับแต่วันที่ผู้ว่าจ้างได้รับมอบงานทั้งหมดตามสัญญาแล้ว 
                                        โดยหากงานตามสัญญาเกิดชำรุดบกพร่อง ซึ่งเหตุชำรุดเสียหายนั้นเกิดจากความบกพร่องของผู้รับจ้างทำไว้ไม่เรียบร้อย 
                                        หรือใช้วัสดุอุปกรณ์ที่ไม่ดีไม่มาตรฐาน หรือทำงานไม่ถูกต้องตามหลักวิชา 
                                        ผู้รับจ้างจะต้องดำเนินการแก้ไขให้เป็นที่เรียบร้อยภายในระยะเวลาอันสมควร 
                                        โดยผู้รับจ้างไม่เรียกร้องค่าใช้จ่ายหรือค่าตอบแทนใด ๆ ทั้งสิ้น แต่ทั้งนี้ความเสื่อมตามปกติของบ้านตามสัญญา 
                                        หรือผู้ว่าจ้างหรือตัวแทนผู้ว่าจ้างเป็นผู้ดำเนินการตบแต่งต่อเติมหรือใช้วัสดุอุปกรณ์ไม่ได้มาตรฐาน ใช้สอยผิดลักษณะปกติ 
                                        ทำงานไม่ถูกต้องตามหลักวิชา หรือมีส่วนร่วมในการก่อให้เกิดความเสียหายแก่งานตามสัญญาแล้ว 
                                        ผู้รับจ้างไม่จำเป็นต้องรับผิดชอบแต่ประการใด
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 16. หากผู้ว่าจ้างและผู้รับจ้างทำการตกลงใด ๆ ขึ้นมาใหม่นอกเหนือจากสัญญานี้แล้ว 
                                        ผู้ว่าจ้างและผู้รับจ้างตกลงจะทำข้อตกลงเป็นลายลักษณ์อักษรลงลายมือชื่อคู่สัญญาทั้งสองฝ่าย 
                                        และให้นำข้อตกลงใหม่ผนวกรวมกับสัญญานี้และถือเป็นส่วนหนึ่งของสัญญานี้ 
                                        และข้อตกลงใหม่บังคับใช้ได้เท่าที่ไม่ขัดหรือแย้งกับสัญญานี้
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        ข้อ 17. คำบอกกล่าวใด ๆ ตามสัญญานี้ จะต้องทำเป็นหนังสือ และส่งทางไปรษณีย์ไปยังผู้จะซื้อตามที่อยู่ข้างต้นในกรณีที่ผู้ว่าจ้างเปลี่ยนที่อยู่/ย้าย 
                                        ทำให้ส่งหนังสือถึงผู้ว่าจ้างไม่ได้ ให้ถือว่าหลักฐานทางไปรษณีย์ที่คืนกลับมา เป็นการส่งถึงผู้ว่าจ้างโดยสมบูรณ์แล้ว 
                                        การแจ้งไปยังผู้รับจ้างให้แจ้งไป ณ สำนักงานเลขที่ 47/87 หมู่ที่ 3 ตำบลคลองสวนพลู อำเภอพระนครศรีอยุธยา 
                                        จังหวัดพระนครศรีอยุธยา หรือไปยังที่อยู่อื่นตามที่คู่สัญญาฝ่ายหนึ่งฝ่ายใดจะได้แจ้งเป็นหนังสือให้อีกฝ่ายหนึ่งทราบ 
                                        แต่สำหรับการแจ้งเปลี่ยนที่อยู่นั้น ให้ถือเอาวันที่ได้รับหนังสือการแจ้งเปลี่ยนที่อยู่เป็นวันที่แจ้งได้
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date">/หนังสือสัญญานี้...........</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 95%">
                            <tr>
                                <td align="left" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้ว่าจ้าง</span></td>
                                <td align="right" class="report-font"><span id="view_promise_date">ลงชื่อ..................................................................ผู้รับจ้าง</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

            </table>
        </div>
    </div>

    <div class="printableArea1">
        <div style="width: 100%; 
             color: #000;
             border-radius: 1px;
             " >
            <table style="width: 100%;" height="907" border="0" align="center" >

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" ><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-2">
                            <img src="<?= base_url('assets/images/deposit.png') ?>" width="250" height="150" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td height="47" colspan="5">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td style="font-size:18px; font-family:'Angsana New';" >
                                    <div align="center"  style="width: 100%; display: inline-block;">
                                        เลขที่ 87 หมู่ที่ 2 ตำบลบ้านกรด อำเภอบางปะอิน  จังหวัดพระนครศรีอยุธยา 13160 โทร 035-881888-9 โทรสาร 035-881881
                                    </div>
                                    <div align="center" style="width: 100%; display: inline-block;">
                                        87 MOO 2 BANKOT BANG-PA-IN AYUTTHAYA THAILAND 13160 TEL : 035-881888-9   FAX : 035-881881
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">

                        <table style="width: 100%">
                            <tr>
                                <td  class="report-font">
                                    <div align="center" style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;"><div align="center">-8-</div></td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        หนังสือสัญญานี้มีทั้งหมด 8  หน้า ทำขึ้นเป็นสองฉบับมีข้อความถูกต้องตรงกันคู่สัญญาทั้งสองฝ่ายต่างยึดถือไว้ฝ่ายละหนึ่งฉบับ 
                                        และคู่สัญญาทั้งสองฝ่ายต่างเข้าใจในข้อความนี้โดยตลอดแล้ว จึงได้ลงลายมือชื่อไว้เป็นสำคัญต่อหน้าพยาน
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td align="left" class="report-font" >
                                    <div style="width: 450px; display: inline-block; ">
                                        ลงชื่อ...........................................................ผู้ว่าจ้าง
                                    </div>
                                    <div style="width: 390px; display: inline-block; ">
                                        ลงชื่อ......................................................ผู้รับจ้าง

                                    </div>
                                    <div style="width: 450px; display: inline-block; text-indent: 3.0em; ">
                                        <span id="cardcen1"></span>	
                                    </div>
                                    <div style="width: 390px; display: inline-block; text-indent: 1.8em;">
                                        (บริษัท พรพิศโฮม คอนสตรัคชั่น จำกัด

                                    </div>
                                    <div style="width: 450px; display: inline-block; text-indent: 3.0em;">
                                        <span id="cardcen2"></span>	
                                    </div>
                                    <div style="width: 390px; display: inline-block; text-indent: 2.2em;">
                                        โดยนางสมพิศ  ด่านชัยวิจิตร)	
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr>
                                <td align="left" class="report-font" >
                                    <div style="width: 450px; display: inline-block; ">
                                        ลงชื่อ............................................................พยาน
                                    </div>
                                    <div style="width: 390px; display: inline-block; ">
                                        ลงชื่อ.......................................................พยาน

                                    </div>
                                    <div style="width: 450px; display: inline-block; text-indent: 2.0em;">
                                        (......................................................)
                                    </div>
                                    <div style="width: 390px; display: inline-block; text-indent: 2.0em;">
                                        (......................................................)

                                    </div>
                                    <div style="width: 450px; display: inline-block; text-indent: 3.0em;">
                                        	
                                    </div>
                                    <div style="width: 390px; display: inline-block; ">


                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

                <tr>
                    <td colspan="5" height="20">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="5" height="50">
                        <table style="width: 100%">
                            <tr>
                                <td align="right" class="report-font"><span id="view_promise_date"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr valign="top">
                    <td width="90" height="50"><p>&nbsp;</p></td>
                    <td colspan="3">
                        <table style="width: 100%">
                            <tr valign="top">
                                <td class="report-font">

                                    <div style="width: 100%; display: inline-block;">
                                        เอกสารแนบท้าย ซึ่งถือเป็นส่วนหนึ่งของสัญญา
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        1.	แบบแปลนบ้านแบบ  พรพิศมณี
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        2.	รายการวัสดุก่อสร้าง
                                    </div>
                                    <div style="width: 100%; display: inline-block; text-indent: 2.0em;">
                                        3.	เอกสารประกอบของคู่สัญญา
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="90" height="50"><p>&nbsp;</p></td>
                </tr>

            </table>
        </div>
    </div>

</div>
