<style>
    .label-custom {
        /*display: inline-block;*/
        max-width: 100%;
        margin-bottom: 5px;
        font-size: 16px;
    }

    img.scanned {
        height: 200px; /** Sets the display size */
        margin-right: 12px;
    }

    div#images {
        margin-top: 20px;
    }

    u {    
        border-bottom: 2px dotted #000;
        text-decoration: none;
    }
    .map-result{
        border-bottom: 1px dashed #000;
        text-decoration: none;
    }
</style>
<div class="container pl-3 pr-3" >
    <div class="card-header">
        <label class="font-weight-bold text-white"></label>
    </div>
    <div class="card-body bg-white">

        <input type="hidden" id="hidden_user" name="hidden_user"  value="<?= $userrole ?>"/>
        <div class="row pt-2">
            <div class="col-12 text-center">
                <img  src="<?= base_url('assets/images/logo.jpg') ?>" style="width: 8%; height: 90%">
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <label class="font-weight-bold">สัญญาการจอง</label>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-left">

                <label>สัญญาการจองเลขที่ &nbsp;</label><i class="fa fa-tag" style="color: green;"></i>&nbsp;<?= $order_no ?>  
                <input type="hidden" name="hidden_order_number" id="hidden_order_number" value="<?= $order_no ?>"/>
            </div>
        </div>

        <div class="row" style="display: none;">
            <div class="col-12 text-right">
                <label id="bookdate">&nbsp;</label>

                <input id="hidden_bookdate" name="hidden_bookdate" type="hidden" >
                <input id="hidden_date_due" name="hidden_date_due" type="hidden" value="<?= $due_date_display ?>">
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <form  method="post" id='form' data-toggle="validator">
                    <input type="hidden" name="order_no" id="order_no" class="form-control" />
                    <input type="hidden" name="agreementid" id="agreementid" class="form-control"  />
                    <input type="hidden" name="input_hidden_plan_array" id="input_hidden_plan_array" class="form-control"  />
                    <input type="hidden" id="input_hidden_plan_master" name="plan_master"  value="<?= $plan_master ?>"/>
                    <input type="hidden" name="agreementid_check" id="agreementid_check" class="form-control"  />
                    <input type="hidden" name="agreementid_seq" id="agreementid_seq" class="form-control"  />


                    <input type="hidden" name="home_type" id="home_type" class="form-control" value="<?= $home_type ?>" />
                    <input type="hidden" name="home_name" id="home_name" class="form-control" value="<?= $home_name ?>" />
                    <input type="hidden" name="home_price" id="home_price" class="form-control" value="<?= $home_price ?>" />

                    <div class="row  pt-1 pl-3">
                        <div class="col-12">
                            <table >
                                <tr>
                                    <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน<span style="color: red">*</span></td>
                                    <td width="330">
                                        <input type="text" name="pid" id="pid" class="form-control " style="width:320px;  height: 30px;"  required="true"/> 
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row  pt-2 pl-3">
                        <div class="col-12">
                            <table border="0">
                                <tr>
                                    <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                                    <td width="123">
                                        <input type="text" name="title" id="title" class="form-control" style="width:90px; height: 30px;" required="true"/>
                                    </td>
                                    <td align="right" class="label-custom">ชื่อ</td>
                                    <td width="230">
                                        <input type="text" name="firstname" id="firstname" class="form-control" style="width:220px; height: 30px; " required="true"/>
                                    </td>
                                    <td align="right" class="label-custom">นามสกุล</td>
                                    <td width="230">
                                        <input type="text" name="lastname" id="lastname" class="form-control" style="width:220px;  height: 30px;"/>
                                    </td>
                                    <td align="right" class="label-custom">อายุ</td>
                                    <td >
                                        <input type="text" name="age" id="age" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);"/>
                                    </td>
                                    <td align="left" colspan="2" class="label-custom" >ปี</td>
                                    <td width="10"></td>
            <!--                        <td rowspan="2">
                                        <button type="button" class="btn btn-success" id="smardCard" style="height: 80px;">อ่านข้อมูล Smart Card</button>
                                    </td>-->
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row  pt-2 pl-3">
                        <div class="col-12">
                            <table border="0">                
                                <tr class="pt-2">
                                    <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                                    <td width="110">
                                        <input type="text" name="houseNumber" id="houseNumber" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);"/>
                                    </td>
                                    <td class="label-custom">หมู่ที่</td>
                                    <td width="120">
                                        <input type="text" name="group" id="group" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);"/>
                                    </td>
                                    <td align="right" class="label-custom">ซอย</td>
                                    <td width="225">
                                        <input type="text" name="alley" id="alley" class="form-control" style="width:180px; height: 30px;"/>
                                    </td>

                                    <td align="right" class="label-custom">ถนน</td>
                                    <td  colspan="2">
                                        <input type="text" name="street" id="street" class="form-control" style="width:260px; height: 30px;"/>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table border="0">
                                <tr>
                                    <td width="100" align="right" class="label-custom">ตำบล</td>
                                    <td width="230">
                                        <input type="text" name="subDistrict" id="subDistrict" class="form-control" style="width:200px; height: 30px;" />
                                    </td>
                                    <td align="right" class="label-custom">อำเภอ</td>
                                    <td width="230">
                                        <input type="text" name="district" id="district" class="form-control" style="width:200px; height: 30px;"/>
                                    </td>
                                    <td align="right" class="label-custom">จังหวัด</td>
                                    <td width="260">
                                        <input type="text" name="province" id="province" class="form-control" style="width:258px; height: 30px;"/>
                                    </td>
                                    <td width="18"></td>
            <!--                        <td rowspan="2">
                                        <button type="button" class="btn btn-danger" id="print" style="height: 80px; width: 173px;">เอกสาร Scan</button>
                                    </td>-->
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row  pt-2 pl-3">
                        <div class="col-12">
                            <table border="0">
                                <tr>
                                    <td width="100" align="right" class="label-custom">รหัสไปรษณีย์<span style="color: red">*</span></td>
                                    <td width="209">
                                        <input type="text" name="postcode" id="postcode" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;"/>
                                    </td>
                                    <td align="right" class="label-custom">โทรศัพท์<span style="color: red">*</span></td>
                                    <td width="240">
                                        <input type="text" name="phone" id="phone" class="form-control" style="width:200px; height: 30px;"/>
                                    </td>
                                    <td align="right" class="label-custom">อีเมล์</td>
                                    <td >
                                        <input type="email" name="email" id="email" class="form-control" style="width:258px; height: 30px;"/>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td width="100" align="right" class="label-custom" >แปลงที่</td>
                                    <td width="220">
                                        <input type="text" name="plan" id="plan" class="form-control" style="width:200px; height: 30px;" value="<?= $planssave ?>" readonly="true"/>
                                        <input type="hidden" id="hidden_plan" name="hidden_plan" value="<?= $input_form_plan ?>">
                                        <input type="hidden" id="hidden_planssave" name="hidden_planssave" value="<?= $planssave ?>">
                                    </td>
                                    <td width="55" align="right" class="label-custom">จำนวน</td>
                                    <td width="120">
                                        <input type="text" name="plan_total" id="plan_total" class="form-control" style="width:110px; height: 30px;"  readonly="true" value=" <?= count($planssave) ?>"/>
                                    </td>
                                    <td width="60" class="label-custom">แปลง</td>
                                    <td width="100" align="right" class="label-custom">ราคาตารางวา</td>
                                    <td width="260">
                                        <input type="text" name="planprice" id="planprice" class="form-control" style="width:255px; height: 30px;" value="<?= $input_form_price_tarangwa ?>" readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
            <!--                        <td rowspan="2">
                                        <button type="button" class="btn btn-primary" id="ex" style="height: 80px; width: 173px;">แสดงตัวอย่าง</button> 
                                    </td>-->
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td style="width: 105px;" align="right" class="label-custom">แปลงที่ดินเปล่า</td>
                                    <td>
                                        <div id="check_land" style="font-size: 15px; display: inline-block; padding-left: 20px;"></div>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td style="width: 100px;" align="right" class="label-custom">ราคาแปลง</td>
                                    <td style="width: 136px;">
                                        <input type="text" name="plan_price_order" id="plan_price_order" class="form-control" style="width:129px; height: 30px;"  readonly="true"/>
                                    </td>
                                    <td width="30" class="label-custom">บาท</td>  
                                    <td width="325" class="label-custom"><span id="plan_price_order_char"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td style="width: 100px;" align="right" class="label-custom">ราคาที่ดินเปล่า</td>
                                    <td style="width: 136px;">
                                        <input type="text" name="check_land_price" id="check_land_price" class="form-control" style="width:129px; height: 30px;"  readonly="true"/>
                                    </td>
                                    <td width="30" class="label-custom">บาท</td>  
                                    <td width="325" class="label-custom"><span id="check_land_price_char"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td width="100" align="right" class="label-custom">รวมเป็นเงิน</td>
                                    <td width="140">
                                        <input type="text" name="pricetotal" id="pricetotal" class="form-control" style="width:130px; height: 30px;" value="<?= $input_form_price ?>" readonly="true"/>
                                        <input type="hidden" name="pricetotal_hidden" id="pricetotal_hidden"  value="<?= $input_form_price ?>" />
                                    </td>
                                    <td width="30" class="label-custom">บาท</td>
                                    <td width="325" class="label-custom"><span id="pricetotal_char"></span></td>
                                <input type="hidden" value="<?= $input_form_price_word ?>" id="input_form_price_word">
                                </tr>
                            </table>
                        </div>
                    </div>


                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td width="100" align="right" class="label-custom">แบบบ้าน</td>
                                    <td width="140">
                                        <input type="text" name="input_home_name" id="input_home_name" class="form-control" style="width:130px; height: 30px;" value="<?= $home_name ?>" readonly="true"/>
                                    </td>
                                    
                                    <td width="58" align="right" class="label-custom">ราคา</td>
                                    <td width="120">
                                        <input type="text" name="input_home_price" id="input_home_price" class="form-control" style="width:142px; height: 30px;"  readonly="true" value=" <?= $home_price ?>"/>
                                    </td>
                                    <td width="74" class="label-custom">บาท</td>
                                    <td width="400" class="label-custom"><span id="input_home_price_char"></span>

                                    </td>   
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td width="100" align="right" class="label-custom">วันที่จอง</td>
                                    <td width="210">
                                        <input type="text" name="order_date" id="order_date" class="form-control" readonly="true" style="width:200px; height: 30px;"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td width="100" align="right" class="label-custom">ชำระเงินจอง<span style="color: red">*</span></td>
                                    <td width="210">
                                        <input type="text" name="pricepay" id="pricepay" class="form-control" oninput="numberOnly(this.id);" style="width:200px; height: 30px;"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
                                    <td width="400" class="label-custom"><span id="order_price_char"></span>
                                        <input type="hidden" id="hidden_order_price_char"/>
                                    </td>

                                    <td rowspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>



                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td width="135" align="right" class="label-custom">วันที่นัดทำสัญญา </td>
                                    <td width="300">
                                        <input id="dateorder" type="text" name="dateorder" class="form-control"  style="width:180px; height: 30px;">
                                        <input id="hidden_datepicker" name="hidden_datepicker" type="hidden">
                                        <input id="hidden_dateorder_newdate" name="hidden_dateorder_newdate" type="hidden">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row  pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td width="80" class="label-custom">หมายเหตุ <label class="text-danger pt-2">*</label> </td>
                                    <td>
                                        <small>
                                            หากพ้นกำหนดดังกล่าว
                                            ให้ถือว่าใบจองนี้เป็นอันยกเลิกและข้าพเจ้ายินยอมให้ค่าจองนี้ตกเป็น
                                            กรรมสิทธิ์ของบริษัท ตติเชษฐ์ จำกัด ทันทีโดยไม่โต้แย้งใด ๆ ทั้งสิ้น
                                        </small>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </form>
                <form id='redirectForm' method='POST' action=''>

                    <!--data to page scan-->
                    <input type="hidden" name="order_no_scan" id="order_no_scan" value="<?= $order_no ?>"/>
                    <input type="hidden" id="input_form_plan_save_scan"  name="input_form_plan_save_scan"  value="<?= $planssave ?>"/>
                    <input type="hidden" id="input_hidden_plan_master_scan" name="plan_master_scan"  value="<?= $plan_master ?>"/>
                    <input type="hidden" id="input_form_price_scan"  name="input_form_price_scan"  value="<?= $input_form_price ?>"/>
                    <input type="hidden" id="input_form_price_tarangwa_scan"  name="input_form_price_tarangwa_scan"  value="<?= $input_form_price_tarangwa ?>"/>
                    <input type="hidden" id="input_form_plan_scan"  name="input_form_plan_scan"  value="<?= $input_form_plan ?>"/>
                    <input type="hidden" id="due_date_order"  name="due_date_order"  value="<?= $due_date_order ?>"/>
                    <input type="hidden" id="due_date_display"  name="due_date_display"  value="<?= $due_date_display ?>"/>
                    <input type="hidden" id="input_form_price_word"  name="input_form_price_word"  value="<?= $input_form_price_word ?>"/>
                    <input type="hidden" id="plan_master_id" name="plan_master_id"  value="<?= $plan_master ?>"/>
                    <input type="hidden" id="scan_home_type"  name="scan_home_type"  value="<?= $home_type ?>"/>
                    <input type="hidden" id="scan_home_name"  name="scan_home_name"  value="<?= $home_name ?>"/>
                    <input type="hidden" id="scan_home_price" name="scan_home_price"  value="<?= $home_price ?>"/>


                    <div class="row  pt-3 pl-5">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-success" id="smardCard" style="height: 50px;">อ่านข้อมูล Smart Card</button> 
                            <button type="button" class="btn btn-warning" id="scancard" style="height: 50px; ">Scan สำเนาบัตร</button>
                            <button type="button" class="btn btn-danger" id="scan" style="height: 50px; ">เอกสาร Scan</button>
                            <button type="button" class="btn btn-dark" id="ex" style="height: 50px; ">แสดงตัวอย่าง</button>
                            <button type="button" class="btn btn-primary" id="btn_prommise" style="height: 50px; ">ทำสัญญาซื้อขาย</button>
                            <!--<button type="button" class="btn btn-dark" id="crop" style="height: 50px; ">Crop</button>-->
                        </div>
                    </div>
                    <div class="row pt-2 ">
                        <div class="col-12 text-center" >
                            <div id="images" style="display: none;">
                            </div>
                        </div>

                    </div>
                </form>
                <form id="formredirec_promise" method="post" action='#'>
                    <input type="hidden" id="hiden_person_order_no" name="hiden_person_order_no"  value="<?= $order_no ?>"/>
                    <input type="hidden" id="hiden_person_master_plan" name="hiden_person_master_plan"  value="<?= $plan_master ?>"/>
                    <input type="hidden" id="person_plan_save" name="person_plan_save"  value="<?= $planssave ?>"/>
                </form>



            </div>
        </div>

        <div class="row  pt-3 pl-5">
            <div class="col-12 text-center">
                <button type="button" class="btn btn-primary" id="btn_save">บันทึกข้อมูลการจอง</button> 
                <button type="button" class="btn btn-warning" id="btn_edit">บันทึกข้อมูลแก้ไขการจอง</button>
            </div>
        </div>
    </div>
    <!--modal information-->
    <div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message">
        <div class="modal-dialog modal-sm" style="max-width: 470px;">
            <div class="modal-content">

                <div style="border-bottom: 1px solid #e9ecef;">
                    <div class="text-center">
                        <pre class="text-center pt-3" id="idmessage"></pre>
                    </div>

                </div>
                <div class="modal-body p-2">
                    <div class="text-center">
                        <button type="button" class="btn btn-warning" id="modal-btn-n-message" style="width: 30%;" >ตกลง</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--modal-->
    <div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
        <div class="modal-dialog modal-sm" style="max-width: 350px;">
            <div class="modal-content">
                <div style="border-bottom: 1px solid #e9ecef;">
                    <div class="text-center">
                        <div class="row p-2">
                            <div class="col-11 text-center pt-2"><pre class="text-center " id="lablepopup"></pre></div>
                            <div class="col-1 text-left pt-1"><button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button></div>
                        </div>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <button type="button" class="btn btn-primary" id="modal-btn-y-edit" style="width: 30%;">Yes</button>
                        <button type="button" class="btn btn-danger" id="modal-btn-n-edit" style="width: 30%;">No</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'view_agreement.php'; ?>
<?php include 'view_deposit.php'; ?>

