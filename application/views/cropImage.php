<style>
    .container {
        max-width: 640px;
        margin: 20px auto;
    }

    img {
        max-width: 100%;
    }
</style>
<div class="container">
    <h1>Minimum and maximum cropped dimensions</h1>
    <h3>Image</h3>
    <div>
        <!--scancard/P1-2532561.jpg-->
        <img id="image" src="<?= base_url('assets/images/picture.jpg') ?>" alt="Picture" >
    </div>
    <p>Maximum Width: 640, Maximum Height: 320, Minimum Width: 320, Minimum Height: 160</p>
    <p>Data: <span id="data"></span></p>
    <h3>Result</h3>
    <p>
        <button type="button" id="button">Crop</button>
    </p>
    <div id="result"></div>
</div>
