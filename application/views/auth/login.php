<link type="text/css" href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" id="bootstrap-css"/>
<link type="text/css" href="<?= base_url('assets/css/login.css') ?>" rel="stylesheet" />
<link rel="icon" href="<?= base_url('assets/images/logo.jpg') ?>" type="image/x-icon">
<script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/jquery/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('js/login.js') ?>"></script>

<!--<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar">
    <a class="navbar-brand mr-0 mr-md-2" href="/" aria-label="Bootstrap">

    </a>
</header>-->

<!--<div class="navbar-inner">
    <div class="container-fluid">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <a href="http://bootply.com/" class="brand" title="Bootstrap editor, templates and snippets.">
            Bootply
        </a>
    </div>
</div>-->
<nav class="navbar navbar-default" style="background-color: #0d5000; height: 8%">
    <div style="width: 100%; padding-right: 15px;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <img  src="<?= base_url('assets/images/logo.jpg') ?>" style="width: 38%;">
            </a>
        </div>
    </div>
</nav>
<section class="login-block">
    <div class="container">
        <div class="row">
            <div class="col-md-4 login-sec">
                <h2 class="text-center">Login</h2>
                <form class="login-form" method="post" action="<?=base_url('auth/login')?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="text-uppercase">Username</label>
                        <input type="text" id="identity" name="identity"class="form-control" placeholder="">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="text-uppercase">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="">
                    </div>


                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="1"  id="remember" name="remember">
                            <small>Remember Me</small>
                        </label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--                        <label class="form-check-label">
                            <a href="<?//= base_url('auth/register') ?>"><small class="text-info">Register</small></a>
                        </label>-->
                        <input type="submit" class="btn btn-login float-right" value="Login" />
                    </div>

                </form>
                <div class="copy-text">Created with <i class="fa fa-heart"></i> by <a href="http://pornpis.com">pornpis.com</a></div>
            </div>
            <div class="col-md-8 banner-sec">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <!--                    <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                        </ol>-->
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">

                        </div>
                        <!--                        <div class="carousel-item">
                                                    <img class="d-block img-fluid" src="https://images.pexels.com/photos/7097/people-coffee-tea-meeting.jpg" alt="First slide">
                                                    <div class="carousel-caption d-none d-md-block">
                                                        <div class="banner-text">
                                                            <h2>This is Heaven</h2>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                                                        </div>	
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img class="d-block img-fluid" src="https://images.pexels.com/photos/872957/pexels-photo-872957.jpeg" alt="First slide">
                                                    <div class="carousel-caption d-none d-md-block">
                                                        <div class="banner-text">
                                                            <h2>This is Heaven</h2>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                                                        </div>	
                                                    </div>
                                                </div>-->
                    </div>	   

                </div>
            </div>
        </div>
</section>

