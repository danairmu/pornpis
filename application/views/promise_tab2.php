<!--บ้าน-->
<div>
    <div class="pt-2"></div>
    <div class="row">
        <div class="col-12 text-right ">
            <div class="m-auto p-1" >
                <!--<button type = "button"  onclick="showOptionModalManualTab2()" class="btn btn-primary fa fa-user-plus  waves-effect p-1">เพิ่มบุคคล Manual</button>-->
                <!--<button type = "button"  onclick="showOptionModalSmartCardTab2()" class="btn btn-primary fa fa-user-plus  waves-effect p-1">เพิ่มบุคคล SmartCard</button>-->
            </div>
        </div>
    </div>

    <hr class="bg-success">
    <div class="pt-2"></div>
    <div class="row pt-2">
        <div class="col-12 text-center">
            <img  src="<?= base_url('assets/images/logo.jpg') ?>" style="width: 8%; height: 90%">
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <label class="font-weight-bold">สัญญาจ้างก่อสร้างบ้าน</label>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-left">
            <label>สัญญาซื้อขายเลขที่ &nbsp;</label><i class="fa fa-tag" style="color: green;"></i>&nbsp;<label id="promisehomeid"></label> 
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            <label style="display: none;">&nbsp;<?= $due_date_display ?></label>
            <input id="hidden_date_due2" name="hidden_date_due2" type="hidden" value="<?= $due_date_display ?>">
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <form  method="post" id='form2' data-toggle="validator">
                <input type="hidden" name="order_no2" id="order_no2" class="form-control" />
                <input type="hidden" name="agreementid2" id="agreementid2" class="form-control"  />
                <input type="hidden" name="input_hidden_plan_array2" id="input_hidden_plan_array2" class="form-control"  />
                <input type="hidden" id="input_hidden_plan_master2" name="plan_master2"  value=" plan_master "/>
                <input type="hidden" name="agreementid_check2" id="agreementid_check2" class="form-control"  />
                <input type="hidden" name="agreementid_seq2" id="agreementid_seq2" class="form-control"  />

                <input type="hidden" name="pid_scan_tab2" id="pid_scan_tab2"  />
                <input type="hidden" name="flag_pid_scan_tab2" id="pid_scan_tab2" value="2"  />
                <div class="div-detail">
                    <div id="list-detail-person_home1" >
                        <form id="formlisthome1">
                            <div class="pt-1 pl-3"> 
                                <table>
                                    <tr>
                                        <td style="width: 0px;"></td>
                                        <td><label class="font-weight-bold"><u>ข้อมูลบุคคล</u></label></td>
                                    </tr>
                                </table>
                            </div>

                            <div class="row  pt-1 pl-3">
                                <div class="col-12">
                                    <table >
                                        <tr>
                                            <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน</td>
                                            <td width="330">
                                                <input type="text" name="pid4" id="pid4" class="form-control " style="width:320px;  height: 30px;"  required="true" readonly="true"/> 
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row  pt-2 pl-3">
                                <div class="col-12">
                                    <table border="0">
                                        <tr>
                                            <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                                            <td width="123">
                                                <input type="text" name="title4" id="title4" class="form-control" style="width:90px; height: 30px;" required="true" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">ชื่อ</td>
                                            <td width="230">
                                                <input type="text" name="firstname4" id="firstname4" class="form-control" style="width:220px; height: 30px; " required="true" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">นามสกุล</td>
                                            <td width="230">
                                                <input type="text" name="lastname4" id="lastname4" class="form-control" style="width:220px;  height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">อายุ</td>
                                            <td >
                                                <input type="text" name="age4" id="age4" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                            </td>
                                            <td align="left" colspan="2" class="label-custom" >ปี</td>
                                            <td width="10"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 pl-3">
                                <div class="col-12">
                                    <table border="0">                
                                        <tr class="pt-2">
                                            <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                                            <td width="110">
                                                <input type="text" name="houseNumber4" id="houseNumber4" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);" readonly="true"/>
                                            </td>
                                            <td class="label-custom">หมู่ที่</td>
                                            <td width="120">
                                                <input type="text" name="group4" id="group4" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">ซอย</td>
                                            <td width="225">
                                                <input type="text" name="alley4" id="alley4" class="form-control" style="width:180px; height: 30px;" readonly="true"/>
                                            </td>

                                            <td align="right" class="label-custom">ถนน</td>
                                            <td  colspan="2">
                                                <input type="text" name="street4" id="street4" class="form-control" style="width:260px; height: 30px;" readonly="true"/>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row pt-2 pl-3">
                                <div class="col-12">
                                    <table border="0">
                                        <tr>
                                            <td width="100" align="right" class="label-custom">ตำบล</td>
                                            <td width="230">
                                                <input type="text" name="subDistrict4" id="subDistrict4" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">อำเภอ</td>
                                            <td width="230">
                                                <input type="text" name="district4" id="district4" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">จังหวัด</td>
                                            <td width="260">
                                                <input type="text" name="province4" id="province4" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td width="18"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 pl-3 pb-4">
                                <div class="col-12">
                                    <table border="0">
                                        <tr>
                                            <td width="100" align="right" class="label-custom">รหัสไปรษณีย์</td>
                                            <td width="215">
                                                <input type="text" name="postcode4" id="postcode4" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">โทรศัพท์</td>
                                            <td width="240">
                                                <input type="text" name="phone4" id="phone4" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">อีเมล์</td>
                                            <td >
                                                <input type="email" name="email4" id="email4" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                    </div>

                    <div id="list-detail-person_home2" style="display: none;">
                        <hr>
                        <form id="formlisthome2">
                            <div class="row  pt-1 pl-3">
                                <div class="col-12">
                                    <table >
                                        <tr>
                                            <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน</td>
                                            <td width="330">
                                                <input type="text" name="pid5" id="pid5" class="form-control " style="width:320px;  height: 30px;"  required="true" readonly="true"/> 
                                                
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row  pt-2 pl-3">
                                <div class="col-12">
                                    <table border="0">
                                        <tr>
                                            <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                                            <td width="123">
                                                <input type="text" name="title5" id="title5" class="form-control" style="width:90px; height: 30px;" required="true" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">ชื่อ</td>
                                            <td width="230">
                                                <input type="text" name="firstname5" id="firstname5" class="form-control" style="width:220px; height: 30px; " required="true" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">นามสกุล</td>
                                            <td width="230">
                                                <input type="text" name="lastname5" id="lastname5" class="form-control" style="width:220px;  height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">อายุ</td>
                                            <td >
                                                <input type="text" name="age5" id="age5" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                            </td>
                                            <td align="left" colspan="2" class="label-custom" >ปี</td>
                                            <td width="10"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 pl-3">
                                <div class="col-12">
                                    <table border="0">                
                                        <tr class="pt-2">
                                            <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                                            <td width="110">
                                                <input type="text" name="houseNumber5" id="houseNumber5" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);" readonly="true"/>
                                            </td>
                                            <td class="label-custom">หมู่ที่</td>
                                            <td width="120">
                                                <input type="text" name="group5" id="group5" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">ซอย</td>
                                            <td width="225">
                                                <input type="text" name="alley5" id="alley5" class="form-control" style="width:180px; height: 30px;" readonly="true"/>
                                            </td>

                                            <td align="right" class="label-custom">ถนน</td>
                                            <td  colspan="2">
                                                <input type="text" name="street5" id="street5" class="form-control" style="width:260px; height: 30px;" readonly="true"/>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row pt-2 pl-3">
                                <div class="col-12">
                                    <table border="0">
                                        <tr>
                                            <td width="100" align="right" class="label-custom">ตำบล</td>
                                            <td width="230">
                                                <input type="text" name="subDistrict5" id="subDistrict5" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">อำเภอ</td>
                                            <td width="230">
                                                <input type="text" name="district5" id="district5" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">จังหวัด</td>
                                            <td width="260">
                                                <input type="text" name="province5" id="province5" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td width="18"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 pl-3 pb-4">
                                <div class="col-12">
                                    <table border="0">
                                        <tr>
                                            <td width="100" align="right" class="label-custom">รหัสไปรษณีย์</td>
                                            <td width="215">
                                                <input type="text" name="postcode5" id="postcode5" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">โทรศัพท์</td>
                                            <td width="240">
                                                <input type="text" name="phone5" id="phone5" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">อีเมล์</td>
                                            <td >
                                                <input type="email" name="email5" id="email5" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                    </div>

                    <div id="list-detail-person_home3" style="display: none;" >
                        <hr>
                        <form id="formlisthome3">
                            <div class="row  pt-1 pl-3">
                                <div class="col-12">
                                    <table >
                                        <tr>
                                            <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน</td>
                                            <td width="330">
                                                <input type="text" name="pid6" id="pid6" class="form-control " style="width:320px;  height: 30px;"  required="true" readonly="true"/> 
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row  pt-2 pl-3">
                                <div class="col-12">
                                    <table border="0">
                                        <tr>
                                            <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                                            <td width="123">
                                                <input type="text" name="title6" id="title6" class="form-control" style="width:90px; height: 30px;" required="true" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">ชื่อ</td>
                                            <td width="230">
                                                <input type="text" name="firstname6" id="firstname6" class="form-control" style="width:220px; height: 30px; " required="true" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">นามสกุล</td>
                                            <td width="230">
                                                <input type="text" name="lastname6" id="lastname6" class="form-control" style="width:220px;  height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">อายุ</td>
                                            <td >
                                                <input type="text" name="age6" id="age6" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                            </td>
                                            <td align="left" colspan="2" class="label-custom" >ปี</td>
                                            <td width="10"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 pl-3">
                                <div class="col-12">
                                    <table border="0">                
                                        <tr class="pt-2">
                                            <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                                            <td width="110">
                                                <input type="text" name="houseNumber6" id="houseNumber6" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);" readonly="true"/>
                                            </td>
                                            <td class="label-custom">หมู่ที่</td>
                                            <td width="120">
                                                <input type="text" name="group6" id="group6" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">ซอย</td>
                                            <td width="225">
                                                <input type="text" name="alley6" id="alley6" class="form-control" style="width:180px; height: 30px;" readonly="true"/>
                                            </td>

                                            <td align="right" class="label-custom">ถนน</td>
                                            <td  colspan="2">
                                                <input type="text" name="street6" id="street6" class="form-control" style="width:260px; height: 30px;" readonly="true"/>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row pt-2 pl-3">
                                <div class="col-12">
                                    <table border="0">
                                        <tr>
                                            <td width="100" align="right" class="label-custom">ตำบล</td>
                                            <td width="230">
                                                <input type="text" name="subDistrict6" id="subDistrict6" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">อำเภอ</td>
                                            <td width="230">
                                                <input type="text" name="district6" id="district6" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">จังหวัด</td>
                                            <td width="260">
                                                <input type="text" name="province6" id="province6" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td width="18"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row  pt-2 pl-3 pb-4">
                                <div class="col-12">
                                    <table border="0">
                                        <tr>
                                            <td width="100" align="right" class="label-custom">รหัสไปรษณีย์</td>
                                            <td width="215">
                                                <input type="text" name="postcode6" id="postcode6" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">โทรศัพท์</td>
                                            <td width="240">
                                                <input type="text" name="phone6" id="phone6" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                            </td>
                                            <td align="right" class="label-custom">อีเมล์</td>
                                            <td >
                                                <input type="email" name="email6" id="email6" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                    </div>

                </div>
                <div class="pt-1"></div>
                <div class="div-detail">

                    <div class="pt-3 pl-3"> 
                        <table>
                            <tr>
                                <td style="width: 0px;"></td>
                                <td><label class="font-weight-bold">รายละเอียดข้อมูลที่ดิน</label></td>
                            </tr>
                        </table>
                    </div>

                    <div class="row  pt-1 pl-2">
                        <div class="col-12">
                            <table >
                                <tr>
                                    <td align="right"  class="label-custom">โฉนดที่ดินเลขที่</td>
                                    <td width="330">
                                        <input type="text" name="landid2" id="landid2" class="form-control " style="width:320px;  height: 30px;"  required="true"/> 
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row  pt-1 pl-3">
                        <div class="col-12">
                            <table >
                                <tr>
                                    <td style="width: 100px;" align="right"  class="label-custom">ที่ดินเลขที่</td>
                                    <td width="330">
                                        <input type="text" name="landid3" id="landid3" class="form-control " style="width:320px;  height: 30px;"  required="true"/> 
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td width="100" align="right" class="label-custom">ส่วนลด</td>
                                    <td width="140">
                                        <input type="text" name="price_reduct" id="price_reduct" class="form-control" style="width:230px; height: 30px;" oninput="floatOnly(this.id);"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
                                    <td width="225" class="label-custom"><span id="prict_reduct_char"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td width="100" align="right" class="label-custom" >แปลงที่</td>
                                    <td width="220">
                                        <input type="text" name="plan2" id="plan2" class="form-control" style="width:200px; height: 30px;" value="<?= $planssave ?>" readonly="true"/>
                                        <input type="hidden" id="hidden_plan2" name="hidden_plan2" value=" input_form_plan ">
                                        <input type="hidden" id="hidden_planssave2" name="hidden_planssave2" value=" <?= $planssave ?> ">
                                    </td>
                                    <td width="55" align="right" class="label-custom">จำนวน</td>
                                    <td width="120">
                                        <input type="text" name="plan_total2" id="plan_total2" class="form-control" style="width:110px; height: 30px;"  readonly="true" />
                                    </td>
                                    <td width="60" class="label-custom">แปลง</td>
                                    <td width="100" align="right" class="label-custom">ราคาตารางวา</td>
                                    <td width="260">
                                        <input type="text" name="planprice2" id="planprice2" class="form-control" style="width:255px; height: 30px;" readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td style="width: 105px;" align="right" class="label-custom">แปลงที่ดินเปล่า</td>
                                    <td style="width: 500px;">
                                        <div id="check_land2" style="display: inline-block; padding-left: 20px;"></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td style="width: 100px;" align="right" class="label-custom">ราคาแปลง</td>
                                    <td style="width: 136px;">
                                        <input type="text" name="plan_price_tab2" id="plan_price_tab2" class="form-control" style="width:125px; height: 30px;"  readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>    
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td style="width: 100px;" align="right" class="label-custom">ราคาที่ดินเปล่า</td>
                                    <td style="width: 136px;">
                                        <input type="text" name="check_land_price2" id="check_land_price2" class="form-control" style="width:125px; height: 30px;"  readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>    
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td width="100" align="right" class="label-custom">รวมเป็นเงิน</td>
                                    <td width="140">
                                        <input type="text" name="pricetotal2" id="pricetotal2" class="form-control" style="width:130px; height: 30px;" readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
                                    <td width="325" class="label-custom"><span id="total_price_char2"></span></td>
                                <input type="hidden" value="input_form_price_word" id="input_form_price_word2">
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td width="100" align="right" class="label-custom">ชำระเงินจอง</td>
                                    <td width="210">
                                        <input type="text" name="pricepay2" id="pricepay2" class="form-control" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
                                    <td width="400" class="label-custom"><span id="order_price_char2"></span>
                                        <input type="hidden" id="hidden_order_price_char2"/>
                                    </td>

                                    <td rowspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 138px;" align="right" class="label-custom" >แบบบ้าน</td>
                                    <td style="width: 348px;">
                                        <input type="text" name="home_name2" id="home_name2" class="form-control" style="width:200px; height: 30px;"  readonly="true"/>
                                        <input type="hidden" name="home_id2" id="home_id2" />
                                    </td>
                                    <td width="55" align="right" class="label-custom">ราคา</td>
                                    <td style="width: 196px;">
                                        <input type="text" name="home_price2" id="home_price2" class="form-control" style="width:180px; height: 30px;"  readonly="true" />
                                    </td>
                                    <td width="550" class="label-custom"><span id="home_price_char2"></span></td>

                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    
                                    <td style="width: 115px;" align="right" class="label-custom">วันที่จอง </td>
                                    <td style="width: 207px;">
                                        <input id="db_order_date2" type="text" name="db_order_date2" class="form-control"  style="width:193px; height: 30px;" readonly="true">
                                        <input id="db_order_date_hidden2" name="db_order_date_hidden2" type="hidden">
                                    </td>
                                    
                                    <td style="width: 141px;" align="right" class="label-custom">วันที่ทำสัญญาจอง </td>
                                    <td width="300">
                                        <input id="date_create_agreenment2" type="text" name="date_create_agreenment2" class="form-control"  style="width:180px; height: 30px;" readonly="true">  
                                        <input id="db_hidden_date_create_agreenment2" name="db_hidden_date_create_agreenment2" type="hidden">
                                    </td>
                                    
                                    
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row pt-2 pl-0 pb-4">
                        <div class="col-12">
                            <table>
                                <tr>
                                    
                                    <td style="width: 122px;" align="right" class="label-custom">วันที่นัดทำสัญญา </td>
                                    <td width="300">
                                        <input id="dateorder2" type="text" name="dateorder2" class="form-control"  style="width:180px; height: 30px;" readonly="true">
                                        <input id="hidden_datepicker2" name="hidden_datepicker2" type="hidden">
                                        <input id="hidden_dateorder_newdate2" name="hidden_dateorder_newdate2" type="hidden">
                                        <input id="db_agerrment_date_hidden2" name="db_agerrment_date_hidden2" type="hidden">
                                    </td>
                                    
                                    
                                    
                                    
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    
                    
                </div>

                <div class="pt-1"></div>
                <div class="div-detail">
                    <div class="pt-3 pl-3"> 
                        <table>
                            <tr>
                                <td style="width: 0px;"></td>
                                <td><label class="font-weight-bold">รายละเอียดการชำระเงิน</label></td>
                            </tr>
                        </table>
                    </div>


                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 287px;" align="right" class="label-custom">โดยชำระเงินค่ามัดจำก่อสร้างบ้านเป็นเงิน</td>
                                    <td width="200">
                                        <input id="pay_price_home" type="text" name="pay_price_home" class="form-control"  style="width:180px; height: 30px;" oninput="numberOnly(this.id);">
                                    </td>
                                    <td width="550" class="label-custom"><span id="pay_price_create_home_char"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <!--                    <div class="row pt-2 pl-0">
                                            <div class="col-12">
                                                <table>
                                                    <tr>
                                                        <td style="width: 347px;" align="right" class="label-custom">และชำระเงินทำสัญญา ณ วันทำสัญญาเป็นเงิน</td>
                                                        <td width="200">
                                                            <input id="pay_price_home_date" type="text" name="pay_price_home_date" class="form-control"  style="width:180px; height: 30px;" oninput="numberOnly(this.id);">
                                                        </td>
                                                        <td width="550" class="label-custom"><span id="pay_price_create_home_date_char"></span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>-->

                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 472px;" align="right" class="label-custom">หลังจากนั้นจะชำระค่าจ้างก่อสร้างงานตามสัญญาออกเป็นงวดงาน ดังนี้</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 131px;" align="right" class="label-custom">งวดที่ 1	ชำระเงิน</td>
                                    <td width="200">
                                        <input id="pay_price_pirod1" type="text" name="pay_price_pirod1" class="form-control"  style="width:180px; height: 30px;" oninput="numberOnly(this.id);">
                                    </td>
                                    <td width="550" class="label-custom"><span id="pay_home_price_period1_char"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 131px;" align="right" class="label-custom">งวดที่ 2	ชำระเงิน</td>
                                    <td width="200">
                                        <input id="pay_price_pirod2" type="text" name="pay_price_pirod2" class="form-control"  style="width:180px; height: 30px;" oninput="numberOnly(this.id);">
                                    </td>
                                    <td width="550" class="label-custom"><span id="pay_home_price_period2_char"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 131px;" align="right" class="label-custom">งวดที่ 3	ชำระเงิน</td>
                                    <td width="200">
                                        <input id="pay_price_pirod3" type="text" name="pay_price_pirod3" class="form-control"  style="width:180px; height: 30px;" oninput="numberOnly(this.id);">
                                    </td>
                                    <td width="550" class="label-custom"><span id="pay_home_price_period3_char"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 131px;" align="right" class="label-custom">งวดที่ 4	ชำระเงิน</td>
                                    <td width="200">
                                        <input id="pay_price_pirod4" type="text" name="pay_price_pirod4" class="form-control"  style="width:180px; height: 30px;" oninput="numberOnly(this.id);">
                                    </td>
                                    <td width="550" class="label-custom"><span id="pay_home_price_period4_char"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row pt-2 pl-0 pb-4">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 131px;" align="right" class="label-custom">งวดที่ 5	ชำระเงิน</td>
                                    <td width="200">
                                        <input id="pay_price_pirod5" type="text" name="pay_price_pirod5" class="form-control"  style="width:180px; height: 30px;" oninput="numberOnly(this.id);">
                                    </td>
                                    <td width="550" class="label-custom"><span id="pay_home_price_period5_char"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
                
                <div class="pt-1"></div>
                <div class="div-detail" >

                    <div class="pt-3 pl-3"> 
                        <table>
                            <tr>
                                <td style="width: 0px;"></td>
                                <td><label class="font-weight-bold"><u>ข้อมูลพยาน</u></label></td>
                            </tr>
                        </table>
                    </div>

                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 160px;" align="right" class="label-custom">พยานบุคคลที่ 1. ชื่อ</td>
                                    <td style="width: 230px;">
                                        <input type="text" name="witness2_name1" id="witness2_name1" class="form-control"  style="width:200px; height: 30px;"  value="นายกฤษณะ"/>
                                    </td>
                                    <td style="width: 45px;" class="label-custom">นามสกุล</td>
                                    <td style="width: 230px;">
                                        <input type="text" name="witness2_lastname1" id="witness2_lastname1" class="form-control" style="width:200px; height: 30px;" value="จำสนิท"/>
                                    </td>

                                    <td rowspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-0 pb-4">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 160px;" align="right" class="label-custom">พยานบุคคลที่ 2. ชื่อ</td>
                                    <td style="width: 230px;">
                                        <input type="text" name="witness2_name2" id="witness2_name2" class="form-control"  style="width:200px; height: 30px;" value="นายศุภกร"/>
                                    </td>
                                    <td style="width: 45px;" class="label-custom">นามสกุล</td>
                                    <td style="width: 230px;">
                                        <input type="text" name="witness2_lastname2" id="witness2_lastname2" class="form-control"  style="width:200px; height: 30px;" value="ตรีโรจน์"/>
                                    </td>

                                    <td rowspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
<!--                <div class="pt-1 "></div>
                <div class="div-detail" >

                    <div class="pt-3 pl-3"> 
                        <table>
                            <tr>
                                <td style="width: 0px;"></td>
                                <td><label class="font-weight-bold"><u>เลือกการออกแบบสัญญา</u></label></td>
                            </tr>
                        </table>
                    </div>
                    <div class="pl-3 pb-4">
                        <table>
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckHome1">
                                        <label class="form-check-label" for="exampleCheckHome1">สัญญาก่อสร้างบ้านกรณีที่ 1</label>
                                    </div>
                                </td>
                                <td style="padding-left: 10px;">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckHome2">
                                        <label class="form-check-label" for="exampleCheckHome2">สัญญาก่อสร้างบ้านกรณีที่ 3</label>
                                    </div>
                                </td>
                                <td style="padding-left: 10px;">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckHome3">
                                        <label class="form-check-label" for="exampleCheckHome3">สัญญาก่อสร้างบ้านกรณีที่ 4</label>
                                    </div>
                                </td>
                                <td style="padding-left: 10px;">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckHome4">
                                        <label class="form-check-label" for="exampleCheckHome4">สัญญาก่อสร้างบ้านกรณีที่ 5</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckAttached1">
                                        <label class="form-check-label" for="exampleCheckAttached1">ต่อท้ายกรณีที่ 1</label>
                                    </div>
                                </td>
                                <td style="padding-left: 10px;">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckAttached2">
                                        <label class="form-check-label" for="exampleCheckAttached2">ต่อท้ายกรณีที่ 2</label>
                                    </div>
                                </td>
                                <td style="padding-left: 10px;">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckAttached3">
                                        <label class="form-check-label" for="exampleCheckAttached3">ต่อท้ายกรณีที่ 3</label>
                                    </div>
                                </td>
                                <td style="padding-left: 10px;">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckAttached4">
                                        <label class="form-check-label" for="exampleCheckAttached4">ต่อท้ายกรณีที่ 4</label>
                                    </div>
                                </td>
                                <td style="padding-left: 15px;">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckAttached5">
                                        <label class="form-check-label" for="exampleCheckAttached5">ต่อท้ายกรณีที่ 5</label>
                                    </div>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
                </div>-->
                
                <div  id="divscoretab2"></div>
                <div class="pt-1"></div>
                <div >
                    <div class="pt-1"></div>
                    <div >
                        <div class="row pt-2 pl-1">
                            <label class="text-dark pl-2 font-weight-bold">บุคคลที่เกี่ยวข้องกับสัญญา</label>
                        </div>
                        <div class="row pt-0">
                            <div style="width: 100%;" class="pl-2 pr-2" >
                                <table id="personlisttab2" class="display responsive nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead class="thead-light bg-success">
                                        <tr>
                                            <th scope="col" width="8%" class="text-center">ลำดับที่</th>
                                            <th scope="col" class="text-center">บัตรประจำตัวประชาชน</th>
                                            <th scope="col" class="text-center">ชื่อ-นามสกุล</th>
                                            <th scope="col" class="text-center">ที่อยู่</th>
                                            <th scope="col" class="text-center">สถานะสแกน</th>
                                            <th scope="col" width="8%" class="text-center">เลือก</th>
<!--                                            <th scope="col" width="8%" class="text-center">ยกเลิก</th>
                                            <th scope="col" width="8%" class="text-center">สแกน</th>-->
                                        </tr>
                                    </thead>
                                    <tbody id="id-tbody-tab2"></tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-2">
                    <div class="col-12 text-center p-1">
                        <button type="button" id="btn_save_home" class="btn btn-primary fa fa-book  waves-effect p-1">ทำสัญญาซื้อขายบ้าน</button>&nbsp;
                        <button type="button" id="btn_edit_home" class="btn btn-warning fa fa-edit  waves-effect p-1 pl-2">แก้ไขสัญญาซื้อขาย</button>&nbsp;
                        <button type="button" class="btn btn-success p-1 pl-2" id="scan2">Scan เอกสาร</button>&nbsp;
                        <!--<button type="button" class="btn btn-success p-1 pl-2" id="scancard2">Scan สำเนาบัตร</button>&nbsp;-->
                        <button type="button" class="btn btn-danger p-1 pl-2" id="ex_home">แสดงตัวอย่าง</button>
                        <button type="button" class="btn btn-danger p-1 pl-2" id="canclepromise_home">ยกเลิกสัญญา</button>
                    </div>
                </div>
                <br>
            </form>

            <form id='scanForm2' method='POST' action=''>
                <input type="hidden" name="hiddin_scan_promiseid2" id="hiddin_scan_promiseid2" class="form-control"  />
                <input type="hidden" name="hiddin_scan_masterplan2" id="hiddin_scan_masterplan2" class="form-control"  />
                <input type="hidden" name="hiddin_scan_orderno2" id="hiddin_scan_orderno2" class="form-control"  />

            </form>
        </div>
    </div>
</div>



<!-- popup -->
<div id="myModal-tab2" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 750px;">
        <div class="modal-content w-100">
            <div class="modal-content p-3 " >
                <div class="modal-header">
                    <h6 class="modal-title font-weight-bold" id="myModalLabel-tab2"><i class="icon-star"></i> เพิ่มบุคคลทำสัญญา</h6>
                    <button onclick="closeOptionModalAddTab2()" type="button" class="close">×</button>
                </div>
                <div class="modal-body ">
                    <form id="formpopup">
                        <input type="hidden" name="planidtab2" id="planidtab2" />
                        <input type="hidden" name="plan_mastertab2" id="plan_mastertab2" />
                        <div class="row  pt-1">
                            <div class="col-12">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 140px;">เลขประตัวประชาชน</td>
                                        <td>
                                            <input type="text" name="poppidtab2" id="poppidtab2" class="form-control" required style="width:200px;  height: 30px;" oninput="numberOnly(this.id);" /> 
                                            <input type="hidden" name="input_hidden_pidstab2" id="input_hidden_pidstab2" />
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row  pt-2 ">
                            <div class="col-12">
                                <table border="0" style="width: 100%">    
                                    <tr>
                                        <td style="width: 91px;" align="right">ชื่อคำนำหน้า</td>
                                        <td >
                                            <input type="text" name="poptitletab2" id="poptitletab2" class="form-control" required style=" width: 80px; height: 30px;"  /> 
                                        </td>
                                        <td >ชื่อ</td>
                                        <td >
                                            <input type="text" name="popfnametab2" id="popfirstnametab2" class="form-control" required style="height: 30px;"  /> 
                                        </td>
                                        <td >นามสกุล</td>
                                        <td >
                                            <input type="text" name="poplnametab2" id="poplastnametab2" class="form-control" required style="height: 30px;"  /> 
                                        </td>
                                        <td >อายุ</td>
                                        <td >
                                            <input type="text" name="poplnametab2" id="popagetab2" class="form-control" required style="width: 60px; height: 30px;" oninput="numberOnly(this.id);" /> 
                                        </td>
                                        <td>ปี</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row  pt-2 ">
                            <div class="col-12">
                                <table border="0">                
                                    <tr class="pt-2">
                                        <td style="width: 91px;"  align="right" class="label-custom">บ้านเลขที่</td>
                                        <td >
                                            <input  type="text" name="pophouseNumbertab2" id="pophouseNumbertab2" class="form-control" style=" width: 86px; height: 30px; " oninput="numberOnly(this.id);"/>
                                        </td>
                                        <td style="width: 37px;" class="label-custom">หมู่ที่</td>
                                        <td >
                                            <input type="text" name="popgrouptab2" id="popgrouptab2" class="form-control" style="width: 69px; height: 30px;" oninput="numberOnly(this.id);"/>
                                        </td>
                                        <td align="right" class="label-custom">ซอย</td>
                                        <td style="width: 168px;">
                                            <input type="text" name="popalleytab2" id="popalleytab2" class="form-control" style=" height: 30px; width: 139px;"/>
                                        </td>

                                        <td align="right" class="label-custom">ถนน</td>
                                        <td  colspan="2">
                                            <input type="text" name="popstreettab2" id="popstreettab2" class="form-control" style=" height: 30px;"/>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row  pt-2 ">
                            <div class="col-12">
                                <table border="0">
                                    <tr>
                                        <td style="width: 91px;"  align="right" class="label-custom">ตำบล</td>
                                        <td style="width: 175px;">
                                            <input type="text" name="popsubDistricttab2" id="popsubDistricttab2" class="form-control" style=" height: 30px; width: 148px;" />
                                        </td>
                                        <td style="width: 55px;" align="right" class="label-custom">อำเภอ</td>
                                        <td style="width: 140px;">
                                            <input type="text" name="popdistricttab2" id="popdistricttab2" class="form-control" style=" height: 30px; "/>
                                        </td>
                                        <td style="width: 58px;" align="right" class="label-custom">จังหวัด</td>
                                        <td >
                                            <input type="text" name="popprovincetab2" id="popprovincetab2" class="form-control" style=" height: 30px;"/>
                                        </td>
                                        <td ></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row  pt-2 ">
                            <div class="col-12">
                                <table border="0">
                                    <tr>
                                        <td style="width: 96px;" align="right" class="label-custom">รหัสไปรษณีย์<span style="color: red">*</span></td>
                                        <td style="width: 168px;">
                                            <input type="text" name="poppostcodetab2" id="poppostcodetab2" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="height: 30px; width: 148px;"/>
                                        </td>
                                        <td style="width: 63px;" align="right" class="label-custom">โทรศัพท์<span style="color: red">*</span></td>
                                        <td style="width: 161px;">
                                            <input type="text" name="popphonetab2" id="popphonetab2" class="form-control" style="height: 30px; width: 139px;"/>
                                        </td>
                                        <td align="right" class="label-custom">อีเมล์</td>
                                        <td >
                                            <input type="email" name="popemailtab2" id="popemailtab2" class="form-control" style=" height: 30px;"/>
                                            <input type="hidden" name="popissuedDate" id="popissuedDate" />
                                            <input type="hidden" name="popexpiredDate" id="popexpiredDate" />
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="row pt-2">
                            <div class="col-12 text-center">
                                <div class="m-auto">
                                    <button type = "button" class="btn btn-success fa fa-edit p-1" id="btn_pop_insert_tab2">บันทึกข้อมูล</button>
                                    <button onclick="closeOptionModalAddTab2()" type="button" class="btn btn-success fa fa-close p-1">ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!--modal-->
<!--modal-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-tab2">
    <div class="modal-dialog modal-sm" style="max-width: 350px;">
        <div class="modal-content">
            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <div class="row p-2">
                        <div class="col-11 text-center pt-2"><pre class="text-center " id="lablepopup-tab2"></pre></div>
                        <div class="col-1 text-left pt-1"><button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button></div>
                    </div>
                </div>

            </div>
            <div class="modal-body">
                <div class="text-center">
                    <input id="hidden_user" type="hidden"/>
                    <button type="button" class="btn btn-primary" id="modal-btn-cancel-y-tab2" style="width: 30%;">Yes</button>
                    <button type="button" class="btn btn-danger" id="modal-btn-cancel-n-tab2" style="width: 30%;">No</button>

                </div>
            </div>
        </div>
    </div>
</div>

<!--modal information-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message-tab2">
    <div class="modal-dialog modal-sm" style="max-width: 400px;">
        <div class="modal-content">
            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <dd id="idmessage-tab2" class="pt-3"></dd>
                </div>

            </div>
            <div class="modal-body">
                <div class="text-center">
                    <button type="button" class="btn btn-warning" id="modal-btn-n-message-tab2" style="width: 30%;" >ตกลง</button>
                </div>

            </div>
        </div>
    </div>
</div>

<!--modal information for cancel-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message-cancel-tab2">
    <div class="modal-dialog modal-sm" style="max-width: 400px;">
        <div class="modal-content">
            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <dd id="idmessage-cancel-tab2" class="pt-3"></dd>
                </div>

            </div>
            <div class="modal-body">
                <div class="text-center">
                    <button type="button" class="btn btn-warning" id="modal-btn-n-message-cancel-tab2" style="width: 30%;" >ตกลง</button>
                </div>

            </div>
        </div>
    </div>
</div>

<!--pop scan-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-promisepromise_pop2" style="max-height: 600px;">
    <div class="modal-dialog modal-sm" style="max-width: 1000px;">
        <div class="modal-content">
            
            <div class="modal-body">
                <?php include_once 'scanpromise_pop2.php'; ?>

            </div>
        </div>
    </div>
</div>