<style>
    #xx.item:empty:before {
        content: "\200b"; 
    }
    @page{
        margin: 0cm;
    }

    
</style>
<!-------------------------------------------เงินมัดจำ--------------------------------------------------------------->
<div class="container" style="display: none;">
    <div class="pt-1"></div>
    <div class="printableArea">
        <div style="width: 100%; 
             border-style: solid; 
             color: #000;
             border-width: 8px;
             border-radius: 1px;
             font-size: 18px;" >
            <table style="width: 100%;" height="907" border="0" align="center">
                <tr>
                    <td height="120" colspan="5">
                        <div align="center" class="pt-3">
                            <img src="<?= base_url('assets/images/deposit.png') ?>" width="200" height="150" />
                        </div></td>
                </tr>
                <tr>
                    <td height="47" colspan="5"><div align="center" class="font-weight-bold" style="font-size: 32px;">เงินมัดจำ</div></td>
                </tr>
                <tr>
                    <td width="70" height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 60px; font-size:30px; font-family:'Angsana New';">เลขที่ PPE-<label id="agreenment_id2"></label></td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="70" height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td align="right"></td> <!-- <span id="date_view_de_2"></span> -->
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="86" height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 60px; font-size:30px; font-family:'Angsana New';">ข้าพเจ้า&nbsp;</td>
                                <td valign="bottom" style="font-size:30px; font-family:'Angsana New';">
                                    <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_fullname"></span></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50">&nbsp;</td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 113px; font-size:30px; font-family:'Angsana New';">อยู่บ้านเลขที่&nbsp;</td>
                                <td valign="bottom" style="width: 70px; font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_homeno"></span></div></td>
                                <td style="width: 50px; font-size:30px; font-family:'Angsana New';">หมู่ที่&nbsp;</td>
                                <td valign="bottom" style="width: 57px; font-size:30px; font-family:'Angsana New';" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_group"></span></div></td>
                                <td style="width: 50px; font-size:30px; font-family:'Angsana New';">ถนน&nbsp;</td>
                                <td valign="bottom" style="width: 130px; font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_street"></span></div></td>
                                <td style="width: 50px; font-size:30px; font-family:'Angsana New';">ตำบล&nbsp;</td>
                                <td valign="bottom" style="font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_subDistrict"></span></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            
                            <tr>
                                <td style="width: 35px; font-size:30px; font-family:'Angsana New'">อำเภอ&nbsp;</td>
                                <td valign="bottom" style="font-size:30px; font-family:'Angsana New'"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_district"></span></div></td>
                                <td style="width: 54px; font-size:30px; font-family:'Angsana New'">จังหวัด&nbsp;</td>
                                <td valign="bottom" style="width: 210px; font-size:30px; font-family:'Angsana New'"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_province"></span></div></td>
                                <td style="width: 120px; font-size:30px; font-family:'Angsana New'">รหัสไปรษณีย์&nbsp;</td>
                                <td valign="bottom" style="width: 193px; font-size:30px; font-family:'Angsana New'"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_postcode"></span></div></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td height="50">&nbsp;</td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New';">โทรศัพท์&nbsp;</td>
                                <td valign="bottom" style="width: 239px; font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_phone"></span></div></td>
                                <td style="width: 201px; font-size:30px; font-family:'Angsana New';">ว่าจ้างก่อสร้างบ้านแบบ&nbsp;</td>
                                <td valign="bottom" style="font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_home"></span></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 73px; font-size:30px; font-family:'Angsana New';">ในราคา&nbsp;</td>
                                <td align="center" style="width: 246px; font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_home_price"></span></div></td>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New';">บาท&nbsp;</td>
                                <td align="center" style="font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="vview_home_price_char"></span></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 401px; font-size:30px; font-family:'Angsana New';">โดยชำระค่ามัดจำก่อสร้างบ้านบนแปลงหมายเลข&nbsp;</td>
                                <td align="center" valign="bottom" style="font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_plans"></span></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50"><p>&nbsp;</p></td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 70px; font-size:30px; font-family:'Angsana New';">เป็นเงิน&nbsp;</td>
                                <td align="center" valign="bottom" style="width: 247px; font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_money"></span></div></td>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New';">บาท&nbsp;</td>
                                <td align="center" valign="bottom" style="font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_chat_price"></span></div></td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td height="50">&nbsp;</td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 260px; font-size:30px; font-family:'Angsana New';">และนัดมาทำสัญญาภายในวันที่&nbsp;</td>
                                <td style="width: 380px; font-size:30px; font-family:'Angsana New';" valign="bottom" align="center"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_price_order"></span></div></tview_price_orderd>
                                <td style="font-size:30px; font-family:'Angsana New';">หากพ้นกำหนดดังกล่าว</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="29">&nbsp;</td>
                    <td colspan="4">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 130px; font-size:30px; font-family:'Angsana New';">
                                    ให้ถือว่าเงินมัดจำนี้เป็นอันยกเลิกและข้าพเจ้ายินยอมให้ค่าจองนี้ตกเป็นกรรมสิทธิ์ของบริษัท พรพิศโฮมคอนสตรัคชั่น จำกัด ทันทีโดยไม่โต้แย้งใด ๆ ทั้งสิ้น
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td height="50" colspan="2">&nbsp;</td>
                    <td width="450">&nbsp;</td>
                    <td colspan="2">
                        <table style="width: 90%">
                            <tr>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New';">ลงชื่อ&nbsp;</td>
                                <td style="width: 194px; font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_price_order"></span></div></td>
                                <td style="font-size:30px; font-family:'Angsana New';">ผู้จอง</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" colspan="2">&nbsp;</td>
                    <td width="200">&nbsp;</td>
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 20px; font-size:30px; font-family:'Angsana New';">ลงชื่อ&nbsp;</td>
                                <td style="width: 194px; font-size:30px; font-family:'Angsana New';"><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"><span id="view_de_price_order"></span></div></td>
                                <td style="font-size:30px; font-family:'Angsana New';">ผู้รับมัดจำ/ผู้รับเงิน</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="120" colspan="5"><div align="center"><small>การชำระเงินด้วยเช็ค  จะถือว่าชำระเสร็จแล้วต่อเมื่อเช็คนั้นขึ้นเงิน  และผ่านเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว</small></div></td>
                </tr>
                <tr>
                    <td  colspan="5">
                        <div align="center" class="pt-2" style="width: 100%">
                            <div style="width: 380px; height: 350px;" id="imagecarddeposit"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="45" colspan="5" align="center" valign="bottom">88  หมู่ที่ 2 ต.บ้านกรด อ.บางปะอิน จ.พระนครศรีอยุธยา 13160</td>
                </tr>
            </table>
        </div>
    </div>
</div>