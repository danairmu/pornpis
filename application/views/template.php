<!doctype html>
<html>
    <head>
        <title>PORNPIS SMART</title>
        <meta charset="utf-8">
        <meta name="description" content="<?php echo $this->template->description; ?>">
        <meta name="author" content="">
        <!--     Fonts and icons     -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <!--<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>-->
        <!-- CSS Files -->
        <!--<link href="assets/plugins/material_dashboard/css/material-dashboard.css" rel="stylesheet" />-->
        <link rel="icon" href="<?= base_url('assets/images/logo.jpg') ?>" type="image/x-icon">
        <link href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" />
        <link href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.css') ?>" rel="stylesheet" />
        <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet"/>

        <?php echo $this->template->meta; ?>
        <?php echo $this->template->stylesheet; ?>
    </head>
    <body class="bg-light">
        <div id="tabs">
            <nav class="navbar navbar-expand-lg bg-success"  > <!-- style="box-shadow: 0px 3px 10px -2px rgba(0, 0, 0, 0.5); background-color: #0d9604; background:linear-gradient(#086102,#21b118);"-->
                <a class="navbar-brsand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="true" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"><i class="fa fa-bars"></i></span>
                </button>
                <div class="collapse navbar-collapse p-0" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item rounded pr-1" id="home" data-value="HOME" >
                            <a class="nav-link"  href="<?= base_url('home?menu=home') ?>" id="home">
                                <i class="fa fa-home animated" style="color:white;"></i>
                                <label class="text-menu" style="color:white;">หน้าหลัก</label>
                            </a>
                        </li>
                        <li class="nav-item rounded pl-1 pr-1" id="projectplan">
                            <a class="nav-link" href="<?= base_url('projectplan?menu=projectplan') ?>" id="plan">
                                <i class="fa fa-sticky-note-o" style="color:white;"></i>
                                <label class="text-menu" style="color:white;">แผนผังโครงการ</label>
                            </a>
                        </li>
                        <!--                        <li class="nav-item rounded pr-1" id="home" data-value="HOME" >
                                                    <a class="nav-link"  href="<//?= base_url('?menu=home') ?>" id="home">
                                                        <i class="fa fa-line-chart" style="color:white;"></i>
                                                        <label class="text-menu" style="color:white;">ภาพรวมรายงาน</label>
                                                    </a>
                                                </li>
                                                <li class="nav-item rounded pl-1 pr-1" id="reports">
                                                    <a class="nav-link" href="<//?= base_url('reports?menu=reports') ?>" id="plan">
                                                        <i class="fa fa-table" style="color:white;"></i>
                                                        <label class="text-menu" style="color:white;">รายงานการขาย</label>
                                                    </a>
                                                </li>-->

                        <?php if ($userrole == "admin") { ?>

                            <li class="nav-item dropdown rounded pl-1 pr-1">
                                <a class="nav-link rounded" href="#" id="managementplan"  data-toggle="dropdown"  aria-expanded="false">
                                    <i class="fa fa-cog" style="color:white;"></i><label class="text-menu" style="color:white;">จัดการแปลง/บ้าน</label>&nbsp;<i class="fa fa-caret-down" style="color:white;"></i>
                                </a>
                                <div class="dropdown-menu " aria-labelledby="managementplan" style="background-color: #28a745; border-radius: 15px;">
                                    <a class="dropdown-item rounded" id="sub1managementplan" style="color:white;" href="<?= base_url('managementplan?menu=sub1managementplan') ?>"><i class="fa fa-file" style="color:white;"></i> จัดการข้อมูลที่ดิน</a>
                                    <div style="padding-top: 1px;"></div>
                                    <a class="dropdown-item rounded" id="sub2managementplan" style="color:white;" href="<?= base_url('managementplan/managhome?menu=sub2managementplan') ?>"><i class="fa fa-bank" style="color:white;"></i> จัดการข้อมูลบ้าน</a>
                                </div>

                            </li> 
                        <?php } ?>

                        <?php if ($userrole == "admin") { ?>
                            <li class="nav-item rounded pl-1" id="register">
                                <a class="nav-link" href="<?= base_url('register?menu=register') ?>" id="register">
                                    <i class="fa fa-user-o" style="color:white;"></i>
                                    <label class="text-menu" style="color:white;">จัดการผู้ใช้งาน</label>
                                </a>
                            </li>
                        <?php } ?>

                        <li class="nav-item dropdown rounded pl-1 pr-1">
                            <a class="nav-link rounded" href="#" id="reports"  data-toggle="dropdown"  aria-expanded="false">
                                <i class="fa fa-table" style="color:white;"></i>&nbsp;<label class="text-menu" style="color:white;">รายงาน</label>&nbsp;<i class="fa fa-caret-down" style="color:white;"></i>
                            </a>
                            <div class="dropdown-menu " aria-labelledby="reports" style="background-color: #28a745; border-radius: 15px;">
                                <a class="dropdown-item rounded" id="sub1report" style="color:white;" href="<?= base_url('board?menu=sub1report') ?>"><i class="fa fa-line-chart" style="color:white;"></i>&nbsp;ภาพรวมรายงาน</a>
                                <div style="padding-top: 1px;"></div>
                                <a class="dropdown-item rounded" id="sub2report" style="color:white;" href="<?= base_url('reports?menu=sub2report') ?>"><i class="fa fa-table" style="color:white;"></i>&nbsp;รายงานการขาย</a>
                            </div>

                        </li>
                        <li class="nav-item rounded pl-1 pr-1" id="calculate">
                            <a class="nav-link" href="<?= base_url('Calculate?menu=calculate') ?>" id="plan">
                                <i class="fa fa-calculator" style="color:white;"></i>
                                <label class="text-menu" style="color:white;">คำนวนผ่อนชำระ</label>
                            </a>
                        </li>   

                        <li class="nav-item rounded pl-1" >
                            <a class="nav-link" href="<?= base_url('auth/logout') ?>">
                                <i class="fa fa-sign-out" style="color:white;"></i>
                                <label class="text-menu" style="color:white;">ออกจากระบบ</label>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>



        <!---- End Nave ---->

        <!---- Content ---->
        <div class="container-fluid p-0">
            <!-- Sidebar -->
            <!--                <div id="sidebar" class="float-none float-md-left d-none d-md-block">
                                image
                            </div>-->

            <!-- Content -->
            <!--style="background-color: #dcfbc2; box-shadow: 0px 3px 10px -2px rgba(0, 0, 0, 0.5);"-->
            <div id="content"  style="background-color: #d8ffb5; padding-bottom: 12px;" >
                <?php
                // This is the main content partial
                echo $this->template->content;
                ?>
            </div>
        </div>
        <!---- End Content ---->

        <!---- Footer ---->
        <footer class="footer-my">
            <div class="row">

                <div class="col-sm-12 text-center m-0 pr-2">
                    <b>
                        <label>
                            Pornpis Smart<i class="fa fa-home" style="color:green;"></i>
                        </label>
                    </b>
                </div>
            </div> 
        </footer>



        <!--   Core JS Files   -->
        <script src="<?= base_url('assets/plugins/material_dashboard/js/core/jquery.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('assets/plugins/material_dashboard/js/core/popper.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('assets/plugins/material_dashboard/js/plugins/perfect-scrollbar.jquery.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('assets/plugins/material_dashboard/js/core/bootstrap-material-design.min.js') ?>" type="text/javascript"></script>
        <!-- Chartist JS -->
        <script src="<?= base_url('assets/plugins/material_dashboard/js/plugins/chartist.min.js') ?>"></script>
        <!--  Notifications Plugin    -->
        <script src="<?= base_url('assets/plugins/material_dashboard/js/plugins/bootstrap-notify.js') ?>"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="<?= base_url('assets/plugins/material_dashboard/js/material-dashboard.min.js?v=2.1.0') ?>" type="text/javascript"></script>

        <?php echo $this->template->javascript; ?>




    </body>
</html>
<script type="text/javascript">
    $(document).ready(function () {

        try {
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };

            var menu = getUrlParameter('menu');
            console.log(menu);

            $(".nav-item").click(function () {
                if ($('.nav-item').is(":selected")) {
                    $(this).css("background-color", "#095a15");
                }
            });

            if (menu === 'home' || menu === undefined) {
                $('#home').css("background-color", "#095a15");
            } else if (menu === 'projectplan') {
                $('#projectplan').css("background-color", "#095a15");
            } else if (menu === 'register') {
                $('#register').css("background-color", "#095a15");
            } else if (menu === 'managementplan') {
                $('#managementplan').css("background-color", "#095a15");
            } else if (menu === 'sub1managementplan') {
                $('#managementplan').css("background-color", "#095a15");
                $('#sub1managementplan').css("background-color", "#095a15");
            } else if (menu === 'sub2managementplan') {
                $('#managementplan').css("background-color", "#095a15");
                $('#sub2managementplan').css("background-color", "#095a15");
            } else if (menu === 'reports') {
                $('#reports').css("background-color", "#095a15");
            } else if (menu === 'sub1report') {
                $('#reports').css("background-color", "#095a15");
                $('#sub1report').css("background-color", "#095a15");
            } else if (menu === 'sub2report') {
                $('#reports').css("background-color", "#095a15");
                $('#sub2report').css("background-color", "#095a15");

            } else if (menu === 'calculate') {
                $('#calculate').css("background-color", "#095a15");
            }

            console.log('menu: ' + menu);
        } catch (e) {
            console.log(e.message);

        }



    });

</script>    
