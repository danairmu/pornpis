<style type="text/css">
    .table-wrapper .fg-toolbar {
        background-color: #d8ffb5;
        border: none;
    }

    .table-bordered {
        border-bottom: 1px solid #fff !important;
    }

    #dashboardChart .chartist-tooltip {
        margin-left: 390px;
        margin-top: 220px;
    }

    #rstable_wrapper > .fg-toolbar:first-child {
        display: none;
    }

    .table .thead-light th { 
        background-color: #28a745;
        text-align: center;
        font-weight: bold;
        font-size: 15px;
        top: 0;
        color: white;

    }
</style>
<div class="container" style="height: 1400px;">
    <div class="row" style="padding-top: 50px;">
        <div class="col-12">
            <h2 class="text-center"><strong>WELCOME TO</strong> <i class="fa fa-home" style="color:#4caf50;"></i> <strong>PORNPIS
                    SMART</strong></h2>
        </div>
    </div>

    <br>

    <div class="p-5 bg-white" >
        <h4><i class="fa fa-area-chart" aria-hidden="true"></i> สรุปจำนวน การจอง/การขาย</h4>

        <br><br>

        <div id="dashboardChart" style="height: 390px;"></div>
    </div>

    <div class="pt-1"></div>
    <!-- === CRITERIA === -->
    <!--    <div class="card text-dark">
    
            <div class="card-body" >-->
    <div class="p-5 bg-white">
        <h4><i class="fa fa-table" aria-hidden="true"></i> ตารางข้อมูลการจอง/การขาย</h4>
        <br>
        <table style="width: 100%;">
            <tr style="display: none;">
                <td>
                </td>
                <td colspan="3" class="text-dark">
                    <input type="radio" name="reportType" id="reportType1" value="1"					                                                                					                                                                 <?php echo $report_type == 1 ? 'checked' : ''; ?>/>
                    <label for="reportType1">ข้อมูลการจอง</label>
                    <input type="radio" name="reportType" id="reportType2" value="2"					                                                                					                                                                 <?php echo $report_type == 2 ? 'checked' : ''; ?>/>
                    <label for="reportType2">ข้อมูลการขาย</label>
                </td>
            </tr>
            <tr>
                <td style="width: 70px;" class="text-dark">
                    ชื่อผู้ขาย
                </td>
                <td colspan="3">
                    <?php echo form_dropdown($sale_filter, $sale_filter_dropdown_data, $sale_filter_dropdown_default, ['class' => 'form-control', 'id' => 'saleFilter', 'style' => 'width: 320px;']); ?>
                </td>
            </tr>
            <tr style="display: none;">
                <td class="text-dark">
                    จากวันที่
                </td>
                <td style="width: 130px;">
                    <input data-format="dd/mm/yyyy" type="datetime" id="startDate" name="startDate" class="form-control datepicker"
                           style="width: 120px;" />
                </td>
                <td style="width: 70px; text-align: center;" class="text-dark">
                    ถึงวันที่
                </td>
                <td>
                    <input data-format="dd/mm/yyyy" type="datetime" id="endDate" name="endDate" class="form-control datepicker" style="width: 120px;" />
                </td>
            </tr>
        </table>
        <!-- === END CRITERIA === -->


        <br>
        <!--<div class="table-wrapper">-->
        <!--class="display nowrap table table-hover table-striped table-bordered "-->
        <div align="right">
            <button type = "button" id="btn_export"  class="btn btn-success fa fa-file-excel-o  waves-effect p-1">Export Excel</button>
        </div>
        <div class="pt-1"></div>
        <table id="rstable" class="display responsive nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%"  cellspacing="0" width="100%">
            <thead class="thead-light bg-success">
                <tr>
                    <th scope="col" width="8%" class="text-center bg-success text-white">ลำดับที่</th>
                    <th scope="col" width="20%" class="text-center bg-success text-white">ปี-เดือน</th>
                    <th scope="col" width="40%" class="text-center bg-success text-white">ชื่อผู้ขาย</th>
                    <th scope="col" class="text-center bg-success text-white">จำนวนการจอง</th>
                    <th scope="col" class="text-center bg-success text-white">จำนวนการขาย</th>
                    <th scope="col" class="d-none"></th>
                </tr>
            </thead>
        </table>
        <!--</div>-->
    </div>
    <!--        </div>
        </div>-->

</div>










<!--<div class="row pt-5" >-->

<!--        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
            <div class="our-services-wrapper mb-60">
                <div class="services-inner" data-aos="flip-down">
                    <div class="our-services-img">
                        <img src="https://www.orioninfosolutions.com/assets/img/icon/Agricultural-activities.png" width="68px" alt="">
                    </div>
                    <div class="our-services-text">
                        <h4>business growth</h4>
                        <p>Proin varius pellentesque nuncia tincidunt ante. In id lacus</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
            <div class="our-services-wrapper mb-60">
                <div class="services-inner" data-aos="flip-down">
                    <div class="our-services-img">
                        <img src="https://www.orioninfosolutions.com/assets/img/icon/Agricultural-activities.png" width="68px" alt="">
                    </div>
                    <div class="our-services-text">
                        <h4>business growth</h4>
                        <p>Proin varius pellentesque nuncia tincidunt ante. In id lacus</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
            <div class="our-services-wrapper mb-60">
                <div class="services-inner" data-aos="flip-down">
                    <div class="our-services-img">
                        <img src="https://www.orioninfosolutions.com/assets/img/icon/Agricultural-activities.png" width="68px" alt="">
                    </div>
                    <div class="our-services-text">
                        <h4>business growth</h4>
                        <p>Proin varius pellentesque nuncia tincidunt ante. In id lacus</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row pt-3">
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
            <div class="our-services-wrapper mb-60">
                <div class="services-inner" data-aos="flip-down">
                    <div class="our-services-img">
                        <img src="https://www.orioninfosolutions.com/assets/img/icon/Agricultural-activities.png" width="68px" alt="">
                    </div>
                    <div class="our-services-text">
                        <h4>business growth</h4>
                        <p>Proin varius pellentesque nuncia tincidunt ante. In id lacus</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
            <div class="our-services-wrapper mb-60">
                <div class="services-inner" data-aos="flip-down">
                    <div class="our-services-img">
                        <img src="https://www.orioninfosolutions.com/assets/img/icon/Agricultural-activities.png" width="68px" alt="">
                    </div>
                    <div class="our-services-text">
                        <h4>business growth</h4>
                        <p>Proin varius pellentesque nuncia tincidunt ante. In id lacus</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
            <div class="our-services-wrapper mb-60">
                <div class="services-inner" data-aos="flip-down">
                    <div class="our-services-img">
                        <img src="https://www.orioninfosolutions.com/assets/img/icon/Agricultural-activities.png" width="68px" alt="">
                    </div>
                    <div class="our-services-text">
                        <h4>business growth</h4>
                        <p>Proin varius pellentesque nuncia tincidunt ante. In id lacus</p>
                    </div>
                </div>
            </div>
        </div>-->
<!--</div>-->
</div>
<!--<script src="<?//= base_url('assets/plugins/aos/js/aos.js'); ?>"></script>
<script>
    AOS.init();
</script>-->
