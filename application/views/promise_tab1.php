<!--ที่ดิน-->
<div>
    <div class="pt-2"></div>
    <div class="row">
        <div class="col-12 text-right ">
            <div class="m-auto p-1" >
                <button type = "button"  onclick="showOptionModalManual()" class="btn btn-primary fa fa-user-plus  waves-effect p-1">เพิ่มบุคคล Manual</button>
                <button type = "button"  onclick="showOptionModalSmartCard()" class="btn btn-primary fa fa-user-plus  waves-effect p-1">เพิ่มบุคคล SmartCard</button>
            </div>
        </div>
    </div>

    <hr class="bg-success">
    <div class="pt-2"></div>
    <div class="row pt-2">
        <div class="col-12 text-center">
            <img  src="<?= base_url('assets/images/logo.jpg') ?>" style="width: 8%; height: 90%">
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <label class="font-weight-bold">สัญญาซื้อขายที่ดิน</label>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-left">
            <label>สัญญาซื้อขายเลขที่ &nbsp;</label><i class="fa fa-tag" style="color: green;"></i>&nbsp;<label id="promiseid"></label> 
            <input id="hidden_promiseid" name="hidden_promiseid" type="hidden" >
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            <label style="display: none;">&nbsp;<?= $due_date_display ?></label>
            <input id="hidden_date_due" name="hidden_date_due" type="hidden" value="<?= $due_date_display ?>">
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <form  method="post" id='formtab1' >
                <input type="hidden" name="order_no" id="order_no" class="form-control" />
                <input type="hidden" name="agreementid" id="agreementid" class="form-control"  />
                <input type="hidden" name="input_hidden_plan_array" id="input_hidden_plan_array" class="form-control"  />
                <input type="hidden" id="input_hidden_plan_master" name="plan_master"  value=" plan_master "/>
                <input type="hidden" name="agreementid_check" id="agreementid_check" class="form-control"  />
                <input type="hidden" name="agreementid_seq" id="agreementid_seq" class="form-control"  />

                <input type="hidden" name="pid_scan_tab1" id="pid_scan_tab1"  />
                <input type="hidden" name="flag_pid_scan_tab1" id="pid_scan_tab1" value="1"  />
                <div class="div-detail" >
                    <div>
                        <div id="list-detail-person1">
                            <form id="formlist1">
                                <div class="pt-1 pl-3"> 
                                    <table>
                                        <tr>
                                            <td style="width: 0px;"></td>
                                            <td><label class="font-weight-bold"><u>ข้อมูลบุคคล</u></label></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="row  pt-1 pl-3">
                                    <div class="col-12">
                                        <table >
                                            <tr>
                                                <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน</td>
                                                <td width="330">
                                                    <input type="text" name="pid" id="pid" class="form-control " style="width:320px;  height: 30px;"  required="true" readonly="true"/> 

                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="row  pt-2 pl-3">
                                    <div class="col-12">
                                        <table border="0">
                                            <tr>
                                                <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                                                <td width="123">
                                                    <input type="text" name="title" id="title" class="form-control" style="width:90px; height: 30px;" required="true" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">ชื่อ</td>
                                                <td width="230">
                                                    <input type="text" name="firstname" id="firstname" class="form-control" style="width:220px; height: 30px; " required="true" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">นามสกุล</td>
                                                <td width="230">
                                                    <input type="text" name="lastname" id="lastname" class="form-control" style="width:220px;  height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">อายุ</td>
                                                <td >
                                                    <input type="text" name="age" id="age" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                                </td>
                                                <td align="left" colspan="2" class="label-custom" >ปี</td>
                                                <td width="10"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row  pt-2 pl-3">
                                    <div class="col-12">
                                        <table border="0">                
                                            <tr class="pt-2">
                                                <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                                                <td width="110">
                                                    <input type="text" name="houseNumber" id="houseNumber" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);" readonly="true"/>
                                                </td>
                                                <td class="label-custom">หมู่ที่</td>
                                                <td width="120">
                                                    <input type="text" name="group" id="group" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">ซอย</td>
                                                <td width="225">
                                                    <input type="text" name="alley" id="alley" class="form-control" style="width:180px; height: 30px;" readonly="true"/>
                                                </td>

                                                <td align="right" class="label-custom">ถนน</td>
                                                <td  colspan="2">
                                                    <input type="text" name="street" id="street" class="form-control" style="width:260px; height: 30px;" readonly="true"/>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="row pt-2 pl-3">
                                    <div class="col-12">
                                        <table border="0">
                                            <tr>
                                                <td width="100" align="right" class="label-custom">ตำบล</td>
                                                <td width="230">
                                                    <input type="text" name="subDistrict" id="subDistrict" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">อำเภอ</td>
                                                <td width="230">
                                                    <input type="text" name="district" id="district" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">จังหวัด</td>
                                                <td width="260">
                                                    <input type="text" name="province" id="province" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td width="18"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row  pt-2 pl-3 pb-4">
                                    <div class="col-12">
                                        <table border="0">
                                            <tr>
                                                <td width="100" align="right" class="label-custom">รหัสไปรษณีย์</td>
                                                <td width="215">
                                                    <input type="text" name="postcode" id="postcode" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">โทรศัพท์</td>
                                                <td width="240">
                                                    <input type="text" name="phone" id="phone" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">อีเมล์</td>
                                                <td >
                                                    <input type="email" name="email" id="email" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="list-detail-person2" style="display: none;">
                            <hr>
                            <form id="formlist2">
                                <div class="row  pt-1 pl-3">
                                    <div class="col-12">
                                        <table >
                                            <tr>
                                                <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน</td>
                                                <td width="330">
                                                    <input type="text" name="pid2" id="pid2" class="form-control " style="width:320px;  height: 30px;"  required="true" readonly="true"/> 
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="row  pt-2 pl-3">
                                    <div class="col-12">
                                        <table border="0">
                                            <tr>
                                                <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                                                <td width="123">
                                                    <input type="text" name="title2" id="title2" class="form-control" style="width:90px; height: 30px;" required="true" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">ชื่อ</td>
                                                <td width="230">
                                                    <input type="text" name="firstname2" id="firstname2" class="form-control" style="width:220px; height: 30px; " required="true" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">นามสกุล</td>
                                                <td width="230">
                                                    <input type="text" name="lastname2" id="lastname2" class="form-control" style="width:220px;  height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">อายุ</td>
                                                <td >
                                                    <input type="text" name="age2" id="age2" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                                </td>
                                                <td align="left" colspan="2" class="label-custom" >ปี</td>
                                                <td width="10"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row  pt-2 pl-3">
                                    <div class="col-12">
                                        <table border="0">                
                                            <tr class="pt-2">
                                                <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                                                <td width="110">
                                                    <input type="text" name="houseNumber2" id="houseNumber2" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);" readonly="true"/>
                                                </td>
                                                <td class="label-custom">หมู่ที่</td>
                                                <td width="120">
                                                    <input type="text" name="group2" id="group2" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">ซอย</td>
                                                <td width="225">
                                                    <input type="text" name="alley2" id="alley2" class="form-control" style="width:180px; height: 30px;" readonly="true"/>
                                                </td>

                                                <td align="right" class="label-custom">ถนน</td>
                                                <td  colspan="2">
                                                    <input type="text" name="street2" id="street2" class="form-control" style="width:260px; height: 30px;" readonly="true"/>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="row pt-2 pl-3">
                                    <div class="col-12">
                                        <table border="0">
                                            <tr>
                                                <td width="100" align="right" class="label-custom">ตำบล</td>
                                                <td width="230">
                                                    <input type="text" name="subDistrict2" id="subDistrict2" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">อำเภอ</td>
                                                <td width="230">
                                                    <input type="text" name="district2" id="district2" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">จังหวัด</td>
                                                <td width="260">
                                                    <input type="text" name="province2" id="province2" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td width="18"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row  pt-2 pl-3 pb-4">
                                    <div class="col-12">
                                        <table border="0">
                                            <tr>
                                                <td width="100" align="right" class="label-custom">รหัสไปรษณีย์</td>
                                                <td width="215">
                                                    <input type="text" name="postcode2" id="postcode2" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">โทรศัพท์</td>
                                                <td width="240">
                                                    <input type="text" name="phone2" id="phone2" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">อีเมล์</td>
                                                <td >
                                                    <input type="email" name="email2" id="email2" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div id="list-detail-person3" style="display: none;">
                            <hr>
                            <form id="formlist3">
                                <div class="row  pt-1 pl-3">
                                    <div class="col-12">
                                        <table >
                                            <tr>
                                                <td width="195" class="label-custom">หมายเลขประจำตัวประชาชน</td>
                                                <td width="330">
                                                    <input type="text" name="pid3" id="pid3" class="form-control " style="width:320px;  height: 30px;"  required="true" readonly="true"/> 
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="row  pt-2 pl-3">
                                    <div class="col-12">
                                        <table border="0">
                                            <tr>
                                                <td width="100" align="right" class="label-custom">ชื่อคำนำหน้า </td>
                                                <td width="123">
                                                    <input type="text" name="title3" id="title3" class="form-control" style="width:90px; height: 30px;" required="true" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">ชื่อ</td>
                                                <td width="230">
                                                    <input type="text" name="firstname3" id="firstname3" class="form-control" style="width:220px; height: 30px; " required="true" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">นามสกุล</td>
                                                <td width="230">
                                                    <input type="text" name="lastname3" id="lastname3" class="form-control" style="width:220px;  height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">อายุ</td>
                                                <td >
                                                    <input type="text" name="age3" id="age3" class="form-control" style="width:120px;  height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                                </td>
                                                <td align="left" colspan="2" class="label-custom" >ปี</td>
                                                <td width="10"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row  pt-2 pl-3">
                                    <div class="col-12">
                                        <table border="0">                
                                            <tr class="pt-2">
                                                <td width="100" align="right" class="label-custom">บ้านเลขที่</td>
                                                <td width="110">
                                                    <input type="text" name="houseNumber3" id="houseNumber3" class="form-control" style="width:90px;  height: 30px; " oninput="numberOnly(this.id);" readonly="true"/>
                                                </td>
                                                <td class="label-custom">หมู่ที่</td>
                                                <td width="120">
                                                    <input type="text" name="group3" id="group3" class="form-control" style="width:100px; height: 30px;" oninput="numberOnly(this.id);" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">ซอย</td>
                                                <td width="225">
                                                    <input type="text" name="alley3" id="alley3" class="form-control" style="width:180px; height: 30px;" readonly="true"/>
                                                </td>

                                                <td align="right" class="label-custom">ถนน</td>
                                                <td  colspan="2">
                                                    <input type="text" name="street3" id="street3" class="form-control" style="width:260px; height: 30px;" readonly="true"/>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="row pt-2 pl-3">
                                    <div class="col-12">
                                        <table border="0">
                                            <tr>
                                                <td width="100" align="right" class="label-custom">ตำบล</td>
                                                <td width="230">
                                                    <input type="text" name="subDistrict3" id="subDistrict3" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">อำเภอ</td>
                                                <td width="230">
                                                    <input type="text" name="district3" id="district3" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">จังหวัด</td>
                                                <td width="260">
                                                    <input type="text" name="province3" id="province3" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td width="18"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row  pt-2 pl-3 pb-4">
                                    <div class="col-12">
                                        <table border="0">
                                            <tr>
                                                <td width="100" align="right" class="label-custom">รหัสไปรษณีย์</td>
                                                <td width="215">
                                                    <input type="text" name="postcode3" id="postcode3" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">โทรศัพท์</td>
                                                <td width="240">
                                                    <input type="text" name="phone3" id="phone3" class="form-control" style="width:200px; height: 30px;" readonly="true"/>
                                                </td>
                                                <td align="right" class="label-custom">อีเมล์</td>
                                                <td >
                                                    <input type="email" name="email3" id="email3" class="form-control" style="width:258px; height: 30px;" readonly="true"/>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="pt-1"></div>
                <div class="div-detail" >
                    <div class="pt-3 pl-3"> 
                        <table>
                            <tr>
                                <td style="width: 0px;"></td>
                                <td><label class="font-weight-bold"><u>รายละเอียดราคาที่ดิน</u></label></td>
                            </tr>
                        </table>
                    </div>

                    <div class="row  pt-2 pl-3 ">
                        <div class="col-12">
                            <table >
                                <tr>
                                    <td align="right"  class="label-custom">โฉนดที่ดินเลขที่</td>
                                    <td width="330">
                                        <input type="text" name="landid" id="landid" class="form-control " style="width:320px;  height: 30px;"  required="true" /> 
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td width="100" align="right" class="label-custom" >แปลงที่</td>
                                    <td width="220">
                                        <input type="text" name="plan" id="plan" class="form-control" style="width:200px; height: 30px;" value="<?= $planssave ?>" readonly="true"/>
                                        <input type="hidden" id="hidden_plan" name="hidden_plan" value=" input_form_plan ">
                                        <input type="hidden" id="hidden_planssave" name="hidden_planssave" value=" <?= $planssave ?> ">
                                    </td>
                                    <td width="55" align="right" class="label-custom">จำนวน</td>
                                    <td width="120">
                                        <input type="text" name="plan_total" id="plan_total" class="form-control" style="width:110px; height: 30px;"  readonly="true" />
                                    </td>
                                    <td width="60" class="label-custom">แปลง</td>
                                    <td width="100" align="right" class="label-custom">ราคาตารางวา</td>
                                    <td width="260">
                                        <input type="text" name="planprice" id="planprice" class="form-control" style="width:255px; height: 30px;" readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
            <!--                        <td rowspan="2">
                                        <button type="button" class="btn btn-primary" id="ex" style="height: 80px; width: 173px;">แสดงตัวอย่าง</button> 
                                    </td>-->
                                </tr>
                            </table>
                        </div>
                    </div>


                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td style="width: 105px;" align="right" class="label-custom">แปลงที่ดินเปล่า</td>
                                    <td style="width: 500px;">
                                        <div id="check_land" style="display: inline-block; padding-left: 20px;"></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td style="width: 100px;" align="right" class="label-custom">ราคาแปลง</td>
                                    <td style="width: 136px;">
                                        <input type="text" name="plan_price" id="plan_price" class="form-control" style="width:125px; height: 30px;"  readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>    
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td style="width: 100px;" align="right" class="label-custom">ราคาที่ดินเปล่า</td>
                                    <td style="width: 136px;">
                                        <input type="text" name="check_land_price" id="check_land_price" class="form-control" style="width:125px; height: 30px;"  readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>    
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>    
                                <tr>
                                    <td width="100" align="right" class="label-custom">รวมเป็นเงิน</td>
                                    <td width="140">
                                        <input type="text" name="pricetotal" id="pricetotal" class="form-control" style="width:130px; height: 30px;" readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
                                    <td width="345" class="label-custom"><span id="total_price_char"></span></td>
                                <input type="hidden" value=" input_form_price_word " id="input_form_price_word">
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td width="100" align="right" class="label-custom">ชำระเงินจอง</td>
                                    <td width="210">
                                        <input type="text" name="pricepay" id="pricepay" class="form-control" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" readonly="true"/>
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
                                    <td width="400" class="label-custom"><span id="order_price_char"></span>
                                        <input type="hidden" id="hidden_order_price_char"/>
                                    </td>

                                    <td rowspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-3">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 122px;" align="right" class="label-custom" >แบบบ้าน</td>
                                    <td width="326">
                                        <input type="text" name="home_name" id="home_name" class="form-control" style="width:200px; height: 30px;"  readonly="true"/>
                                        <input type="hidden" name="home_id" id="home_id" />
                                    </td>
                                    <td width="55" align="right" class="label-custom">ราคา</td>
                                    <td width="120">
                                        <input type="text" name="home_price" id="home_price" class="form-control" style="width:135px; height: 30px;"  readonly="true" />
                                    </td>
                                    <td width="550" class="label-custom"><span id="home_price_char"></span></td>

                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>

                                    <td style="width: 115px;" align="right" class="label-custom">วันที่จอง </td>
                                    <td style="width: 206px;">
                                        <input id="db_order_date" type="text" name="db_order_date" class="form-control"  style="width:200px; height: 30px;" readonly="true">
                                        <input id="db_order_date_hidden" name="db_order_date_hidden" type="hidden">
                                    </td>

                                    <td style="width: 141px;" align="right" class="label-custom">วันที่ทำสัญญาจอง </td>
                                    <td width="300">
                                        <input id="date_create_agreenment" type="text" name="date_create_agreenment" class="form-control"  style="width:180px; height: 30px;" readonly="true">
                                        <input id="db_hidden_date_create_agreenment" name="db_hidden_date_create_agreenment" type="hidden">

                                    </td>


                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-0 pb-4">
                        <div class="col-12">
                            <table>
                                <tr>

                                    <td style="width: 122px;" align="right" class="label-custom">วันที่นัดทำสัญญา </td>
                                    <td width="200">
                                        <input id="dateorder" type="text" name="dateorder" class="form-control"  style="width:193px; height: 30px;" readonly="true">
                                        <input id="hidden_datepicker" name="hidden_datepicker" type="hidden">
                                        <input id="hidden_dateorder_newdate" name="hidden_dateorder_newdate" type="hidden">
                                        <input id="db_agerrment_date_hidden" name="db_agerrment_date_hidden" type="hidden">
                                    </td>




                                </tr>
                            </table>
                        </div>
                    </div>

                </div>




                <div class="pt-1"></div>
                <div class="div-detail" >

                    <div class="pt-3 pl-3"> 
                        <table>
                            <tr>
                                <td style="width: 0px;"></td>
                                <td><label class="font-weight-bold"><u>การชำระเงินและการจดทะเบียนโอนกรรมสิทธิ์</u></label></td>
                            </tr>
                        </table>
                    </div>



                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 388px;" align="right" class="label-custom">คู่สัญญาทั้งสองฝ่ายตกลงที่จะซื้อจะขายทรัพย์สินในราคา </td>
                                    <td width="500">
                                        <span id="detail_price_promise_char"></span>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 160px;" align="right" class="label-custom">เงินทำสัญญาเป็นเงิน</td>
                                    <td width="150">
                                        <input type="text" name="pay_price_promise" id="pay_price_promise" class="form-control" oninput="numberOnly(this.id);" style="width:140px; height: 30px;" />
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
                                    <td class="label-custom">
                                        <span id="pay_price_promise_char"></span><span>ให้ถือว่าเงินจองและเงินทำสัญญาเป็นส่วนหนึ่งของการชำระราคาด้วย </span>
                                    </td>

                                    <td rowspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-0 pb-4">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 238px;" align="right" class="label-custom">คงเหลือเงินที่ต้องชำระอีกเป็นเงิน</td>
                                    <td width="210">
                                        <input type="text" name="pay_price_promise_result" id="pay_price_promise_result" class="form-control" oninput="numberOnly(this.id);" style="width:200px; height: 30px;" />
                                    </td>
                                    <td width="45" class="label-custom">บาท</td>
                                    <td class="label-custom">
                                        <span id="pay_price_promise_result_char"></span>
                                    </td>

                                    <td rowspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="pt-1"></div>
                <div class="div-detail" >

                    <div class="pt-3 pl-3"> 
                        <table>
                            <tr>
                                <td style="width: 0px;"></td>
                                <td><label class="font-weight-bold"><u>ข้อมูลพยาน</u></label></td>
                            </tr>
                        </table>
                    </div>

                    <div class="row pt-2 pl-0">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 160px;" align="right" class="label-custom">พยานบุคคลที่ 1. ชื่อ</td>
                                    <td style="width: 230px;">
                                        <input type="text" name="witness_name1" id="witness_name1" class="form-control"  style="width:200px; height: 30px;" value="นางสาวรัตน์มณี" />
                                    </td>
                                    <td style="width: 45px;" class="label-custom">นามสกุล</td>
                                    <td style="width: 230px;">
                                        <input type="text" name="witness_lastname1" id="witness_lastname1" class="form-control" style="width:200px; height: 30px;" value="  ไตรสะดับ"/>
                                    </td>

                                    <td rowspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-2 pl-0 pb-4">
                        <div class="col-12">
                            <table>
                                <tr>
                                    <td style="width: 160px;" align="right" class="label-custom">พยานบุคคลที่ 2. ชื่อ</td>
                                    <td style="width: 230px;">
                                        <input type="text" name="witness_name2" id="witness_name2" class="form-control"  style="width:200px; height: 30px;" value="นางสาวทรรศนีย์"/>
                                    </td>
                                    <td style="width: 45px;" class="label-custom">นามสกุล</td>
                                    <td style="width: 230px;">
                                        <input type="text" name="witness_lastname2" id="witness_lastname2" class="form-control"  style="width:200px; height: 30px;" value="เอี่ยมละม้าย"/>
                                    </td>

                                    <td rowspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

<!--                <div class="pt-1"></div>
                <div class="div-detail" >

                    <div class="pt-3 pl-3"> 
                        <table>
                            <tr>
                                <td style="width: 0px;"></td>
                                <td><label class="font-weight-bold"><u>เลือกการออกแบบสัญญา</u></label></td>
                            </tr>
                        </table>
                    </div>
                    <div class="pl-3 pb-4">
                        <table>
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckPlan1" >
                                        <label class="form-check-label" for="exampleCheckPlan1" >ใบสัญญาที่ดินกรณีที่1,2,3</label>
                                    </div>
                                </td>
                                <td style="padding-left: 10px;">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckPlan2" >
                                        <label class="form-check-label" for="exampleCheckPlan2"  >ใบสัญญาที่ดินสำหรับ4และ5</label>
                                    </div>
                                </td>
                                <td style="padding-left: 10px;">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheckPlan3" >
                                        <label class="form-check-label" for="exampleCheckPlan3" >บันทึกต่อท้ายสัญญาที่ดิน</label>
                                    </div>
                                </td>
                            </tr>
                            
                        </table>
                    </div>

                </div>-->


                <div  id="divscoretab1"></div>
                <div class="pt-1"></div>
                <div >
                    <div class="row pt-2 pl-1">
                        <label class="text-dark pl-2 font-weight-bold">บุคคลที่เกี่ยวข้องกับสัญญา</label>
                    </div>
                    <div class="row pt-0">
                        <div style="width: 100%;" class="pl-2 pr-2" >
                            <table id="personlist" class="display responsive nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead class="thead-light bg-success">
                                    <tr>
                                        <th scope="col" width="8%" class="text-center">ลำดับที่</th>
                                        <th scope="col" class="text-center">บัตรประจำตัวประชาชน</th>
                                        <th scope="col" class="text-center">ชื่อ-นามสกุล</th>
                                        <th scope="col" class="text-center">ที่อยู่</th>
                                        <th scope="col" class="text-center">สถานะสแกน</th>
                                        <th scope="col" width="8%" class="text-center">เลือก</th>
                                        <th scope="col" width="8%" class="text-center">ยกเลิก</th>
                                        <th scope="col" width="8%" class="text-center">สแกน</th>
                                    </tr>
                                </thead>
                                <tbody id="id-tbody"></tbody>

                            </table>
                        </div>
                    </div>
                </div>



                <div class="row pt-2">
                    <div class="col-12 text-center p-1">
                        <button type="button" id="btn_save" class="btn btn-primary fa fa-book  waves-effect p-1">บันทึกสัญญาซื้อขายที่ดิน</button>&nbsp;
                        <button type="button" id="btn_edit" class="btn btn-warning fa fa-edit  waves-effect p-1 pl-2">บันทึกแก้ไขสัญญาซื้อขาย</button>&nbsp;
                        <button type="button" class="btn btn-success p-1 pl-2" id="scan1">Scan เอกสาร</button>&nbsp;
                        <!--<button type="button" class="btn btn-success p-1 pl-2" id="scancard">Scan สำเนาบัตร</button>&nbsp;-->
                        <button type="button" class="btn btn-danger p-1 pl-2" id="ex">แสดงตัวอย่าง</button>
                        <button type="button" class="btn btn-danger p-1 pl-2" id="canclepromise_plan">ยกเลิกสัญญา</button>
                    </div>
                </div>
                <br>
                <form id='scanForm1' method='POST' action=''>
                    <input type="hidden" name="hiddin_scan_promiseid" id="hiddin_scan_promiseid" class="form-control"  />
                    <input type="hidden" name="hiddin_scan_masterplan" id="hiddin_scan_masterplan" class="form-control"  />
                    <input type="hidden" name="hiddin_scan_orderno" id="hiddin_scan_orderno" class="form-control"  />
                    <input type="hidden" name="hiddin_scan_plan_save" id="hiddin_scan_plan_save" class="form-control"  />
                    <input type="hidden" name="hiddin_scan_due_date_order" id="hiddin_scan_due_date_order" class="form-control"  />
                    <input type="hidden" name="hiddin_scan_due_date_display" id="hiddin_scan_due_date_display" class="form-control"  />

                </form>
        </div>
    </div>
</div>

<!-- popup -->
<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 750px;">
        <div class="modal-content w-100">
            <div class="modal-content p-3 " >
                <div class="modal-header">
                    <h6 class="modal-title font-weight-bold" id="myModalLabel"><i class="icon-star"></i> เพิ่มบุคคลทำสัญญา</h6>
                    <button onclick="closeOptionModalAdd()" type="button" class="close">×</button>
                </div>
                <div class="modal-body ">
                    <form id="formpopup">
                        <input type="hidden" name="planid" id="planid" />
                        <input type="hidden" name="plan_master" id="plan_master" />
                        <div class="row  pt-1">
                            <div class="col-12">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 140px;">เลขประตัวประชาชน</td>
                                        <td>
                                            <input type="text" name="poppid" id="poppid" class="form-control" required style="width:200px;  height: 30px;" oninput="numberOnly(this.id);" /> 
                                            <input type="hidden" name="input_hidden_pids" id="input_hidden_pids" />
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row  pt-2 ">
                            <div class="col-12">
                                <table border="0" style="width: 100%">    
                                    <tr>
                                        <td style="width: 91px;" align="right">ชื่อคำนำหน้า</td>
                                        <td >
                                            <input type="text" name="poptitle" id="poptitle" class="form-control" required style=" width: 80px; height: 30px;"  /> 
                                        </td>
                                        <td >ชื่อ</td>
                                        <td >
                                            <input type="text" name="popfname" id="popfirstname" class="form-control" required style="height: 30px;"  /> 
                                        </td>
                                        <td >นามสกุล</td>
                                        <td >
                                            <input type="text" name="poplname" id="poplastname" class="form-control" required style="height: 30px;"  /> 
                                        </td>
                                        <td >อายุ</td>
                                        <td >
                                            <input type="text" name="poplname" id="popage" class="form-control" required style="width: 60px; height: 30px;" oninput="numberOnly(this.id);" /> 
                                        </td>
                                        <td>ปี</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row  pt-2 ">
                            <div class="col-12">
                                <table border="0">                
                                    <tr class="pt-2">
                                        <td style="width: 91px;"  align="right" class="label-custom">บ้านเลขที่</td>
                                        <td >
                                            <input  type="text" name="pophouseNumber" id="pophouseNumber" class="form-control" style=" width: 86px; height: 30px; " oninput="numberOnly(this.id);"/>
                                        </td>
                                        <td style="width: 37px;" class="label-custom">หมู่ที่</td>
                                        <td >
                                            <input type="text" name="popgroup" id="popgroup" class="form-control" style="width: 69px; height: 30px;" oninput="numberOnly(this.id);"/>
                                        </td>
                                        <td align="right" class="label-custom">ซอย</td>
                                        <td style="width: 168px;">
                                            <input type="text" name="popalley" id="popalley" class="form-control" style=" height: 30px; width: 139px;"/>
                                        </td>

                                        <td align="right" class="label-custom">ถนน</td>
                                        <td  colspan="2">
                                            <input type="text" name="popstreet" id="popstreet" class="form-control" style=" height: 30px;"/>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row  pt-2 ">
                            <div class="col-12">
                                <table border="0">
                                    <tr>
                                        <td style="width: 91px;"  align="right" class="label-custom">ตำบล</td>
                                        <td style="width: 175px;">
                                            <input type="text" name="popsubDistrict" id="popsubDistrict" class="form-control" style=" height: 30px; width: 148px;" />
                                        </td>
                                        <td style="width: 55px;" align="right" class="label-custom">อำเภอ</td>
                                        <td style="width: 140px;">
                                            <input type="text" name="popdistrict" id="popdistrict" class="form-control" style=" height: 30px; "/>
                                        </td>
                                        <td style="width: 58px;" align="right" class="label-custom">จังหวัด</td>
                                        <td >
                                            <input type="text" name="popprovince" id="popprovince" class="form-control" style=" height: 30px;"/>
                                        </td>
                                        <td ></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row  pt-2 ">
                            <div class="col-12">
                                <table border="0">
                                    <tr>
                                        <td style="width: 96px;" align="right" class="label-custom">รหัสไปรษณีย์<span style="color: red">*</span></td>
                                        <td style="width: 168px;">
                                            <input type="text" name="poppostcode" id="poppostcode" class="form-control"  maxlength="5" oninput="numberOnly(this.id);" style="height: 30px; width: 148px;"/>
                                        </td>
                                        <td style="width: 63px;" align="right" class="label-custom">โทรศัพท์<span style="color: red">*</span></td>
                                        <td style="width: 161px;">
                                            <input type="text" name="popphone" id="popphone" class="form-control" style="height: 30px; width: 139px;"/>
                                        </td>
                                        <td align="right" class="label-custom">อีเมล์</td>
                                        <td >
                                            <input type="email" name="popemail" id="popemail" class="form-control" style=" height: 30px;"/>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row pt-2">
                            <div class="col-12 text-center">
                                <div class="m-auto">
                                    <button type = "button" class="btn btn-success fa fa-edit p-1" id="btn_pop_insert">บันทึกข้อมูล</button>
                                    <button onclick="closeOptionModalAdd()" type="button" class="btn btn-success fa fa-close p-1">ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!--modal-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
    <div class="modal-dialog modal-sm" style="max-width: 350px;">
        <div class="modal-content">
            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <div class="row p-2">
                        <div class="col-11 text-center pt-2"><pre class="text-center " id="lablepopup"></pre></div>
                        <div class="col-1 text-left pt-1"><button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button></div>
                    </div>
                </div>

            </div>
            <div class="modal-body">
                <div class="text-center">
                    <input id="hidden_user" type="hidden"/>
                    <button type="button" class="btn btn-primary" id="modal-btn-cancel-y" style="width: 30%;">Yes</button>
                    <button type="button" class="btn btn-danger" id="modal-btn-cancel-n" style="width: 30%;">No</button>

                </div>
            </div>
        </div>
    </div>
</div>

<!--modal information-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message">
    <div class="modal-dialog modal-sm" style="max-width: 400px;">
        <div class="modal-content">
            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <dd id="idmessage" class="pt-3"></dd>
                </div>

            </div>
            <div class="modal-body">
                <div class="text-center">
                    <button type="button" class="btn btn-warning" id="modal-btn-n-message" style="width: 30%;" >ตกลง</button>
                </div>

            </div>
        </div>
    </div>
</div>


<!--modal information for cancel-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-message-cancel">
    <div class="modal-dialog modal-sm" style="max-width: 400px;">
        <div class="modal-content">
            <div style="border-bottom: 1px solid #e9ecef;">
                <div class="text-center">
                    <dd id="idmessage-cancel" class="pt-3"></dd>
                </div>

            </div>
            <div class="modal-body">
                <div class="text-center">
                    <button type="button" class="btn btn-warning" id="modal-btn-n-message-cancel" style="width: 30%;" >ตกลง</button>
                </div>

            </div>
        </div>
    </div>
</div>

<!--pop scan-->
<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-promisepromise_pop" style="max-height: 600px;">
    <div class="modal-dialog modal-sm" style="max-width: 1000px;">
        <div class="modal-content">

            <div class="modal-body">
                <?php include_once 'scanpromise_pop.php'; ?>

            </div>
        </div>
    </div>
</div>